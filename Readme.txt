Webcome to the instruction how to install theme

1) The first, please install Magento and make sure it is working fine

2) Upload 3 folders: app, pub, lib to magento root folder 

3) After uploaded all theme files and patches, please run the following steps:

    - Login to your server with your SSH account

    - Switch to apache user

    - Change to magento root directory

    - Run command line: php bin/magento module:enable MGS_Mpanel
    - Run command line: php bin/magento module:enable MGS_Mmegamenu
    - Run command line: php bin/magento module:enable MGS_Portfolio
    - Run command line: php bin/magento module:enable MGS_Testimonial
    - Run command line: php bin/magento module:enable MGS_Brand
    - Run command line: php bin/magento module:enable MGS_Promobanners
    - Run command line: php bin/magento module:enable MGS_StoreLocator
    - Run command line: php bin/magento module:enable MGS_Blog
    - Run command line: php bin/magento module:enable MGS_QuickView
    - Run command line: php bin/magento module:enable MGS_Social
    - Run command line: php bin/magento module:enable MGS_Lookbook
    - Run command line: php bin/magento module:enable MGS_AjaxCart
    - Run command line: php bin/magento module:enable MGS_Protabs
    - Run command line: php bin/magento module:enable MGS_InstantSearch
    - Run command line: php bin/magento module:enable MGS_Ajaxlayernavigation
    - Run command line: php bin/magento module:enable MGS_Landing (if you are using magento 2.2+)

    - Third Party Extension: MagePlaza Social Login ( please note that if you already installed Mageplaza Social Login by composer or have been using this extension, you can ignore this step )
    - Run command line: php bin/magento module:enable Mageplaza_Core
    - Run command line: php bin/magento module:enable Mageplaza_SocialLogin
    * If you have any problems or any request for this extension, please create support ticket here: https://mageplaza.freshdesk.com/support/home

    - Run command line: php bin/magento setup:upgrade
    - Run command line: php bin/magento setup:static-content:deploy or php bin/magento setup:static-content:deploy -f ( for Magento 2.2+ )

4) Upload patches for the version of Magento

    - If you are using magento version 2.1.x, the version 2.1.6 and later: Please read Readme file and apply patch_for_magento_2.1.6_to_magento_2.1.x to magento root folder

- If you are using magento version 2.2.x ( x from 0 to 7 ): Please read Readme file and apply patch_for_magento_2.2.0_to_2.2.7 to magento root folder

- If you are using magento version 2.2.8 or later: Please read Readme file and apply patch_for_magento_2.2.8+ to magento root folder

- If you are using magento version 2.3.0: Please read Readme file and apply patch_for_magento_2.3.0 to magento root folder

- If you are using magento version 2.3.1 or later: Please read Readme file and apply patch_for_magento_2.3.1+ to magento root folder

5) Note
 
    - On some server, you need to set permission for var, pub or generated ( magento 2.2.2 or later ) folder, so please ask your server provider to set permission
for those folders.

6) The last, go to admin MGS -> Theme Settings then expand "General" section, click "Install Theme" button then expand "Import" section and Import the homepage
you like. Run this command to refresh cache and reindex data

    - php bin/magento cache:clean
    - php bin/magento indexer:reindex
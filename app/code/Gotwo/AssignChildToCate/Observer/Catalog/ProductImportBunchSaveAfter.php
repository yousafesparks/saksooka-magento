<?php


namespace Gotwo\AssignChildToCate\Observer\Catalog;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Registry;

class ProductImportBunchSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
	
    protected $productRepository;
    protected $searchCriteriaBuilder;
    protected $linkManagement;
    protected $registry;
	
    public function __construct(
        LinkManagementInterface $linkManagement,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Registry $registry
    ) {
        $this->linkManagement = $linkManagement;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->registry = $registry;
    }

    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		/* $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/assigntemplog.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($child->getSku()); */
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $_product = $observer->getProduct();  // you will get product object
		if($_product->getTypeId() == 'configurable'){
			
			$previousCategoryIds = $_product->getCategoryIds(); 
			$childProducts = $this->getChilds($_product->getId());
			$array_cp = array();
			foreach ($childProducts as $child) {
				$product = $objectManager->get('\Magento\Catalog\Api\ProductRepositoryInterface')->get($child->getSku());
				if(!in_array($product->getColor(),$array_cp)){
					array_push($array_cp,$product->getColor());
					$categoryLink = $objectManager->get('\Magento\Catalog\Api\CategoryLinkManagementInterface');
					$categoryLink->assignProductToCategories($child->getSku(), $previousCategoryIds);
				}else{
					$categoryLink->assignProductToCategories($child->getSku(),array('2'));
				}
			}
		}
	    
		
    }
	public function getChilds($parentId) 
    {
		
      $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('type_id', 'configurable')
            ->addFilter('entity_id', $parentId)
            ->create();

      $configurableProducts = $this->productRepository
            ->getList($searchCriteria);

      foreach ($configurableProducts->getItems() as $configurableProduct) {
        $childProducts = $this->linkManagement
            ->getChildren($configurableProduct->getSku());
      }
      return $childProducts;
    }
}

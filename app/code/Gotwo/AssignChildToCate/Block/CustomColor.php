<?php
namespace Gotwo\AssignChildToCate\Block;

class CustomColor extends \Magento\Framework\View\Element\Template
{
    protected $_objectManager;
	protected $_productloader;  
    public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectmanager,		
        \Magento\Catalog\Model\ProductFactory $_productloader
    ){
        $this->_objectManager = $objectmanager;
		$this->_productloader = $_productloader;
        parent::__construct($context);
    }
	public function getChildProducts($product)
	{
		return $product->getTypeInstance()->getUsedProductIds($product);
    }
    public function getRelatedProducts($product)
    {
		return $product->getRelatedProducts();
    }
	public function getColorArrayByRelated($product){
   if(is_array($product->getRelatedProducts())){
		$relatedProducts = $this->getRelatedProducts($product);
		$color_array = array();
		if (!empty($relatedProducts)) {
			foreach ($relatedProducts as $relatedProduct) {
				$productrelatedProduct	= $this->getThatProduct($relatedProduct->getId());
				$_configChild 			= $this->getChildProducts($productrelatedProduct);
				
				foreach ($_configChild as $child){
					$product		= $this->getThatProduct($child);
					$color_code		= "color";
					$colorValue 	= $this->getThatProduct($product->getId())->getData($color_code);
					$swatchHelper	= $this->_objectManager->get("Magento\Swatches\Helper\Data");
					$swatchData 	= $swatchHelper->getSwatchesByOptionsId([$colorValue]);
					array_push(
						$color_array,[
							'product_id' => $productrelatedProduct->getId(),
							'color_value' => $swatchData[$colorValue]['value'],
							'productname' => $productrelatedProduct->getName(),
							'productcolor' => $product->getResource()->getAttribute($color_code)->getFrontend()->getValue($product),
							'productimage' => $this->getImageUrl($productrelatedProduct),
							'producturl'  => $productrelatedProduct->getProductUrl()
							
						]
					);
					break;									
				}
				
			}
		}
   }
		return $color_array;		
	}
	public function getColorArrayByThis($product){	
	
		$_configChild = $this->getChildProducts($product);
		$color_array = array();		
		foreach ($_configChild as $child){
			$productchild		= $this->getThatProduct($child);
			$color_code		= "color";
			$colorValue 	= $this->getThatProduct($productchild->getId())->getData($color_code);
			$swatchHelper	= $this->_objectManager->get("Magento\Swatches\Helper\Data");
			$swatchData 	= $swatchHelper->getSwatchesByOptionsId([$colorValue]);
			array_push(
				$color_array,[
					'product_id' => $product->getId(),
					'color_value' => $swatchData[$colorValue]['value'],
					'productcolor' => $product->getResource()->getAttribute($color_code)->getFrontend()->getValue($product),
					'productimage' => $this->getImageUrl($product),
					'productname' => $product->getName(),
					'producturl'  => $product->getProductUrl()
					
				]
			);
			break;									
		}
		return $color_array;	
	}
	public function getSizeArrayByThis($product){	
	
		$_configChild = $this->getChildProducts($product);
		$color_array = array();		
		foreach ($_configChild as $child){
			$productchild		= $this->getThatProduct($child);
			array_push(
				$color_array,[
					'productsize' => $productchild->getResource()->getAttribute('size')->getFrontend()->getValue($productchild)
					
				]
			);							
		}
		return $color_array;	
	}
	public function getImageUrl($product)
    {
		$imageHelper  	= $this->_objectManager->get('\Magento\Catalog\Helper\Image');
		$image_url 		= $imageHelper->init($product, 'product_thumbnail_image')->setImageFile($product->getFile())->resize('60','87')->getUrl();
		return $image_url;
   }
	public function getThatProduct($id)
    {
       return $this->_productloader->create()->load($id);
    }
}
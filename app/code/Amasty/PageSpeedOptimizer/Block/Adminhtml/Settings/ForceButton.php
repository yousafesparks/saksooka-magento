<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */

namespace Amasty\PageSpeedOptimizer\Block\Adminhtml\Settings;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class ForceButton extends Field
{
    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setData('value', __("Run Optimization now"));
        $element->setData('class', "action-default");

        $block = $this->getLayout()
            ->createBlock(\Magento\Backend\Block\Template::class)
            ->setTemplate('Amasty_PageSpeedOptimizer::force_optimize.phtml')
            ->setProcessUrl($this->getActionUrl())
            ->setCountUrl($this->getCountActionUrl());

        return parent::_getElementHtml($element).$block->toHtml();
    }

    /**
     * @return string
     */
    public function getActionUrl()
    {
        return $this->_urlBuilder->getUrl('amoptimizer/image/run');
    }

    /**
     * @return string
     */
    public function getCountActionUrl()
    {
        return $this->_urlBuilder->getUrl('amoptimizer/image/imagecount');
    }
}

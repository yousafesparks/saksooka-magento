<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class ConfigProvider extends \Amasty\Base\Model\ConfigProviderAbstract
{
    protected $pathPrefix = 'amoptimizer/';

    /**#@+
     * Constants defined for xpath of system configuration
     */
    const XPATH_ENABLED = 'general/enabled';
    const MOVE_JS = 'settings/javascript/movejs';
    const MOVE_FONT = 'settings/css/move_font';
    const LAZY_LOAD = 'images/lazy_load';
    const LAZY_LOAD_SCRIPT = 'images/lazy_load_script';
    const PRELOAD_IMAGES = 'images/preload_images';
    const SKIP_IMAGES_COUNT = 'images/skip_images_count';
    const OPTIMIZE_AUTOMATICALLY = 'images/optimize_automatically';
    const JPEG_COMMAND = 'images/jpeg_tool';
    const PNG_COMMAND = 'images/png_tool';
    const GIF_COMMAND = 'images/gif_tool';
    const DUMP_ORIGINAL = 'images/dump_original';
    const IGNORE_IMAGES = 'images/ignore_list';
    /**#@-*/

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->isSetFlag(self::XPATH_ENABLED);
    }

    /**
     * @return bool
     */
    public function isMoveJS()
    {
        return $this->isSetFlag(self::MOVE_JS);
    }

    /**
     * @return bool
     */
    public function isLazyLoad()
    {
        return $this->isSetFlag(self::LAZY_LOAD);
    }

    /**
     * @return int
     */
    public function lazyLoadScript()
    {
        return (int)$this->getValue(self::LAZY_LOAD_SCRIPT);
    }

    /**
     * @return bool
     */
    public function isAutomaticallyOptimizeImages()
    {
        return (bool)$this->getValue(self::OPTIMIZE_AUTOMATICALLY);
    }

    /**
     * @return int
     */
    public function getJpegCommand()
    {
        return (int)$this->getValue(self::JPEG_COMMAND, 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * @return int
     */
    public function getPngCommand()
    {
        return (int)$this->getValue(self::PNG_COMMAND, 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * @return int
     */
    public function getGifCommand()
    {
        return (int)$this->getValue(self::GIF_COMMAND, 0, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * @return bool
     */
    public function isDumpOriginal()
    {
        return $this->isSetFlag(self::DUMP_ORIGINAL);
    }

    /**
     * @return bool
     */
    public function isMoveFont()
    {
        return $this->isSetFlag(self::MOVE_FONT);
    }

    /**
     * @return bool
     */
    public function isPreloadImages()
    {
        return $this->isSetFlag(self::PRELOAD_IMAGES);
    }

    /**
     * @return int
     */
    public function skipImagesCount()
    {
        return (int)$this->getValue(self::SKIP_IMAGES_COUNT);
    }

    /**
     * @return array
     */
    public function getIgnoreImages()
    {
        $ignoreList = $this->getValue(self::IGNORE_IMAGES);
        if (empty($ignoreList)) {
            return [];
        }

        return explode("\n", $ignoreList);
    }
}

<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Model\Image;

use Magento\Framework\App\Filesystem\DirectoryList;

class ForceOptimization
{
    const IMAGE_DIRECTORIES = [
        'wysiwyg',
        'catalog'
    ];

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    private $mediaDirectory;

    /**
     * @var Process
     */
    private $imageProcess;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Amasty\PageSpeedOptimizer\Model\Image\Process $imageProcess
    ) {
        $this->mediaDirectory = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $this->imageProcess = $imageProcess;
    }

    /**
     * @param int $skip
     *
     * @return void
     */
    public function execute($skip)
    {
        foreach (array_slice($this->getFiles(), $skip, 10) as $file) {
            $this->imageProcess->execute($this->mediaDirectory->getAbsolutePath($file));
        }
    }

    /**
     * @return int
     */
    public function getFilesCount()
    {
        return count($this->getFiles());
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        $result = [];
        foreach (self::IMAGE_DIRECTORIES as $imageDirectory) {
            $files = $this->mediaDirectory->readRecursively($imageDirectory);
            foreach ($files as $file) {
                if ($this->mediaDirectory->isFile($file)) {
                    $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
                        $result[] = $file;
                    }
                }
            }
        }

        return $result;
    }
}

<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Model\Image;

use Amasty\PageSpeedOptimizer\Model\OptionSource\GifOptimization;
use Amasty\PageSpeedOptimizer\Model\OptionSource\JpegOptimization;
use Amasty\PageSpeedOptimizer\Model\OptionSource\PngOptimization;
use Magento\Framework\App\Filesystem\DirectoryList;

class Process
{
    const DUMP_DIRECTORY = 'amasty' . DIRECTORY_SEPARATOR .  'amoptimizer_dump' . DIRECTORY_SEPARATOR;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    private $mediaDirectory;

    /**
     * @var \Amasty\PageSpeedOptimizer\Model\ConfigProvider
     */
    private $configProvider;

    public function __construct(
        \Amasty\PageSpeedOptimizer\Model\ConfigProvider $configProvider,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->configProvider = $configProvider;
    }

    /**
     * @param string $imagePath
     *
     * @return array
     */
    public function execute($imagePath)
    {
        $output = [];
        $mediaImagePath = str_replace($this->mediaDirectory->getAbsolutePath(''), '', $imagePath);
        if (!$this->mediaDirectory->isExist($mediaImagePath)) {
            return $output;
        }

        /** @codingStandardsIgnoreStart */
        $ext = strtolower(pathinfo($imagePath, PATHINFO_EXTENSION));
        /** @codingStandardsIgnoreEnd */
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                if ($this->configProvider->getJpegCommand() != JpegOptimization::DO_NOT_OPTIMIZE) {
                    $this->dumpOriginalImage($mediaImagePath);
                    exec(
                        str_replace(
                            '%f',
                            $imagePath,
                            JpegOptimization::TOOLS[$this->configProvider->getJpegCommand()]['command']
                        ),
                        $output
                    );
                }
                break;
            case 'png':
                if ($this->configProvider->getPngCommand() != PngOptimization::DO_NOT_OPTIMIZE) {
                    $this->dumpOriginalImage($mediaImagePath);
                    exec(
                        str_replace(
                            '%f',
                            $imagePath,
                            PngOptimization::TOOLS[$this->configProvider->getPngCommand()]['command']
                        ),
                        $output
                    );
                }
                break;
            case 'gif':
                if ($this->configProvider->getGifCommand() != GifOptimization::DO_NOT_OPTIMIZE) {
                    $this->dumpOriginalImage($mediaImagePath);
                    exec(
                        str_replace(
                            '%f',
                            $imagePath,
                            GifOptimization::TOOLS[$this->configProvider->getGifCommand()]['command']
                        ),
                        $output
                    );
                }
                break;
        }

        return $output;
    }

    public function dumpOriginalImage($imagePath)
    {
        if ($this->configProvider->isDumpOriginal()) {
            $dumpImagePath = self::DUMP_DIRECTORY . $imagePath;

            if (!$this->mediaDirectory->isExist($dumpImagePath)) {
                $this->mediaDirectory->copyFile($imagePath, $dumpImagePath);
            }
        }
    }

    public function removeDumpImage($imagePath)
    {
        if ($this->configProvider->isDumpOriginal()) {
            $dumpImagePath = self::DUMP_DIRECTORY . $imagePath;

            if ($this->mediaDirectory->isExist($dumpImagePath)) {
                $this->mediaDirectory->delete($dumpImagePath);
            }
        }
    }
}

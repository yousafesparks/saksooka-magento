<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Plugin;

use Amasty\PageSpeedOptimizer\Model\ConfigProvider;
use Amasty\PageSpeedOptimizer\Model\Image\Process;

class AdapterImage
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var Process
     */
    private $imageProcessor;

    /**
     * @var string
     */
    private $image;
    public function __construct(
        ConfigProvider $configProvider,
        Process $imageProcessor
    ) {
        $this->configProvider = $configProvider;
        $this->imageProcessor = $imageProcessor;
    }

    /**
     * @param $subject
     * @param $image
     */
    public function beforeSave($subject, $image)
    {
        $this->image = $image;
    }

    /**
     * @param $subject
     * @param $result
     *
     * @return mixed
     */
    public function afterSave($subject, $result)
    {
        if ($this->configProvider->isEnabled()
            && $this->configProvider->isAutomaticallyOptimizeImages() && $this->image
        ) {
            $this->imageProcessor->execute($this->image);
        }

        return $result;
    }
}

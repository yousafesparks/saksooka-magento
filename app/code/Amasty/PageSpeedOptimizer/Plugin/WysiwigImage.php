<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Plugin;

use Amasty\PageSpeedOptimizer\Model\ConfigProvider;
use Amasty\PageSpeedOptimizer\Model\Image\Process;

class WysiwigImage
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var Process
     */
    private $imageProcessor;

    public function __construct(
        ConfigProvider $configProvider,
        Process $imageProcessor
    ) {
        $this->configProvider = $configProvider;
        $this->imageProcessor = $imageProcessor;
    }

    /**
     * @param $subject
     * @param $result
     *
     * @return string
     */
    public function afterUploadFile($subject, $result)
    {
        if ($this->configProvider->isAutomaticallyOptimizeImages()) {
            $this->imageProcessor->execute($result['path'] . DIRECTORY_SEPARATOR . $result['file']);
        }

        return $result;
    }

    /**
     * @param $subject
     * @param $target
     */
    public function beforeDeleteFile($subject, $target)
    {
        if ($this->configProvider->isDumpOriginal()) {
            $this->imageProcessor->removeDumpImage($target);
        }
    }
}

<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_PageSpeedOptimizer
 */


namespace Amasty\PageSpeedOptimizer\Plugin;

use Amasty\PageSpeedOptimizer\Model\OptionSource\LazyLoadScript;

class ModifyOutput
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Amasty\PageSpeedOptimizer\Model\ConfigProvider
     */
    private $configProvider;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    private $assetRepo;

    /**
     * @var \Magento\Framework\Code\Minifier\Adapter\Js\JShrink
     */
    private $JShrink;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Amasty\PageSpeedOptimizer\Model\ConfigProvider $configProvider,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\Code\Minifier\Adapter\Js\JShrink $JShrink
    ) {
        $this->registry = $registry;
        $this->configProvider = $configProvider;
        $this->assetRepo = $assetRepo;
        $this->JShrink = $JShrink;
    }

    /**
     * @param \Magento\Framework\View\Layout $subject
     * @param string $output
     *
     * @return string
     */
    public function afterGetOutput($subject, $output)
    {
        if (!$this->configProvider->isEnabled() || $this->registry->registry('amoptimizer_continue')) {
            return $output;
        }

        if ($this->configProvider->isMoveJS()) {
            $requireJs = $this->registry->registry('requireJsScript');
            if ($requireJs) {
                if (strpos($output, $requireJs) !== false) {
                    $output = str_replace($requireJs, '%require_js_script% %other_scripts%', $output);
                } else {
                    $output .= '%require_js_script% %other_scripts%';
                }

                $scripts = [];
                $output = preg_replace_callback(
                    '/<script.*?>.*?<\/script.*?>/is',
                    function ($script) use (&$scripts) {
                        $scripts[] .= $script[0];
                        return '';
                    },
                    $output
                );

                $scriptsOutput = '';
                foreach ($scripts as $script) {
                    try {
                        $scriptsOutput .= $this->JShrink->minify($script);
                    } catch (\Exception $e) {
                        $scriptsOutput .= $script;
                    }
                }

                $output = str_replace(
                    '%require_js_script% %other_scripts%',
                    '%lazy_before%' . $requireJs . '%lazy_after%' . $scriptsOutput,
                    $output
                );
            }
        }

        $this->lazyLoad($output);

        $this->moveFont($output);

        return $output;
    }

    /**
     * @param string $output
     *
     * @return void
     */
    public function lazyLoad(&$output)
    {
        if ($this->configProvider->isLazyLoad()) {
            if (false === strpos($output, '%lazy_before%')) {
                $output .= '%lazy_before%%lazy_after%';
            }
            $regExp = "<img(.*?)src=(\"|\'|)(.*?)(\"|\'| )(.*?)>";

            if ($this->configProvider->isPreloadImages()) {
                $skipImages = $this->configProvider->skipImagesCount();
            } else {
                $skipImages = 0;
            }

            $tempOutput = preg_replace('/<script.*?>.*?<\/script.*?>/is', '', $output);
            if (preg_match_all('/' . $regExp . '/is', $tempOutput, $images)) {
                $ignoreList = $this->configProvider->getIgnoreImages();
                foreach ($images as $key => &$imagePart) {
                    $imagePart = array_slice($imagePart, $skipImages);
                }

                foreach ($images[0] as $key => $image) {
                    $skip = false;
                    foreach ($ignoreList as $item) {
                        if (strpos($image, $item) !== false) {
                            $skip = true;
                            break;
                        }
                    }
                    if ($skip) {
                        continue;
                    }
                    
                    $replace = 'src=' . $images[2][$key] . $images[3][$key] . $images[4][$key];
                    $placeholder = 'src="' . $this->assetRepo->getUrl(
                        'Amasty_PageSpeedOptimizer/images/placeholder.png'
                    ) . '"';
                    $newImg = str_replace($replace, $placeholder . ' data-am' . $replace, $image);

                    $output = str_replace($image, $newImg, $output);
                }
            }

            switch ($this->configProvider->lazyLoadScript()) {
                case LazyLoadScript::NATIVE_LAZY:
                    $nativeLazy = '<script src="' . $this->assetRepo->getUrl(
                        'Amasty_PageSpeedOptimizer/js/nativejs.lazy.js'
                    ) . '"></script>';

                    $output = str_replace(['%lazy_before%', '%lazy_after%'], [$nativeLazy, ''], $output);
                    break;
                case LazyLoadScript::JQUERY_LAZY:
                default:
                    $jQLazy = '<script>
                        require(["jquery"], function (jquery) {
                            require(["Amasty_PageSpeedOptimizer/js/jquery.lazy"], function(lazy) {
                                if (document.readyState === "complete") {
                                    window.jQuery("img[data-amsrc]").lazy({"bind":"event", "attribute": "data-amsrc"});
                                } else {
                                    window.jQuery("img[data-amsrc]").lazy({"attribute": "data-amsrc"});
                                }
                            })
                        });
                    </script>';

                    $output = str_replace(['%lazy_before%', '%lazy_after%'], ['', $jQLazy], $output);
                    break;
            }
        }

        $output = str_replace(['%lazy_before%', '%lazy_after%'], '', $output);
    }

    public function moveFont(&$output)
    {
        if ($cssName = $this->registry->registry('mergedCssName')) {
            $output .= '<noscript id="deferred-fonts"><link rel="stylesheet"  type="text/css"  media="all" href="'
                . str_replace(basename($cssName), 'fonts_' . basename($cssName), $cssName)
                . '" /></noscript><script>'
                . 'var loadDeferredStyles = function() {'
                    . 'var addStylesNode = document.getElementById("deferred-fonts");'
                    . 'var replacement = document.createElement("div");'
                    . 'replacement.innerHTML = addStylesNode.textContent;'
                    . 'document.body.appendChild(replacement);'
                    . 'addStylesNode.parentElement.removeChild(addStylesNode);'
                . '};'
                . 'window.addEventListener(\'load\', loadDeferredStyles);</script>';
        }
    }
}

<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magento\Framework\App\ObjectManager;
use Magento\Sales\Model\Order\Invoice as InvoiceModel;
use Magento\Sales\Model\Order\InvoiceFactory as InvoiceFactory;
use Magento\Framework\Exception\LocalizedException;
use Magenest\QuickBooksOnline\Model\TaxFactory;
use Magenest\QuickBooksOnline\Model\Config;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order\TaxFactory as SalesOrderTax;
use Magento\Sales\Model\ResourceModel\Order\Tax\Item as TaxItem;

/**
 * Class Invoice using to sync Invoice
 * @package Magenest\QuickBooksOnline\Model\Sync
 * @method \Magento\Sales\Model\Order getModel()
 */
class Invoice extends Synchronization
{
    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var Customer
     */
    protected $_syncCustomer;

    /**
     * @var Item
     */
    protected $_item;

    /**
     * @var InvoiceModel
     */
    protected $_invoice;

    /**
     * @var InvoiceFactory
     */
    protected $_invoiceFactory;

    /**
     * @var InvoiceModel
     */
    protected $_currentModel;

    /**
     * @var TaxFactory
     */
    protected $tax;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Order
     */
    protected $orderSync;

    /**
     * @var InvoiceShipping
     */
    protected $invoiceShipping;

    /**
     * @var CardCharge
     */
    protected $cardCharge;

    /**
     * @var CodDeposit
     */
    protected $codDeposit;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var PaymentMethods
     */
    protected $_paymentMethods;

    /**
     * @var PreferenceSetting
     */
    protected $_preference;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var InvoiceModel\ItemFactory
     */
    protected $itemInvoice;

    /**
     * @var TaxItem
     */
    protected $taxItem;

    /**
     * @var SalesOrderTax
     */
    protected $salesOrderTax;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * Invoice constructor.
     *
     * @param Client $client
     * @param Log $log
     * @param InvoiceModel $invoice
     * @param InvoiceFactory $invoiceFactory
     * @param Item $item
     * @param Customer $customer
     * @param InvoiceShipping $invoiceShipping
     * @param CardCharge $cardCharge
     * @param CodDeposit $codDeposit
     * @param TaxFactory $taxFactory
     * @param TaxItem $taxItem
     * @param SalesOrderTax $salesOrderTax
     * @param PreferenceSetting $preferenceSetting
     * @param Config $config
     * @param \Magenest\QuickBooksOnline\Model\PaymentMethodsFactory $paymentMethods
     * @param Order $orderSync
     * @param \Magento\Catalog\Model\ProductFactory $product
     * @param InvoiceModel\ItemFactory $invoiceItemFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param OrderFactory $orderFactory
     * @param Context $context
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        Client $client,
        Log $log,
        InvoiceModel $invoice,
        InvoiceFactory $invoiceFactory,
        Item $item,
        Customer $customer,
        InvoiceShipping $invoiceShipping,
        CardCharge $cardCharge,
        CodDeposit $codDeposit,
        TaxFactory $taxFactory,
        TaxItem $taxItem,
        SalesOrderTax $salesOrderTax,
        PreferenceSetting $preferenceSetting,
        Config $config,
        \Magenest\QuickBooksOnline\Model\PaymentMethodsFactory $paymentMethods,
        Order $orderSync,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Sales\Model\Order\Invoice\ItemFactory $invoiceItemFactory,
        \Psr\Log\LoggerInterface $logger,
        OrderFactory $orderFactory,
        Context $context,
        TimezoneInterface $timezone
    ) {
        parent::__construct($client, $log, $context);
        $this->_invoice        = $invoice;
        $this->_invoiceFactory = $invoiceFactory;
        $this->_item           = $item;
        $this->_syncCustomer   = $customer;
        $this->invoiceShipping = $invoiceShipping;
        $this->cardCharge      = $cardCharge;
        $this->codDeposit      = $codDeposit;
        $this->tax             = $taxFactory;
        $this->type            = 'invoice';
        $this->_preference     = $preferenceSetting;
        $this->config          = $config;
        $this->_paymentMethods = $paymentMethods;
        $this->orderSync       = $orderSync;
        $this->_orderFactory   = $orderFactory;
        $this->logger          = $logger;
        $this->product         = $product;
        $this->taxItem         = $taxItem;
        $this->itemInvoice     = $invoiceItemFactory;
        $this->salesOrderTax   = $salesOrderTax;
        $this->timezone        = $timezone;
    }

    /**
     * @param $incrementId
     *
     * @return mixed
     * @throws \Zend_Http_Client_Exception
     * @throws \Exception
     */
    public function sync($incrementId)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $invoiceModel */
        $invoiceModel = $this->_invoiceFactory->create()->loadByIncrementId($incrementId);
        $checkInvoice = $this->checkInvoice($incrementId);
        if (isset($checkInvoice['Id'])) {
            $this->addLog($incrementId, $checkInvoice['Id'], 'This Invoice already exists.', 'skip');
            $qboId = $checkInvoice['Id'];
        } else {
            try {
                if (!$invoiceModel->getId()) {
                    throw new LocalizedException(__('We can\'t find the Invoice #%1', $incrementId));
                }

                /** @var \Magento\Sales\Model\Order $order */
                $order = $invoiceModel->getOrder();
                if ($order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery')
                    $isCOD = true;
                else
                    $isCOD = false;
                $this->setModel($order);
                $this->prepareParams($incrementId, $invoiceModel->getCreatedAt(), $isCOD);
                $params   = array_replace_recursive($this->getParameter(), $checkInvoice);
                $response = $this->sendRequest(\Zend_Http_Client::POST, 'invoice', $params);
                if (!empty($response['Invoice']['Id'])) {
                    $qboId = $response['Invoice']['Id'];
                    $this->addLog($incrementId, $qboId);
                }
                $this->parameter = [];

                /** @var \Magento\Framework\Registry $registryObject */
                $registryObject = ObjectManager::getInstance()->get('Magento\Framework\Registry');

                $registryObject->register('skip_log', true);

                if (isset($qboId)) {
                    if ($this->config->getTrackQty()) {
                        foreach ($order->getAllItems() as $orderItem) {
                            $registryObject->unregister('check_to_syn' . $orderItem->getProductId());
                            if ($orderItem->getProductType() == 'bundle') {
                                $this->_item->sync($orderItem->getProductId());
                            } else {
                                //product with customizable options cannot be synced by SKU
                                if (!empty($orderItem->getProductOptions()['info_buyRequest']['options']))
                                    $this->_item->sync($orderItem->getProductId(), true, null);
                                else
                                    $this->_item->syncBySku($orderItem->getSku(), true);
                            }
                        }
                    }
                    $registryObject->unregister('skip_log');

                    //sync COD deposit
                    if ($isCOD)
                        $this->codDeposit->sync($incrementId);

                    //sync shipping fee as bill
                    //if ($invoiceModel->getShippingAmount() > 0) //skip checking shipping amount
                        $this->invoiceShipping->sync($incrementId);

                    //sync card processing fee as expense
                    $this->cardCharge->sync($incrementId);
                }

                return isset($qboId) ? $qboId : null;

            } catch (LocalizedException $e) {
                $this->addLog($incrementId, null, $e->getMessage());
            }
        }

        return null;
    }

    /**
     * @param $id
     * @param $createdAt
     * @param bool $isCOD
     *
     * @return $this
     * @throws \Exception
     */
    protected function prepareParams($id, $createdAt, $isCOD = false)
    {
        /** @var \Magento\Sales\Model\Order $model */
        $model  = $this->getModel();
        $prefix = $this->config->getPrefix($this->type);
        $params = [
            'DocNumber'   => $prefix . $id,
            'TxnDate'     => (new \DateTimeZone($this->timezone->getConfigTimezone()))->getOffset(new \DateTime) == 0 ? $createdAt :
                $this->timezone->date($createdAt)->format('Y-m-d'),
            'CustomerRef' => $this->prepareCustomerId(),
            'Line'        => $this->prepareLineInvoice($isCOD),
            'TotalAmt'    => $model->getBaseTotalInvoiced() - $model->getShippingInvoiced(),
            'BillEmail'   => ['Address' => mb_substr((string)$model->getCustomerEmail(), 0, 100)],
            'PrivateNote' => 'Invoice for order ' . $model->getIncrementId()
        ];

        //set billing address
        $this->prepareBillingAddress();

        //set shipping address
        if ($this->_preference->getShippingAllow() == true) {
            $this->prepareShippingAddress();
        }

        $this->setParameter($params);

        return $this;
    }

    /**
     * Unused in Customization
     * get payment method
     */
    public function preparePaymentMethod()
    {
        $modelOrder    = $this->_orderFactory->create()->load($this->getModel()->getOrderId());
        $code          = $modelOrder->getPayment()->getMethodInstance()->getCode();
        $paymentMethod = $this->_paymentMethods->create()->load($code, 'payment_code');
        if ($paymentMethod->getId()) {
            $params['PaymentMethodRef'] = [
                'value' => $paymentMethod->getQboId(),
            ];
            if (!empty($paymentMethod->getDepositAccount())) {
                $params['DepositToAccountRef'] = [
                    'value' => $paymentMethod->getDepositAccount(),
                ];
            }
            $this->setParameter($params);
        }
    }

    /**
     * get Billing
     */
    public function prepareBillingAddress()
    {
        /** @var \Magento\Sales\Model\Order\Address $billAddress */
        $billAddress = $this->getModel()->getBillingAddress();
        if ($billAddress !== null) {
            $params['BillAddr'] = $this->getAddress($billAddress);
            $this->setParameter($params);
        }
    }

    /**
     * get shipping
     */
    public function prepareShippingAddress()
    {
        $shippingAddress = $this->getModel()->getShippingAddress();
        if ($shippingAddress !== null) {
            $params['ShipAddr'] = $this->getAddress($shippingAddress);
            $this->setParameter($params);
        }
    }

    /**
     * @param null $customerIsGuest
     *
     * @return array
     * @throws LocalizedException
     */
    public function prepareCustomerId($customerIsGuest = null)
    {
        try {
            /** @var \Magento\Sales\Model\Order $modelOrder */
            $modelOrder = $this->getModel();
            $customerId = $modelOrder->getCustomerId();
            if ($customerId and $customerIsGuest == false) {
                $cusRef = $this->_syncCustomer->sync($customerId, false);
            } else {
                $cusRef = $this->_syncCustomer->syncGuest(
                    $modelOrder->getBillingAddress(),
                    $modelOrder->getShippingAddress()
                );
            }

            return ['value' => $cusRef];
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Can\'t sync customer on Invoice to QBO')
            );
        }
    }

    /**
     * @param bool $isCod
     *
     * @return array
     * @throws LocalizedException
     */
    public function prepareLineInvoice($isCod)
    {
        try {
            $i     = 1;
            $lines = [];
            /** @var \Magento\Sales\Model\Order\Item $item */
            foreach ($this->getModel()->getItems() as $item) {
                $productType    = $item->getProductType();
                $registryObject = ObjectManager::getInstance()->get('Magento\Framework\Registry');
                if ($productType == 'configurable') {
                    $total         = $item->getBaseRowTotal();
                    $childrenItems = $item->getChildrenItems();
                    if (isset($childrenItems[0])) {
                        $productId = $childrenItems[0]->getProductId();
                        $sku       = $childrenItems[0]->getSku();
                        $qty       = $childrenItems[0]->getQtyInvoiced();
                    } else {
                        $productId = $item->getProductId();
                        $sku       = $item->getSku();
                        $qty       = $item->getQtyInvoiced();
                    }
                    $price = $qty > 0 ? $total / $qty : $item->getBasePrice();
                    $registryObject->unregister('check_to_syn' . $productId);
                    $itemId = $this->_item->syncBySku($sku, false);
                    if (!$itemId) throw new \Exception(
                        __('Can\'t sync Product with SKU:%1 on Order to QBO', $sku)
                    );
                } else if ($item->getParentItem() && ($productType == 'virtual' || $productType == 'simple')) {
                    if ($item->getParentItem()->getProductType() == 'configurable') {
                        continue;
                    } else {
                        $productId = $item->getProductId();
                        $sku       = $item->getSku();
                        $qty       = $item->getQtyInvoiced();
                        $total     = $item->getBaseRowTotal();
                        $price     = $qty > 0 ? $total / $qty : $item->getBasePrice();

                        $registryObject->unregister('check_to_syn' . $productId);
                        if (!empty($item->getProductOptions()['info_buyRequest']['options']))
                            $itemId = $this->_item->sync($item->getProductId());
                        else
                            $itemId = $this->_item->syncBySku($sku);
                        $registryObject->unregister('check_to_syn' . $productId);
                        if (!$itemId) throw new \Exception(
                            __('Can\'t sync Product with SKU:%1 on Order to QBO', $sku)
                        );
                    }
                } else {
                    $productId = $item->getProductId();
                    $sku       = $item->getSku();
                    $qty       = $item->getQtyInvoiced();
                    $total     = $item->getBaseRowTotal();
                    $price     = $qty > 0 ? $total / $qty : $item->getBasePrice();

                    $registryObject->unregister('check_to_syn' . $productId);
                    if ($productType == 'bundle') {
                        $priceType = $item->getProductOptionByCode('product_calculations');
                        if ($priceType == 0) {
                            $price = 0;
                            $total = 0;
                        }
                        $itemId = $this->_item->sync($productId);
                    } else if (!empty($item->getProductOptions()['info_buyRequest']['options']))
                        $itemId = $this->_item->sync($item->getProductId());
                    else
                        $itemId = $this->_item->syncBySku($sku);
                    $registryObject->unregister('check_to_syn' . $productId);
                    if (!$itemId) throw new \Exception(
                        __('Can\'t sync Product with SKU:%1 on Order to QBO', $sku)
                    );
                }
                if (!empty($itemId)) {
                    $taxId   = $this->prepareTaxCodeRef($item->getItemId());
                    $params = [
                        'LineNum'             => $i,
                        'Amount'              => $total,
                        'DetailType'          => 'SalesItemLineDetail',
                        'SalesItemLineDetail' => [
                            'ItemRef'    => ['value' => $itemId],
                            'UnitPrice'  => $price,
                            'Qty'        => $qty,
                            'TaxCodeRef' => ['value' => 'NON']
                        ],
                    ];
                    if ($this->config->getCountry() == 'GLOBAL')
                        $params['SalesItemLineDetail']['TaxCodeRef'] = ['value' => $taxId ? $taxId : $this->getTaxFreeId()];

                    $lines[] = $params;

                    $i++;
                } else continue;
            }

            if ($isCod) {
                $shippingMethod = $this->getModel()->getShippingMethod();
//                $shippingMapping = $this->_shippingFactory->create()->loadByCode($shippingMethod);
            }

            //Shipping fee
            if ($this->_preference->getShippingAllow() == true && $this->getModel()->getShippingAmount() > 0) {
                $lines[] = $this->prepareLineShippingFee();
            }

            // set discount
            $lines[] = $this->prepareLineDiscountAmount();

            return $lines;
        } catch (\Exception $exception) {
            throw new LocalizedException(
                __('Error when syncing products: %1', $exception->getMessage())
            );
        }
    }

    /**
     * @return array
     */
    protected function prepareLineShippingFee()
    {
        /** @var \Magento\Sales\Model\Order $model */
        $model          = $this->getModel();
        $shippingAmount = $model->getBaseShippingAmount();
        if ($this->config->getCountry() != 'OTHER') {
            $taxItems = $this->taxItem->getTaxItemsByOrderId($model->getId());
            foreach ($taxItems as $key => $value) {
                if (isset($value['taxable_item_type']) && $value['taxable_item_type'] == 'shipping') {
                    $taxId     = $value['tax_id'];
                    $taxCodeId = $this->getTaxQBOIdFromTaxItem($taxId);
                    break;
                }
            }

            if ($model->getBaseShippingAmount() == 0)
                $taxCodeId = $this->getTaxFreeId();

            $lines = [
                'Amount'              => $shippingAmount ? $shippingAmount : 0,
                'DetailType'          => 'SalesItemLineDetail',
                'SalesItemLineDetail' => [
                    'ItemRef'    => ['value' => 'SHIPPING_ITEM_ID'],
                    'TaxCodeRef' => ['value' => isset($taxCodeId) ? $taxCodeId : $this->config->getTaxShipping()],
                ],
            ];
        } else {
            $lines = [
                'Amount'              => $shippingAmount ? $shippingAmount : 0,
                'DetailType'          => 'SalesItemLineDetail',
                'SalesItemLineDetail' => [
                    'ItemRef' => ['value' => 'SHIPPING_ITEM_ID'],
                ],
            ];
        }

        return $lines;
    }

    /**
     * @param $taxId
     *
     * @return int
     */
    protected function getTaxQBOIdFromTaxItem($taxId)
    {
        $code      = $this->salesOrderTax->create()->load($taxId)->getCode();
        $taxCodeId = $this->tax->create()->loadByCode($code)->getQboId();

        return $taxCodeId;
    }

    /**
     * @return mixed
     */
    public function getTaxFreeId()
    {
        $localId   = $this->config->getTaxFree();
        $taxCodeId = $this->tax->create()->load($localId, 'tax_id')->getQboId();

        return $taxCodeId;
    }

    /**
     * @param $itemId
     *
     * @return bool|int
     */
    public function prepareTaxCodeRef($itemId)
    {
        $taxCode = 'tax_zero_qb';
        /** @var \Magento\Sales\Model\Order\Tax\Item $modelTaxItem */
        $modelTaxItem = ObjectManager::getInstance()->create(\Magento\Sales\Model\Order\Tax\Item::class)->load($itemId, 'item_id');
        if ($modelTaxItem) {
            $taxId        = $modelTaxItem->getData('tax_id');
            $modelTax     = ObjectManager::getInstance()->create('Magento\Sales\Model\Order\TaxFactory');
            $modelTaxData = $modelTax->create()->getCollection()
                ->addFieldToFilter("tax_id", $taxId)->getFirstItem();

            /** @var \Magento\Sales\Model\Order\Tax $modelTaxData */
            if (!empty($modelTaxData->getCode())) {
                $taxCode = $modelTaxData->getCode();
            }
            $tax = $this->tax->create()->loadByCode($taxCode);
            if ($tax->getQboId() && $tax->getQboId() > 0) {
                $taxCodeId = $tax->getQboId();

                return $taxCodeId;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    protected function prepareLineDiscountAmount()
    {
        /** @var \Magento\Sales\Model\Order $model */
        $model                = $this->getModel();
        $discountAmount       = $model->getDiscountInvoiced();
        $discountCompensation = $model->getDiscountTaxCompensationInvoiced();
        $lines                = [
            'Amount'             => $discountAmount ? (-1 * $discountAmount - $discountCompensation) : 0,
            'DetailType'         => 'DiscountLineDetail',
            'DiscountLineDetail' => [
                'PercentBased' => false,
            ]
        ];

        return $lines;
    }


    /**
     * Check invoice by Increment Id
     *
     * @param $id
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkInvoice($id)
    {
        $prefix = $this->config->getPrefix('invoice');
        $name   = $prefix . $id;
        $query  = "SELECT Id, SyncToken FROM invoice WHERE DocNumber='{$name}'";

        return $this->query($query);
    }
}

<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magenest\QuickBooksOnline\Model\Config;
use Magenest\QuickBooksOnline\Model\PaymentMethodsFactory;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\Order\InvoiceFactory;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class CardCharge
 * @package Magenest\QuickBooksOnline\Model\Synchronization
 */
class CardCharge extends Synchronization
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var PaymentMethodsFactory
     */
    protected $paymentMethods;

    /**
     * @var InvoiceFactory
     */
    protected $_invoiceFactory;

    /**
     * CardCharge constructor.
     *
     * @param Client $client
     * @param Log $log
     * @param Context $context
     * @param Config $config
     * @param PaymentMethodsFactory $paymentMethods
     * @param InvoiceFactory $invoice
     */
    public function __construct(
        Client $client,
        Log $log,
        Context $context,
        Config $config,
        PaymentMethodsFactory $paymentMethods,
        InvoiceFactory $invoice
    ) {
        parent::__construct($client, $log, $context);
        $this->config          = $config;
        $this->paymentMethods  = $paymentMethods;
        $this->_invoiceFactory = $invoice;
        $this->type            = 'charge';
    }

    /**
     * @param $incrementId
     *
     * @return mixed
     * @throws \Exception
     */
    public function sync($incrementId)
    {
        try {
            $invoiceModel = $this->_invoiceFactory->create()->loadByIncrementId($incrementId);
            if (!$invoiceModel->getId()) {
                throw new LocalizedException(__('We can\'t find the Invoice #%1', $incrementId));
            }

            $checkInvoice = $this->checkInvoice($incrementId);
            if (!isset($checkInvoice['Id'])) {
                throw new LocalizedException(__('The linked invoice is not yet synced'));
            }

            $checkCharge = $this->checkProcessingFee($incrementId);
            if (isset($checkCharge['Id'])) {
                $this->addLog($incrementId, $checkCharge['Id'], 'Payment fee already synced', 'skip');

                return $checkCharge['Id'];
            }

            $paymentMethod  = $invoiceModel->getOrder()->getPayment()->getMethodInstance()->getCode();
            $paymentMapping = $this->paymentMethods->create()->loadByCode($paymentMethod);
//            if (!$paymentMapping->getId()) {
//                throw new LocalizedException(__('Method is not synced to QuickBooks'));
//            }

            //has payment charge
            if ($paymentMapping->getId() && $paymentMapping->getCardFee() > 0) {
                if (!$paymentMapping->getPayableAccount() || !$paymentMapping->getExpenseType())
                    throw new LocalizedException(__('Method mapping incomplete'));

                $this->parameter = [];
                $this->setModel($invoiceModel->getOrder());
                $this->prepareParams($incrementId, $paymentMapping->getData());

                $params            = $this->getParameter();
                $params['TxnDate'] = $checkInvoice['TxnDate'];
                $response          = $this->sendRequest(\Zend_Http_Client::POST, 'purchase', $params);
                if (!empty($response['Purchase']['Id'])) {
                    $qboId = $response['Purchase']['Id'];
                    $this->addLog($incrementId, $qboId);
                }
                $this->parameter = [];
            }

            return isset($qboId) ? $qboId : null;

        } catch (LocalizedException $e) {
            $this->addLog($incrementId, null, __('Error syncing card fee for invoice %1: %2', $incrementId, $e->getMessage()));
        }

        return null;
    }

    /**
     * @param int $incrementId
     * @param array $mapping
     *
     * @return $this
     */
    protected function prepareParams($incrementId, $mapping)
    {
        /** @var \Magento\Sales\Model\Order $model */
        $model  = $this->getModel();
        $prefix = $this->config->getPrefix('card');
        $fee    = $model->getGrandTotal() * $mapping['card_fee'] /100;
        $params = [
            'DocNumber'   => $prefix . $incrementId,
            'PaymentType' => 'CreditCard',
            'AccountRef'  => ['value' => $mapping['payable_account']],
            'EntityRef'   => ['value' => $mapping['vendor_id']],
            'TotalAmt'    => $fee,
            'Line'        => [0 => [
                'DetailType'                    => 'AccountBasedExpenseLineDetail',
                'Amount'                        => $fee,
                'Description'                   => 'Payment processor fee for order ' . $model->getIncrementId(),
                'AccountBasedExpenseLineDetail' => [
                    'AccountRef' => ['value' => $mapping['expense_type']]
                ]
            ]]
        ];
        $this->setParameter($params);

        return $this;
    }

    /**
     * Check invoice by Increment Id
     *
     * @param $id
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkInvoice($id)
    {
        $prefix = $this->config->getPrefix('invoice');
        $name   = $prefix . $id;
        $query  = "SELECT Id, TxnDate FROM invoice WHERE DocNumber='{$name}'";

        return $this->query($query);
    }

    /**
     * Check if expense exists
     * @param $id
     *
     * @return array
     * @throws LocalizedException
     */
    protected function checkProcessingFee($id)
    {
        $prefix = $this->config->getPrefix('card');
        $name   = $prefix . $id;
        $query  = "SELECT Id FROM purchase WHERE DocNumber='{$name}'";

        return $this->query($query);
    }
}

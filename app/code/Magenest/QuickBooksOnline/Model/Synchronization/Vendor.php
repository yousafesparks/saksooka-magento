<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magenest\QuickBooksOnline\Model\VendorFactory;
use Magenest\QuickBooksOnline\Model\ResourceModel\Vendor as VendorResource;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Vendor
 * @package Magenest\QuickBooksOnline\Model\Synchronization
 */
class Vendor extends Synchronization
{
    /**
     * @var VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var VendorResource
     */
    protected $_vendorResource;

    /**
     * Vendor constructor.
     *
     * @param VendorResource $vendorResource
     * @param VendorFactory $vendorFactory
     * @param Client $client
     * @param Log $log
     * @param Context $context
     */
    public function __construct(
        VendorResource $vendorResource,
        VendorFactory $vendorFactory,
        Client $client,
        Log $log,
        Context $context
    ) {
        parent::__construct($client, $log, $context);
        $this->_vendorResource = $vendorResource;
        $this->_vendorFactory  = $vendorFactory;
    }

    /**
     * @throws LocalizedException
     */
    public function fetchVendors()
    {
        try {
            $query = 'select Id,DisplayName from vendor';

            $path      = 'query?query=' . rawurlencode($query);
            $responses = $this->sendRequest(\Zend_Http_Client::GET, $path);
            foreach ($responses as $response) {
                if (is_array($response)) {
                    foreach ($response as $item) {
                        if (is_array($item) && isset($item[0]['Id'])) {
                            $data = $item;
                            break;
                        }
                    }
                }
            }
            /** @var \Magenest\QuickBooksOnline\Model\Vendor $model */
            $model      = $this->_vendorFactory->create();
            $collection = $model->getCollection();
            $connection = $collection->getConnection();
            $tableName  = $collection->getMainTable();
            $connection->truncateTable($tableName);
            if (isset($data))
                foreach ($data as $datum) {
                    $_model = $this->_vendorFactory->create();
                    $params = [
                        'qbo_id' => $datum['Id'],
                        'name'   => $datum['DisplayName']
                    ];
                    $_model->addData($params);
                    $this->_vendorResource->save($_model);
                }
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }
}
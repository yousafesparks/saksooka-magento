<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magenest\QuickBooksOnline\Model\ShippingFactory;
use Magenest\QuickBooksOnline\Model\ResourceModel\Shipping as ShippingResource;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\Order\InvoiceFactory;
use Magento\Framework\Exception\LocalizedException;
use Magenest\QuickBooksOnline\Model\Config;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class InvoiceShipping
 * @package Magenest\QuickBooksOnline\Model\Synchronization
 */
class InvoiceShipping extends Synchronization
{
    /**
     * @var ShippingFactory
     */
    protected $_shippingFactory;

    /**
     * @var ShippingResource
     */
    protected $_shippingResource;

    /**
     * @var InvoiceFactory
     */
    protected $_invoiceFactory;

    /**
     * @var Item
     */
    protected $_item;

    /**
     * @var Account
     */
    protected $_account;

    /**
     * @var Customer
     */
    protected $_syncCustomer;

    /**
     * @var Invoice
     */
    protected $_syncInvoice;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var TimezoneInterface
     */
    protected $_timezone;

    /**
     * InvoiceShipping constructor.
     *
     * @param Client $client
     * @param Log $log
     * @param Context $context
     * @param ShippingFactory $shippingFactory
     * @param ShippingResource $shippingResource
     * @param Item $item
     * @param Customer $customer
     * @param Account $account
     * @param Config $config
     * @param TimezoneInterface $timezone
     * @param InvoiceFactory $invoiceFactory
     */
    public function __construct(
        Client $client,
        Log $log,
        Context $context,
        ShippingFactory $shippingFactory,
        ShippingResource $shippingResource,
        Item $item,
        Customer $customer,
        Account $account,
        Config $config,
        TimezoneInterface $timezone,
        InvoiceFactory $invoiceFactory
    ) {
        parent::__construct($client, $log, $context);
        $this->_shippingFactory  = $shippingFactory;
        $this->_shippingResource = $shippingResource;
        $this->_invoiceFactory   = $invoiceFactory;
        $this->_item             = $item;
        $this->_account          = $account;
        $this->_syncCustomer     = $customer;
        $this->config            = $config;
        $this->_timezone         = $timezone;
        $this->type              = 'shipping';
    }

    /**
     * @param $incrementId
     *
     * @return mixed
     * @throws \Exception
     */
    public function sync($incrementId)
    {
        try {
            $invoiceModel = $this->_invoiceFactory->create()->loadByIncrementId($incrementId);
            if (!$invoiceModel->getId()) {
                throw new LocalizedException(__('We can\'t find the Invoice #%1', $incrementId));
            }

            $checkInvoice = $this->checkInvoice($incrementId);
            if (!isset($checkInvoice['Id'])) {
                throw new LocalizedException(__('The linked invoice is not yet synced'));
            }

            $checkExpense = $this->checkShippingBill($incrementId);
            if (isset($checkExpense['Id'])) {
                $this->addLog($incrementId, $checkExpense['Id'], __('Bill already synced'), 'skip');

                return $checkExpense['Id'];
            }

            $orderModel      = $invoiceModel->getOrder();
            $shippingMethod  = $orderModel->getShippingMethod();
            $shippingMapping = $this->_shippingFactory->create()->loadByCode($shippingMethod);
            if (!$shippingMapping->getId() || !$shippingMapping->getQboId() || !$shippingMapping->getPayFrom() || !$shippingMapping->getExpenseType()) {
                throw new LocalizedException(__('Mapping for shipping method with code %1 is unfinished', $shippingMethod));
            }

            $this->parameter = [];

            $this->setModel($invoiceModel);
            $paymentMethod = $orderModel->getPayment()->getMethodInstance()->getCode();
            if ($paymentMethod == 'cashondelivery') {
                $this->prepareParams($shippingMapping->getData(), $checkInvoice, $orderModel->getIncrementId(), true);
            } else {
                $this->prepareParams($shippingMapping->getData(), $checkInvoice, $orderModel->getIncrementId());
            }
            $params   = $this->getParameter();
            $response = $this->sendRequest(\Zend_Http_Client::POST, 'bill', $params);
            if (!empty($response['Bill']['Id'])) {
                $qboId = $response['Bill']['Id'];
                $this->addLog($incrementId, $qboId);
            }
            $this->parameter = [];

            return isset($qboId) ? $qboId : null;


        } catch (LocalizedException $e) {
            $this->addLog($incrementId, null, __('Error when syncing shipping expense for invoice %1: %2', $incrementId, $e->getMessage()));
        }

        return null;
    }

    /**
     * @param array $mapping
     * @param array $invoiceData
     * @param string $orderIcrId
     * @param bool $isCod
     *
     * @return $this
     */
    protected function prepareParams($mapping, $invoiceData, $orderIcrId, $isCod = false)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $model */
        $model        = $this->getModel();
        $invoiceIcrId = $model->getIncrementId();
        $prefix       = $this->config->getPrefix($this->type);
        $params       = [
            'DocNumber'    => $prefix . $invoiceIcrId,
            'TxnDate'      => $invoiceData['TxnDate'],
            'APAccountRef' => ['value' => $mapping['pay_from']],
            'VendorRef'    => ['value' => $mapping['qbo_id']],
            'TotalAmt'     => $model->getShippingAmount(),
            'Line'         => [0 => [
                'DetailType'                    => 'AccountBasedExpenseLineDetail',
                'Amount'                        => 20 * (1.05),
                'Description'                   => 'Shipping fee for order ' . $orderIcrId,
                'AccountBasedExpenseLineDetail' => [
                    'AccountRef' => ['value' => $mapping['expense_type']]
                ]
            ]]
        ];

        if ($isCod) {
            $params['Line'][] = [
                'DetailType'                    => 'AccountBasedExpenseLineDetail',
                'Amount'                        => 10 * (1.05),
                'Description'                   => 'COD fee for order ' . $orderIcrId,
                'AccountBasedExpenseLineDetail' => [
                    'AccountRef' => ['value' => $mapping['expense_type']]
                ]
            ];
        }

        $this->setParameter($params);

        return $this;
    }

    /**
     * Check invoice by Increment Id
     *
     * @param $id
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkInvoice($id)
    {
        $prefix = $this->config->getPrefix('invoice');
        $name   = $prefix . $id;
        $query  = "SELECT Id, TxnDate FROM invoice WHERE DocNumber='{$name}'";

        return $this->query($query);
    }

    /**
     * Check shipping expense by Invoice Increment Id
     *
     * @param $id
     *
     * @return array
     * @throws LocalizedException
     */
    protected function checkShippingBill($id)
    {
        $prefix = $this->config->getPrefix('shipping');
        $name   = $prefix . $id;
        $query  = "SELECT Id FROM bill WHERE DocNumber='{$name}'";

        return $this->query($query);
    }
}
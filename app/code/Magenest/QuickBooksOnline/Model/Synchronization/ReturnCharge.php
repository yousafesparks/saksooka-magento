<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magenest\QuickBooksOnline\Model\ShippingFactory;
use Magenest\QuickBooksOnline\Model\ResourceModel\Shipping as ShippingResource;
use Magenest\QuickBooksOnline\Model\Config;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Creditmemo as CreditmemoModel;

/**
 * Class ReturnCharge
 * @package Magenest\QuickBooksOnline\Model\Synchronization
 */
class ReturnCharge extends Synchronization
{
    /**
     * @var CreditmemoModel
     */
    protected $_creditMemo;

    /**
     * @var ShippingFactory
     */
    protected $_shippingFactory;

    /**
     * @var ShippingResource
     */
    protected $_shippingResource;

    /**
     * @var Config
     */
    protected $config;

    /**
     * ReturnCharge constructor.
     *
     * @param CreditmemoModel $creditmemo
     * @param ShippingFactory $shippingFactory
     * @param ShippingResource $shippingResource
     * @param Config $config
     * @param Client $client
     * @param Log $log
     * @param Context $context
     */
    public function __construct(
        CreditmemoModel $creditmemo,
        ShippingFactory $shippingFactory,
        ShippingResource $shippingResource,
        Config $config,
        Client $client,
        Log $log,
        Context $context
    ) {
        parent::__construct($client, $log, $context);
        $this->_creditMemo       = $creditmemo;
        $this->_shippingFactory  = $shippingFactory;
        $this->_shippingResource = $shippingResource;
        $this->config            = $config;
        $this->type              = 'rtg';
    }

    /**
     * @param $incrementId
     *
     * @return mixed
     * @throws \Exception
     */
    public function sync($incrementId)
    {
        try {
            $model = $this->loadByIncrementId($incrementId);
            if (!$model->getId())
                throw new LocalizedException(__('Creditmemo %1 not found', $incrementId));

            if ($model->getAdjustmentNegative() == 0) {
                $this->addLog($incrementId, null, __('This creditmemo has no return fee'), 'skip');

                return null;
            }

            $checkReturn = $this->checkReturnBill($incrementId);
            if (isset($checkReturn['Id'])) {
                $this->addLog($incrementId, $checkReturn['Id'], __('This return bill already exists'), 'skip');

                return $checkReturn['Id'];
            }

            $checkMemo = $this->checkCreditmemo($incrementId);
            if (empty($checkMemo['Id']))
                throw new LocalizedException(__('Credit Memo is not synced to QuickBooks'));

            $shippingMethod  = $model->getOrder()->getShippingMethod();
            $shippingMapping = $this->_shippingFactory->create()->loadByCode($shippingMethod);
            if (!$shippingMapping->getId() || !$shippingMapping->getQboId() || !$shippingMapping->getPayFrom() || !$shippingMapping->getExpenseType()) {
                throw new LocalizedException(__('Mapping for shipping method with code %1 is unfinished', $shippingMethod));
            }

            $this->parameter = [];

            $this->setModel($model);
            $this->prepareParams($shippingMapping->getData(), $checkMemo);
            $params   = $this->getParameter();
            $response = $this->sendRequest(\Zend_Http_Client::POST, 'bill', $params);
            if (!empty($response['Bill']['Id'])) {
                $qboId = $response['Bill']['Id'];
                $this->addLog($incrementId, $qboId);
            }
            $this->parameter = [];

            return isset($qboId) ? $qboId : null;

        } catch (LocalizedException $e) {
            $this->addLog($incrementId, null, __('Error when syncing return fee for credit memo %1: %2', $incrementId, $e->getMessage()));
        }

        return null;
    }

    /**
     * @param $mapping
     * @param $memoData
     *
     * @return $this
     */
    protected function prepareParams($mapping, $memoData)
    {
        /** @var \Magento\Sales\Model\Order\Creditmemo $model */
        $model     = $this->getModel();
        $memoIcrId = $model->getIncrementId();
        $prefix    = $this->config->getPrefix('rtg');
        $params    = [
            'DocNumber'    => $prefix . $memoIcrId,
            'TxnDate'      => $memoData['TxnDate'],
            'APAccountRef' => ['value' => $mapping['pay_from']],
            'VendorRef'    => ['value' => $mapping['qbo_id']],
            'TotalAmt'     => $model->getAdjustmentNegative(),
            'Line'         => [0 => [
                'DetailType'                    => 'AccountBasedExpenseLineDetail',
                'Amount'                        => $model->getAdjustmentNegative(),
                'Description'                   => 'Return fee for memo ' . $memoIcrId,
                'AccountBasedExpenseLineDetail' => [
                    'AccountRef' => ['value' => $mapping['expense_type']]
                ]
            ]]
        ];

        $this->setParameter($params);

        return $this;

    }

    /**
     * Check if return bill already exists
     *
     * @param $id
     *
     * @return array
     * @throws LocalizedException
     */
    protected function checkReturnBill($id)
    {
        $prefix = $this->config->getPrefix('rtg');
        $name   = $prefix . $id;
        $query  = "SELECT Id FROM bill WHERE DocNumber='{$name}'";

        return $this->query($query);
    }

    /**
     * Check creditmemo by Increment Id
     *
     * @param $id
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkCreditmemo($id)
    {
        $prefix = $this->config->getPrefix('creditmemos');
        $name   = $prefix . $id;
        $query  = "SELECT Id, TxnDate FROM CreditMemo WHERE DocNumber='{$name}'";

        return $this->query($query);
    }

    /**
     * @param $id
     *
     * @return \Magento\Sales\Model\Order\Creditmemo
     */
    public function loadByIncrementId($id)
    {
        $ids = $this->_creditMemo->getCollection()->addFieldToFilter('increment_id', $id)->getAllIds();

        if (!empty($ids)) {
            reset($ids);
            $this->_creditMemo->load(current($ids));
        }

        return $this->_creditMemo;
    }
}
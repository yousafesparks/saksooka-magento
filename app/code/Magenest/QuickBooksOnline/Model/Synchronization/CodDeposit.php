<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magenest\QuickBooksOnline\Model\Config;
use Magenest\QuickBooksOnline\Model\ShippingFactory;
use Magenest\QuickBooksOnline\Model\CodDepositFactory;
use Magenest\QuickBooksOnline\Model\ResourceModel\CodDeposit as DepositResource;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\InvoiceFactory;
use Magento\Framework\App\Action\Context;

/**
 * Class CodDeposit
 * @package Magenest\QuickBooksOnline\Model\Synchronization
 */
class CodDeposit extends Synchronization
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Account
     */
    protected $account;

    /**
     * @var InvoiceFactory
     */
    protected $invoiceFactory;

    /**
     * @var ShippingFactory
     */
    protected $shippingFactory;

    /**
     * @var CodDepositFactory
     */
    protected $codDepositFactory;

    /**
     * @var DepositResource
     */
    protected $depositResource;

    /**
     * CodDeposit constructor.
     *
     * @param Client $client
     * @param Log $log
     * @param Context $context
     * @param Config $config
     * @param Account $account
     * @param InvoiceFactory $invoiceFactory
     * @param ShippingFactory $shippingFactory
     * @param CodDepositFactory $codDepositFactory
     * @param DepositResource $depositResource
     */
    public function __construct(
        Client $client,
        Log $log,
        Context $context,
        Config $config,
        Account $account,
        InvoiceFactory $invoiceFactory,
        ShippingFactory $shippingFactory,
        CodDepositFactory $codDepositFactory,
        DepositResource $depositResource
    ) {
        parent::__construct($client, $log, $context);
        $this->config            = $config;
        $this->account           = $account;
        $this->invoiceFactory    = $invoiceFactory;
        $this->shippingFactory   = $shippingFactory;
        $this->codDepositFactory = $codDepositFactory;
        $this->depositResource   = $depositResource;
        $this->type              = 'coddeposit';
    }

    /**
     * @param $incrementId
     *
     * @throws \Exception
     */
    public function sync($incrementId)
    {
        try {
            $invoiceModel = $this->invoiceFactory->create()->loadByIncrementId($incrementId);
            if (!$invoiceModel->getId()) {
                throw new LocalizedException(__('We can\'t find the Invoice #%1', $incrementId));
            }

            $checkInvoice = $this->checkInvoice($incrementId);
            if (!isset($checkInvoice['Id'])) {
                throw new LocalizedException(__('The linked invoice is not yet synced'));
            }

            $checkDeposit = $this->codDepositFactory->create()->loadByInvoiceId($incrementId);
            if (!empty($checkDeposit->getId())) {
                $this->addLog($incrementId, $checkDeposit->getQboId(), __('This deposit is already synced'), 'skip');

                return;
            }

            $orderModel      = $invoiceModel->getOrder();
            $shippingMethod  = $orderModel->getShippingMethod();
            $shippingMapping = $this->shippingFactory->create()->loadByCode($shippingMethod);
            if (!$shippingMapping->getId() || !$shippingMapping->getCodDeposit()) {
                throw new LocalizedException(__('Mapping for shipping method with code %1 is unfinished', $shippingMethod));
            }

            //deposit from product income account to vendor's bank acc
            $params = [
                'DepositToAccountRef' => ['value' => $shippingMapping->getCodDeposit()],
                'TxnDate'             => $checkInvoice['TxnDate'],
                'TotalAmt'            => $invoiceModel->getGrandTotal(),
                'PrivateNote'         => 'COD Deposit for order ' . $invoiceModel->getOrder()->getIncrementId() . ' , invoice ' . $incrementId,
                'Line'                => [0 => [
                    'DetailType'        => 'DepositLineDetail',
                    'Description'       => 'COD Deposit for invoice ' . $incrementId,
                    'Amount'            => $invoiceModel->getGrandTotal(),
                    'DepositLineDetail' => [
                        'AccountRef' => ['value' => $this->account->sync()]
                    ],
                ]]
            ];

            $response = $this->sendRequest(\Zend_Http_Client::POST, 'deposit', $params);
            if (!empty($response['Deposit']['Id'])) {
                $qboId = $response['Deposit']['Id'];
                $this->addLog($incrementId, $qboId);

                $storeDeposit = $this->codDepositFactory->create()->addData([
                    'qbo_id'     => $qboId,
                    'invoice_id' => $incrementId
                ]);
                $this->depositResource->save($storeDeposit);
            }

        } catch (LocalizedException $e) {
            $this->addLog($incrementId, null, $e->getMessage());
        }
    }

    /**
     * Check invoice by Increment Id
     *
     * @param $id
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkInvoice($id)
    {
        $prefix = $this->config->getPrefix('invoice');
        $name   = $prefix . $id;
        $query  = "SELECT Id, TxnDate FROM invoice WHERE DocNumber='{$name}'";

        return $this->query($query);
    }
}
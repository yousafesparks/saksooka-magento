<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magenest\QuickBooksOnline\Model\AccountFactory;

/**
 * Class Expense
 * @package Magenest\QuickBooksOnline\Model\Config\Source
 */
class Expense implements SourceInterface, OptionSourceInterface
{
    /**
     * @var AccountFactory
     */
    protected $accountFactory;

    /**
     * Deposit constructor.
     *
     * @param AccountFactory $accountFactory
     */
    public function __construct(
        AccountFactory $accountFactory
    ) {
        $this->accountFactory = $accountFactory;
    }

    /**
     * Retrieve option array
     * @return string[]
     */
    public function getOptionArray()
    {
        $expenses = $this->accountFactory->create()->getCollection()->addFieldToFilter('type', 'Expense');
        $result   = [];

        foreach ($expenses as $expense) {
            $accountId   = $expense->getQboId();
            $accountName = $expense->getName();
            $result[]    = ['value' => $accountId, 'label' => $accountName];
        }

        return $result;
    }

    /**
     * Retrieve option array with empty value
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $option) {
            $result[] = ['value' => $option['value'], 'label' => $option['label']];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     *
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }

    /**
     * Get options as array
     * @return array
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}

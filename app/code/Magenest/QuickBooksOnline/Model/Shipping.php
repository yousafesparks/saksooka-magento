<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Shipping
 * @package Magenest\QuickBooksOnline\Model
 * @method int getQboId()
 * @method string getName()
 * @method int getPayFrom()
 * @method int getExpenseType()
 * @method string getCodName()
 * @method int getCodDeposit()
 */
class Shipping extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\QuickBooksOnline\Model\ResourceModel\Shipping');
    }

    /**
     * @param $code
     *
     * @return Shipping
     */
    public function loadByCode($code)
    {
        return $this->load($code, 'code');
    }
}
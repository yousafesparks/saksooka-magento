<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class CodDeposit
 * @package Magenest\QuickBooksOnline\Model
 */
class CodDeposit extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\QuickBooksOnline\Model\ResourceModel\CodDeposit');
    }

    /**
     * @param $id
     *
     * @return CodDeposit
     */
    public function loadByInvoiceId($id)
    {
        return $this->load($id, 'invoice_id');
    }
}
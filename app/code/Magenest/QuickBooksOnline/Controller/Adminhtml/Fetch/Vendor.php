<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Fetch;

use Magenest\QuickBooksOnline\Model\Synchronization\Vendor as FetchVendor;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Vendor
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Fetch
 */
class Vendor extends Action
{
    /**
     * @var FetchVendor
     */
    protected $fetchVendor;

    /**
     * Vendor constructor.
     *
     * @param Context $context
     * @param FetchVendor $fetchVendor
     */
    public function __construct(
        Context $context,
        FetchVendor $fetchVendor
    )
    {
        parent::__construct($context);
        $this->fetchVendor = $fetchVendor;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $this->fetchVendor->fetchVendors();

            $this->messageManager->addSuccessMessage(
                __('All Vendors are fetched successfully.')
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while syncing vendors. Please check your connection. Detail: ' . $e->getMessage())
            );
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_QuickBooksOnline::config_qbonline');
    }
}
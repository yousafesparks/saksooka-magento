<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\QuickBooksOnline\Model\ShippingFactory;

/**
 * Class AbstractShipping
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping
 */
abstract class AbstractShipping extends Action
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ShippingFactory
     */
    protected $shippingModel;

    /**
     * AbstractShipping constructor.
     *
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param PageFactory $resultPageFactory
     * @param ShippingFactory $shippingModel
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        PageFactory $resultPageFactory,
        ShippingFactory $shippingModel
    ) {
        parent::__construct($context);
        $this->_scopeConfig      = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->shippingModel     = $shippingModel;
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_QuickBooksOnline::shipping')
            ->addBreadcrumb(__('View Shipping'), __('View Shipping'));
        $resultPage->getConfig()->getTitle()->set(__('View Shipping'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_QuickBooksOnline::shipping');
    }
}
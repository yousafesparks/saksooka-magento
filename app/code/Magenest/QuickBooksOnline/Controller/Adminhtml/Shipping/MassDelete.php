<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping;

use Magenest\QuickBooksOnline\Model\ResourceModel\Shipping\CollectionFactory;
use Magenest\QuickBooksOnline\Model\ShippingFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping
 */
class MassDelete extends AbstractShipping
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * MassDelete constructor.
     *
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param PageFactory $resultPageFactory
     * @param ShippingFactory $shippingModel
     * @param Filter $filter
     * @param CollectionFactory $collection
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        PageFactory $resultPageFactory,
        ShippingFactory $shippingModel,
        Filter $filter,
        CollectionFactory $collection
    ) {
        parent::__construct($context, $scopeConfig, $resultPageFactory, $shippingModel);
        $this->filter     = $filter;
        $this->collection = $collection;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
    }
}
<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping;

/**
 * Class Index
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping
 */
class Index extends AbstractShipping
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend((__('Mapping Carriers')));
        return $resultPage;
    }
}
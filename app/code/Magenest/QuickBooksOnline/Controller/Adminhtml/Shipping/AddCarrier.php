<?php
/**
 * Copyright © 2019 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping;

use Magenest\QuickBooksOnline\Model\ShippingFactory;
use Magenest\QuickBooksOnline\Model\ResourceModel\Shipping;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Shipping\Model\Config\Source\Allmethods;

/**
 * Class AddCarrier
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Shipping
 */
class AddCarrier extends AbstractShipping
{
    /**
     * @var Shipping
     */
    protected $_resource;

    /**
     * @var Allmethods
     */
    protected $allMethods;

    /**
     * AddCarrier constructor.
     *
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param PageFactory $resultPageFactory
     * @param Allmethods $allMethods
     * @param ShippingFactory $shippingModel
     * @param Shipping $shipping
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        PageFactory $resultPageFactory,
        ShippingFactory $shippingModel,
        Allmethods $allMethods,
        Shipping $shipping
    ) {
        parent::__construct($context, $scopeConfig, $resultPageFactory, $shippingModel);
        $this->allMethods = $allMethods;
        $this->_resource  = $shipping;
    }

    public function execute()
    {
        try {
            $methods = $this->allMethods->toOptionArray(true);
            array_shift($methods);
            foreach ($methods as $method) {
                $code  = $method['value'][0]['value'];
                $title = $method['label'];

                $model = $this->shippingModel->create()->loadByCode($code);
                if (!$model->getId()) {
                    $params = [
                        'code' => $code,
                        'name' => $title
                    ];
                    $model->addData($params);
                    $this->_resource->save($model);
                }
            }
            $this->messageManager->addSuccessMessage(__('All shipping methods are added to mapping.'));

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('An error happened when adding shipping methods to mapping: %1', $e->getMessage()));
        }

        return $this->_redirect('*/*/index');
    }
}
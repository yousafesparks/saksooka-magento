<?php
/**
 * @category    CleverSoft
 * @package     CleverBase
 * @copyright   Copyright © 2017 CleverSoft., JSC. All Rights Reserved.
 * @author 		ZooExtension.com
 * @email       magento.cleversoft@gmail.com
 */

namespace CleverSoft\Base\Model\System\Config\Source\Product;

class Pagelayout implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'horizontal_thumb', 'label' => __('Horizontal Thumb')],
            ['value' => 'vertical_thumb', 'label' => __('Vertical Thumb')],
            ['value' => 'center_vertical_thumb', 'label' => __('Center Vertical Thumb')],
            ['value' => 'accordion_tabs', 'label' => __('Accordion Tabs')],
            ['value' => 'carousel_thumb', 'label' => __('Carousel Thumb')],
            ['value' => 'grid_thumb', 'label' => __('Grid Thumb')],
            ['value' => 'sticky_layout_1', 'label' => __('Sticky Layout 1')],
            ['value' => 'sticky_layout_2', 'label' => __('Sticky Layout 2')],
            ['value' => 'sticky_layout_3', 'label' => __('Sticky Layout 3')],
        ];
    }

    public function toArray()
    {
        return [
            'default' => __('Default'),
            'sticky_2' => __('Sticky 2')
        ];
    }
}

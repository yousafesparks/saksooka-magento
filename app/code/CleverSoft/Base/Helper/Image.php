<?php
/**
 * @category    CleverSoft
 * @package     CleverBase
 * @copyright   Copyright © 2017 CleverSoft., JSC. All Rights Reserved.
 * @author      ZooExtension.com
 * @email       magento.cleversoft@gmail.com
 */

namespace CleverSoft\Base\Helper;

class Image extends \Magento\Framework\App\Helper\AbstractHelper{

    protected $_catalogImageHelper;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Helper\Image $imageHelper
    ){
        parent::__construct($context);
        $this->_catalogImageHelper = $imageHelper;
    }

    /**
     * Get theme's main settings (single option)
     *
     * @return string
     */
    public function getCfg($optionString, $scopeCode = null)
    {
        return $this->scopeConfig->getValue('cleversoftbase/' . $optionString, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    public function getImg($product, $w, $h, $imgVersion='image', $file=NULL)
    {
        if ($h <= 0)
        {

            return $url = $this->_catalogImageHelper
                ->init($product, $imgVersion)
                ->setImageFile($file)
                ->constrainOnly(false)
                ->keepAspectRatio(true)
                ->keepFrame(false)
                ->resize($w)
                ->getUrl();
        }
        else
        {
            return $url = $this->_catalogImageHelper
                ->init($product, $imgVersion)
                ->setImageFile($file)
                ->resize($w, $h)
                ->getUrl();
        }
    }

    public function getImageScrset($product, $width, $height,$imgVersion='image', $file=NULL) {
        $original_url = $this->getImageScrsetByType($product, $width, $height, $imgVersion, $file,'');
        $thumbnail_url = $this->getImageScrsetByType($product, $width, $height, $imgVersion, $file,'thumbnail');
        $small_url = $this->getImageScrsetByType($product, $width, $height, $imgVersion, $file,'small');
        $medium_url = $this->getImageScrsetByType($product, $width, $height, $imgVersion, $file,'medium');
        $large_url = $this->getImageScrsetByType($product, $width, $height, $imgVersion, $file,'large');

        return $original_url .',' . $thumbnail_url . ',' . $small_url . ',' . $medium_url . ',' . $large_url;
    }

    public function getImageScrsetByType($product, $width, $height,$imgVersion, $file=NULL,$type = '') {
        switch ($type) {
            case 'thumbnail':
                $max_width = intval($this->getCfg('category/thumbnail_width'));
                $max_height = intval($this->getCfg('category/thumbnail_height'));
                break;
            case 'small':
                $max_width = intval($this->getCfg('category/small_width'));
                $max_height = intval($this->getCfg('category/small_height'));
                break;
            case 'medium':
                $max_width = intval($this->getCfg('category/medium_width'));
                $max_height = intval($this->getCfg('category/medium_height'));
                break;
            case 'large':
                $max_width = intval($this->getCfg('category/large_width'));
                $max_height = intval($this->getCfg('category/large_height'));
                break;
            default:
                $max_width = $width;
                $max_height = $height;
        }

        if ($max_height <= 0) {
            return $url = $this->_catalogImageHelper
                ->init($product, $imgVersion)
                ->setImageFile($file)
                ->constrainOnly(false)
                ->keepAspectRatio(true)
                ->keepFrame(false)
                ->resize($max_width)
                ->getUrl().' '.$max_width.'w';
        }
        if ($height <= 0) {
            $height = $max_height;
        }

        if($height > $width) {
            $srcset_height = $max_height;
            $srcset_width = floor($max_height*$width/$height);
        } else {
            $srcset_width = $max_width;
            $srcset_height = floor($max_width*$height/$width);
        }
        return $url = $this->_catalogImageHelper
                ->init($product, $imgVersion)
                ->setImageFile($file)
                ->resize($srcset_width, $srcset_height)
                ->getUrl().' '.$srcset_width.'w';
    }
}

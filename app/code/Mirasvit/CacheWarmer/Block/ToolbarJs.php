<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-cache-warmer
 * @version   1.3.25
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\CacheWarmer\Block;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\App\PageCache\Identifier as CacheIdentifier;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\View\Element\Template;
use Mirasvit\CacheWarmer\Service\PageService;
use Mirasvit\CacheWarmer\Api\Repository\PageRepositoryInterface;
use Mirasvit\CacheWarmer\Service\Config\DebugConfig;

class ToolbarJs extends Template
{
    const COOKIE = 'mst-cache-warmer-toolbar';

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;


    /**
     * @var CacheIdentifier
     */
    private $cacheIdentifier;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;


    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;

    /**
     * @var DebugConfig
     */
    private $debugConfig;
    /**
     * @var PageService
     */
    private $pageService;

    /**
     * ToolbarJs constructor.
     * @param CookieManagerInterface $cookieManager
     * @param CacheIdentifier $cacheIdentifier
     * @param PageRepositoryInterface $pageRepository
     * @param PageService $pageService
     * @param DebugConfig $debugConfig
     * @param Context $context
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        CacheIdentifier $cacheIdentifier,
        PageRepositoryInterface $pageRepository,
        PageService $pageService,
        DebugConfig $debugConfig,
        Context $context
    ) {
        $this->cookieManager   = $cookieManager;
        $this->cacheIdentifier = $cacheIdentifier;
        $this->pageRepository  = $pageRepository;
        $this->pageService  = $pageService;
        $this->debugConfig = $debugConfig;

        $this->urlBuilder = $context->getUrlBuilder();

        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getMageInit()
    {
        return [
            'Mirasvit_CacheWarmer/js/toolbar' => [
                'cookieName'  => self::COOKIE,
                'cookieValue' => $this->cookieManager->getCookie(self::COOKIE),
                'pageId'      => $this->getPageId(),
                'toolbarUrl'  => $this->urlBuilder->getUrl('cache_warmer/toolbar'),
            ],
        ];
    }

    /**
     * @return string|false
     */
    public function getPageId()
    {
        $cacheId = $this->cacheIdentifier->getValue();
        $varyDataString = $this->pageService->getVaryDataString($this->getRequest());
        $varyDataHash = $this->pageService->getVaryDataHash($this->getRequest());
        $page = $this->pageRepository->getByCacheId(
            $cacheId,
            $varyDataHash
        );
        return $page ? $page->getId() : $cacheId." <br>".$varyDataHash. " ".$varyDataString;
    }

    /**
     * {@inheritdoc}
     */
    public function toHtml()
    {
        if (!$this->debugConfig->isInfoBlockEnabled() || !$this->debugConfig->isDebugAllowed()) {
            return '';
        }

        return parent::toHtml();
    }
}

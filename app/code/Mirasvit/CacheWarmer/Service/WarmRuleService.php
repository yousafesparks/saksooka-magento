<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-cache-warmer
 * @version   1.3.25
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\CacheWarmer\Service;

use Mirasvit\CacheWarmer\Api\Data\PageInterface;
use Mirasvit\CacheWarmer\Api\Data\WarmRuleInterface;
use Mirasvit\CacheWarmer\Api\Repository\PageRepositoryInterface;
use Mirasvit\CacheWarmer\Api\Repository\WarmRuleRepositoryInterface;
use Mirasvit\CacheWarmer\Helper\Serializer;

class WarmRuleService
{
    /**
     * @var WarmRuleRepositoryInterface
     */
    private $ruleRepository;

    /**
     * @var Serializer
     */
    private $serializer;
    /**
     * @var \Mirasvit\Core\Service\CompatibilityService
     */
    private $compatibilityService;
    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * WarmRuleService constructor.
     * @param PageRepositoryInterface $pageRepository
     * @param WarmRuleRepositoryInterface $ruleRepository
     * @param \Mirasvit\Core\Service\CompatibilityService $compatibilityService
     * @param Serializer $serializer
     */
    public function __construct(
        PageRepositoryInterface $pageRepository,
        WarmRuleRepositoryInterface $ruleRepository,
        \Mirasvit\Core\Service\CompatibilityService $compatibilityService,
        Serializer $serializer
    ) {
        $this->ruleRepository       = $ruleRepository;
        $this->pageRepository       = $pageRepository;
        $this->compatibilityService = $compatibilityService;
        $this->serializer = $serializer;
    }

    /**
     * @param PageInterface     $page
     * @param WarmRuleInterface $rule
     * @return PageInterface
     */
    public function modifyPage(PageInterface $page, WarmRuleInterface $rule = null)
    {
        if (!$rule) {
            return $page;
        }

        if (!$rule->getHeaders()) {
            return $page;
        }
        $p = clone $page;
        if ($rule->getHeaders()) {
            $p->setHeaders($rule->getHeaders());
        }
        return $p;
    }

    /**
     * @return void
     */
    public function refreshPagesByRules()
    {
        $jobRuleCollection = $this->ruleRepository->getCollection();
        $versions          = [];
        foreach ($jobRuleCollection as $rule) {
            $versions[] = $rule->getConditionsSerialized();
        }
        $version = sha1(implode("|", $versions));

        $pageCollection = $this->pageRepository->getCollection();
        $pageCollection->getSelect()
            ->where(PageInterface::WARM_RULE_VERSION .
                " != ? OR ISNULL(" . PageInterface::WARM_RULE_VERSION . ")", $version);

        while ($page = $pageCollection->fetchItem()) {
            /** @var PageInterface $page */
            $ruleIds = [];

            foreach ($jobRuleCollection as $jobRule) {
                if ($jobRule->getRule()->validate($page)) {
                    $ruleIds[] = $jobRule->getId();
                }
            }

            $page->setWarmRuleIds($ruleIds);
            $page->setWarmRuleVersion($version);
            $this->pageRepository->save($page);
        }
    }
}

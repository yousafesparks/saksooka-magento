<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-cache-warmer
 * @version   1.3.25
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\CacheWarmer\Service;

use Magento\Framework\Stdlib\DateTime;
use Mirasvit\CacheWarmer\Api\Data\JobInterface;
use Mirasvit\CacheWarmer\Api\Data\PageInterface;
use Mirasvit\CacheWarmer\Api\Data\WarmRuleInterface;
use Mirasvit\CacheWarmer\Api\Repository\JobRepositoryInterface;
use Mirasvit\CacheWarmer\Api\Repository\PageRepositoryInterface;
use Mirasvit\CacheWarmer\Api\Repository\WarmRuleRepositoryInterface;
use Mirasvit\CacheWarmer\Api\Service\JobServiceInterface;
use Mirasvit\CacheWarmer\Api\Service\WarmerServiceInterface;
use Mirasvit\CacheWarmer\Api\Service\MigrateServiceInterface;
use Mirasvit\CacheWarmer\Logger\Logger;
use Mirasvit\CacheWarmer\Model\Config;
use Mirasvit\CacheWarmer\Service\Rate\CacheFillRateService;
use Mirasvit\CacheWarmer\Service\Rate\ServerLoadRateService;
use Mirasvit\CacheWarmer\Service\Warmer\PageWarmStatus;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class JobService implements JobServiceInterface
{
    const MAX_ERRORS_TO_STOP   = 30;
    const MAX_ATTEMPTS = 3;

    /**
     * @var JobRepositoryInterface
     */
    private $jobRepository;

    /**
     * @var CacheFillRateService
     */
    private $cacheFillRateService;

    /**
     * @var ServerLoadRateService
     */
    private $serverLoadRateService;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * @var WarmerServiceInterface
     */
    private $warmerService;

    /**
     * @var WarmRuleService
     */
    private $warmRuleService;

    /**
     * @var WarmRuleRepositoryInterface
     */
    private $ruleRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFactory
     */
    private $dateFactory;

    /**
     * @var MigrateServiceInterface
     */
    private $migrateService;
    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * JobService constructor.
     * @param JobRepositoryInterface $jobRepository
     * @param PageRepositoryInterface $pageRepository
     * @param WarmerServiceInterface $warmerService
     * @param CacheFillRateService $cacheFillRateService
     * @param ServerLoadRateService $serverLoadRateService
     * @param WarmRuleService $warmRuleService
     * @param StatusService $statusService
     * @param MigrateServiceInterface $migrateService
     * @param WarmRuleRepositoryInterface $ruleRepository
     * @param Config $config
     * @param Logger $logger
     * @param DateTime\DateTimeFactory $dateFactory
     */
    public function __construct(
        JobRepositoryInterface $jobRepository,
        PageRepositoryInterface $pageRepository,
        WarmerServiceInterface $warmerService,
        CacheFillRateService $cacheFillRateService,
        ServerLoadRateService $serverLoadRateService,
        WarmRuleService $warmRuleService,
        StatusService $statusService,
        MigrateServiceInterface $migrateService,
        WarmRuleRepositoryInterface $ruleRepository,
        Config $config,
        Logger $logger,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    ) {
        $this->jobRepository  = $jobRepository;
        $this->pageRepository = $pageRepository;
        $this->ruleRepository = $ruleRepository;

        $this->warmerService         = $warmerService;
        $this->warmRuleService       = $warmRuleService;
        $this->statusService       = $statusService;
        $this->cacheFillRateService  = $cacheFillRateService;
        $this->serverLoadRateService = $serverLoadRateService;
        $this->migrateService = $migrateService;


        $this->config = $config;
        $this->logger = $logger;
        $this->dateFactory = $dateFactory;
    }


    /**
     * {@inheritdoc}
     */
    public function run(JobInterface $job)
    {

        $ts = microtime(true);

        $this->startJob($job);

        if (!$this->canRunJob()) {
            return $this->finishJob($job, JobInterface::STATUS_MISSED);
        }

        $this->migrateService->migrateData();

        $this->warmRuleService->refreshPagesByRules();
        $this->statusService->runPartialStatusUpdate();

        $jobRuleCollection = $this->ruleRepository->getCollection();
        $jobRuleCollection->addFieldToFilter(WarmRuleInterface::IS_ACTIVE, 1);
        $jobRuleCollection->setOrder(WarmRuleInterface::PRIORITY, "DESC");

        foreach ($jobRuleCollection as $rule) {
            $pages = $this->getPageCollection($rule);

            $errorCounter = 0;
            $this->logger->info("Using warm rule: #".$rule->getId());
            foreach ($this->warmerService->warmCollection($pages, $rule) as $status) {
                $this->logWarmStatus($job, $status);
                $this->handlePageStatus($status);

                if ($status->isError() && !$status->isSoftError()) {
                    $errorCounter++;

                    if ($errorCounter >= self::MAX_ERRORS_TO_STOP) {
                        $this->logError($job, 'Stopped execution. Reached errors limit.', [$errorCounter]);

                        return $this->finishJob($job, JobInterface::STATUS_ERROR);
                    }
                }


                if ($this->isTimeout($ts)) {
                    break;
                }
            }
        }

        $this->logger->info('Execution Time', [round(microtime(true) - $ts, 1)]);

        $this->finishJob($job);

        return $this;
    }

    /**
     * @param JobInterface $job
     * @return $this
     */
    private function startJob($job)
    {
        $this->logger->setJob($job);

        set_error_handler([$this, 'errorHandler']);

        $this->logger->info('Start job');

        $job->setStartedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT))
            ->setStatus(JobInterface::STATUS_RUNNING);

        $this->jobRepository->save($job);
        $this->logCacheFillRate($job)
            ->logServerLoadRate($job);

        return $this;
    }

    /**
     * @param JobInterface $job
     * @return $this
     */
    private function logServerLoadRate($job)
    {
        $message = 'Server Load Rate';
        $rate    = $this->serverLoadRateService->getRate();

        $this->logger->info($message, [$rate]);

        $info             = $job->getInfo();
        $info[$message][] = $rate . '%';
        $job->setInfo($info);

        $this->jobRepository->save($job);

        return $this;
    }

    /**
     * @param JobInterface $job
     * @return $this
     */
    private function logCacheFillRate($job)
    {
        $message = 'Cache Fill Rate';
        $rate    = $this->cacheFillRateService->getRate();

        $this->logger->info($message, [$rate]);

        $info             = $job->getInfo();
        $info[$message][] = $rate . '%';
        $job->setInfo($info);

        $this->jobRepository->save($job);

        return $this;
    }

    /**
     * @return bool
     */
    private function canRunJob()
    {
        if (!$this->config->isPageCacheEnabled()) {
            $this->logger->warning('Page Cache is disabled');

            return false;
        }

        $serverLoadRate      = $this->serverLoadRateService->getRate();
        $serverLoadThreshold = $this->config->getServerLoadThreshold();

        if ($serverLoadRate > $serverLoadThreshold) {
            $this->logger->warning('Server load threshold reached', [
                'rate'      => $serverLoadRate,
                'threshold' => $serverLoadThreshold,
            ]);

            return false;
        }

        $cacheFillRate      = $this->cacheFillRateService->getRate();
        $cacheFillThreshold = $this->config->getCacheFillThreshold();

        if ($cacheFillRate > $cacheFillThreshold) {
            $this->logger->warning('Cache fill threshold reached', [
                'rate'      => $cacheFillRate,
                'threshold' => $cacheFillThreshold,
            ]);

            return false;
        }

        return true;
    }

    /**
     * @param JobInterface $job
     * @param string       $status
     * @return $this
     */
    private function finishJob($job, $status = null)
    {
        $job->setFinishedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));

        if (!$status) {
            $status = JobInterface::STATUS_COMPLETED;
        }

        $job->setStatus($status);

        $this->jobRepository->save($job);

        $this->logCacheFillRate($job)
            ->logServerLoadRate($job);

        $this->logger->info('Finish job');

        restore_error_handler();

        return $this;
    }


    /**
     * @param JobInterface   $job
     * @param PageWarmStatus $status
     * @return $this
     */
    private function logWarmStatus($job, $status)
    {
        $message = 'Warmed Pages';
        $this->logger->info($status->toString());

        $info           = $job->getInfo();
        $info[$message] = isset($info[$message]) ? $info[$message] + 1 : 1;
        $job->setInfo($info);

        return $this;
    }

    /**
     * @param JobInterface $job
     * @param string       $message
     * @param array|null   $context
     * @return $this
     */
    private function logError($job, $message, array $context = null)
    {
        $this->logger->error($message, $context);

        $info          = $job->getInfo();
        $info['Error'] = $message;
        $job->setInfo($info);

        $this->jobRepository->save($job);

        return $this;
    }

    /**
     * @param PageWarmStatus $status
     * @return $this
     */
    private function handlePageStatus(PageWarmStatus $status)
    {
        $page = $status->getPage();
        if ($status->isError()) {
            if ($status->isSoftError()) {
                $this->logger->warning('Remove page. Response code: ', ['code'=>$status->getCode(), 'url'=>$page->getUri()]);
                // Removing page directly from the database
                $this->pageRepository->deletePage($page);
                return $this;
            }
            if ($page->getAttempts() >= self::MAX_ATTEMPTS) {
                $this->logger->warning('Remove page (2).', ['code'=>$status->getCode(), 'url'=>$page->getUri()]);
                // Removing page directly from the database
                $this->pageRepository->deletePage($page);
            } else {
                $page->setAttempts($page->getAttempts() + 1);
                $this->pageRepository->save($page);
            }
        } elseif ($page->getAttempts() > 0) {
            $page->setAttempts(0);
            $this->pageRepository->save($page);
        }
        return $this;
    }

    /**
     * @param int $startTime
     * @return bool
     */
    private function isTimeout($startTime)
    {
        return (microtime(true) - $startTime) > $this->config->getJobRunThreshold();
    }

    /**
     * @param string $type
     * @param string $msg
     * @param string $file
     * @param string $line
     * @return void
     */
    public function errorHandler($type, $msg, $file, $line)
    {
        $message = $msg . " in " . $file . ":" . $line;
        $this->logger->err($message);

        $job = $this->logger->getJob();
        $this->finishJob($job, JobInterface::STATUS_ERROR);
    }

    /**
     * @param WarmRuleInterface $rule
     * @return PageInterface[]|\Mirasvit\CacheWarmer\Model\ResourceModel\Page\Collection
     */
    private function getPageCollection(WarmRuleInterface $rule)
    {
        $collection = $this->pageRepository->getCollection();
        $collection->getSelect()->where("FIND_IN_SET(?,warm_rule_ids)", $rule->getId());
        $collection->getSelect()
            ->where("status = '' OR status = '".PageInterface::STATUS_PENDING."' 
            OR (status = '".PageInterface::STATUS_UNCACHEABLE."' AND date_add(updated_at, INTERVAL 30 MINUTE) < NOW())");
        $collection->setOrder(PageInterface::POPULARITY, 'desc');
        return $collection;
    }
}

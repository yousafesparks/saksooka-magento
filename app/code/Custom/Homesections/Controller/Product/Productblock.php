<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Custom\Homesections\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;

/**
 * Webkul Marketplace Productlist controller.
 */
class Productblock  extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helperData;

    /**
     * @var NotificationHelper
     */
    protected $notificationHelper;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
	
	 protected $_layout;
	
	
    /**
     * @param Context                         $context
     * @param PageFactory                     $resultPageFactory
     * @param \Webkul\Marketplace\Helper\Data $helperData
     * @param NotificationHelper              $notificationHelper
     * @param CollectionFactory               $collectionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		\Magento\Framework\View\LayoutInterface $layout,
		\MGS\Mpanel\Helper\Data $themeHelper,
		\Magento\Catalog\Helper\Image $imagehelper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
		$this->_layout = $layout;
		  $this->themeHelper = $themeHelper;
		$this->_imagehelper = $imagehelper;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
		
		$this->_view->loadLayout();
		$html =  $this->_view->getLayout()
          ->createBlock('MGS\Mpanel\Block\Products\Attributes')
          
		  ->setMgsPanelTitle('الأكثر مبيعاً')
		   ->setMgsPanelNote('الموديلات الاكثر مبيعاً هذا الاسبوع')
		   ->setLimit(20)
		   ->setRatio(4)
			->setAttributeCode('trending')
			->setUseSlider('1')
		//	->setLoadmore(0)
			->setPerRow(4)
			->setAutoplay(1)
			->setAdditionalData("")
		//	->setStopAuto(1)
		//	->setNavigation(0)
		//	->setPagination(0)
		//	->setNumberRow(1)
			->setThemeHelper($this->themeHelper)
			->setImageHelper($this->_imagehelper)
			->setTemplate('Magento_Theme::html/grid_items.phtml')
          ->toHtml();
		$this->getResponse()->setHeader('Content-type', 'text/plain', true);
		$this->getResponse()->setBody($html);
	}
	public function getThemeHelper(){
		return $this->themeHelper;
	}
}

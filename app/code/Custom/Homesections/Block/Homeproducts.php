<?php


namespace Custom\Homesections\Block;

class Homeproducts extends \Magento\Framework\View\Element\Template
{
    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
	protected $_objectManager;
	protected $storeManagerInterface;
	protected $categoryRepository;
	protected $_productCollectionFactory;
	protected $_categoryFactory;
	protected $_imagehelper;
	
  public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
	  	\Magento\Framework\ObjectManagerInterface $objectManager,
	     \MGS\Mpanel\Helper\Data $themeHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeManagerInterface = $storeManagerInterface;
	    $this->_objectManager = $objectManager;
	  $this->themeHelper = $themeHelper;
    }
	public function getThemeHelper(){
		return $this->themeHelper;
	}

	
}

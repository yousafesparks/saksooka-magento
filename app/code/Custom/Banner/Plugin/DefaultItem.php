<?php

namespace Custom\Banner\Plugin;

use Magento\Quote\Model\Quote\Item;

class DefaultItem
{
    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $data = $proceed($item);
        $product = $item->getProduct();

        $atts = [
            "short_description" => strip_tags($product->getShortDescription())
        ];

        return array_merge($data, $atts);
    }
}
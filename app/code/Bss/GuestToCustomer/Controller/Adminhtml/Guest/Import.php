<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_GuestToCustomer
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\GuestToCustomer\Controller\Adminhtml\Guest;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Serialize\SerializerInterface;

class Import extends Action
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
//    const ADMIN_RESOURCE = '';

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @var \Bss\GuestToCustomer\Model\GuestFactory
     */
    protected $guestFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Import constructor.
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory
     * @param \Bss\GuestToCustomer\Model\GuestFactory $guestFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Bss\GuestToCustomer\Model\GuestFactory $guestFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->guestFactory = $guestFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $result = [
            'success' => false,
            'errorMessage' => '',
        ];
        $model = $this->guestFactory->create();
        $emails_exist = $this->getCustomerEmails();
        $this->getGuestExist($emails_exist, $model);
        $guests = $this->getGuestNotExist($emails_exist);

        try {
            if (!empty($guests)) {
                $model->importGuest($guests);
            }
            $result['success'] = true;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result['errorMessage'] = $e->getMessage();
        } catch (\Exception $e) {
            $message = __($e->getMessage());
            $result['errorMessage'] = $message;
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($result);
    }

    /**
     * @return array
     */
    private function getCustomerEmails()
    {
        $customers = $this->customerCollectionFactory->create();
        $emails = [];
        if ($customers->getSize() > 0) {
            foreach ($customers as $customer) {
                $emails[] = $customer->getEmail();
            }
        }
        return $emails;
    }

    /**
     * @param $emails
     * @param $model
     */
    private function getGuestExist(&$emails, $model)
    {
        $guests = $model->getCollection();
        if ($guests->getSize() > 0) {
            foreach ($guests as $guest) {
                $emails[] = $guest->getEmail();
            }
        }
    }

    /**
     * @param array $emails
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getGuestNotExist($emails = [])
    {
        $from = $this->getRequest()->getParam('from', false);
        $to = $this->getRequest()->getParam('to')? : date('Y-m-d');

        $orders = $this->orderCollectionFactory->create();
        $orders->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            ['null' => true]
        )->addFieldToFilter(
            'customer_email',
            ['nin' => $emails]
        )->addFieldToFilter(
            'created_at',
            ['from' => $from, 'to' => $to]
        )->setOrder(
            'created_at',
            'desc'
        );

        $guests = [];
        if ($orders->getSize() > 0) {
            foreach ($orders as $order) {
                $shippingAddress = $order->getShippingAddress();
                $billingAddress = $order->getBillingAddress();
                $arrShippingAddress = $shippingAddress ? $shippingAddress->getData() : [];
                $arrBillingAddress = $billingAddress->getData();
                $emailGuest =  $order->getData('customer_email');
                $storeId =  $order->getData('store_id');
                $store = $this->storeManager->getStore($storeId);
                $guest_info = [
                    'email' =>  $emailGuest,
                    'first_name' => $arrBillingAddress['firstname'],
                    'last_name' => $arrBillingAddress['lastname'],
                    'website_id' => $store->getWebsite()->getWebsiteId(),
                    'store_id' => $storeId,
                    'shipping_address' => $this->serializer->serialize($arrShippingAddress),
                    'billing_address' => $this->serializer->serialize($arrBillingAddress)
                ];
                $guests[] = $guest_info;
            }
        }
        return $guests;
    }
}

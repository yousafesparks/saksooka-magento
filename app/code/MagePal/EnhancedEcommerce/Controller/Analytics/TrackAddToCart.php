<?php
/**
 * Copyright © MagePal LLC. All rights reserved.
 * See COPYING.txt for license details.
 * https://www.magepal.com | support@magepal.com
 */

namespace MagePal\EnhancedEcommerce\Controller\Analytics;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json as Json;
use Magento\Framework\Controller\Result\JsonFactory;
use MagePal\EnhancedEcommerce\Model\Session as EnhancedEcommerceSession;

class TrackAddToCart extends Action
{
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;
    /**
     * @var EnhancedEcommerceSession
     */
    private $enhancedEcommerceSession;

    /**
     * TrackAddToCart constructor.
     * @param  Context  $context
     * @param  JsonFactory  $jsonResultFactory
     * @param  EnhancedEcommerceSession  $enhancedEcommerceSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory,
        EnhancedEcommerceSession $enhancedEcommerceSession
    ) {
        parent::__construct($context);
        $this->jsonResultFactory = $jsonResultFactory;
        $this->enhancedEcommerceSession = $enhancedEcommerceSession;
    }

    /**
     * @return Json
     */
    public function execute()
    {
        $itemAddToCart = $this->enhancedEcommerceSession->getItemAddToCart(true);

        $hasData = (bool) !empty($itemAddToCart);

        $data = [
            'success' => $hasData,
            'data' => $hasData ? $itemAddToCart : []
        ];

        $result = $this->jsonResultFactory->create();
        $result->setHeader('Cache-Control', 'max-age=0, must-revalidate, no-cache, no-store', true);
        $result->setHeader('Pragma', 'no-cache', true);
        return $result->setData($data);
    }
}

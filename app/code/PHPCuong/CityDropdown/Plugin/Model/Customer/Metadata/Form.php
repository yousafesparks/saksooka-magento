<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Model\Customer\Metadata;

class Form
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $_cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(\PHPCuong\CityDropdown\Model\CityFactory $cityFactory)
    {
        $this->_cityFactory = $cityFactory;
    }

    /**
     * @param \Magento\Customer\Model\Metadata\Form $metadataForm
     * @return array $data
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExtractData(
        \Magento\Customer\Model\Metadata\Form $metadataForm,
        $data
    ) {
        if (isset($data['city_id'])) {
            $cityId = (int)$data['city_id'];
            if ($cityId) {
                $cityModel = $this->_cityFactory->create();
                $cityModel->load($cityId);
                if ($cityModel->getId()) {
                    $data['city'] = $cityModel->getName();
                }
            }
        }
        return $data;
    }
}

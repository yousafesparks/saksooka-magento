<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Model\Directory\ResourceModel\Region;

class Collection
{
    /**
     * Set allowed countries filter based on the given store.
     * This is a convenience method for collection filtering based on store configuration settings.
     *
     * @param \Magento\Directory\Model\ResourceModel\Region\Collection $subject
     * @param callable $proceed
     * @param null|int|string|\Magento\Store\Model\Store $store
     * @return \Magento\Directory\Model\ResourceModel\Region\Collection
     */
    public function aroundAddAllowedCountriesFilter(
        \Magento\Directory\Model\ResourceModel\Region\Collection $subject,
        callable $proceed,
        $store = null
    ) {
        $subject->addFieldToFilter('main_table.visibility', \PHPCuong\CityDropdown\Model\Visibility::VISIBILITY_VISIBLE);
        return $proceed($store);
    }
}

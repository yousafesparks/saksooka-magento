<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_GiaPhuFashion
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Model\SalesRule\Rule\Condition;

class Address
{
    /**
     * @param \Magento\SalesRule\Model\Rule\Condition\Address $subject
     * @return \Magento\SalesRule\Model\Rule\Condition\Address $result
     *
     * @return $this
     */
    public function aroundSetData(
        \Magento\SalesRule\Model\Rule\Condition\Address $subject,
        callable $proceed,
        $key,
        $value = null
    ) {
        if ($key == 'attribute_option') {
            $attributes = $value;
            foreach ($value as $attributeId => $attributeValue) {
                if ($attributeId == 'postcode') {
                    $value['city_id'] = __('Shipping City');
                }
            }
        }

        return $proceed($key, $value);
    }

    /**
     * @param \Magento\SalesRule\Model\Rule\Condition\Address $subject
     * @param string $result
     *
     * @return string
     */
    public function afterGetInputType(\Magento\SalesRule\Model\Rule\Condition\Address $subject, $result)
    {
        switch ($subject->getAttribute()) {
            case 'city_id':
                return 'select';
        }
        return $result;
    }

    /**
     * @param \Magento\SalesRule\Model\Rule\Condition\Address $subject
     * @param string $result
     *
     * @return string
     */
    public function afterGetValueElementType(\Magento\SalesRule\Model\Rule\Condition\Address $subject, $result)
    {
        switch ($subject->getAttribute()) {
            case 'city_id':
                return 'select';
        }
        return $result;
    }

    /**
     * @param \Magento\SalesRule\Model\Rule\Condition\Address $subject
     * @param array|mixed $result
     *
     * @return array|mixed
     */
    public function afterGetValueSelectOptions(\Magento\SalesRule\Model\Rule\Condition\Address $subject, $result)
    {
        switch ($subject->getAttribute()) {
            case 'city_id':
                $options = $this->getAllCity()->toOptionArray();
                $subject->setData('value_select_options', $options);
                return $subject->getData('value_select_options');
                break;

            default:
                $options = [];
        }
        return $result;
    }

    /**
     * @return \PHPCuong\CityDropdown\Model\Config\Source\AllCity
     */
    protected function getAllCity()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get(\PHPCuong\CityDropdown\Model\Config\Source\AllCity::class);
    }
}

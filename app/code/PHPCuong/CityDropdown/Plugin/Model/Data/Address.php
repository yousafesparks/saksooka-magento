<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Model\Data;

class Address
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $_cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(\PHPCuong\CityDropdown\Model\CityFactory $cityFactory)
    {
        $this->_cityFactory = $cityFactory;
    }

    /**
     * @param \Magento\Customer\Model\Data\Address $address
     * @param string $result
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetCity(\Magento\Customer\Model\Data\Address $address, $result)
    {
        $cityIdAttribute = $address->getCustomAttribute('city_id');
        if ($cityIdAttribute) {
            $cityId = $cityIdAttribute->getValue();
            if (is_numeric($cityId)) {
                $cityModel = $this->_cityFactory->create();
                $cityModel->load($cityId);
                if ($cityModel->getId() && $address->getRegionId() == $cityModel->getRegionId()) {
                    return $cityModel->getName();
                }
            }
        }
        return $result;
    }
}

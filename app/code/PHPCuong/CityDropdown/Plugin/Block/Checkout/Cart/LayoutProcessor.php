<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Block\Checkout\Cart;

class LayoutProcessor
{
    /**
     * @var \PHPCuong\CityDropdown\Helper\Data
     */
    protected $cityDropdownHelper;

    /**
     * @var \Magento\Checkout\Block\Checkout\AttributeMerger
     */
    protected $merger;

    /**
     * @param \PHPCuong\CityDropdown\Helper\Data $cityDropdownHelper
     * @param \Magento\Checkout\Block\Checkout\AttributeMerger $merger
     */
    public function __construct(
        \PHPCuong\CityDropdown\Helper\Data $cityDropdownHelper,
        \Magento\Checkout\Block\Checkout\AttributeMerger $merger
    ) {
        $this->cityDropdownHelper = $cityDropdownHelper;
        $this->merger = $merger;
    }

    /**
     * Process js Layout of block
     *
     * @param \Magento\Checkout\Block\Cart\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Cart\LayoutProcessor $subject,
        $jsLayout
    ) {
        if (isset($jsLayout['components']['checkoutProvider']['dictionaries'])) {
            $regionIds = [];
            if (!empty($jsLayout['components']['checkoutProvider']['dictionaries']['region_id'])) {
                foreach ($jsLayout['components']['checkoutProvider']['dictionaries']['region_id'] as $region) {
                    $regionIds[] = $region['value'];
                }
            }
            $jsLayout['components']['checkoutProvider']['dictionaries']['city_id'] = $this->cityDropdownHelper->getCityOptionArray($regionIds);
        }

        if (isset($jsLayout['components']['block-summary']['children']['block-shipping']['children']
            ['address-fieldsets']['children'])
        ) {
            $city = $jsLayout['components']['block-summary']['children']['block-shipping']['children']
            ['address-fieldsets']['children']['city'];

            $city['visible'] = false;
            $jsLayout['components']['block-summary']['children']['block-shipping']['children']
            ['address-fieldsets']['children']['city'] = $city;

            $jsLayout['components']['block-summary']['children']['block-shipping']['children']
            ['address-fieldsets']['children']['city_id'] = [
                'component' => 'PHPCuong_CityDropdown/js/form/element/city',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'customEntry' => 'shippingAddress.city',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/select',
                    'id' => 'city_id',
                    'skipValidation' => true
                ],
                'dataScope' => 'shippingAddress.extension_attributes.city_id',
                'label' => __('City'),
                'provider' => 'checkoutProvider',
                'validation' => [
                   'required-entry' => false
                ],
                'sortOrder' => 113,
                'options' => [],
                'visible' => true,
                'filterBy' => [
                    'target' => '${ $.provider }:shippingAddress.region_id',
                    'field' => 'region_id',
                ],
                'imports' => [
                    'initialOptions' => 'index = checkoutProvider:dictionaries.city_id',
                    'setOptions' => 'index = checkoutProvider:dictionaries.city_id'
                ]
            ];
        }

        return $jsLayout;
    }
}

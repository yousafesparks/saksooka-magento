<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Block\Checkout;

class DirectoryDataProcessor
{
    /**
     * @var \PHPCuong\CityDropdown\Helper\Data
     */
    protected $cityDropdownHelper;

    /**
     * @param \PHPCuong\CityDropdown\Helper\Data $cityDropdownHelper
     */
    public function __construct(
        \PHPCuong\CityDropdown\Helper\Data $cityDropdownHelper
    ) {
        $this->cityDropdownHelper = $cityDropdownHelper;
    }

    /**
     * Process js Layout of block
     *
     * @param \Magento\Checkout\Block\Checkout\DirectoryDataProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\DirectoryDataProcessor $subject,
        $jsLayout
    ) {
        if (isset($jsLayout['components']['checkoutProvider']['dictionaries'])) {
            $regionIds = [];
            if (!empty($jsLayout['components']['checkoutProvider']['dictionaries']['region_id'])) {
                foreach ($jsLayout['components']['checkoutProvider']['dictionaries']['region_id'] as $region) {
                    $regionIds[] = $region['value'];
                }
            }
            $jsLayout['components']['checkoutProvider']['dictionaries']['city_id'] = $this->cityDropdownHelper->getCityOptionArray($regionIds);
        }

        return $jsLayout;
    }
}

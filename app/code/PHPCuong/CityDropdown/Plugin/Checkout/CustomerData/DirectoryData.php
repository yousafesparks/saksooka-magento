<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Plugin\Checkout\CustomerData;

class DirectoryData
{
    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $directoryHelper;

    /**
     * @var \PHPCuong\CityDropdown\Helper\Data
     */
    protected $cityDropdownHelper;

    /**
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param \PHPCuong\CityDropdown\Helper\Data $cityDropdownHelper
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Directory\Helper\Data $directoryHelper,
        \PHPCuong\CityDropdown\Helper\Data $cityDropdownHelper
    ) {
        $this->directoryHelper = $directoryHelper;
        $this->cityDropdownHelper = $cityDropdownHelper;
    }

    /**
     * @param \Magento\Checkout\CustomerData\DirectoryData $subject
     * @param array $output
     * @return array
     */
    public function afterGetSectionData(\Magento\Checkout\CustomerData\DirectoryData $subject, $output)
    {
        $citiesData = $this->cityDropdownHelper->getCityData();
        $regionsData = $this->directoryHelper->getRegionData();

        foreach ($this->directoryHelper->getCountryCollection() as $code => $data) {
            if (array_key_exists($code, $regionsData)) {
                foreach ($regionsData[$code] as $key => $region) {
                    if (array_key_exists($key, $citiesData)) {
                        foreach ($citiesData as $cities) {
                            foreach ($cities as $cityId => $city) {
                                if ($city['country_id'] == $code) {
                                    $output[$code]['cities'][$cityId] = $city['name'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $output;
    }
}

<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Block\Adminhtml\Region\City;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var int
     */
    protected $_defaultLimit = 100;

    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $cityFactory;

    /**
     * @var \PHPCuong\CityDropdown\Model\Visibility
     */
    protected $cityDropdownVisibility;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     * @param \PHPCuong\CityDropdown\Model\Visibility $visibility
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory,
        \PHPCuong\CityDropdown\Model\Visibility $visibility,
        array $data = []
    ) {
        $this->cityFactory = $cityFactory;
        $this->cityDropdownVisibility = $visibility;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('phpcuong_citydropdown_cities');
        $this->setDefaultSort('city_id');
        $this->setUseAjax(true);
        $this->setEmptyText(__('There are no cities found.'));
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        $regionId = (int)$this->getRequest()->getParam('id');
        $collection = $this->cityFactory->create()->getCollection()->addFieldToFilter('region_id', $regionId);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Return row url for js event handlers
     *
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return void
     */
    public function getRowUrl($item)
    {

    }

    /**
     * Retrieve the visibility option of the city
     *
     * @return array
     */
    public function getVisibilityOption()
    {
        $options = ['' => __('--Select one--')];
        foreach($this->cityDropdownVisibility->toOptionArray() as $visibility) {
            $options[$visibility['value']] = $visibility['label'];
        }
        return $options;
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'city_id',
            [
                'header' => __('ID'),
                'index' => 'city_id',
                'type' => 'text'
            ]
        );

        $this->addColumn(
            'city_default_name',
            [
                'header' => __('Name'),
                'index' => 'city_default_name',
                'type' => 'text'
            ]
        );

        $this->addColumn(
            'city_visibility',
            [
                'header' => __('Visibility'),
                'index' => 'city_visibility',
                'type' => 'options',
                'options' => $this->getVisibilityOption()
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/city/grid', ['_current' => true]);
    }
}

<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Block\Adminhtml\City\Edit\Button;

class Delete extends \PHPCuong\CityDropdown\Block\Adminhtml\Region\Edit\Button\Generic
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $cityId = $this->getId();
        if (!empty($cityId) && $this->_isAllowedAction('PHPCuong_CityDropdown::city_delete')) {
            return [
                'label' => __('Delete'),
                'on_click' => "if (confirm('Are you sure you want to delete this item?')) {".sprintf("location.href = '%s';", $this->getDeleteUrl())."}",
                'class' => 'delete',
                'sort_order' => 20
            ];
        }
    }

    /**
     * @param array $args
     * @return string
     */
    public function getDeleteUrl(array $args = [])
    {
        $params = array_merge($this->getDefaultUrlParams(), $args);
        return $this->getUrl('*/*/delete', $params);
    }

    /**
     * @return array
     */
    protected function getDefaultUrlParams()
    {
        return ['_current' => true, '_query' => ['isAjax' => null]];
    }
}

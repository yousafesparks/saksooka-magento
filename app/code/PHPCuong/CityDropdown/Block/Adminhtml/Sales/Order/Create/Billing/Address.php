<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Block\Adminhtml\Sales\Order\Create\Billing;

use Magento\Framework\Convert\ConvertArray;
use Magento\Framework\App\ObjectManager;

class Address extends \Magento\Sales\Block\Adminhtml\Order\Create\Billing\Address
{
    /**
     * Prepare Form and add elements to form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        // Start override
        $cityIdElement = $this->_form->getElement('city_id');

        if ($cityIdElement) {
            $addressForm = $this->_customerFormFactory->create('customer_address', 'adminhtml_customer_address');
            $regionIdElementValue = $this->_form->getElement('region_id')->getValue();
            $optionValues = [];
            if ($regionIdElementValue) {
                $cityCollection = $this->_getCityModel()->getCollection()->addFieldToFilter(
                    'region_id', ['eq' => (int)$regionIdElementValue]
                )->addFieldToFilter('city_visibility', '1');
                foreach ($cityCollection as $city) {
                    $optionValues[] = [
                        'value' => $city->getId(),
                        'label' => $city->getName()
                    ];
                }
            }

            $cityElement = $this->_form->getElement('city')->addClass('city-element');
            $cityIdElement->setType('select')->addClass('admin__control-select validate-select')->setRequired(true)->setRenderer(
                $this->getLayout()->createBlock(
                    'PHPCuong\CityDropdown\Block\Adminhtml\Sales\Order\Address\Form\Renderer\CityId'
                )->setData(
                    [
                        'city_element' => $cityElement,
                        'region_id_element' => $this->_form->getElement('region_id'),
                        'country_id_element' => $this->_form->getElement('country_id'),
                        'city_options' => $optionValues
                    ]
                )
            );
            if ($cityElement) {
                $cityElement->setNoDisplay(true);
            }
        }
        // End override

        return $this;
    }

    /**
     * Get City Model instance
     *
     * @return \PHPCuong\CityDropdown\Model\City
     */
    private function _getCityModel()
    {
        return ObjectManager::getInstance()->get(\PHPCuong\CityDropdown\Model\City::class);
    }
}

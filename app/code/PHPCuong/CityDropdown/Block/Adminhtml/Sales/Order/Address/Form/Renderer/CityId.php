<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Block\Adminhtml\Sales\Order\Address\Form\Renderer;

class CityId extends \Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element
{
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = 'sales/order/create/address/form/renderer/cityid.phtml';

    /**
     * Get the Html Id.
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function getElementId($element)
    {
        return $element->getForm()->getHtmlIdPrefix() . $element->getData('html_id') . $element->getForm()->getHtmlIdSuffix();
    }
}

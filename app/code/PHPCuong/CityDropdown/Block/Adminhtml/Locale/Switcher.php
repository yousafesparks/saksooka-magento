<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Block\Adminhtml\Locale;

class Switcher extends \Magento\Backend\Block\Template
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $_elementFactory;

    /**
     * @var \Magento\Config\Block\System\Config\Form\Field
     */
    protected $_formField;

    /**
     * Data Form object
     *
     * @var \Magento\Framework\Data\Form
     */
    protected $_form;

    /**
     * Form factory
     *
     * @var \Magento\Framework\Data\FormFactory
     */
    protected $_formFactory;

    /**
     * @var \PHPCuong\CityDropdown\Model\LocaleView
     */
    protected $localeView;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Magento\Config\Block\System\Config\Form\Field $formField
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \PHPCuong\CityDropdown\Model\LocaleView $localeView
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        \Magento\Config\Block\System\Config\Form\Field $formField,
        \Magento\Framework\Data\FormFactory $formFactory,
        \PHPCuong\CityDropdown\Model\LocaleView $localeView,
        array $data = []
    ) {
        $this->_elementFactory = $elementFactory;
        $this->_formField = $formField;
        $this->_formFactory = $formFactory;
        $this->localeView = $localeView;
        parent::__construct($context, $data);
    }

    /**
     * Return Form object
     *
     * @return \Magento\Framework\Data\Form
     */
    public function getForm()
    {
        if ($this->_form === null) {
            $this->_form = $this->_formFactory->create();
        }
        return $this->_form;
    }

    /**
     * Return Locale View Element
     *
     * @return string
     */
    public function getLocaleViewSelects()
    {
        $localeViewId = $this->getRequest()->getParam('locale_view_id');
        $field = $this->_elementFactory->create('select');
        $field->setName(
            'locale_view_id'
        )->setId(
            'locale_view_id'
        )->setValue(
            $localeViewId
        )->setValues(
            $this->localeView->toOptionArray()
        )->setForm(
            $this->getForm()
        )->setRenderer(
            $this->_formField
        );
        return $field->toHtml();
    }

    /**
     * @return string
     */
    public function getSwitchUrl()
    {
        return $this->getUrl(
            '*/*/*',
            [
                '_current' => true,
                'locale_view_id' => null
            ]
        );
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->getRequest()->getParam('id');
    }
}

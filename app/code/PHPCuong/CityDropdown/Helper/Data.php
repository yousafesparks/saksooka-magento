<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Zend\Escaper\Escaper
     */
    protected $escaper;

    /**
     * Json representation of cities data
     *
     * @var string
     */
    protected $_cityJson;

    /**
     * City options array data
     *
     * @var string
     */
    protected $_cityOptionArray;

    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $_cityFactory;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Country collection
     *
     * @var \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    protected $_countryCollection;

    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Cache\Type\Config $configCacheType
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\ResourceModel\Country\Collection $countryCollection
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\ResourceModel\Country\Collection $countryCollection,
        \Magento\Framework\Module\ModuleListInterface $moduleList
    ) {
        parent::__construct($context);
        $this->_configCacheType = $configCacheType;
        $this->_cityFactory = $cityFactory;
        $this->_regionFactory = $regionFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_storeManager = $storeManager;
        $this->_countryCollection = $countryCollection;
        $this->escaper = new \Zend\Escaper\Escaper;
        $this->moduleList = $moduleList;
    }

    /**
     * Returns extension version.
     *
     * @return string
     */
    public function getExtensionVersion()
    {
        $moduleCode = 'PHPCuong_CityDropdown';
        $moduleInfo = $this->moduleList->getOne($moduleCode);
        return $moduleInfo['setup_version'];
    }

    /**
     * Retrieve the URL for reporting issues on this extension
     *
     * @return string
     */
    public function getLinkForReportingIssues()
    {
        return 'https://github.com/php-cuong/magento2-city-dropdown';
    }

    /**
     * Retrieve country collection
     *
     * @param null|int|string|\Magento\Store\Model\Store $store
     * @return \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    public function getCountryCollection($store = null)
    {
        if (!$this->_countryCollection->isLoaded()) {
            $this->_countryCollection->loadByStore($store);
        }
        return $this->_countryCollection;
    }

    /**
     * Retrieve cities data json
     *
     * @return string
     */
    public function getCityJson()
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        if (!$this->_cityJson) {
            $cacheKey = 'DIRECTORY_CITIES_JSON_STORE' . $this->_storeManager->getStore()->getId();
            $json = $this->_configCacheType->load($cacheKey);
            if (empty($json)) {
                $regions = $this->getCityData();
                $json = $this->jsonHelper->jsonEncode($regions);
                if ($json === false) {
                    $json = 'false';
                }
                $this->_configCacheType->save($json, $cacheKey);
            }
            $this->_cityJson = $json;
        }

        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);
        return $this->_cityJson;
    }

    /**
     * Retrieve the select options array
     *
     * @param array $regionIds
     * @return array
     */
    public function getCityOptionArray($regionIds)
    {
        if (!$this->_cityOptionArray) {
            $this->_cityOptionArray = $this->getCityCollection($regionIds)->toOptionArray();
        }

        return $this->_cityOptionArray;
    }

    /**
     * Retrieve the city collection
     *
     * @param array $regionIds
     * @return \PHPCuong\CityDropdown\Model\ResourceModel\City\Collection
     */
    private function getCityCollection($regionIds = [])
    {
        if (empty($regionIds)) {
            $countryIds = [];
            foreach ($this->getCountryCollection() as $country) {
                $countryIds[] = $country->getCountryId();
            }
            $regionIds = [];
            $regionCollection = $this->_regionFactory->create()->getCollection()->addCountryFilter(
                $countryIds
            )->addFieldToFilter('visibility', '1');
            foreach ($regionCollection as $region) {
                $regionIds[] = $region->getRegionId();
            }
        }
        return $this->_cityFactory->create()->getCollection()->addFieldToFilter(
            'city_visibility', '1'
        )->addFieldToFilter(
            'main_table.region_id', ['in' => $regionIds]
        );
    }

    /**
     * Retrieve cities data
     *
     * @return array
     */
    public function getCityData()
    {
        $cityCollection = $this->getCityCollection()->addCountryIdToResult();
        $cities = [];
        foreach ($cityCollection as $city) {
            /** @var $city \PHPCuong\CityDropdown\Model\City */
            if (!$city->getCityId()) {
                continue;
            }
            $cities[$city->getRegionId()][$city->getCityId()] = [
                'code' => $city->getCityId(),
                'name' => (string)$city->getName(),
                'country_id' => $city->getCountryId()
            ];
        }

        return $cities;
    }

    /**
     * Escape a string for the HTML attribute context.
     *
     * @param string $string
     * @param boolean $escapeSingleQuote
     * @return string
     */
    public function escapeHtmlAttr($string, $escapeSingleQuote = true)
    {
        if ($escapeSingleQuote) {
            return $this->escaper->escapeHtmlAttr((string) $string);
        }

        return htmlspecialchars($string, ENT_COMPAT, 'UTF-8', false);
    }
}

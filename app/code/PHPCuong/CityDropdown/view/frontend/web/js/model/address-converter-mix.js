/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */
define([
    'jquery',
    'mage/utils/wrapper',
    'PHPCuong_CityDropdown/js/model/city-assigner'
], function ($, wrapper, cityAssigner) {
    'use strict';

    return function (addressConverter) {

        var formAddressDataToQuoteAddress = wrapper.wrap(addressConverter.formAddressDataToQuoteAddress, function(originalAction, formData) {
            cityAssigner(formData);
            return originalAction(formData);
        });

        addressConverter.formAddressDataToQuoteAddress = formAddressDataToQuoteAddress;

        return addressConverter;
    };
});

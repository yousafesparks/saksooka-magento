/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'mage/translate'
], function (_, registry, Select, $t) {
    'use strict';

    return Select.extend({
        defaults: {
            skipValidation: false,
            setOptionsValue: false,
            imports: {
                update: '${ $.parentName }.region_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var region = registry.get(this.parentName + '.' + 'region_id'),
                options = region.indexedOptions,
                option;

            if (value) {
                this.setOptionsValue = true;
                return;
            }

            option = options[value];

            if (option) {
                this.toggleSelect(true);
            } else {
                this.toggleSelect(false);
            }
        },

        /**
         * Filters 'initialOptions' property by 'field' and 'value' passed,
         * calls 'setOptions' passing the result to it
         *
         * @param {*} value
         * @param {String} field
         */
        filter: function (value, field) {
            var region = registry.get(this.parentName + '.' + 'region_id');
            // console.log(region);
            if (!region) {
                this.toggleSelect(false);
                return;
            }
            if (region) {
                var option = region.indexedOptions[value];
                if (!option || !value) {
                    this.toggleSelect(false);
                    return;
                }
                if (value) {
                    this.setOptionsValue = true;
                }
                this._super(value, field);
                this.toggleSelect(true);
                return;
            }
        },

        /**
         * Change visibility for select.
         *
         * @param {Boolean} isVisible
         */
        toggleSelect: function (isVisible) {
            // hide select and corresponding text input field if city must not be shown for selected city
            this.validation['required-entry'] = isVisible;
            this.required(isVisible);
            this.setVisible(isVisible);
            if (this.customEntry) {
                this.toggleInput(!isVisible);
            }
            if (this.skipValidation) {
                this.validation['required-entry'] = false;
                this.required(false);
            }
        },

        /**
         * Change visibility for input.
         *
         * @param {Boolean} isVisible
         */
        toggleInput: function (isVisible) {
            var self = this;
            registry.get(this.customName, function (input) {
                input.validation['required-entry'] = isVisible;
                input.required(isVisible);
                input.setVisible(isVisible);
                if (self.skipValidation) {
                    input.validation['required-entry'] = false;
                    input.required(false);
                }
            });
        },

        /**
         * Sets 'data' to 'options' observable array, if instance has
         * 'customEntry' property set to true, calls 'setHidden' method
         *  passing !options.length as a parameter
         *
         * @param {Array} data
         * @returns {Object} Chainable
         */
        setOptions: function (data) {
            var region = registry.get(this.parentName + '.' + 'region_id'),
                initialValue = null;
            if (region) {
                initialValue = region.initialValue;
            }
            this.caption($t('Please select a city, district or town.'));
            if (initialValue || this.setOptionsValue) {
                this._super(data);
            }
            return this;
        }
    });
});

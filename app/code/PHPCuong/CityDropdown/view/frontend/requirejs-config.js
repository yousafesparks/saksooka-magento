/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/address-converter': {
                'PHPCuong_CityDropdown/js/model/address-converter-mix': true
            },
            'Magento_Checkout/js/model/cart/cache': {
                'PHPCuong_CityDropdown/js/model/cart/cache-mix': true
            }
        }
    },
    map: {
        '*': {
            cityUpdater: 'PHPCuong_CityDropdown/js/city-updater',
            'Magento_Checkout/js/model/new-customer-address': 'PHPCuong_CityDropdown/js/model/new-customer-address',
            'Magento_Checkout/js/model/shipping-rate-processor/new-address': 'PHPCuong_CityDropdown/js/model/shipping-rate-processor/new-address',
            'Magento_Checkout/template/shipping-address/address-renderer/default.html': 'PHPCuong_CityDropdown/template/shipping-address/address-renderer/default.html',
            'Magento_Checkout/template/billing-address/details.html': 'PHPCuong_CityDropdown/template/billing-address/details.html'
        }
    }
};

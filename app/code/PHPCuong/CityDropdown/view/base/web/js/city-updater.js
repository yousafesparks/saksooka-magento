/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

define([
    'jquery',
    'mage/template',
    'jquery/ui',
    'mage/validation'
], function ($, mageTemplate) {
    'use strict';

    $.widget('mage.cityUpdater', {
        options: {
            cityTemplate:
                '<option value="<%- data.value %>" <% if (data.isSelected) { %>selected="selected"<% } %>>' +
                    '<%- data.title %>' +
                '</option>',
            loop: 0
        },

        /**
         *
         * @private
         */
        _create: function () {
            var self = this;
            this.currentCityOption = this.options.currentCity;
            this.cityTmpl = mageTemplate(this.options.cityTemplate);

            this.options.runFunctions = setInterval(function() {
                if ($(self.element).is(':visible') && !$(self.element).is(':disabled')) {
                    var regionId = $(self.element).find('option:selected').val();
                    if (regionId) {
                        self._updateCity(regionId);
                        self._stopUpdateCity();
                        return;
                    }
                }
                if (!$(self.element).is(':disabled')) {
                    self._removeSelectOptions($(self.options.cityListId));
                    self._hideCityListElement();
                }
                self.options.loop = self.options.loop + 1;
                if (self.options.loop >= 11) {
                    self._stopUpdateCity();
                }
            }, 500);

            $(this.element).on('change', function() {
                self._updateCity($(this).find('option:selected').val());
            });

            $(this.options.country).on('change', function() {
                self._removeSelectOptions($(self.options.cityListId));
                if ($(self.element).is(':visible') && !$(self.element).is(':disabled')) {
                    $(self.element).removeAttr('disabled');
                } else {
                    $(self.element).attr('disabled', 'disabled');
                    self._hideCityListElement();
                }
            });

            $(this.options.cityListId).on('change', $.proxy(function (e) {
                this.setOption = false;
                this.currentCityOption = $(e.target).val();
            }, this));

            $(this.options.cityInputId).on('focusout', $.proxy(function () {
                this.setOption = true;
            }, this));
        },

        /**
         * Stop functions
         *
         * @private
         */
        _stopUpdateCity: function() {
            clearInterval(this.options.runFunctions);
        },

        /**
         * Remove options from dropdown list
         *
         * @param {Object} selectElement - jQuery object for dropdown list
         * @private
         */
        _removeSelectOptions: function (selectElement) {
            selectElement.find('option').each(function (index) {
                if (index) {
                    $(this).remove();
                }
            });
        },

        /**
         * Render dropdown list
         * @param {Object} selectElement - jQuery object for dropdown list
         * @param {String} key - city code
         * @param {Object} value - city object
         * @private
         */
        _renderSelectOption: function (selectElement, key, value) {
            selectElement.append($.proxy(function () {
                var name = value.name.replace(/[!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~]/g, '\\$&'),
                    tmplData,
                    tmpl;

                if (value.code && $(name).is('span')) {
                    key = value.code;
                    value.name = $(name).text();
                }

                tmplData = {
                    value: key,
                    title: value.name,
                    isSelected: false
                };

                if (this.options.defaultCity === key) {
                    tmplData.isSelected = true;
                }

                tmpl = this.cityTmpl({
                    data: tmplData
                });

                return $(tmpl);
            }, this));
        },

        /**
         * Takes clearError callback function as first option
         * If no form is passed as option, look up the closest form and call clearError method.
         * @private
         */
        _clearError: function () {
            if (this.options.clearError && typeof this.options.clearError === 'function') {
                this.options.clearError.call(this);
            } else {
                if (!this.options.form) {
                    this.options.form = this.element.closest('form').length ? $(this.element.closest('form')[0]) : null;
                }

                this.options.form = $(this.options.form);

                this.options.form && this.options.form.data('validator') && this.options.form.validation('clearError',
                    this.options.cityListId, this.options.cityInputId);

                // Clean up errors on region & zip fix
                $(this.options.cityInputId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.cityListId).removeClass('mage-error').parent().find('[generated]').remove();
            }
        },

        /**
         * Update dropdown list based on the region id selected
         *
         * @param {integer} region_id
         * @private
         */
        _updateCity: function (region_id) {
            // Clear validation error messages
            var cityList = $(this.options.cityListId),
                cityInput = $(this.options.cityInputId),
                label = cityList.parent().siblings('label'),
                requiredLabel = cityList.parents('div.field').addClass('required');

            this._clearError();

            // Populate city dropdown list if available or use input box
            if (!region_id) {
                this._hideCityListElement();
                return;
            }
            if (this.options.cityJson[region_id]) {
                this._removeSelectOptions(cityList);
                $.each(this.options.cityJson[region_id], $.proxy(function (key, value) {
                    this._renderSelectOption(cityList, key, value);
                }, this));

                if (this.currentCityOption) {
                    cityList.val(this.currentCityOption);
                }

                if (this.setOption) {
                    cityList.find('option').filter(function () {
                        return this.text === cityInput.val();
                    }).attr('selected', true);
                }

                this._showCityListElement();

                // Add defaultvalue attribute to state/province select element
                cityList.attr('defaultvalue', this.options.defaultCity);
            } else {
                this._hideCityListElement();
            }
        },

        /**
         * Hide the city list field
         *
         * @private
         */
        _hideCityListElement: function()
        {
            var cityList = $(this.options.cityListId),
                cityInput = $(this.options.cityInputId),
                label = cityList.parent().siblings('label'),
                requiredLabel = cityList.parents('div.field').addClass('required');

            cityList.removeClass('required-entry validate-select').removeAttr('data-validate');
            cityList.attr('disabled', 'disabled');
            cityList.hide();
            cityInput.addClass('required-entry').removeAttr('disabled');
            label.attr('for', cityInput.attr('id'));
            cityInput.show();
        },

        /**
         * show the city list field
         *
         * @private
         */
        _showCityListElement: function() {
            var cityList = $(this.options.cityListId),
                cityInput = $(this.options.cityInputId),
                label = cityList.parent().siblings('label'),
                requiredLabel = cityList.parents('div.field').addClass('required');
            cityList.addClass('required-entry validate-select').removeAttr('disabled');
            label.attr('for', cityList.attr('id'));
            cityList.show();
            cityInput.hide();
            cityInput.removeClass('required-entry');
            cityInput.attr('disabled', 'disabled');
        }
    });

    return $.mage.cityUpdater;
});

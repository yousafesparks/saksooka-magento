/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

var config = {
    map: {
        '*': {
            'Magento_Customer/template/default-address.html': 'PHPCuong_CityDropdown/template/default-address.html',
            'Magento_Sales/order/create/scripts': 'PHPCuong_CityDropdown/js/order/create/scripts',
            cityUpdater: 'PHPCuong_CityDropdown/js/city-updater'
        }
    }
};


/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'mage/translate'
], function (_, registry, Select, $t) {
    'use strict';

    return Select.extend({
        defaults: {
            skipValidation: false,
            setOptionsValue: false,
            imports: {
                update: '${ $.parentName }.region_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var region = registry.get(this.parentName + '.' + 'region_id'),
                options = region.indexedOptions,
                option;
            if (value) {
                this.setOptionsValue = true;
                return;
            }

            option = options[value];

            if (option) {
                this.toggleSelect(true);
            } else {
                this.toggleSelect(false);
            }
        },

        /**
         * Change visibility for select.
         *
         * @param {Boolean} isVisible
         */
        toggleSelect: function (isVisible) {
            // hide select and corresponding text input field if city must not be shown for selected city
            this.validation['required-entry'] = isVisible;
            this.required(isVisible);
            this.setVisible(isVisible);
            if (this.customEntry) {
                this.toggleInput(!isVisible);
            }
        },

        /**
         * Change visibility for input.
         *
         * @param {Boolean} isVisible
         */
        toggleInput: function (isVisible) {
            registry.get(this.customName, function (input) {
                input.validation['required-entry'] = isVisible;
                input.required(isVisible);
                input.setVisible(isVisible);
            });
        },

        /**
         * Get option from indexedOptions list.
         *
         * @param {Number} value
         * @returns {Object} Chainable
         */
        getOption: function (value) {
            if (this.indexedOptions && this.indexedOptions[value] != undefined) {
                return this._super(value);;
            }
            return null;
        },
    });
});

<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Ui\Component\Listing\Column\City;

class Actions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Url path to edit
     *
     * @var string
     */
    const CITY_URL_PATH_EDIT = 'phpcuong_citydropdown/city/edit';

    /**
     * Url path to delete
     *
     * @var string
     */
    const CITY_URL_PATH_DELETE = 'phpcuong_citydropdown/city/delete';

    /**
     * URL builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * constructor
     *
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\AuthorizationInterface $authorization,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->_authorization = $authorization;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['city_id'])) {
                    if ($this->_isAllowedAction('PHPCuong_CityDropdown::city_update')) {
                         $item[$name]['edit'] = [
                            'href' => $this->_urlBuilder->getUrl(self::CITY_URL_PATH_EDIT, ['id' => $item['city_id']]),
                            'label' => __('Edit')
                        ];
                    }

                    if ($this->_isAllowedAction('PHPCuong_CityDropdown::city_delete')) {
                        $item[$name]['delete'] = [
                            'href' => $this->_urlBuilder->getUrl(self::CITY_URL_PATH_DELETE, ['id' => $item['city_id']]),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete the item'),
                                'message' => __('Are you sure you wan\'t to do this?')
                            ]
                        ];
                    }
                }
            }
        }
        return $dataSource;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}


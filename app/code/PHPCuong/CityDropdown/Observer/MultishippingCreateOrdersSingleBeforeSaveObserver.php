<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Observer;

class MultishippingCreateOrdersSingleBeforeSaveObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (empty($observer->getEvent()->getOrder()) || empty($observer->getEvent()->getQuote()) || empty($observer->getEvent()->getAddress())) {
            return $this;
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        /** @var \Magento\Customer\Model\Address $address */
        $address = $observer->getEvent()->getAddress();

        if (!empty($order->getShippingAddress())) {
            $order->getShippingAddress()->setCity($address->getCity());
            $order->getShippingAddress()->setCityId($address->getCityId());
        }

        if (!empty($order->getBillingAddress()) && !empty($quote->getBillingAddress())) {
            $order->getBillingAddress()->setCity($quote->getBillingAddress()->getCity());
            $order->getBillingAddress()->setCityId($quote->getBillingAddress()->getCityId());
        }

        return $this;
    }
}

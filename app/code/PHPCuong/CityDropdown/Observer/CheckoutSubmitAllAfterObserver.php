<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Observer;

class CheckoutSubmitAllAfterObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
    ) {
        $this->cityFactory = $cityFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (empty($observer->getEvent()->getOrder()) || empty($observer->getEvent()->getQuote())) {
            return $this;
        }

        $shippingAddress = $observer->getEvent()->getQuote()->getShippingAddress();
        $billingAddress = $observer->getEvent()->getQuote()->getBillingAddress();

        $orderShippingAddress = $observer->getEvent()->getOrder()->getShippingAddress();
        $orderBillingAddress = $observer->getEvent()->getOrder()->getBillingAddress();

        $cityId = (int)$shippingAddress->getCityId();
        if ($cityId) {
            /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
            $cityFactory = $this->cityFactory->create()->load($cityId);
            if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $shippingAddress->getRegionId()) {
                $orderShippingAddress->setCity($cityFactory->getName())->setCityId($cityFactory->getCityId())->save();
            }
        }

        $cityId = (int)$billingAddress->getCityId();
        if ($cityId) {
            /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
            $cityFactory = $this->cityFactory->create()->load($cityId);
            if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $billingAddress->getRegionId()) {
                $orderBillingAddress->setCity($cityFactory->getName())->setCityId($cityFactory->getCityId())->save();
            }
        }

        return $this;
    }
}

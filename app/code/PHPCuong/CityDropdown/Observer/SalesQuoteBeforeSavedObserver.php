<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Observer;

class SalesQuoteBeforeSavedObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
    ) {
        $this->cityFactory = $cityFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (empty($observer->getEvent()->getQuote())) {
            return $this;
        }

        $shippingAddress = $observer->getEvent()->getQuote()->getShippingAddress();
        $billingAddress = $observer->getEvent()->getQuote()->getBillingAddress();

        $shippingExtensionAttributes = $shippingAddress->getExtensionAttributes();
        if (!empty($shippingExtensionAttributes)) {
            $cityId = (int)$shippingExtensionAttributes->getCityId();
            /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
            $cityFactory = $this->cityFactory->create()->load($cityId);
            if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $shippingAddress->getRegionId()) {
                $shippingAddress->setCity($cityFactory->getName());
                $shippingAddress->setCityId($cityFactory->getCityId());
            }
        }

        $billingExtensionAttributes = $billingAddress->getExtensionAttributes();
        if (!empty($billingExtensionAttributes)) {
            $cityId = (int)$billingExtensionAttributes->getCityId();
            /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
            $cityFactory = $this->cityFactory->create()->load($cityId);
            if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $billingAddress->getRegionId()) {
                $billingAddress->setCity($cityFactory->getName());
                $billingAddress->setCityId($cityFactory->getCityId());
            }
        }

        return $this;
    }
}

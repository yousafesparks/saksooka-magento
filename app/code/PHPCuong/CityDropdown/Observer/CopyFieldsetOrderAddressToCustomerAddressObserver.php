<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Observer;

class CopyFieldsetOrderAddressToCustomerAddressObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
    ) {
        $this->cityFactory = $cityFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $target = $observer->getEvent()->getTarget();
        if (!empty($target->getCityId())) {
            $attributeValue = new \Magento\Framework\Api\AttributeValue();
            $attributeValue->setAttributeCode('city_id')->setValue($target->getCityId());
            $target->setCustomAttributes(['city_id' => $attributeValue]);
        }
        return $this;
    }
}

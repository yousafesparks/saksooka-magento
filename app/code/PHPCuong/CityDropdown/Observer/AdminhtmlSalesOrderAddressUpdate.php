<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Observer;

class AdminhtmlSalesOrderAddressUpdate implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $cityFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $regionFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     */
    public function __construct(
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory
    ) {
        $this->cityFactory = $cityFactory;
        $this->orderFactory = $orderFactory;
        $this->regionFactory = $regionFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $saleOrderId = $observer->getEvent()->getOrderId();
        $order = $this->orderFactory->create()->load($saleOrderId);
        if ($order->getId()) {
            // Process updating shipping address for the order
            $shippingAddress = $order->getShippingAddress();
            $cityId = $shippingAddress->getCityId();
            /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
            $cityFactory = $this->cityFactory->create()->load($cityId);
            if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $shippingAddress->getRegionId()) {
                $shippingAddress->setCity($cityFactory->getCityDefaultName())->save();
            }
            $newRegion = $this->regionFactory->create()->load($shippingAddress->getRegionId());
            $shippingAddress->setRegion($newRegion->getDefaultName())->save();

            // Process updating billing address for the order
            $billingAddress = $order->getBillingAddress();
            $cityId = $billingAddress->getCityId();
            /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
            $cityFactory = $this->cityFactory->create()->load($cityId);
            if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $billingAddress->getRegionId()) {
                $billingAddress->setCity($cityFactory->getCityDefaultName())->save();
            }
            $newRegion = $this->regionFactory->create()->load($billingAddress->getRegionId());
            $billingAddress->setRegion($newRegion->getDefaultName())->save();
        }

        return $this;
    }
}

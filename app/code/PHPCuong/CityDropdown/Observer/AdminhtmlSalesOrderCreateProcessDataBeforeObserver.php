<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Observer;

class AdminhtmlSalesOrderCreateProcessDataBeforeObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
    ) {
        $this->cityFactory = $cityFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $requestModel = $observer->getEvent()->getRequestModel();
        $postOrder = $requestModel->getPost('order');
        if (isset($postOrder['shipping_address'])) {
            if (!empty($postOrder['shipping_address']['city_id'])) {
                if (empty($postOrder['shipping_address']['region_id'])) {
                    $postOrder['shipping_address']['city_id'] = null;
                } else {
                    $cityId = (int)$postOrder['shipping_address']['city_id'];
                    /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
                    $cityFactory = $this->cityFactory->create()->load($cityId);
                    if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $postOrder['shipping_address']['region_id']) {
                        $postOrder['shipping_address']['city'] = $cityFactory->getName();
                        $postOrder['shipping_address']['city_id'] = $cityFactory->getCityId();
                    }
                }
            }
        }

        if (isset($postOrder['billing_address'])) {
            if (!empty($postOrder['billing_address']['city_id'])) {
                if (empty($postOrder['billing_address']['region_id'])) {
                    $postOrder['billing_address']['city_id'] = null;
                } else {
                    $cityId = (int)$postOrder['billing_address']['city_id'];
                    /** @var \PHPCuong\CityDropdown\Model\CityFactory $cityFactory */
                    $cityFactory = $this->cityFactory->create()->load($cityId);
                    if ($cityFactory->getCityId() && $cityFactory->getRegionId() == $postOrder['billing_address']['region_id']) {
                        $postOrder['billing_address']['city'] = $cityFactory->getName();
                        $postOrder['billing_address']['city_id'] = $cityFactory->getCityId();
                    }
                }
            }
        }

        $requestModel->setPostValue('order', $postOrder);

        return $this;
    }
}

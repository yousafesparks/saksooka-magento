<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Controller\Adminhtml\Region;

use Magento\Framework\Setup\SampleData\Context as SampleDataContext;
use Magento\Framework\Exception\LocalizedException;

class ImportPost extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'PHPCuong_CityDropdown::region_create';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvReader;

    /**
     * @var \Magento\Framework\Setup\SampleData\FixtureManager
     */
    protected $fixtureManager;

    /**
     * Media directory object (writable).
     *
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $regionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param SampleDataContext $sampleDataContext
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        SampleDataContext $sampleDataContext,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Directory\Model\RegionFactory $regionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->csvReader = $sampleDataContext->getCsvReader();
        $this->fixtureManager = $sampleDataContext->getFixtureManager();
        $this->mediaDirectory = $filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->regionFactory = $regionFactory;
        parent::__construct($context);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $postData = $this->getRequest()->getPostValue();
        if ($postData) {
            $filePath = $this->mediaDirectory->getAbsolutePath('phpcuong/tmp/citydropdown/'.$postData['import_file'][0]['name']);
            $fileName = $this->fixtureManager->getFixture($filePath);
            if (!file_exists($fileName)) {
                $this->messageManager->addError(__('Something went wrong while importing regions.'));
                return $resultRedirect->setPath('*/*/import');
            }
            $rows = $this->csvReader->getData($fileName);
            $header = array_shift($rows);
            $messages = [];
            foreach ($rows as $row) {
                $data = [];
                foreach ($row as $key => $value) {
                    $data[trim($header[$key])] = trim($value);
                }
                $row = $data;
                $countryId = $postData['country_id'];
                $region = $this->regionFactory->create()
                    ->loadByCode($row['region_code'], $countryId)
                    ->setCode($row['region_code'])
                    ->setCountryId($countryId)
                    ->setDefaultName($row['region_name'])
                    ->setVisibility($row['visibility']);
                try {
                    $region->save();
                } catch (LocalizedException $e) {
                    $messages[] = $e->getMessage();
                }
            }
            if (!empty($messages)) {
                foreach ($messages as $message) {
                    $this->messageManager->addError($message);
                }
            } else {
                $this->messageManager->addSuccess(__('You have imported the regions successfully.'));
                return $resultRedirect->setPath('*/*/index');
            }
        }
        return $resultRedirect->setPath('*/*/import');
    }
}

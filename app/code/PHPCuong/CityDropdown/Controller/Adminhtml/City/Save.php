<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Controller\Adminhtml\City;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $data['city_id'];
            $data['city_default_name'] = $data['city_name'];

            if (empty($data['city_id'])) {
                $data['city_id'] = null;
            }

            /** @var \PHPCuong\CityDropdown\Model\City $model */
            $model = $this->_objectManager->create('PHPCuong\CityDropdown\Model\City')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This city no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            try {
                if ($data['locale_view_id']) {
                    unset($data['city_default_name']);
                } else {
                    $data['locale_view_id'] = null;
                    $data['city_name'] = null;
                }
                $model->setData($data);
                $model->save();
                $this->messageManager->addSuccess(__('You saved the city.'));
                $this->dataPersistor->clear('city');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), 'locale_view_id' => $data['locale_view_id']]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the city.'));
            }

            if (!$data['locale_view_id']) {
                $data['city_name'] = $data['city_default_name'];
            }

            $this->dataPersistor->set('city', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id, 'locale_view_id' => $data['locale_view_id']]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Authorization level of a basic admin session
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('PHPCuong_CityDropdown::city_update') || $this->_authorization->isAllowed('PHPCuong_CityDropdown::city_create');
    }
}

<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Controller\Adminhtml\City\Import;

use Magento\Framework\Setup\SampleData\Context as SampleDataContext;

class Validate extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'PHPCuong_CityDropdown::city_create';

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Setup\SampleData\FixtureManager
     */
    protected $fixtureManager;

    /**
     * Media directory object (writable).
     *
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param SampleDataContext $sampleDataContext
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Filesystem $filesystem,
        SampleDataContext $sampleDataContext
    ) {
        parent::__construct($context);
        $this->fixtureManager = $sampleDataContext->getFixtureManager();
        $this->resultJsonFactory = $resultJsonFactory;
        $this->mediaDirectory = $filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }

    /**
     * Validate The Brand before saving by Ajax
     *
     * @return \Magento\Framework\Controller\Result\Json
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $response->setError(false);
        $postData = $this->getRequest()->getPostValue();
        $messages = [];
        if (empty($postData['import_file'][0]['tmp_name'])) {
            $messages[] = __('Please select a city import file.');
            $response->setError(true);
        } else {
            $filePath = $this->mediaDirectory->getAbsolutePath('phpcuong/tmp/citydropdown/'.$postData['import_file'][0]['name']);
            $fileName = $this->fixtureManager->getFixture($filePath);
            if (!file_exists($fileName)) {
                $messages[] = __('City Import file no longer exists.');
                $response->setError(true);
            }
        }
        if (empty($postData['region_id'])) {
            $messages[] = __('Please select a region name.');
            $response->setError(true);
        }
        if (empty($postData['country_id'])) {
            $messages[] = __('Please select a country name.');
            $response->setError(true);
        }
        if ($response->getError()) {
            $response->setMessages($messages);
        }
        return $this->resultJsonFactory->create()->setData($response);
    }
}

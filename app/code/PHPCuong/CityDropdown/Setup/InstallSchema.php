<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create the table 'phpcuong_directory_country_region_city'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('phpcuong_directory_country_region_city')
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'unsigned' => true, 'primary' => true],
            'City ID'
        )->addColumn(
            'city_default_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'City Name'
        )->addColumn(
            'city_visibility',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['unsigned' => true, 'nullable' => false, 'default' => 1],
            'City Visibility'
        )->addColumn(
            'city_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'City Code'
        )->addColumn(
            'region_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Region Id'
        )->addForeignKey(
            $installer->getFkName('phpcuong_directory_country_region_city', 'region_id', 'directory_country_region', 'region_id'),
            'region_id',
            $installer->getTable('directory_country_region'),
            'region_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('phpcuong_directory_country_region_city'),
                ['city_default_name'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['city_default_name'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'Directory Country Region City'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'phpcuong_directory_country_region_city_name'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('phpcuong_directory_country_region_city_name')
        )->addColumn(
            'locale',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            8,
            ['nullable' => false, 'primary' => true, 'default' => false],
            'Locale'
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true, 'default' => '0'],
            'City Id'
        )->addColumn(
            'city_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'City Name'
        )->addIndex(
            $installer->getIdxName('phpcuong_directory_country_region_city_name', ['city_id']),
            ['city_id']
        )->addForeignKey(
            $installer->getFkName(
                'phpcuong_directory_country_region_city_name',
                'city_id',
                'phpcuong_directory_country_region_city',
                'city_id'
            ),
            'city_id',
            $installer->getTable('phpcuong_directory_country_region_city'),
            'city_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Directory Country Region City Name'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}

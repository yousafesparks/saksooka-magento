<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\SampleData\Context as SampleDataContext;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;

/**
 * Install Data
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var \Magento\Framework\Setup\SampleData\FixtureManager
     */
    private $fixtureManager;

    /**
     * @var \Magento\Framework\File\Csv
     */
    private $csvReader;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    private $regionFactory;

    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    private $cityFactory;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param SampleDataContext $sampleDataContext
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param EavSetupFactory $eavSetupFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        EavSetupFactory $eavSetupFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
    ) {
        $this->fixtureManager = $sampleDataContext->getFixtureManager();
        $this->csvReader = $sampleDataContext->getCsvReader();
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->regionFactory = $regionFactory;
        $this->cityFactory = $cityFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->addCityIdAttribute($setup);
        $this->updateAttributeCity($setup);
        $this->installDataProvincesAndCitiesOfVietnam($setup);
    }

    /**
     * Add the address city id attribute
     *
     * @return void
     */
    protected function addCityIdAttribute(ModuleDataSetupInterface $setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType(\Magento\Customer\Api\AddressMetadataInterface::ATTRIBUTE_SET_ID_ADDRESS);
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $data = [
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => [
                'adminhtml_customer_address',
                'customer_address_edit',
                'customer_register_address'
            ]
        ];

        if (!$customerSetup->getAttributeId('customer_address', 'city_id')) {
            $customerSetup->addAttribute('customer_address', 'city_id', [
                'type' => 'int',
                'label' => 'City',
                'input' => 'hidden',
                'required' => false,
                'visible' => true,
                'system' => 0,
                'visible_on_front' => true,
                'sort_order' => 101,
                'position' => 101,
                'source' => 'PHPCuong\CityDropdown\Model\ResourceModel\Address\Attribute\Source\City'
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'city_id')
                ->addData($data);

            $attribute->save();
        }
    }

    /**
     * Install the data of provinces and cities in the Vietnam
     *
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    protected function installDataProvincesAndCitiesOfVietnam(ModuleDataSetupInterface $setup)
    {
        $provincesCities = 'PHPCuong_CityDropdown::fixtures/provinces-cities-vn.csv';
        $fileName = $this->fixtureManager->getFixture($provincesCities);
        if (!file_exists($fileName)) {
            return;
        }

        $rows = $this->csvReader->getData($fileName);
        $header = array_shift($rows);

        foreach ($rows as $row) {
            $data = [];
            foreach ($row as $key => $value) {
                $data[trim($header[$key])] = trim($value);
            }
            $row = $data;

            if (!in_array($row['visibility'], ['1','2'])) {
                $row['visibility'] = 1;
            }

            $region = $this->regionFactory->create()
                ->loadByCode($row['region_code'], 'VN')
                ->setCountryId('VN')
                ->setDefaultName($this->convertTextToLatin($row['region_name']))
                ->setCode($row['region_code'])
                ->setVisibility($row['visibility']);
            try {
                $region->save();
                $bind = ['locale' => 'vi_VN', 'region_id' => $region->getRegionId(), 'name' => $row['region_name']];
                $setup->getConnection()->insertOnDuplicate($setup->getTable('directory_country_region_name'), $bind);
                $this->installDataDistrictOfProvincesOrCitiesInVietnam($region, $setup);
            } catch (LocalizedException $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * Install the data for the Districts of the provinces or cities in the Vietnam
     *
     * @param \Magento\Directory\Model\Region $region
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    protected function installDataDistrictOfProvincesOrCitiesInVietnam($region, ModuleDataSetupInterface $setup)
    {
        $district = 'PHPCuong_CityDropdown::fixtures/district-vn.csv';
        $fileName = $this->fixtureManager->getFixture($district);
        if (!file_exists($fileName)) {
            return;
        }

        $rows = $this->csvReader->getData($fileName);
        $header = array_shift($rows);

        foreach ($rows as $row) {
            $data = [];
            foreach ($row as $key => $value) {
                $data[trim($header[$key])] = trim($value);
            }
            $row = $data;
            $regionCode = $row['region_code'];
            $regionId = $region->getRegionId();
            if (!in_array($row['visibility'], ['1','2'])) {
                $row['visibility'] = 1;
            }
            if ($region->getCode() == $regionCode) {
                $district = $this->cityFactory->create()
                    ->loadByName($this->convertTextToLatin($row['city_name']), $regionId)
                    ->setCityCode($row['city_code'])
                    ->setRegionId($regionId)
                    ->setCityDefaultName($this->convertTextToLatin($row['city_name']))
                    ->setVisibility($row['visibility']);
                try {
                    $district->save();
                    if ($district->getCityId()) {
                        $bind = ['locale' => 'vi_VN', 'city_id' => $district->getCityId(), 'city_name' => $row['city_name']];
                        $setup->getConnection()->insertOnDuplicate($setup->getTable('phpcuong_directory_country_region_city_name'), $bind);
                    }
                } catch (LocalizedException $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    /**
     * Update the backend_model for the city attribute
     *
     * @return void
     */
    protected function updateAttributeCity(ModuleDataSetupInterface $setup)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerSetup->updateAttribute(
            'customer_address',
            'city',
            'backend_model',
            'PHPCuong\CityDropdown\Model\ResourceModel\Address\Attribute\Backend\City'
        );
    }

    /**
     * Convert a Vietnamese string to a Latin string
     *
     * @param string $string
     * @return string
     */
    private function convertTextToLatin($string)
    {
        $vietnamese = ["à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ","ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ","ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ","ỳ","ý","ỵ","ỷ","ỹ","đ","À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ","È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ","Ì","Í","Ị","Ỉ","Ĩ","Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ","Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ","Ỳ","Ý","Ỵ","Ỷ","Ỹ","Đ"];
        $latin = ["a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","E","E","E","E","E","E","E","E","E","E","E","I","I","I","I","I","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","U","U","U","U","U","U","U","U","U","U","U","Y","Y","Y","Y","Y","D"];
        return str_replace($vietnamese, $latin, $string);
    }
}

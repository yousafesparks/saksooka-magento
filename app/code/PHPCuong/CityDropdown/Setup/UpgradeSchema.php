<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->addCityIdColumn($setup);

        $this->addColumnVisibility($setup);

        $this->addIndexDefaultName($setup);

        $setup->endSetup();
    }

    /**
     * Add the column named city_id
     *
     * @param SchemaSetupInterface $setup
     *
     * @return void
     */
    private function addCityIdColumn(SchemaSetupInterface $setup)
    {
        $cityId = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            'default' => NULL,
            'nullable' => true,
            'unsigned' => true,
            'comment' => 'City ID',
            'after' => 'city'
        ];

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'),
            'city_id',
            $cityId
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'city_id',
            $cityId
        );
    }

    /**
     * Add the new column named visibility to the region table
     *
     * @param SchemaSetupInterface $installer
     * @return void
     */
    private function addColumnVisibility(SchemaSetupInterface $installer)
    {
        $connection = $installer->getConnection();
        $connection->addColumn(
            $installer->getTable('directory_country_region'),
            'visibility',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'unsigned' => true,
                'nullable' => false,
                'default' => 1,
                'comment' => 'Visibility',
                'after' => 'default_name'
            ]
        );
    }

    /**
     * Add index the default name of Region
     *
     * @param SchemaSetupInterface $installer
     * @return void
     */
    private function addIndexDefaultName(SchemaSetupInterface $installer)
    {
        $installer->getConnection()->addIndex(
            $installer->getTable('directory_country_region'),
            $installer->getIdxName(
                'directory_country_region',
                ['default_name'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['default_name'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );
    }
}

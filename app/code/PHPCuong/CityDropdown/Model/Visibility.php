<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model;

class Visibility implements \Magento\Framework\Option\ArrayInterface
{
    const VISIBILITY_VISIBLE = 1;

    const VISIBILITY_INVISIBLE = 2;

    /**
     * Retrieve visible in display
     *
     * @return int
     */
    public function getVisible()
    {
        return self::VISIBILITY_VISIBLE;
    }

    /**
     * Retrieve invisible
     *
     * @return int
     */
    public function getInVisible()
    {
        return self::VISIBILITY_INVISIBLE;
    }

    /**
     * Retrieve both
     *
     * @return array
     */
    public function getBoth()
    {
        return [self::VISIBILITY_INVISIBLE, self::VISIBILITY_VISIBLE];
    }

    /**
     * Get available options
     *
     * @codeCoverageIgnore
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => $this->getVisible(), 'label' => __('Visible')],
            ['value' => $this->getInVisible(), 'label' => __('Invisible')]
        ];
    }
}

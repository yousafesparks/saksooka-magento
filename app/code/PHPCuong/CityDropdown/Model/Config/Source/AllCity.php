<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\Config\Source;

class AllCity implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var array
     */
    protected $_countries;

    /**
     * @var array
     */
    protected $_regions;

    /**
     * @var array
     */
    protected $_options;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $_regionCollectionFactory;

    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $_cityFactory;

    /**
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
    ) {
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_regionCollectionFactory = $regionCollectionFactory;
        $this->_cityFactory = $cityFactory;
    }

    /**
     * @param bool $isMultiselect
     * @return array
     */
    public function toOptionArray($isMultiselect = false)
    {
        if (!$this->_options) {
            $countriesArray = $this->_countryCollectionFactory->create()->load()->toOptionArray(false);
            $this->_countries = [];
            foreach ($countriesArray as $a) {
                $this->_countries[$a['value']] = $a['label'];
            }

            $countryRegionCities = [];
            $regionsCollection = $this->_regionCollectionFactory->create()->load();
            $this->_regions = [];
            foreach ($regionsCollection as $region) {
                $this->_regions[$region->getId()] = '&nbsp;&nbsp;&nbsp;'.$region->getDefaultName();
                $cities = $this->_cityFactory->create()->getCollection()->addFieldToFilter(
                    'main_table.region_id', ['in' => $region->getId()]
                );
                foreach ($cities as $city) {
                    $countryRegionCities[$region->getCountryId()][$region->getId()][$city->getId()] = $city->getName();
                }
            }
            uksort($countryRegionCities, [$this, 'sortCities']);
            $this->_options = [];
            foreach ($countryRegionCities as $countryId => $regions) {
                $regionOptions = [];
                foreach ($regions as $regionId => $cities) {
                    $cityOptions = [];
                    foreach ($cities as $cityId => $cityName) {
                        $cityOptions[] = ['label' => $cityName, 'value' => $cityId];
                    }
                    $regionOptions[] = ['label' => $this->_regions[$regionId], 'value' => $cityOptions];
                }
                $this->_options[] = ['label' => $this->_countries[$countryId], 'value' => $regionOptions];
            }
        }
        $options = $this->_options;
        if (!$isMultiselect) {
            array_unshift($options, ['value' => '', 'label' => '']);
        }

        return $options;
    }

    /**
     * @param string $a
     * @param string $b
     * @return int
     */
    public function sortCities($a, $b)
    {
        return strcmp($this->_countries[$a], $this->_countries[$b]);
    }
}

<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model;

class City extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'phpcuong_directory_country_region_city';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('PHPCuong\CityDropdown\Model\ResourceModel\City');
    }

    /**
     * Retrieve city name
     *
     * If name is no declared, then city_default_name is used
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->getData('city_name');
        if ($name === null) {
            $name = $this->getData('city_default_name');
        }
        return $name;
    }

    /**
     * Load city by name
     *
     * @param string $name
     * @param string $regionId
     * @return $this
     */
    public function loadByName($name, $regionId)
    {
        $this->_getResource()->loadByName($this, $name, $regionId);
        return $this;
    }

    /**
     * Retrieve the City ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData('city_id');
    }

    /**
     * Retrieve the Visibility
     *
     * @return int
     */
    public function getVisibility()
    {
        return $this->getData('city_visibility');
    }
}

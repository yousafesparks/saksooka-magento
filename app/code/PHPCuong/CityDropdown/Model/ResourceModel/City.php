<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;

class City extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Table with localized city names
     *
     * @var string
     */
    protected $_cityNameTable;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $_localeResolver;

    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    protected $_localeLists;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param \Magento\Framework\Locale\ListsInterface $localeLists
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_localeResolver = $localeResolver;
        $this->_localeLists = $localeLists;
    }

    /**
     * Define main and locale city name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('phpcuong_directory_country_region_city', 'city_id');
        $this->_cityNameTable = $this->getTable('phpcuong_directory_country_region_city_name');
    }

    /**
     * Perform actions before object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterSave($object);

        if (!\Zend_Validate::is($object->getCityCode(), 'NotEmpty')) {
            throw new LocalizedException(
                __('Please enter the city code.')
            );
        }

        if (!\Zend_Validate::is($object->getRegionId(), 'NotEmpty')) {
            throw new LocalizedException(
                __('Please enter the region id.')
            );
        }

        if (!empty($object->getVisibility()) && !in_array($object->getVisibility(), ['1', '2'])) {
            throw new LocalizedException(
                __('Please enter a valid visibility.')
            );
        }

        $optionLocales = [];
        $optionLocalesLabel = $this->_localeLists->getOptionLocales();
        foreach ($optionLocalesLabel as $optionLocaleLabel) {
            $optionLocales[] = $optionLocaleLabel['value'];
        }

        if (!empty($object->getData('locale_view_id'))) {
            if (!\Zend_Validate::is($object->getCityName(), 'NotEmpty')) {
                throw new LocalizedException(
                    __('Please enter the city name.')
                );
            }

            if (!in_array($object->getData('locale_view_id'), $optionLocales)) {
                throw new LocalizedException(
                    __('Please enter a valid locale view id.')
                );
            }
        } else {
            if (!\Zend_Validate::is($object->getCityDefaultName(), 'NotEmpty')) {
                throw new LocalizedException(
                    __('Please enter the city name.')
                );
            }

            $this->cityNameDuplicateChecking($object);
        }

        return $this;
    }

    /**
     * Checking city name before saving
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    private function cityNameDuplicateChecking(\Magento\Framework\Model\AbstractModel $object)
    {
        $connection = $this->getConnection();
        $select = $connection->select()
            ->from($this->getMainTable())
            ->where('city_id <>?', (int)$object->getCityId())
            ->where('region_id =?', $object->getRegionId())
            ->where('city_code =?', $object->getCityCode());
        $result = $connection->fetchRow($select);
        if ($result) {
            throw new LocalizedException(
                __('City named "%1" for the region id: %2 has already existed.', $object->getCityDefaultName(), $object->getRegionId())
            );
        }

        return $this;
    }

    /**
     * Perform actions after object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterSave($object);
        $this->_saveCityName($object);
        return $this;
    }

    /**
     * Save City Name via Locale view id
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    private function _saveCityName(\Magento\Framework\Model\AbstractModel $object)
    {
        $locale = $object->getData('locale_view_id');
        $name = $object->getData('city_name');
        $cityId = $object->getCityId();
        if ($locale && $name && $cityId) {
            $connection = $this->getConnection();
            $connection->insertOnDuplicate($this->_cityNameTable, ['locale' => $locale, 'city_name' => $name, 'city_id' => $cityId]);
        }
        return $this;
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        $connection = $this->getConnection();

        $locale = $this->_localeResolver->getLocale();
        $systemLocale = \Magento\Framework\AppInterface::DISTRO_LOCALE_CODE;

        $cityField = $connection->quoteIdentifier($this->getMainTable() . '.' . $this->getIdFieldName());

        $condition = $connection->quoteInto('lcn.locale = ?', $locale);
        $select->joinLeft(
            ['lcn' => $this->_cityNameTable],
            "{$cityField} = lcn.city_id AND {$condition}",
            []
        );

        if ($locale != $systemLocale) {
            $nameExpr = $connection->getCheckSql('lcn.city_id is null', 'scn.city_name', 'lcn.city_name');
            $condition = $connection->quoteInto('scn.locale = ?', $systemLocale);
            $select->joinLeft(
                ['scn' => $this->_cityNameTable],
                "{$cityField} = scn.city_id AND {$condition}",
                ['city_name' => $nameExpr]
            );
        } else {
            $select->columns(['city_name'], 'lcn');
        }

        return $select;
    }

    /**
     * Load object by region id and default city name
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param int $regionId
     * @param string $value
     * @param string $field
     * @return $this
     */
    protected function _loadByCountry($object, $regionId, $value, $field)
    {
        $connection = $this->getConnection();
        $locale = $this->_localeResolver->getLocale();
        $joinCondition = $connection->quoteInto('cname.city_id = city.city_id AND cname.locale = ?', $locale);
        $select = $connection->select()->from(
            ['city' => $this->getMainTable()]
        )->joinLeft(
            ['cname' => $this->_cityNameTable],
            $joinCondition,
            ['city_name']
        )->where(
            'city.region_id = ?',
            $regionId
        )->where(
            "city.{$field} = ?",
            $value
        );

        $data = $connection->fetchRow($select);
        if ($data) {
            $object->setData($data);
        }

        $this->_afterLoad($object);

        return $this;
    }

    /**
     * Load data by region id and default city name
     *
     * @param \PHPCuong\CityDropdown\Model\City $city
     * @param string $cityName
     * @param string $regionId
     * @return $this
     */
    public function loadByName(\PHPCuong\CityDropdown\Model\City $city, $cityName, $regionId)
    {
        return $this->_loadByCountry($city, $regionId, (string)$cityName, 'city_default_name');
    }
}


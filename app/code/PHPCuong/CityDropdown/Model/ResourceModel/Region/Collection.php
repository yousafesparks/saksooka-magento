<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\ResourceModel\Region;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'region_id';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('PHPCuong\CityDropdown\Model\Region', 'PHPCuong\CityDropdown\Model\ResourceModel\Region');
    }

    /**
     * Redeclare after load method for specifying collection items original data
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        foreach ($this->_items as $item) {
            if ($item->getName()) {continue;}
            $item->setName($item->getDefaultName());
            $locale = $this->getRequestHttp()->getParam('locale_view_id');
            if ($locale) {
                $connection = $this->getConnection();
                $select = $connection->select()
                    ->from($this->getTable('directory_country_region_name'))
                    ->where('region_id =?', $item->getRegionId())
                    ->where('locale =?', $locale);
                $result = $connection->fetchRow($select);
                $item->setLocaleViewId($locale);
                if ($result) {
                    $item->setName($result['name']);
                }
            }
        }
        return $this;
    }

    /**
     * @return \Magento\Framework\App\Request\Http
     */
    protected function getRequestHttp()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\App\Request\Http::class);
    }

    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $propertyMap = [
            'value' => 'region_id',
            'title' => 'default_name',
            'country_id' => 'country_id',
        ];

        foreach ($this as $item) {
            $option = [];
            foreach ($propertyMap as $code => $field) {
                $option[$code] = $item->getData($field);
            }
            $option['label'] = $item->getName();
            $options[] = $option;
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['title' => null, 'value' => null, 'label' => __('Please select a region, state or province.')]
            );
        }
        return $options;
    }
}

<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\ResourceModel\City;

use Magento\Framework\App\ObjectManager;

/**
 * Class Collection
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'city_id';

    /**
     * Locale city name table name
     *
     * @var string
     */
    protected $_cityNameTable;

    /**
     * Country table name
     *
     * @var string
     */
    protected $_countryTable;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $_localeResolver;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactory $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param mixed $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_localeResolver = $localeResolver;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * Define main, country, locale city name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('PHPCuong\CityDropdown\Model\City', 'PHPCuong\CityDropdown\Model\ResourceModel\City');

        $this->_countryTable = $this->getTable('directory_country');
        $this->_cityNameTable = $this->getTable('phpcuong_directory_country_region_city_name');

        $this->addOrder('city_name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $this->addOrder('city_default_name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }

    /**
     * Initialize select object
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addFilterToMap('city_id', 'main_table.city_id');
        $locale = $this->getRequestHttp()->getParam('locale_view_id');
        if (!$locale) {
            $filters = $this->getRequestHttp()->getParam('filters');
            if (isset($filters['locale_view_id'])) {
                $locale = $filters['locale_view_id'];
            } else {
                $locale = $this->_localeResolver->getLocale();
            }
        }

        $this->addBindParam(':city_locale', $locale);
        $this->getSelect()->joinLeft(
            ['cname' => $this->_cityNameTable],
            'main_table.city_id = cname.city_id AND cname.locale = :city_locale',
            ['city_name']
        );

        return $this;
    }

    /**
     * Convert collection items to select options array
     *
     * @param array $regionIds
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $propertyMap = [
            'value' => 'city_id',
            'title' => 'city_default_name',
            'region_id' => 'region_id',
        ];

        foreach ($this as $item) {
            $option = [];
            foreach ($propertyMap as $code => $field) {
                $option[$code] = $item->getData($field);
            }
            $option['label'] = $item->getName();
            $options[] = $option;
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['title' => null, 'value' => null, 'label' => __('Please select a city, district or town.')]
            );
        }
        return $options;
    }

    /**
     * Add Country Id to the result
     *
     * @return $this
     */
    public function addCountryIdToResult()
    {
        $this->getSelect()->join(
            ['region' => $this->getTable('directory_country_region')],
            'main_table.region_id = region.region_id',
            ['region.country_id as country_id']
        );

        return $this;
    }

    /**
     * Add city name via locale to the result
     *
     * @return $this
     */
    public function addCityNameToResult()
    {
        foreach ($this->_items as $item) {
            $locale = $this->getRequestHttp()->getParam('locale_view_id');
            if ($locale) {
                $item->setLocaleViewId($locale);
            }
            if ($item->getCityName()) {continue;}
            $item->setCityName($item->getCityDefaultName());
        }
        return $this;
    }

    /**
     * Retrieve collection items
     *
     * @return \Magento\Framework\DataObject[]
     */
    public function getItems()
    {
        $this->load();
        $this->addCityNameToResult();
        return $this->_items;
    }

    /**
     * @return \Magento\Framework\App\Request\Http
     */
    protected function getRequestHttp()
    {
        return ObjectManager::getInstance()->get(\Magento\Framework\App\Request\Http::class);
    }
}

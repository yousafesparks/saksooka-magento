<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\ResourceModel\Address\Attribute\Source;

class City extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @var \PHPCuong\CityDropdown\Model\ResourceModel\City\CollectionFactory
     */
    protected $_citiesFactory;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \PHPCuong\CityDropdown\Model\ResourceModel\City\CollectionFactory $citiesFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \PHPCuong\CityDropdown\Model\ResourceModel\City\CollectionFactory $citiesFactory
    ) {
        $this->_citiesFactory = $citiesFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve all city options
     *
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            $cityOptionArray = $this->_createCitiesCollection()->addFieldToFilter('main_table.city_visibility', \PHPCuong\CityDropdown\Model\Visibility::VISIBILITY_VISIBLE);
            $this->_options = $cityOptionArray->load()->toOptionArray();
        }
        return $this->_options;
    }

    /**
     * @return \PHPCuong\CityDropdown\Model\ResourceModel\City\Collection
     */
    protected function _createCitiesCollection()
    {
        return $this->_citiesFactory->create();
    }
}

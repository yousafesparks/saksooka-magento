<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\ResourceModel\Address\Attribute\Backend;

/**
 * Address city attribute backend
 *
 */
class City extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * @var \PHPCuong\CityDropdown\Model\CityFactory
     */
    protected $_cityFactory;

    /**
     * @param \PHPCuong\CityDropdown\Model\CityFactory $cityFactory
     */
    public function __construct(\PHPCuong\CityDropdown\Model\CityFactory $cityFactory)
    {
        $this->_cityFactory = $cityFactory;
    }

    /**
     * Prepare object for save
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     */
    public function beforeSave($object)
    {
        $city = $object->getData('city');
        if (is_numeric($city)) {
            $cityModel = $this->_createCityInstance();
            $cityModel->load($city);
            if ($cityModel->getId() && $object->getRegionId() == $cityModel->getRegionId()) {
                $object->setCityId($cityModel->getId())->setCity($cityModel->getName());
            }
        }
        return $this;
    }

    /**
     * @return \PHPCuong\CityDropdown\Model\City
     */
    protected function _createCityInstance()
    {
        return $this->_cityFactory->create();
    }
}

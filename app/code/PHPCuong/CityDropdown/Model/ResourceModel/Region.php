<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

namespace PHPCuong\CityDropdown\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;

class Region extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Table with localized region names
     *
     * @var string
     */
    protected $_regionNameTable;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $_localeResolver;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $httpRequest;

    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    protected $_localeLists;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param \Magento\Framework\App\Request\Http $httpRequest
     * @param \Magento\Framework\Locale\ListsInterface $localeLists
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\App\Request\Http $httpRequest,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_localeResolver = $localeResolver;
        $this->httpRequest = $httpRequest;
        $this->_localeLists = $localeLists;
    }

    /**
     * Define main and locale region name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('directory_country_region', 'region_id');
        $this->_regionNameTable = $this->getTable('directory_country_region_name');
    }

    /**
     * Perform actions before object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterSave($object);

        if (!\Zend_Validate::is($object->getCode(), 'NotEmpty')) {
            throw new LocalizedException(
                __('Please enter the region code.')
            );
        }

        if (!\Zend_Validate::is($object->getCountryId(), 'NotEmpty')) {
            throw new LocalizedException(
                __('Please enter the country id.')
            );
        }

        if (!empty($object->getVisibility()) && !in_array($object->getVisibility(), ['1', '2'])) {
            throw new LocalizedException(
                __('Please enter a valid visibility.')
            );
        }

        $optionLocales = [];
        $optionLocalesLabel = $this->_localeLists->getOptionLocales();
        foreach ($optionLocalesLabel as $optionLocaleLabel) {
            $optionLocales[] = $optionLocaleLabel['value'];
        }

        if (!empty($object->getData('locale_view_id'))) {
            if (!\Zend_Validate::is($object->getName(), 'NotEmpty')) {
                throw new LocalizedException(
                    __('Please enter the region name.')
                );
            }

            if (!in_array($object->getData('locale_view_id'), $optionLocales)) {
                throw new LocalizedException(
                    __('Please enter a valid locale view id.')
                );
            }
        } else {
            if (!\Zend_Validate::is($object->getDefaultName(), 'NotEmpty')) {
                throw new LocalizedException(
                    __('Please enter the region name.')
                );
            }

            $this->regionNameDuplicateChecking($object);
        }

        return $this;
    }

    /**
     * Checking region name before saving
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    private function regionNameDuplicateChecking(\Magento\Framework\Model\AbstractModel $object)
    {
        $connection = $this->getConnection();
        $select = $connection->select()
            ->from($this->getMainTable())
            ->where('region_id <>?', (int)$object->getRegionId())
            ->where('country_id =?', $object->getCountryId())
            ->where('code =?', $object->getCode());
        $result = $connection->fetchRow($select);
        if ($result) {
            throw new LocalizedException(
                __('Region named "%1" for the country id: %2 has already existed.', $object->getDefaultName(), $object->getCountryId())
            );
        }

        return $this;
    }

    /**
     * Perform actions after object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterSave($object);
        $this->_saveName($object);
        return $this;
    }

    /**
     * Perform actions after object load
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterLoad($object);
        $this->getName($object);
        return $this;
    }

    /**
     * Retrieve the region name via locale
     *
     * @return string
     */
    public function getName(\PHPCuong\CityDropdown\Model\Region $region)
    {
        $region->setName($region->getDefaultName());
        $locale = $this->httpRequest->getParam('locale_view_id', false);
        if ($locale) {
            $connection = $this->getConnection();
            $select = $connection->select()
                ->from($this->getTable('directory_country_region_name'))
                ->where('region_id =?', $region->getRegionId())
                ->where('locale =?', $locale);
            $result = $connection->fetchRow($select);
            if ($result) {
                $region->setName($result['name']);
                $region->setLocaleViewId($locale);
            }
        }
        return $region->getName();
    }

    /**
     * Save Region Name via Locale view id
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    private function _saveName(\Magento\Framework\Model\AbstractModel $object)
    {
        $locale = $object->getData('locale_view_id');
        $name = $object->getData('name');
        $regionId = $object->getRegionId();
        if ($locale && $name && $regionId) {
            $connection = $this->getConnection();
            $connection->insertOnDuplicate($this->_regionNameTable, ['locale' => $locale, 'name' => $name, 'region_id' => $regionId]);
        }
        return $this;
    }
}

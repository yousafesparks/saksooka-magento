<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: https://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Aramexshipping
 * @author   CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright   Copyright CEDCOMMERCE (https://cedcommerce.com/)
 * @license     https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Ced\Aramexshipping\Helper;

/**
 * Class Data
 * @package Ced\Aramexshipping\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $currencyFactory;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\Currency $currencyFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\Currency $currencyFactory
    )
    {
        $this->_storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;
        parent::__construct($context);
    }

    /**
     * Convert currency
     */
    public function currencyConvert($price, $from, $to, $output = '', $round = null)
    {
        $from = strtoupper($from);
        $to = strtoupper($to);

        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
        $currentCurrencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();

        if ('_BASE_' == $from) {
            $from = $baseCurrencyCode;
        } elseif ('_CURRENT_' == $from) {
            $from = $currentCurrencyCode;
        }

        if ('_BASE_' == $to) {
            $to = $baseCurrencyCode;
        } elseif ('_CURRENT_' == $to) {
            $to = $currentCurrencyCode;
        }

        $output = strtolower($output);

        $error = false;
        $result = array('price' => $price, 'currency' => $from);

        if ($from != $to) {
            $allowedCurrencies = $this->currencyFactory->getConfigAllowCurrencies();
            $rates = $this->currencyFactory
                            ->getCurrencyRates(
                                $baseCurrencyCode,
                                array_values($allowedCurrencies)
                            );

            if (empty($rates) || !isset($rates[$from]) || !isset($rates[$to])) {
                $error = true;
            } elseif (empty($rates[$from]) || empty($rates[$to])) {
                $error = true;
            }

            if ($error) {
                if (isset($result[$output])) {
                    return $result[$output];
                } else {
                    return $result;
                }
            }

            $result = array(
                'price' => ($price * $rates[$to]) / $rates[$from],
                'currency' => $to
            );
        }

        if (is_int($round)) {
            $result['price'] = round($result['price'], $round);
        }

        if (isset($result[$output])) {
            return $result[$output];
        }

        return $result;
    }

    /**
     * @param $price
     * @param $currentcurrency
     * @return array|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function convertRateCurrency($price, $currentcurrency)
    {
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
        $result = array('price' => $price, 'currency' => $currentcurrency);

        if ($currentcurrency != $baseCurrencyCode) {
            $result = $this->currencyConvert($price, $currentcurrency, $baseCurrencyCode);
        }
        return $result;
    }
}

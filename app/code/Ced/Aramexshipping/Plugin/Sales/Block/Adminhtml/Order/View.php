<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Aramexshipping
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (https://cedcommerce.com/)
 * @license      https://cedcommerce.com/license-agreement.txt
 */

namespace Ced\Aramexshipping\Plugin\Sales\Block\Adminhtml\Order;

use Magento\Shipping\Block\Adminhtml\View as ShipmentView;
use Magento\Framework\UrlInterface;

/**
 * Class View
 * @package Ced\Aramexshipping\Plugin\Sales\Block\Adminhtml\Order
 */
class View
{
    /** @var \Magento\Framework\UrlInterface */
    protected $_urlBuilder;    

    /**
     * View constructor.
     * @param UrlInterface $url 
     */
    public function __construct(
        UrlInterface $url
    )
    {
        $this->_urlBuilder = $url; 
    }

    /**
     * @param ShipmentView $view
     */
    public function beforeSetLayout(ShipmentView $view)
    {
        $shipment = $view->getShipment();
        $shipment_id = $shipment->getId();
        $shipping_method = $shipment->getOrder()->getShippingMethod();
        if ($shipment_id && strpos($shipping_method, 'aramexshipping') !== false) {
            $url = $this->_urlBuilder->getUrl('aramex/shipment/print', ['id' => $shipment_id]);

            $view->addButton(
                'aramex_label',
                [
                    'label' => __('Aramex Label'),
                    'class' => 'aramex-label',
                    'target' => '_blank',
                    'onclick' => 'window.open(\'' . $url . '\')'
                ]
            );
        }
    }
}

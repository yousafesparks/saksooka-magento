<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Aramexshipping
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (https://cedcommerce.com/)
 * @license      https://cedcommerce.com/license-agreement.txt
 */

namespace Ced\Aramexshipping\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\Store;

/**
 * Class SalesShipment
 * @package Ced\Aramexshipping\Observer
 */
class SalesShipment implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $_moduleReader;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Sales\Model\Order\Shipment\TrackFactory
     */
    protected $trackFactory;

    /**
     * SalesShipment constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     * @param \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Store\Model\Information $storeInfo
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_moduleReader = $moduleReader;
        $this->trackFactory = $trackFactory;
        $this->_storeManager = $storeManager;
        $this->authSession = $authSession;
        $this->_storeInfo = $storeInfo;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \SoapFault
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $cod_amount = 0;
        $shippingMethod = $order->getShippingMethod();
        $s = explode("~", $shippingMethod);
        $shippingMethod = $s [0];
        if (strpos($shippingMethod, 'aramexshipping') !== false) {
            $wsdlPath = $this->_moduleReader->getModuleDir('etc', 'Ced_Aramexshipping') . '/' . 'wsdl';
            $wsdl = $wsdlPath . '/' . 'shipping-services-api-wsdl.wsdl';

            $account_number = $this->_scopeConfig->getValue('carriers/aramexshipping/account_number',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $account_country_code = $this->_scopeConfig->getValue('carriers/aramexshipping/account_country_code',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $account_entity = $this->_scopeConfig->getValue('carriers/aramexshipping/account_entity',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $account_pin = $this->_scopeConfig->getValue('carriers/aramexshipping/account_pin',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $account_username = $this->_scopeConfig->getValue('carriers/aramexshipping/username',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $account_password = $this->_scopeConfig->getValue('carriers/aramexshipping/password',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $weight_unit = $this->_scopeConfig->getValue('carriers/aramexshipping/unit_of_measure',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_country = $this->_scopeConfig->getValue('shipping/origin/country_id',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_city = $this->_scopeConfig->getValue('shipping/origin/city',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_zip = $this->_scopeConfig->getValue('shipping/origin/postcode',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_street1 = $this->_scopeConfig->getValue('shipping/origin/street_line1',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_street2 = $this->_scopeConfig->getValue('shipping/origin/street_line2',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_phone = $this->_scopeConfig->getValue('carriers/aramexshipping/phone_number',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $store_cell = $this->_scopeConfig->getValue('carriers/aramexshipping/cell_number',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            // customer details
            $shippingaddress = $order->getShippingAddress();
            $customer_country = $shippingaddress->getCountryId();
            $customer_postcode = $shippingaddress->getPostcode();
            $customer_postcode =  \Ced\Aramexshipping\Model\Carrier\Aramex::CUSTOMER_DEFAULT_ZIP_CODE;
            $customer_city = $shippingaddress->getCity();
            $items = $order->getAllItems();

            if($store_country == $customer_country){
                $product_group = 'DOM';
                $product_type = 'OND';
            }
            else{
                $product_group = 'EXP';
                $product_type = 'PDX';
            }

            $totalWeight = 0;
            $totalItems = 0;
            $totalPrice = 0;
            $description = '';
            foreach ($items as $item) {
                $qty = $item->getQtyOrdered();
                if ($item->getWeight() != 0) {
                    $weight = $item->getWeight() * $qty;
                } else {
                    $weight = 0.5 * $qty;
                }

                $totalWeight += $weight;
                $totalItems += $qty;
                $totalPrice += $item->getBaseRowTotal();
                $description .= $item->getProduct()->getName() . "||";

            }

            if($order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery'){
                $cod_amount = $totalPrice;
            }

            $admin = $this->authSession->getUser();

            $params = array(
            'Shipments' => array(
                'Shipment' => array(
                        'Shipper'   => array(
                            'Reference1' => $order->getIncrementId(),
                            'AccountNumber' => $account_number,
                            'PartyAddress'  => array(
                                'Line1' => $store_street1,
                                'Line2' => $store_street2,
                                'Line3' => '',
                                'City' => $store_city,
                                'StateOrProvinceCode' => '',
                                'PostCode' => $store_zip,
                                'CountryCode' => $store_country
                            ),
                            'Contact'       => array(
                                'Department'            => '',
                                'PersonName'            => $admin->getData('firstname').' '.$admin->getData('lastname') ,
                                'Title'                 => '',
                                'CompanyName'           => 'Aramex',
                                'PhoneNumber1'          => $store_phone,
                                'PhoneNumber1Ext'       => '',
                                'PhoneNumber2'          => '',
                                'PhoneNumber2Ext'       => '',
                                'FaxNumber'             => '',
                                'CellPhone'             => $store_cell,
                                'EmailAddress'          => $admin->getData('email'),
                                'Type'                  => ''
                            ),
                        ),

                        'Consignee' => array(
                            'Reference1'    => $order->getIncrementId (),
                            'AccountNumber' => '',
                            'PartyAddress'  => array(
                                'Line1' => $shippingaddress->getStreet ()[0],
                                'City' => $customer_city,
                                'StateOrProvinceCode' => '',
                                'PostCode' => $customer_postcode,
                                'CountryCode' => $customer_country
                            ),

                            'Contact'       => array(
                                'Department' => '',
                                'PersonName' => $shippingaddress->getFirstname (),
                                'Title' => '',
                                'CompanyName' => $shippingaddress->getCompany() ? $shippingaddress->getCompany() : $shippingaddress->getFirstname(),
                                'PhoneNumber1' => $shippingaddress->getTelephone (),
                                'FaxNumber' => '',
                                'CellPhone' => $shippingaddress->getTelephone (),
                                'EmailAddress' => $shippingaddress->getEmail (),
                                'Type' => ''
                            ),
                        ),

                        'ThirdParty' => array(
                            'Reference1'    => '',
                            'Reference2'    => '',
                            'AccountNumber' => '',
                            'PartyAddress'  => array(
                                'Line1'                 => '',
                                'Line2'                 => '',
                                'Line3'                 => '',
                                'City'                  => '',
                                'StateOrProvinceCode'   => '',
                                'PostCode'              => '',
                                'CountryCode'           => ''
                            ),
                            'Contact'       => array(
                                'Department'            => '',
                                'PersonName'            => '',
                                'Title'                 => '',
                                'CompanyName'           => '',
                                'PhoneNumber1'          => '',
                                'PhoneNumber1Ext'       => '',
                                'PhoneNumber2'          => '',
                                'PhoneNumber2Ext'       => '',
                                'FaxNumber'             => '',
                                'CellPhone'             => '',
                                'EmailAddress'          => '',
                                'Type'                  => ''
                            ),
                        ),

                        'Reference1'                => $order->getIncrementId (),
                        'ForeignHAWB'               => $order->getIncrementId() . rand(10, 100),
                        'TransportType'             => 0,
                        'ShippingDateTime'          => time(),
                        'DueDate'                   => time(),
                        'PickupLocation'            => 'Reception',
                        'PickupGUID'                => '',
                        'Comments'                  => $order->getIncrementId (),
                        'AccountingInstrcutions'    => '',
                        'OperationsInstructions'    => '',

                        'Details' => array(
                           'ActualWeight' => array(
                                'Value' => $totalWeight,
                                'Unit' => $weight_unit
                            ),

                            'ProductGroup' => $product_group,
                            'ProductType' => $product_type,
                            'PaymentType' => 'P',
                            'PaymentOptions' => '',
                            'Services' => '',
                            'NumberOfPieces' => $totalItems,
                            'DescriptionOfGoods' => $description,
                            'GoodsOriginCountry' => $store_country,

                            'CashOnDeliveryAmount'  => array(
                                'Value' => 0,
                                'CurrencyCode' => $this->_storeManager->getStore()->getCurrentCurrencyCode()
                            ),

                            'InsuranceAmount'       => array(
                                'Value'                 => 0,
                                'CurrencyCode'          => ''
                            ),

                            'CollectAmount'         => array(
                                'Value'                 => 0,
                                'CurrencyCode'          => ''
                            ),

                            'CashAdditionalAmount'  => array(
                                'Value'                 => 0,
                                'CurrencyCode'          => ''
                            ),

                            'CashAdditionalAmountDescription' => '',

                            'CustomsValueAmount' => array(
                                'Value'                 => 0,
                                'CurrencyCode'          => ''
                            ),

                            'Items'                 => array(

                            )
                        ),
                ),
        ),

            'ClientInfo'            => array(
                                        'AccountCountryCode' => $account_country_code,
                                        'AccountEntity' => $account_entity,
                                        'AccountNumber' => $account_number,
                                        'AccountPin' => $account_pin,
                                        'UserName' => $account_username,
                                        'Password' => $account_password,
                                        'Version' => 'v1.0'
                                    ),

            'Transaction'           => array(
                                        'Reference1'            => '001',
                                        'Reference2'            => '',
                                        'Reference3'            => '',
                                        'Reference4'            => '',
                                        'Reference5'            => '',
                                    ),
            'LabelInfo'             => array(
                                        'ReportID'              => 9201,
                                        'ReportType'            => 'URL',
            ),
    );

    if($order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery'){
        $params['Shipments']['Shipment']['Details']['Services'] = 'CODS';
    }
    if($order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery'){
        $params['Shipments']['Shipment']['Details']['CashOnDeliveryAmount']['Value'] = $cod_amount;
    }

    $params ['Shipments'] ['Shipment'] ['Details'] ['Items'] [] = [
                'PackageType' => 'Box',
                'Quantity' => $totalItems,
                'Weight' => [
                    'Value' => $totalWeight,
                    'Unit' => $weight_unit
                ],
                'Comments' => 'Docs',
                'Reference' => ''
            ];

            $soapClient = new \SoapClient($wsdl);
            try {
                $auth_call = $soapClient->CreateShipments($params);
                if ($auth_call->HasErrors) {
                    $notifica = $auth_call->Shipments->ProcessedShipment->Notifications->Notification;

                    $message = ''; 
                    foreach ($notifica as $value) 
                        $message .='   '. $value->Message;

                    throw new \Exception(__( $message));
                } else {
                    $awbno = $auth_call->Shipments->ProcessedShipment->ID;
                    $shipment = $observer->getEvent()->getShipment();
                    $track = $this->trackFactory->create()->setNumber(
                        $awbno
                    )->setCarrierCode(
                        'aramexshipping'
                    )->setTitle(
                        'Aramex Shipping'
                    );
                    $shipment->addTrack($track);
                }
            } catch (\Exception $e) {

               // print_r($params);die($e);
                throw new \Exception(__($e->getMessage()));
            }
        }
    }
}

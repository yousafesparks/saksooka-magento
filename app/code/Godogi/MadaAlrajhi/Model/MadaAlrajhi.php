<?php
namespace Godogi\MadaAlrajhi\Model;

/**
 * Pay In Store payment method model
 */
class MadaAlrajhi extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'madaalrajhi';
}

<?php
namespace Godogi\MadaAlrajhi\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    public function generateRandomString($length = 5) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function encryptAES($str,$key)
    {
        $str = $this->pkcs5_pad($str);
        $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
        $iv="PGKEYENCDECIVSPC";
        $encrypted = openssl_encrypt($str, "aes-256-cbc",$key, OPENSSL_ZERO_PADDING, $iv);
        $encrypted = base64_decode($encrypted);
        $encrypted = unpack('C*', ($encrypted));
        $encrypted = $this->byteArray2Hex($encrypted);
        $encrypted = urlencode($encrypted);

        return $encrypted;
    }
    public function decryptAES($code, $key)
    {
        $code = $this->hex2ByteArray(trim($code));
        $code = $this->byteArray2String($code);
        $iv = "PGKEYENCDECIVSPC";
        $code = base64_encode($code);
        $decrypted = openssl_decrypt($code, 'AES-256-CBC', $key, OPENSSL_ZERO_PADDING,$iv);
        return $this->pkcs5_unpad($decrypted);
    }

    public function pkcs5_pad($text)
    {
        //$blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $blocksize = openssl_cipher_iv_length ($cipher="AES-256-CBC");
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    public function pkcs5_unpad($text)
    {
        $pad = ord($text[($len = strlen($text)) - 1]);
        $len = strlen($text);
        $pad = ord($text[$len-1]);
        return substr($text, 0, strlen($text) - $pad);
    }

    public function byteArray2Hex($byteArray) {
        $chars = array_map("chr", $byteArray);
        $bin = join($chars);
        return bin2hex($bin);
    }

    public function hex2ByteArray($hexString) {
        $string = hex2bin($hexString);
        return unpack('C*', $string);
    }

    public function byteArray2String($byteArray) {
        $chars = array_map("chr", $byteArray);
        return join($chars);
    }
}

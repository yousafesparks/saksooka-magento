define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'uiRegistry',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/action/redirect-on-success'
    ],
    function ($, Component, url, selectPaymentMethodAction, customerData, errorProcessor, fullScreenLoader, checkoutData, quote, checkoutDataResolver, registry, additionalValidators, redirectOnSuccessAction) {
        'use strict';
        return Component.extend({
            redirectAfterPlaceOrder: true, //This is important, so the customer isn't redirected to success.phtml by default
            defaults: {
                template: 'Godogi_MadaAlrajhi/payment/madaalrajhi'
            },
            getEmail: function () {
                if(quote.guestEmail) return quote.guestEmail;
                else return window.checkoutConfig.customerData.email;
            },
            /**
             * Place order.
             */
            placeOrder: function (data, event) {
              var dataForm = jQuery('#madaalrajhi-form');
              var self = this;
              dataForm.mage('validation', {});
              dataForm.validation();
              if(dataForm.validation('isValid')){
                  if (event) {
                      event.preventDefault();
                  }
                  if (this.validate() && additionalValidators.validate() && this.isPlaceOrderActionAllowed() === true) {
                      this.isPlaceOrderActionAllowed(false);
                      var custom_controller_url = url.build('madaalrajhi/madaalrajhi/postdata');
                      $.ajax( custom_controller_url , // create checkout on MadaAlrajhi server
                      {
                          type: 'POST',
                          dataType: 'json',
                          showLoader: true,
                          data: {
                              'email': self.getEmail(),
                              'paymentBrand': $("#paymentBrandMadaAlrajhi").val(),
                              'cardHolder': $("#cardHolderMadaAlrajhi").val(),
                              'cardNumber': $("#cardNumberMadaAlrajhi").val().replace(/\s/g, ''),
                              'cardExpiryMonth': $("#cardExpiryMonthMadaAlrajhi").val(),
                              'cardExpiryYear': $("#cardExpiryYearMadaAlrajhi").val(),
                              'cardCvv': $("#cardCvvMadaAlrajhi").val()
                          },
                          success: function (data, status, xhr) {
                            if(data.result){
                              if(data.result[0].status == 1){
                                  console.log("returned result status 1");
                                  console.log(data);
                                  var result = data.result[0].result;

                                  var index = result.indexOf(":");
                                  var id = result.substr(0, index);
                                  var url = result.substr(index + 1);

                                  window.location.href = url;

                              } else if(data.result[0].errorText){
                                  console.log("returned error text");
                                  console.log(data);
                                  var errorText = data.result[0].errorText;
                                  $('#madaalrajhi-errors').empty();
                                  $('#madaalrajhi-errors').append('<p class="title">'+ $.mage.__('Please solve the following inputs') +':</p>');
                                  var errorHtml = '';
                                  errorHtml += '<ul>';
                                  errorHtml += '<li>'+ $.mage.__(errorText) +'</li>';
                                  errorHtml += '</ul>';
                                  $('#madaalrajhi-errors').append(errorHtml);
                                  $('#madaalrajhi-errors').show(200);
                                  $('html, body').animate({
                                      scrollTop: $("#madaalrajhi-errors").offset().top
                                  }, 500);
                              }
                            } else {
                                console.log("didn't return anything!");
                                console.log(data);
                                $('#madaalrajhi-errors').empty();
                                $('#madaalrajhi-errors').append('<p class="title">'+ $.mage.__('Please solve the following inputs') +':</p>');
                                var errorHtml = '';
                                errorHtml += '<ul>';
                                errorHtml += '<li>'+ $.mage.__('Something strange happened, please contact support.') +'</li>';
                                errorHtml += '</ul>';
                                $('#madaalrajhi-errors').append(errorHtml);
                                $('#madaalrajhi-errors').show(200);
                                $('html, body').animate({
                                    scrollTop: $("#madaalrajhi-errors").offset().top
                                }, 500);
                            }
                            self.isPlaceOrderActionAllowed(true);
                          },
                          error: function (jqXhr, textStatus, errorMessage) {
                              console.log(jqXhr);
                              return false;
                          }
                      });
                  }
                  return false;
              }else{
                  return false;
              }
            },
            /**
             * After place order callback
             */
            afterPlaceOrder: function () {

            },
            /**
             * Initialize view.
             *
             * @return {exports}
             */
            initialize: function () {
                $( document ).ready(function() {
                    var existCondition = setInterval(function() {
                       if ($('#paymentBrandMadaAlrajhi').length) {
                          clearInterval(existCondition);
                          $('#cardNumberMadaAlrajhi').on('input', function() {

                              $('#cardNumberMadaAlrajhi').removeClass('shake');
                              var input = $('#cardNumberMadaAlrajhi').val().split('');
                              var arrayLength = input.length;
                              var output = [];
                              var shake = false;
                              var allowedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                              for (var i = 0; i < arrayLength; i++) {
                                  if (allowedCharacters.includes(input[i])) {
                                      output.push(input[i]);
                                  } else {
                                      shake = true;
                                  }
                              }
                              if(shake){
                                $('#cardNumberMadaAlrajhi').addClass('shake');
                              }

                              var outputString = output.join("");
                              $('#cardNumberMadaAlrajhi').val(outputString);

                              var creditCardNumberVal = $('#cardNumberMadaAlrajhi').val();
                              creditCardNumberVal = creditCardNumberVal.replace(/\s/g, '');
                              if(creditCardNumberVal.match(/^-{0,1}\d+$/)){
                                  if(creditCardNumberVal.length > 16 ){
                                      var output = [];
                                      var input = $('#cardNumberMadaAlrajhi').val().split('');
                                      for (var i = 0; i < 16; i++) {
                                          output.push(input[i]);
                                      }
                                      $('#cardNumberMadaAlrajhi').addClass('shake');
                                      var outputString = output.join("");
                                      $('#cardNumberMadaAlrajhi').val(outputString);
                                  } else if(creditCardNumberVal.length == 16){
                                      $('#cardNumberMadaAlrajhi').addClass('valid');
                                      $('#cardNumberMadaAlrajhi').removeClass('unvalid');
                                  }else{
                                      $('#cardNumberMadaAlrajhi').addClass('unvalid');
                                      $('#cardNumberMadaAlrajhi').removeClass('valid');
                                  }
                              }else{
                                  $('#cardNumberVisaMaster').addClass('unvalid');
                                  $('#cardNumberVisaMaster').removeClass('valid');
                              }
                          });



                          $('#cardCvvMadaAlrajhi').on('input', function() {
                              $('#cardCvvMadaAlrajhi').removeClass('shake');
                              var input = $('#cardCvvMadaAlrajhi').val().split('');
                              var arrayLength = input.length;
                              var output = [];
                              var shake = false;
                              var allowedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                              for (var i = 0; i < arrayLength; i++) {
                                  if (allowedCharacters.includes(input[i])) {
                                      output.push(input[i]);
                                  } else {
                                      shake = true;
                                  }
                              }
                              if(shake){
                                $('#cardCvvMadaAlrajhi').addClass('shake');
                              }

                              var outputString = output.join("");
                              $('#cardCvvMadaAlrajhi').val(outputString);

                              var creditCardNumberVal = $('#cardCvvMadaAlrajhi').val();
                              creditCardNumberVal = creditCardNumberVal.replace(/\s/g, '');
                              if(creditCardNumberVal.match(/^-{0,1}\d+$/)){
                                  if(creditCardNumberVal.length > 4 ){
                                      var output = [];
                                      var input = $('#cardCvvMadaAlrajhi').val().split('');
                                      for (var i = 0; i < 4; i++) {
                                          output.push(input[i]);
                                      }
                                      $('#cardCvvMadaAlrajhi').addClass('shake');
                                      var outputString = output.join("");
                                      $('#cardCvvMadaAlrajhi').val(outputString);
                                  }
                              }

                          });






                        }
                    }, 100);
                });
                var billingAddressCode,
                    billingAddressData,
                    defaultAddressData;
                this._super().initChildren();
                quote.billingAddress.subscribe(function (address) {
                    this.isPlaceOrderActionAllowed(address !== null);
                }, this);
                checkoutDataResolver.resolveBillingAddress();
                billingAddressCode = 'billingAddress' + this.getCode();
                registry.async('checkoutProvider')(function (checkoutProvider) {
                    defaultAddressData = checkoutProvider.get(billingAddressCode);
                    if (defaultAddressData === undefined) {
                        // Skip if payment does not have a billing address form
                        return;
                    }
                    billingAddressData = checkoutData.getBillingAddressFromData();
                    if (billingAddressData) {
                        checkoutProvider.set(
                            billingAddressCode,
                            $.extend(true, {}, defaultAddressData, billingAddressData)
                        );
                    }
                    checkoutProvider.on(billingAddressCode, function (providerBillingAddressData) {
                        checkoutData.setBillingAddressFromData(providerBillingAddressData);
                    }, billingAddressCode);
                });
                return this;
            }
        });
    }
);

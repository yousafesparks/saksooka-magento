<?php
namespace Godogi\MadaHyperpay\Controller\MadaHyperpay;

use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class PostData extends \Magento\Framework\App\Action\Action
{
    protected $_logger;
    protected $_cart;
    protected $_checkoutSession;
    protected $_order;
    protected $_customerSession;
    protected $_storeManager;
    protected $_resultJsonFactory;
    protected $_scopeConfig;

    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $logger,
        Session $checkoutSession,
        Order $order,
        CustomerSession $customerSession,
        Cart $cart,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig
    ){
        parent::__construct($context);
        $this->_logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_order = $order;
        $this->_customerSession = $customerSession;
        $this->_cart = $cart;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hyperpay.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        try {
            $logger->info('=== Post Data Execute ====================================');
            $grandTotal = $this->_cart->getQuote()->getGrandTotal();
            $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
            $useTest = $this->_scopeConfig->getValue('payment/madahyperpay/test', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if( $useTest == 1 ){
                $madahyperpayUrl = 'https://test.oppwa.com/';
            } else {
                $madahyperpayUrl = 'https://oppwa.com/';
            }
            $this->_checkoutSession->setMadaHyperpayGrandTotal($grandTotal);
            $this->_checkoutSession->setMadaHyperpayCurrencyCode($currencyCode);
            $result = json_decode($this->request(), true);
            $this->_checkoutSession->setMadaHyperpayChekoutId($result['id']);
            $post_data = array('result' => $result, 'url' => $madahyperpayUrl, 'amount' => $grandTotal, 'currency'  => $currencyCode, 'baseurl' => $this->_storeManager->getStore()->getBaseUrl());
            $result = $this->_resultJsonFactory->create();
            return $result->setData($post_data);
        } catch (\Exception $e) {
            $logger->info('*** Post Data Execute Exception***');
            $logger->info($e->getMessage());
        }
    }

    public function request() {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hyperpay.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('=== Post Data Request ===');
        try {
            $order = $this->_checkoutSession->getLastRealOrder();
            $lastOrder = $this->_order->getCollection()->getLastItem();
            $lastOrderId = $lastOrder->getIncrementId();
            if (strpos($lastOrderId, '-') !== false) {
                $orderIncrementId = explode("-", $lastOrderId);
                $orderIncrementId = $orderIncrementId[0]."-".sprintf('%07d', ($orderIncrementId[1] + 1));
            } else {
                $orderIncrementId = $lastOrderId + 1 ;
            }


            $logger->info($orderIncrementId);

            $grandTotal = $this->_cart->getQuote()->getGrandTotal();
            $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
            $entityId = $this->_scopeConfig->getValue('payment/madahyperpay/entityid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $bearer = $this->_scopeConfig->getValue('payment/madahyperpay/bearer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $useTest = $this->_scopeConfig->getValue('payment/madahyperpay/test', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $useModeExternal = $this->_scopeConfig->getValue('payment/madahyperpay/modeexternal', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if( $useTest == 1 ){
                $madahyperpayUrl = 'https://test.oppwa.com/';
            } else {
                $madahyperpayUrl = 'https://oppwa.com/';
            }

          	$url = $madahyperpayUrl . "v1/payments";
            $randomString = $this->generateRandomString(5);
          	$data = "entityId=" . $entityId .
                          "&amount=" . number_format((float)$grandTotal, 2, '.', '') .
                          "&currency=" . $currencyCode .
                          "&paymentBrand=" . $this->getRequest()->getPostValue('paymentBrand') .
                          "&paymentType=DB".
                          "&card.number=" . $this->getRequest()->getPostValue('cardNumber') .
                          "&card.holder=" . $this->getRequest()->getPostValue('cardHolder') .
                          "&card.expiryMonth=" . $this->getRequest()->getPostValue('cardExpiryMonth') .
                          "&card.expiryYear=" . $this->getRequest()->getPostValue('cardExpiryYear') .
                          "&card.cvv=" . $this->getRequest()->getPostValue('cardCvv') .
                          "&merchantTransactionId=" . $randomString . "-" .$orderIncrementId .
                          "&customer.email=". $this->getRequest()->getPostValue('email') .
                          "&shopperResultUrl=". $this->_storeManager->getStore()->getBaseUrl() ."/madahyperpay/madahyperpay/checkpayment/";
            if( $useTest == 1 ){
              if($useModeExternal == 1){
                  $data .= "&testMode=EXTERNAL";
              }
            }
            $this->_customerSession->setCustomerEmailAddress($this->getRequest()->getPostValue('email'));
            $ch = curl_init();
          	curl_setopt($ch, CURLOPT_URL, $url);
          	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                             'Authorization:Bearer ' . $bearer));
          	curl_setopt($ch, CURLOPT_POST, 1);
          	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            if( $useTest == 1 ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            } else {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            }
          	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          	$responseData = curl_exec($ch);
          	if(curl_errno($ch)) {
          		return curl_error($ch);
          	}
          	curl_close($ch);
          	return $responseData;
        } catch (\Exception $e) {
            $logger->info('*** Post Data Request Exception***');
            $logger->info($e->getMessage());
        }
    }
    public function generateRandomString($length = 5) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

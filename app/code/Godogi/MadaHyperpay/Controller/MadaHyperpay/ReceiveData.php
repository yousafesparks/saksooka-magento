<?php
namespace Godogi\MadaHyperpay\Controller\MadaHyperpay;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface as TransactionBuilder;
use Magento\Sales\Model\Order\Payment\Transaction;

class ReceiveData extends \Magento\Framework\App\Action\Action
{
    protected $_logger;
    protected $_scopeConfig;
    protected $_checkoutSession;
    protected $_resultJsonFactory;
    protected $_transactionBuilder;

    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig,
        JsonFactory $resultJsonFactory,
        TransactionBuilder $transactionBuilder,
        Session $checkoutSession
    ){
        parent::__construct($context);
        $this->_logger = $logger;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_transactionBuilder = $transactionBuilder;
        $this->_checkoutSession = $checkoutSession;
    }

    public function execute()
    {
            $response = $this->request();
            $response = json_decode($response, true);
            $responseResultCode = $response['result']['code'];
            $arraySuccessCodes = ['000.000.000', '000.000.100', '000.100.110', '000.100.111', '000.100.112', '000.300.000', '000.300.100', '000.300.101', '000.300.102', '000.310.100', '000.310.101', '000.310.110', '000.600.000'];
            if( in_array($responseResultCode, $arraySuccessCodes) ){
                try {
                    $secondResponse = [];
                    $order = $this->_checkoutSession->getLastRealOrder();

                    $payment = $order->getPayment();
                    $payment->setMethod('madahyperpay');
                    $payment->setLastTransId($response['id']);
                    $payment->setTransactionId($response['id']);
                    //$payment->setAdditionalInformation([Transaction::RAW_DETAILS => (array) $response]);

                    // Formatted price
                    $formatedPrice = $order->getBaseCurrency()->formatTxt($order->getGrandTotal());

                    // Prepare transaction
                    $transaction = $this->_transactionBuilder->setPayment($payment)
                    ->setOrder($order)
                    ->setTransactionId($response['id'])
                    // ->setAdditionalInformation([Transaction::RAW_DETAILS => (array) $response])
                    ->setFailSafe(true)
                    ->build(Transaction::TYPE_CAPTURE);

                    // Add transaction to payment
                    $payment->addTransactionCommentsToOrder($transaction, __('The authorized amount is %1.', $formatedPrice));
                    $payment->setParentTransactionId(null);

                    // Save payment, transaction and order
                    $payment->save();
                    $order->save();
                    $transaction->save();

                    $secondResponse['success'] = true;
                    $secondResponse['message'] = 'Transaction ID: ' . $transaction->getTransactionId();

                } catch (\Exception $e) {
                    $this->_logger->critical($e->getMessage());
                    $secondResponse['success'] = false;
                    $secondResponse['message'] = $e->getMessage();
                }
            } else {
                $secondResponse['success'] = false;
                $secondResponse['message'] = $response['result']['description'];
            }

            $result = $this->_resultJsonFactory->create();
            return $result->setData($secondResponse);
    }

    public function request() {
        $useTest = $this->_scopeConfig->getValue('payment/madahyperpay/test', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $entityId = $this->_scopeConfig->getValue('payment/madahyperpay/entityid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $bearer = $this->_scopeConfig->getValue('payment/madahyperpay/bearer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $madahyperpayCheckoutId = $this->_checkoutSession->getMadaHyperpayChekoutId();
        if( $useTest == 1 ){
            $madahyperpayUrl = 'https://test.oppwa.com/';
        } else {
            $madahyperpayUrl = 'https://oppwa.com/';
        }
      	$url = $madahyperpayUrl . "v1/checkouts/" . $madahyperpayCheckoutId . "/payment";
      	$url .= "?entityId=" . $entityId;
        $ch = curl_init();
      	curl_setopt($ch, CURLOPT_URL, $url);
      	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                         'Authorization:Bearer ' . $bearer));
      	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        if( $useTest == 1 ){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        } else {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        }
      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      	$responseData = curl_exec($ch);
      	if(curl_errno($ch)) {
      		return curl_error($ch);
      	}
      	curl_close($ch);
      	return $responseData;
    }
}

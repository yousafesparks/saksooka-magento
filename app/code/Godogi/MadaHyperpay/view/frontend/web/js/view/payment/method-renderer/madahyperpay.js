define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'uiRegistry',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/action/redirect-on-success'
    ],
    function ($, Component, url, selectPaymentMethodAction, customerData, errorProcessor, fullScreenLoader, checkoutData, quote, checkoutDataResolver, registry, additionalValidators, redirectOnSuccessAction) {
        'use strict';
        return Component.extend({
            redirectAfterPlaceOrder: true, //This is important, so the customer isn't redirected to success.phtml by default
            defaults: {
                template: 'Godogi_MadaHyperpay/payment/madahyperpay'
            },
            getEmail: function () {
                if(quote.guestEmail) return quote.guestEmail;
                else return window.checkoutConfig.customerData.email;
            },
            /**
             * Place order.
             */
            placeOrder: function (data, event) {
              var dataForm = jQuery('#madahyperpay-form');
              var self = this;
              dataForm.mage('validation', {});
              dataForm.validation();
              if(dataForm.validation('isValid')){
                  if (event) {
                      event.preventDefault();
                  }
                  if (this.validate() && additionalValidators.validate() && this.isPlaceOrderActionAllowed() === true) {
                      this.isPlaceOrderActionAllowed(false);
                      var custom_controller_url = url.build('madahyperpay/madahyperpay/postdata');
                      $.ajax( custom_controller_url , // create checkout on MadaHyperpay server
                      {
                          type: 'POST',
                          dataType: 'json',
                          showLoader: true,
                          data: {
                              'email': self.getEmail(),
                              'paymentBrand': $("#paymentBrandMada").val(),
                              'cardHolder': $("#cardHolderMada").val(),
                              'cardNumber': $("#cardNumberMada").val().replace(/\s/g, ''),
                              'cardExpiryMonth': $("#cardExpiryMonthMada").val(),
                              'cardExpiryYear': $("#cardExpiryYearMada").val(),
                              'cardCvv': $("#cardCvvMada").val()
                          },
                          success: function (data, status, xhr) {
                            console.log(data);
                            var redirect = data['result']['redirect'];
                            if(redirect){
                                var parametersArray = redirect['parameters'];
                                var form = document.createElement("form");
                                form.method = "POST";
                                form.action = redirect['url'];
                                for(let i = 0; i < parametersArray.length; i++){
                                    var element = document.createElement("input");
                                    element.value=parametersArray[i]['value'];
                                    element.name=parametersArray[i]['name'];
                                    form.appendChild(element);
                                }
                                document.body.appendChild(form);
                                form.submit();
                            } else {
                                // console.log('no redirect!');
                                $('#visamasterhyperpay-errors').empty();
                                $('#visamasterhyperpay-errors').append('<p class="title">'+ $.mage.__('Please solve the following inputs') +':</p>');
                                var errorHtml = '';
                                errorHtml += '<ul>';
                                errorHtml += '<li>'+ $.mage.__(data['result']['result']['description']) +'</li>';
                                errorHtml += '</ul>';
                                $('#visamasterhyperpay-errors').append(errorHtml);
                                $('#visamasterhyperpay-errors').show(200);
                                $('html, body').animate({
                                    scrollTop: $("#visamasterhyperpay-errors").offset().top
                                }, 500);
                            }
                            self.isPlaceOrderActionAllowed(true);
                          },
                          error: function (jqXhr, textStatus, errorMessage) {
                              console.log(jqXhr);
                              return false;
                          }
                      });
                  }
                  return false;
              }else{
                  return false;
              }
            },
            /**
             * After place order callback
             */
            afterPlaceOrder: function () {

            },
            /**
             * Initialize view.
             *
             * @return {exports}
             */
            initialize: function () {
                $( document ).ready(function() {
                    var existCondition = setInterval(function() {
                       if ($('#paymentBrandMada').length) {
                          clearInterval(existCondition);
                          $('#cardNumberMada').on('input', function() {

                              $('#cardNumberMada').removeClass('shake');
                              var input = $('#cardNumberMada').val().split('');
                              var arrayLength = input.length;
                              var output = [];
                              var shake = false;
                              var allowedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                              for (var i = 0; i < arrayLength; i++) {
                                  if (allowedCharacters.includes(input[i])) {
                                      output.push(input[i]);
                                  } else {
                                      shake = true;
                                  }
                              }
                              if(shake){
                                $('#cardNumberMada').addClass('shake');
                              }

                              var outputString = output.join("");
                              $('#cardNumberMada').val(outputString);

                              var creditCardNumberVal = $('#cardNumberMada').val();
                              creditCardNumberVal = creditCardNumberVal.replace(/\s/g, '');
                              if(creditCardNumberVal.match(/^-{0,1}\d+$/)){
                                  if(creditCardNumberVal.length > 16 ){
                                      var output = [];
                                      var input = $('#cardNumberMada').val().split('');
                                      for (var i = 0; i < 16; i++) {
                                          output.push(input[i]);
                                      }
                                      $('#cardNumberMada').addClass('shake');
                                      var outputString = output.join("");
                                      $('#cardNumberMada').val(outputString);
                                  } else if(creditCardNumberVal.length == 16){
                                      $('#cardNumberMada').addClass('valid');
                                      $('#cardNumberMada').removeClass('unvalid');
                                  }else{
                                      $('#cardNumberMada').addClass('unvalid');
                                      $('#cardNumberMada').removeClass('valid');
                                  }
                              }else{
                                  $('#cardNumberVisaMaster').addClass('unvalid');
                                  $('#cardNumberVisaMaster').removeClass('valid');
                              }
                          });



                          $('#cardCvvMada').on('input', function() {
                              $('#cardCvvMada').removeClass('shake');
                              var input = $('#cardCvvMada').val().split('');
                              var arrayLength = input.length;
                              var output = [];
                              var shake = false;
                              var allowedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                              for (var i = 0; i < arrayLength; i++) {
                                  if (allowedCharacters.includes(input[i])) {
                                      output.push(input[i]);
                                  } else {
                                      shake = true;
                                  }
                              }
                              if(shake){
                                $('#cardCvvMada').addClass('shake');
                              }

                              var outputString = output.join("");
                              $('#cardCvvMada').val(outputString);

                              var creditCardNumberVal = $('#cardCvvMada').val();
                              creditCardNumberVal = creditCardNumberVal.replace(/\s/g, '');
                              if(creditCardNumberVal.match(/^-{0,1}\d+$/)){
                                  if(creditCardNumberVal.length > 4 ){
                                      var output = [];
                                      var input = $('#cardCvvMada').val().split('');
                                      for (var i = 0; i < 4; i++) {
                                          output.push(input[i]);
                                      }
                                      $('#cardCvvMada').addClass('shake');
                                      var outputString = output.join("");
                                      $('#cardCvvMada').val(outputString);
                                  }
                              }

                          });






                        }
                    }, 100);
                });
                var billingAddressCode,
                    billingAddressData,
                    defaultAddressData;
                this._super().initChildren();
                quote.billingAddress.subscribe(function (address) {
                    this.isPlaceOrderActionAllowed(address !== null);
                }, this);
                checkoutDataResolver.resolveBillingAddress();
                billingAddressCode = 'billingAddress' + this.getCode();
                registry.async('checkoutProvider')(function (checkoutProvider) {
                    defaultAddressData = checkoutProvider.get(billingAddressCode);
                    if (defaultAddressData === undefined) {
                        // Skip if payment does not have a billing address form
                        return;
                    }
                    billingAddressData = checkoutData.getBillingAddressFromData();
                    if (billingAddressData) {
                        checkoutProvider.set(
                            billingAddressCode,
                            $.extend(true, {}, defaultAddressData, billingAddressData)
                        );
                    }
                    checkoutProvider.on(billingAddressCode, function (providerBillingAddressData) {
                        checkoutData.setBillingAddressFromData(providerBillingAddressData);
                    }, billingAddressCode);
                });
                return this;
            }
        });
    }
);

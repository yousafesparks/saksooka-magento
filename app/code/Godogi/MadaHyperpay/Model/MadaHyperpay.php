<?php
namespace Godogi\MadaHyperpay\Model;

/**
 * Pay In Store payment method model
 */
class MadaHyperpay extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'madahyperpay';
}

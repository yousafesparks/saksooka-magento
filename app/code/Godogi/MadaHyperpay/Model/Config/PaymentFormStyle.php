<?php
namespace Godogi\MadaHyperpay\Model\Config;

class PaymentFormStyle implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'plain', 'label' => __('Plain')],
            ['value' => 'card', 'label' => __('Card')]
        ];
    }
}

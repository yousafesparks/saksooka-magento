<?php
namespace Godogi\MadaHyperpay\Model;

class MadaHyperpayConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    public function getConfig()
    {
        return [
            'key' => 'madahyperpay'
        ];
    }
}

<?php

namespace Godogi\MadaHyperpay\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface as TransactionBuilder;

class PaymentRefund implements ObserverInterface
{
    protected $_logger;
    protected $_orderRepository;
    protected $_scopeConfig;
    protected $_transactions;
    protected $_transactionBuilder;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $transactions,
        TransactionBuilder $transactionBuilder
    ){
        $this->_logger = $logger;
        $this->_orderRepository = $orderRepository;
        $this->_scopeConfig = $scopeConfig;
        $this->_transactions = $transactions;
        $this->_transactionBuilder = $transactionBuilder;
    }

    /**
     * Below is the method that will fire whenever the event runs!
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $incrementId = $creditmemo->getIncrementId();
        $orderId = $creditmemo->getOrderId();
        $order = $this->_orderRepository->get($orderId);
        $payment = $order->getPayment();
        try {
            if( $payment->getMethod() == 'madahyperpay' ){
                  $transactions = $this->_transactions->create()->addOrderIdFilter($orderId);
                  $transactions = $transactions->getItems();
                  $paymentId = '';
                  foreach($transactions as $transaction){
                      $paymentId = $transaction->getTxnId();
                  }
                  $response = $this->request($creditmemo->getGrandTotal(),$order->getOrderCurrencyCode(),$paymentId);
                  $response = json_decode($response, true);
                  //get payment object from order object
                  $payment = $order->getPayment();
                  $payment->setLastTransId($response['id']);
                  $payment->setTransactionId($response['id']);
                  // $payment->setAdditionalInformation([\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $response]);
                  $formatedPrice = $order->getBaseCurrency()->formatTxt(
                      $response['amount']
                  );
                  $message = __('The authorized amount is %1.', $formatedPrice);
                  //get the object of builder class
                  $trans = $this->_transactionBuilder;
                  $transaction = $trans->setPayment($payment)
                  ->setOrder($order)
                  ->setTransactionId($response['id'])
                  // ->setAdditionalInformation([\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $response])
                  ->setFailSafe(true)
                  //build method creates the transaction and returns the object
                  ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_REFUND);
                  $payment->addTransactionCommentsToOrder(
                      $transaction,
                      $message
                  );
                  $payment->setParentTransactionId($response['referencedId']);
                  $payment->save();
                  $order->save();
            }
        } catch (Exception $e) {
            $this->_logger->info($e->getMessage());
        }
    }


    public function request($amount, $currency, $paymentId) {
        $entityId = $this->_scopeConfig->getValue('payment/madahyperpay/entityid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $useTest = $this->_scopeConfig->getValue('payment/madahyperpay/test', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $bearer = $this->_scopeConfig->getValue('payment/madahyperpay/bearer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if( $useTest == 1 ){
            $madahyperpayUrl = 'https://test.oppwa.com/';
        } else {
            $madahyperpayUrl = 'https://oppwa.com/';
        }

      	$url = $madahyperpayUrl . "v1/payments/" . $paymentId;
      	$data = "entityId=" . $entityId .
                      "&amount=" . number_format((float)$amount, 2, '.', '') .
                      "&currency=" . $currency .
                      "&paymentType=RF";

      	$ch = curl_init();
      	curl_setopt($ch, CURLOPT_URL, $url);
      	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                         'Authorization:Bearer ' . $bearer));
      	curl_setopt($ch, CURLOPT_POST, 1);
      	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        if( $useTest == 1 ){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        } else {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        }
      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      	$responseData = curl_exec($ch);
      	if(curl_errno($ch)) {
      		return curl_error($ch);
      	}
      	curl_close($ch);
      	return $responseData;
    }
}

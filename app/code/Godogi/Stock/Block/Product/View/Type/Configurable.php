<?php

namespace Godogi\Stock\Block\Product\View\Type;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\DecoderInterface;
class Configurable
{

    protected $jsonEncoder;
    protected $jsonDecoder;

    public function __construct(
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder
    ) {

        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
    }

    public function aroundGetJsonConfig(
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
        \Closure $proceed
    )
    {
        $config = $proceed();
        $config = $this->jsonDecoder->decode($config);
        $config['optionStocks'] = $this->getOptionStocks($subject);
        return $this->jsonEncoder->encode($config);
    }

    protected function getOptionStocks($subject) {
        $logger = \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class);
        $logger->info('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy');

        $stocks = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $stockObject = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
        $StockState = $objectManager->get('Magento\CatalogInventory\Api\StockStateInterface');
        $getAllowProducts = $subject->getAllowProducts();
        foreach ($getAllowProducts as $product) {
            $color = $product->getColor();
            $size = $product->getSize();
            $productStockObj = $stockObject->getStockItem($product->getId());
            $isInStock = $productStockObj['is_in_stock'];
            $stockQuantity = $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
            $stocks[$color][$size] = ['stockStatus' => $isInStock, 'stockQuantity' => $stockQuantity];
        }
        return $stocks;
    }
}

<?php
namespace Godogi\VisaMasterHyperpay\Controller\VisaMasterHyperpay;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface as TransactionBuilder;
use Magento\Sales\Model\Order\Payment\Transaction;

class CheckPayment extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_scopeConfig;
    protected $checkoutSession;
    protected $orderRepository;
    protected $_transactionBuilder;
    protected $_quoteManagement;
    protected $paymentFactory;
    protected $customerFactory;
    protected $customerRepository;
    protected $_storeManager;
    protected $_customerSession;
    protected $_eventManager;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        TransactionBuilder $transactionBuilder,
        \Magento\Quote\Api\CartManagementInterface $quoteManagement,
        \Magento\Quote\Model\Quote\PaymentFactory $paymentFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Event\ManagerInterface $eventManager)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->_transactionBuilder = $transactionBuilder;
        $this->_quoteManagement = $quoteManagement;
        $this->paymentFactory = $paymentFactory;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->_eventManager = $eventManager;
        parent::__construct($context);
    }
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/hyperpay.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('=== Check Payment ===');
        $customerEmailAddress = $this->_customerSession->getCustomerEmailAddress();
        try {
            $paymentId = $this->getRequest()->getParam('id');
            $logger->info('Payment ID: '.$paymentId);
            $entityId = $this->_scopeConfig->getValue('payment/visamasterhyperpay/entityid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $bearer = $this->_scopeConfig->getValue('payment/visamasterhyperpay/bearer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $useTest = $this->_scopeConfig->getValue('payment/visamasterhyperpay/test', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if( $useTest == 1 ){
                $visamasterhyperpayUrl = 'https://test.oppwa.com/';
            } else {
                $visamasterhyperpayUrl = 'https://oppwa.com/';
            }
            $url = $visamasterhyperpayUrl . "v1/payments/";
            $url .= $paymentId;
            $url .= "?entityId=".$entityId;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                             'Authorization:Bearer ' . $bearer));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            if( $useTest == 1 ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            } else {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);
            if(curl_errno($ch)) {
              return curl_error($ch);
            }
            curl_close($ch);
            $result = json_decode($responseData, true);
            $resultCode = $result['result']['code'];
            if ( $resultCode == '000.000.000' || $resultCode == '000.000.100' || $resultCode == '000.100.110' ||
              $resultCode == '000.100.111' || $resultCode == '000.100.112' || $resultCode == '000.300.000' ||
              $resultCode == '000.300.100' || $resultCode == '000.300.101' || $resultCode == '000.300.102' ||
              $resultCode == '000.310.100' || $resultCode == '000.310.101' || $resultCode == '000.310.110' ||
            $resultCode == '000.600.000') {
                $logger->info($resultCode);
                $quote = $this->checkoutSession->getQuote();
                $store=$this->_storeManager->getStore();
                $websiteId = $this->_storeManager->getStore()->getWebsiteId();
                $customer = $this->customerFactory->create();
                $customer->setWebsiteId($websiteId);
                $billingAdress = $quote->getBillingAddress();
                $customer->loadByEmail($billingAdress->getEmail());
                if ($customer->getId()) {
                    if(!$this->_customerSession->isLoggedIn()){
                        $quote->setCustomerFirstname($billingAdress->getFirstname());
                        $quote->setCustomerLastname($billingAdress->getLastname());
                        $quote->setCustomerEmail($customerEmailAddress);
                        $quote->setCustomerIsGuest(true);
                    }
                } else {
                    $quote->setCustomerFirstname($billingAdress->getFirstname());
                    $quote->setCustomerLastname($billingAdress->getLastname());
                    $quote->setCustomerEmail($customerEmailAddress);
                    $quote->setCustomerIsGuest(true);
                }

                $quote->getPayment()->importData(['method' => 'visamasterhyperpay']);
                $quote->save();
                $quote->setCustomerEmail($customerEmailAddress);
                $order = $this->_quoteManagement->submit($quote);
                $this->_eventManager->dispatch('checkout_submit_all_after', ['order' => $order, 'quote' => $quote]);
                $payment = $order->getPayment();
                $formatedPrice = $order->getBaseCurrency()->formatTxt($order->getGrandTotal());
                $transaction = $this->_transactionBuilder->setPayment($payment)
                    ->setOrder($order)
                    ->setTransactionId($result['id'])
                    ->setFailSafe(true)
                    ->build(Transaction::TYPE_CAPTURE);
                $payment->addTransactionCommentsToOrder($transaction, __('The authorized amount is %1.', $formatedPrice));
                $payment->save();
                $transaction->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                $this->checkoutSession->setLastSuccessQuoteId($order->getQuoteId());
                $this->checkoutSession->setLastQuoteId($order->getQuoteId());
                $this->checkoutSession->setLastOrderId($order->getEntityId());
                $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
                $logger->info($order->getEntityId());
                $logger->info('SUCCESS');
                $resultRedirect->setPath('checkout/onepage/success');
                return $resultRedirect;
            } else {
                $this->messageManager->addError(__($result['result']['description']));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout/cart');
                $logger->info('Hyperpay issue');
                $logger->info($result['result']['description']);
                return $resultRedirect;
            }
        } catch (\Exception $e) {
            $logger->info('*** Check Payment Exception***');
            $logger->info($e->getMessage());
            echo $e->getMessage();
            $this->messageManager->addError(__("Hyperpay: " . $e->getMessage()));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('checkout/cart');
            return $resultRedirect;
        }
    }
}

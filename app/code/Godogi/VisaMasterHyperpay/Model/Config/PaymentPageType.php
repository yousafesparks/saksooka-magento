<?php
namespace Godogi\VisaMasterHyperpay\Model\Config;

class PaymentPageType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Login To Pay (Simple)')],
            ['value' => 1, 'label' => __('Login To Pay')],
            ['value' => 2, 'label' => __('VIP')]
        ];
    }
}

<?php
namespace Godogi\VisaMasterHyperpay\Model;

/**
 * Pay In Store payment method model
 */
class VisaMasterHyperpay extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'visamasterhyperpay';
}

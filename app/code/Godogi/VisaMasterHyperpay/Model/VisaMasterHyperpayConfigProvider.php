<?php
namespace Godogi\VisaMasterHyperpay\Model;

class VisaMasterHyperpayConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    public function getConfig()
    {
        return [
            'key' => 'visamasterhyperpay'
        ];
    }
}

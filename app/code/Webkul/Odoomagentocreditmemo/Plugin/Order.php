<?php
/**
 * Webkul Odoomagentocreditmemo Order Plugin
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentocreditmemo
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentocreditmemo\Plugin;

class Order 
{
    public function __construct(
        \Webkul\Odoomagentoconnect\Model\Order $orderMapping,
        \Magento\Sales\Model\Order $salesOrder,
        \Webkul\Odoomagentocreditmemo\Model\Creditmemo $creditModel
    ) {
        $this->_creditModel = $creditModel;
        $this->_salesOrder = $salesOrder;
        $this->_orderMapping = $orderMapping;
    }

    public function afterExportOrder(\Webkul\Odoomagentoconnect\Model\ResourceModel\Order $subject, $result)
    {
        $mapping = $this->_orderMapping
            ->getCollection()
            ->addFieldToFilter('odoo_id', ['eq'=>$result]);

        if (count($mapping) > 0) {
            $orderId = $mapping->getFirstItem()->getMagentoId();
            $creditmemoCollection = $this->_salesOrder->load($orderId)->getCreditmemosCollection();
            foreach ($creditmemoCollection as $creditmemo) {
                $this->_creditModel->exportCreditMemo($mapping, $creditmemo);
            }
        }
        return $result;
    }
} 

<?php
/**
 * Copyright © 2017 Webkul. All rights reserved.
 * See LICENSE.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webkul_Odoomagentocreditmemo',
    __DIR__
);

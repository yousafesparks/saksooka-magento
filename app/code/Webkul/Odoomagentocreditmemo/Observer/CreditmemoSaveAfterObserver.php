<?php
/**
 * Webkul Odoomagentocreditmemo SalesOrderPlaceAfterObserver Observer Model
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentocreditmemo
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 *
 */
namespace Webkul\Odoomagentocreditmemo\Observer;

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Odoomagentoconnect\Helper\Connection;

class CreditmemoSaveAfterObserver implements ObserverInterface
{
    public function __construct(
        \Webkul\Odoomagentoconnect\Model\Order $orderMapping,
        Connection $connection,
        \Webkul\Odoomagentocreditmemo\Model\Creditmemo $creditModel
    ) {
        $this->_creditModel = $creditModel;
        $this->_connection = $connection;
        $this->_orderMapping = $orderMapping;
    }

    /**
     * Sale Order Save After event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // $route = $this->_requestInterface->getControllerName();
        $creditmemo = $observer->getEvent()->getData('creditmemo');

        $operationFrom = $this->_connection->getSession()->getOperationFrom();
        $this->_connection->getSession()->unsOperationFrom();
        if ($operationFrom == 'odoo') {
            return true;
        }

        $lastOrderId = $creditmemo->getOrderId();
        $mapping = $this->_orderMapping
            ->getCollection()
            ->addFieldToFilter('magento_id', ['eq'=>$lastOrderId]);

        /** @var $orderInstance Order */
        if (count($mapping) > 0) {
            $this->_creditModel->exportCreditMemo($mapping, $creditmemo);
        }
    }
}
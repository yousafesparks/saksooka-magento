<?php
/**
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentocreditmemo
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentocreditmemo\Model;

use Webkul\Odoomagentoconnect\Helper\Connection;
use xmlrpcval;

/**
 * Webkul Odoomagentocreditmemo Class
 */
class Creditmemo
{
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Connection $connection,
        \Webkul\Odoomagentoconnect\Model\Product $productMapping
    ) {
        $this->messageManager = $messageManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_connection = $connection;
        $this->_productMapping = $productMapping;
    }

    public function exportCreditMemo($mappingObj, $creditmemo = null)
    {

        $operationFrom = $this->_connection->getSession()->getOperationFrom();
        $this->_connection->getSession()->unsOperationFrom();
        if ($operationFrom == 'odoo') {
            return true;
        }

        $lastOrderId = $creditmemo->getOrderId();
        $incrementId = $creditmemo->getIncrementId();

        $odooOrderId = 0;
        $odooOrderId = $mappingObj->getFirstItem()->getOdooId();
        $odooOrderName = $mappingObj->getFirstItem()->getOdooOrder();
    
        if ($odooOrderId == 0) {
            $this->messageManager->addError(__('Refund Status not updated at odoo, Cause this order is not yet synced at odoo!!!'));
        }
        $refundInvoice = $this->_scopeConfig->getValue('odoomagentorefund/automatization_settings/refund_invoice');
        $returnDelivery = $this->_scopeConfig->getValue('odoomagentorefund/automatization_settings/return_shipment');
        if ($refundInvoice == 1) {
            if (!$lastOrderId) {
                return;
            }
            $helper = $this->_connection;
            $refundItems = [];
            $otherRefunds = [];
            $discountAmountData = [];
            foreach ($creditmemo->getAllItems() as $item) {
                $discountAmountData[$item->getOrderItemId()] = $item->getDiscountAmount();
            }
            foreach ($creditmemo->getAllItems() as $item) {
                $odooProductId = 0;
                $productId = $item->getProductId();
                $refundQuantity = $item->getQty();
                $discountAmount = $item->getDiscountAmount();
                $mappingCollection = $this->_productMapping
                    ->getCollection()
                    ->addFieldToFilter('magento_id', ['eq'=>$productId]);
                if (count($mappingCollection) > 0) {
                        $odooProductId = $mappingCollection->getFirstItem()->getOdooId();
                }
                if ($odooProductId) {
                    $refundItems[$odooProductId] = $refundQuantity;
                    $parentId = $item->getOrderItem()->getParentItemId();
                    if ($parentId != null) {
                        $parentOrderItem = $item->getOrderItem()->getParentItem();
                        if ($parentOrderItem->getProductType() == 'configurable') {
                            if (isset($discountAmountData[$parentOrderItem->getId()])) {
                                $discountAmount = $discountAmountData[$parentOrderItem->getId()];
                            }
                        }
                    }
                    if ($discountAmount != 0) {
                        $description = "Discount on ".$item->getName();
                        $otherRefunds[$description] = -(float)$discountAmount;
                    }
                }
            }
            $shippingIncludesTax = $this->_scopeConfig->getValue('tax/calculation/shipping_includes_tax');
            if ($shippingIncludesTax) {
                $shippingAmount = $creditmemo->getShippingInclTax();
            } else {
                $shippingAmount = $creditmemo->getShippingAmount();
            }
            if ($shippingAmount) {
                $otherRefunds['Shipping'] = $shippingAmount;
            }
            $adjustment = $creditmemo->getAdjustment();
            if ($adjustment != 0) {
                $otherRefunds['Adjustment'] = $adjustment;
            }
            if ($refundItems || $otherRefunds) {
                $refundMsg = $incrementId;
                $odooOrderId = (int)$odooOrderId;
                if ($refundItems) {
                    $itemParams = php_xmlrpc_encode($refundItems);
                } else {
                    $itemParams = new xmlrpcval([], "struct");
                }
                if ($otherRefunds) {
                    $otherParams = php_xmlrpc_encode($otherRefunds);
                } else {
                    $otherParams = new xmlrpcval([], "struct");
                }
                $refundMsg = new xmlrpcval($refundMsg, "string");
                $resp = $helper->callOdooMethod("sale.order", "compute_refund", [$odooOrderId, $itemParams, $otherParams, $refundMsg], ['check_move_validity'=>false]);
                if ($resp && $resp[0] == 0) {
                    $errorMessage = 'Failed to refund invoice of odoo order, Error: , '.$resp[1];
                    $this->messageManager->addError(__($errorMessage));
                } else {
                    $status = $resp[1]['status'];
                    $status_message = $resp[1]["status_message"];
                    if ($status) {
                        $this->messageManager->addSuccess(__("Odoo Order ".$odooOrderName." Invoice successfully refunded."));
                    } else {
                        $errorMessage = "Odoo Order ".$odooOrderName." Invoice not refunded >> ".$status_message;
                        $this->messageManager->addError(__($errorMessage));
                        $helper->addError($errorMessage);
                    }
                }
            }
            if ($returnDelivery == 1 && $refundItems) {
                $odooOrderId = (int)$odooOrderId;
            
                if ($refundItems) {
                    $return = php_xmlrpc_encode($refundItems);
                } else {
                    $return = new xmlrpcval([], "struct");
                }
                $resp = $helper->callOdooMethod("sale.order", "compute_delivery_return", [$odooOrderId, $return], true);
                if ($resp && $resp[0] == 0) {
                    $errorMessage = 'Failed to return delivery of odoo order, Error: , '.$resp[1];
                    $this->messageManager->addError(__($errorMessage));
                    $helper->addError($errorMessage);
                } else {
                    $this->messageManager->addSuccess(__("Odoo Order ".$odooOrderName." Delivery successfully returned."));
                }
            }
        }

    }
}

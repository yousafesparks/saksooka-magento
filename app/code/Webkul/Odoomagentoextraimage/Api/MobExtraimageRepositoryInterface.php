<?php
/**
 * Webkul MobRepository Model
 * 
 * @category  Webkul
 * @package   Webkul_Odoomagentoconnect
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentoextraimage\Api;

/**
 * @api
 */
interface MobExtraimageRepositoryInterface
{
        /**
     * Create new gallery entry
     *
     * @param int $id
     * @param \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface $entry
     * @return int gallery entry ID
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function saveProductMedia(
        $id,
        \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface $entry
    );

}

<?php
/**
 * Webkul MobExtraimageRepository Model
 * 
 * @category  Webkul
 * @package   Webkul_Odoomagentoextraimage
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */


namespace Webkul\Odoomagentoextraimage\Model;

use Webkul\Odoomagentoextraimage\Api\MobExtraimageRepositoryInterface;
use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface;

/**
 * Defines the implementation class of the mob repository service contract.
 */
class MobExtraimageRepository implements MobExtraimageRepositoryInterface
{

    /**
     * Constructor.
     *
     * @param MobFactory
     */
    public function __construct(
        \Magento\Catalog\Model\Product $productManager,
        \Magento\Catalog\Model\Product\Gallery\GalleryManagement $galleryManagement,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {

        $this->_productManager = $productManager;
        $this->_galleryManagement = $galleryManagement;
        $this->_objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function saveProductMedia($id, ProductAttributeMediaGalleryEntryInterface $entry)
    {
        if ($id) {
            $existingProduct = $this->_productManager->load($id);
            $sku = $existingProduct->getSku();
            if ($sku) {
                $data = $this->_galleryManagement->create($sku, $entry);
                return $data;
            } else {
                throw new InputException(__('The product with this id not found.'));
            }
        } else {
            throw new InputException(__('The product id not found.'));
        }
    }

}

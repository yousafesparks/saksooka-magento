<?php
/**
 * Webkul Odoomagentoextraimage AfterProductSynchronize 
 *
 * @category    Webkul
 * @package     Webkul_Odoomagentoextraimage
 * @author      Webkul Software
 *
 */

namespace Webkul\Odoomagentoextraimage\Observer;

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Odoomagentoconnect\Helper\Connection;

class AfterProductSynchronize implements ObserverInterface
{
    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        Connection $connection,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_connection = $connection;
        $this->_productModel = $productModel;
        $this->_storeManager = $storeManager;
    }

    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $odooId = $observer->getErpProduct();
        $mageId = $observer->getProduct();
        $proType = $observer->getType();
        if ($odooId && $mageId) {
            $helper = $this->_connection;
            $helper->getSocketConnect();
            $userId = $helper->getSession()->getUserId();

            if ($userId) {
                $imageArray = [];
                $productObj = $this->_productModel->load($mageId);
                $images = $productObj->getMediaGalleryEntries();

                $odooObject = 'product.product';
                if ($proType == 'template') {
                    $odooObject = 'product.template';
                }
    
                foreach ($images as $value) {
                    $typeArray = [];
                    if (in_array('image', $value['types'])) {
                        continue;
                    }
                    $proImageArray = [
                        'types' => $value['types'],
                        'name' => $productObj->getName(),
                        'position' => $value['position'],
                        'extra_image_id' => $value['id'],
                    ];
                    $productImagePath = false;
                    try {
                        $store = $this->_storeManager->getStore();
                        $baseImage = 'catalog/product'.$value['file'];
                        $imagePath = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$baseImage;
                        $productImagePath = $helper->getModel(\Magento\Framework\App\Filesystem\DirectoryList::class)->getPath('media').'/'.$baseImage;
                        if (file_exists($productImagePath)) {
                            $imageUrl = $imagePath;
                        } else {
                            $imageUrl = false;
                        }
                    } catch (\Exception $e) {
                        $helper->addError($e, 'odoo_connector_debug.log');
                        $imageUrl = false;
                    }                   
                    if ($imageUrl) {
                        $proImageArray['image_url'] = $imageUrl;
                    } else {
                        $helper->addError("Image path doesn't exist: $productImagePath", 'odoo_connector_debug.log');
                        continue;
                    }
                    array_push($imageArray, $proImageArray);
                }           
        
                if ($imageArray) {
                    $args = [(int)$mageId, (int)$odooId, $imageArray, $odooObject];
                    if ($proType == 'config_child') {
                        array_push($args,true);
                    }
                    $resp = $helper->callOdooMethod("product.image", "create_image", $args, true);
                }
            }
        }
    }
}

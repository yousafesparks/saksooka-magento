<?php

namespace Webkul\Odoomagentomultilang\Model;

/**
 * Webkul Odoomagentomultilang Odoomagentomultilang
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Webkul\Odoomagentomultilang\Api\Data\OdoomagentomultilangInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Odoomagentomultilang extends AbstractModel implements OdoomagentomultilangInterface, IdentityInterface
{

    protected $interfaceOdoomagentomultilangs = [

    OdoomagentomultilangInterface::ODOO_LANG,
    OdoomagentomultilangInterface::MAGENTO_STORE_ID,
    OdoomagentomultilangInterface::CREATED_BY,
    ];

    /**#@+
     * Post's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'odoomagentomultilang_odoomagentomultilang';

    /**
     * @var string
     */
    protected $_cacheTag = 'odoomagentomultilang_odoomagentomultilang';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'odoomagentomultilang_odoomagentomultilang';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Odoomagentomultilang\Model\ResourceModel\Odoomagentomultilang::class);
    }

    /**
     * Prepare post's statuses.
     * Available event to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get the name.
     *
     * @api
     * @return string|null
     */
    public function getOdooLang()
    {
        return $this->getData(self::ODOO_LANG);
    }

    /**
     * Get the magento_store_id.
     *
     * @api
     * @return int|null
     */
    public function getMagentoStoreId()
    {
        return $this->getData(self::MAGENTO_STORE_ID);
    }

    /**
     * Get the created_by.
     *
     * @api
     * @return string|null
     */
    public function getCreatedBy()
    {
        return $this->getData(self::CREATED_BY);
    }

    /**
     * Set odoo_lang
     *
     * @api
     * @param  string $odoo_lang
     * @return $this
     */
    public function setOdooLang($odoo_lang)
    {
        return $this->setData(self::ODOO_LANG, $odoo_lang);
    }

    /**
     * Set magento_store_id
     *
     * @api
     * @param  int $magento_store_id
     * @return $this
     */
    public function setMagentoStoreId($magento_store_id)
    {
        return $this->setData(self::MAGENTO_STORE_ID, $magento_store_id);
    }

    /**
     * Set created_by
     *
     * @api
     * @param  string $created_by
     * @return $this
     */
    public function setCreatedBy($created_by)
    {
        return $this->setData(self::CREATED_BY, $created_by);
    }
}

<?php
/**
 * Webkul Odoomagentomultilang Odoomagentomultilang
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Odoomagentomultilang\Model\ResourceModel;

use Webkul\Odoomagentoconnect\Helper\Connection;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Odoomagentomultilang extends AbstractDb
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param string|null                                       $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\Product $productManager,
        \Webkul\Odoomagentoconnect\Model\Option $optionMapping,
        Connection $connection,
        $resourcePrefix = null
    ) {
        $this->_productManager = $productManager;
        $this->_objectManager = $objectManager;
        $this->_optionMapping = $optionMapping;
        $this->_connection = $connection;
        parent::__construct($context, $resourcePrefix);
    }

    public function syncAllAttributeTranslations($attributeId, $odooId)
    {
        $response = false;
        $helper = $this->_connection;
        $mappingCollection = $this->_objectManager
            ->create(\Webkul\Odoomagentomultilang\Model\Odoomagentomultilang::class)
            ->getCollection();
        foreach ($mappingCollection as $mapping) {
            $odooLang = $mapping->getOdooLang();
            $storeId = $mapping->getMagentoStoreId();
            $collection = $this->_productManager->getResource()
                ->getAttribute($attributeId);
            if (!$collection){
                break;
            }
            $code = $collection->getStoreLabel($storeId);
                
            $args = [(int)$odooId, ['name' => $code]];
            $resp = $helper->callOdooMethod("product.attribute", "write", $args, ['lang' => $odooLang]);
            if ($resp && $resp[0]) {
                $this->syncOptionTranslations($attributeId, $storeId, $odooLang);
                $response = true;
            } else {
                $error = "Error during attrbute translation update , Lang ".$odooLang." Attribute ".$attributeId;
                $error = $error." Reason >>".$resp[1];
                $helper->addError($error);
                
            }
        }
        return $response;
    }

    public function syncOptionTranslations($attributeId, $storeId, $odooLang)
    {
        $response = 0;
        $helper = $this->_connection;
        $collection = $this->_productManager->getResource()
            ->getAttribute($attributeId);
        $options = $collection->setStoreId($storeId)->getSource()->getAllOptions(false);
        foreach ($options as $key) {
            if (is_string($key['label'])){
                $label = $key['label'];
            } else{
                $label = $key['label']->getText();
            }
            
            $optionId = $key['value'];
            $mappingcollection =  $this->_optionMapping->getCollection()
                ->addFieldToFilter('magento_id', $optionId);
            foreach ($mappingcollection as $mapping) {
                $odooOptionId = $mapping->getOdooId();
                $args = [(int)$odooOptionId, ['name'=>$label]];
                
                $resp = $helper->callOdooMethod("product.attribute.value", "write", $args, ['lang' => $odooLang]);
                if ($resp && $resp[0] == 0) {
                    $error = "Error during option translation update , Store id ".$storeId;
                    $error = $error." Attribute Option ".$optionId." Reason >>".$resp[1];
                    $this->_connection->addError($error);
                } else {
                    $response = true;
                }
            }
        }
        return $response;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('odoomagentomultilang_odoomagentomultilang', 'entity_id');
    }
}

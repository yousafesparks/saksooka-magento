<?php
/**
 * Webkul Odoomagentoconnect Odoomagentomultilang Interface
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Api\Data;

interface OdoomagentomultilangInterface
{
    /**
    * Constants for keys of data array. Identical to the name of the getter in snake case
    */

    const ODOO_LANG    = 'odoo_lang';
    const MAGENTO_STORE_ID    = 'magento_store_id';
    const CREATED_BY    = 'created_by';
    const CREATED_AT    = 'created_at';

    /**
     * Get odoo_lang
     *
     * @return string|null
     */
    public function getOdooLang();
    
    /**
     * Get the magento_id.
     *
     * @return int|null
     */
    public function getMagentoStoreId();

    /**
     * Get Created By
     *
     * @return string|null
     */
    public function getCreatedBy();

    /**
     * Set Language
     *
     * @param string $odoo_lang Odoo Language
     *
     * @return $this
     */
    public function setOdooLang($odoo_lang);

    /**
     * Set magento_store_id
     *
     * @param int $magento_store_id Magento Store Id
     *
     * @return $this
     */
    public function setMagentoStoreId($magento_store_id);

    /**
     * Set created_by
     *
     * @param string $created_by User name
     *
     * @return $this
     */
    public function setCreatedBy($created_by);
}

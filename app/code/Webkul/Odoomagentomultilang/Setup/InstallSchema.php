<?php

namespace Webkul\Odoomagentomultilang\Setup;

/**
 * Webkul Odoomagentomultilang Setup
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'odoomagentoconnect_language_mapping'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('odoomagentomultilang_odoomagentomultilang'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )
            ->addColumn(
                'odoo_lang',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Odoo Lang'
            )
            ->addColumn(
                'magento_store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Magento Store Id'
            )
            ->addColumn(
                'created_by',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Created By'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addIndex(
                $installer->getIdxName('odoomagentomultilang_odoomagentomultilang', ['odoo_lang']),
                ['odoo_lang']
            )
            ->addIndex(
                $installer->getIdxName('odoomagentomultilang_odoomagentomultilang', ['magento_store_id']),
                ['magento_store_id']
            );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}

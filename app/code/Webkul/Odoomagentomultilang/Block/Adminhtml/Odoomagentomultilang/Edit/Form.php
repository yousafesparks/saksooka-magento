<?php
/**
 * Webkul Odoomagentomultilang Form
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Block\Adminhtml\Odoomagentomultilang\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}

<?php
/**
 * Webkul Odoomagentomultilang Main
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Block\Adminhtml\Odoomagentomultilang\Edit\Tab;

use Webkul\Odoomagentomultilang\Model\Odoomagentomultilang;
use Webkul\Odoomagentoconnect\Helper\Connection;

/**
 * Cms page edit form main tab
 *
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic
{
    const CURRENT_USER_PASSWORD_FIELD = 'current_password';

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_authSession;

    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    protected $_LocaleLists;

    /**
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param \Magento\Framework\Registry              $registry
     * @param \Magento\Framework\Data\FormFactory      $formFactory
     * @param \Magento\Backend\Model\Auth\Session      $authSession
     * @param \Magento\Framework\Locale\ListsInterface $localeLists
     * @param array                                    $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection,
        array $data = []
    ) {
        $this->_authSession = $authSession;
        $this->_LocaleLists = $localeLists;
        $this->_storeManager = $storeManager;
        $this->_connection = $connection;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @return                                        \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        /**
        * @var $model \Magento\User\Model\User
        */
        $Magento_Store = $this->getMageStoreArray();
        $Odoo_Lang = $this->getErpLanguageArray();
        $model = $this->_coreRegistry->registry('Odoomagentomultilang_user');
        $odoomagentomultilangmodel = $this->_coreRegistry->registry('odoomagentomultilang_odoomagentomultilang');

        /**
        * @var \Magento\Framework\Data\Form $form
        */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('user_');

        $baseFieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Language Mapping'), 'class' => 'fieldset-wide']
        );

        if ($odoomagentomultilangmodel->getEntityId()) {
            $baseFieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        }
        $baseFieldset->addField(
            'magento_store_id',
            'select',
            [
            'label' => __('Magento Store'),
            'title' => __('Magento Store'),
            'name' => 'magento_store_id',
            'required' => true,
            'options' => $Magento_Store]
        );
        $baseFieldset->addField(
            'odoo_lang',
            'select',
            [
                    'label' => __('Odoo Language'),
                    'title' => __('Odoo Language'),
                    'name' => 'odoo_lang',
                    'required' => true,
                    'options' => $Odoo_Lang
            ]
        );

        $data= $odoomagentomultilangmodel->getData();
        $form->setValues($data);

        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getMageStoreArray()
    {
        $selection = [];
        $stores = $this->_storeManager->getStores();
        $selection['0'] ='--Select Magento Store--';

        foreach ($stores as $store) {
            $selection[$store->getId()] = $store->getName();
        }
        return $selection;
    }

    public function getErpLanguageArray()
    {
        $selection = [];
        $helper = $this->_connection;
        $selection[''] ='--Select Odoo Language--';

        $resp = $helper->callOdooMethod("res.lang", "search_read", [[], ['name', 'code']]);
        if ($resp && $resp[0]) {
            $value = $resp[1];
            foreach ($value as $val) {
                $selection[$val['code']] = $val['name'];
            }
            return $selection;
        } else {
            $errorMessage = 'Not Available- Error: '.$resp[1];
            $selection['error'] = $errorMessage;
            return $selection;
        }
    }
}

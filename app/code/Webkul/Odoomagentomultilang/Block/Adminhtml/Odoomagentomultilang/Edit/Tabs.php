<?php
/**
 * Webkul Odoomagentomultilang Tabs
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Block\Adminhtml\Odoomagentomultilang\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('page_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Language Mapping Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $content = $this
            ->getLayout()
            ->createBlock(\Webkul\Odoomagentomultilang\Block\Adminhtml\Odoomagentomultilang\Edit\Tab\Main::class)
            ->toHtml();
        $this->addTab(
            'main_section',
            [
                'label' => __('Language Manual Mapping'),
                'title' => __('Language Manual Mapping'),
                'content' => $content,
                'active' => true
            ]
        );
        return parent::_beforeToHtml();
    }
}

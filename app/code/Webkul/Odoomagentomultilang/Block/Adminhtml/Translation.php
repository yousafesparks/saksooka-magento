<?php
/**
 * Webkul Odoomagentomultilang
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Block\Adminhtml;

/**
 * Webkul Odoomagentomultilang Translation Block
 */
class Translation extends \Magento\Backend\Block\Template
{

}

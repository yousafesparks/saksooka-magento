<?php

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml;

/**
 * Webkul Odoomagentomultilang Odoomagentomultilang
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

abstract class Odoomagentomultilang extends \Magento\Backend\App\AbstractAction
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * User model factory
     *
     * @var \Magento\User\Model\UserFactory
     */
    protected $_userFactory;

    /**
     * Odoomagentomultilang model factory
     *
     * @var \Webkul\Odoomagentomultilang\Model\OdoomagentomultilangFactory
     */
    protected $_odoomagentomultilangFactory;

    /**
     * Odoomagentomultilang model factory
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_salesOrderCollectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context                            $context
     * @param \Magento\Framework\Registry                                    $coreRegistry
     * @param \Magento\User\Model\UserFactory                                $userFactory
     * @param \Webkul\Odoomagentomultilang\Model\OdoomagentomultilangFactory $odoomagentomultilangFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory     $salesOrderCollectionFactory,
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\User\Model\UserFactory $userFactory,
        \Webkul\Odoomagentomultilang\Model\OdoomagentomultilangFactory $odoomagentomultilangFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory,
        \Webkul\Odoomagentomultilang\Model\Odoomagentomultilang $languageMapping,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_userFactory = $userFactory;
        $this->_odoomagentomultilangFactory = $odoomagentomultilangFactory;
        $this->_salesOrderCollectionFactory = $salesOrderCollectionFactory;
        $this->_languageMapping = $languageMapping;
        $this->_storeManager = $storeManager;
        $this->_connection = $connection;
    }

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu(
            'Webkul_Odoomagentoconnect::manager'
        );
        return $this;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Odoomagentoconnect::attribute_save');
    }
}

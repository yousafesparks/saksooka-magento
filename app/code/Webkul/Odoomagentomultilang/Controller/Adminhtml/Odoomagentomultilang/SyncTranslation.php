<?php
/**
 * Webkul Odoomagentomultilang Language SyncTranslation Controller
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang;

/**
 * Webkul Odoomagentoconnect Language SyncTranslation Controller class
 */
class SyncTranslation extends \Magento\Backend\App\Action
{
    /**
    * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Event\ManagerInterface $eventManager
    ) {
        parent::__construct($context);
        $this->_eventManager = $eventManager;
    }

    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $response = 0;
        $params = $this->getRequest()->getParams();
        $total = $params['total'];
        $productData = $params['mapData'];
        $counter = $params['counter'];
        
        if ($productData) {
            $this->_eventManager->dispatch('catalog_product_sync_after', $productData);
        }

        if ($counter == $total) {
            $message = " Catalog Product(s) have been successfully Translated to Odoo.";
            $this->messageManager->addSuccess(__("Total of ".$counter.$message));
        }
        $this->getResponse()->clearHeaders()->setHeader('content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode(['response'=>true]));
    }
}

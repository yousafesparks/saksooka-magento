<?php
/**
 * Webkul Odoomagentomultilang Export Controller
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang;

class Export extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\Forward
     */
    protected $resultForwardFactory;

    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Webkul\Odoomagentoconnect\Model\Attribute $attributeMapping,
        \Webkul\Odoomagentomultilang\Model\ResourceModel\Odoomagentomultilang $languageResourceModel
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_attributeMapping = $attributeMapping;
        $this->_languageResourceModel = $languageResourceModel;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Odoomagentomultilang::odoomagentomultilang_save');
    }

    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $mappingcollection =  $this->_attributeMapping->getCollection();
        foreach ($mappingcollection as $mapping) {
            $attributeId = $mapping->getMagentoId();
            $odooId = $mapping->getOdooId();
            $this->_languageResourceModel->syncAllAttributeTranslations($attributeId, $odooId);
        }
        /**
         * @var \Magento\Backend\Model\View\Result\Forward $resultForward
         */
        $this->messageManager->addSuccess(__('All attribute translation are successfully synced at Odoo.'));
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('index');
    }
}

<?php

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang;

/**
 * Webkul Odoomagentomultilang Edit Controller
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Locale\Resolver;

class Edit extends \Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang
{
    /**
     * @return void
     */
    public function execute()
    {
        $odoomagentomultilangId=(int)$this->getRequest()->getParam('id');
        $odoomagentomultilangmodel=$this->_odoomagentomultilangFactory->create();
        if ($odoomagentomultilangId) {
            $odoomagentomultilangmodel->load($odoomagentomultilangId);
            if (!$odoomagentomultilangmodel->getEntityId()) {
                $this->messageManager->addError(__('This Language Mapping no longer exists.'));
                $this->_redirect('odoomagentomultilang/*/');
                return;
            }
        }
        $recordId = $odoomagentomultilangmodel->getId();

        /**
        * @var \Magento\User\Model\User $model
        */
        $model = $this->_userFactory->create();

        $data = $this->_session->getUserData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('odoomagentomultilang_user', $model);
        $this->_coreRegistry->register('odoomagentomultilang_odoomagentomultilang', $odoomagentomultilangmodel);

        if (isset($recordId)) {
            $breadcrumb = __('Edit Language Mapping');
        } else {
            $breadcrumb = __('New Language Mapping');
        }
        $this->_initAction()->_addBreadcrumb($breadcrumb, $breadcrumb);
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Users'));
        $this->_view->getPage()->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('Language Manual Mapping'));
        $this->_view->renderLayout();
    }
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Webkul_Odoomagentomultilang::odoomagentomultilang_view');
    }
}

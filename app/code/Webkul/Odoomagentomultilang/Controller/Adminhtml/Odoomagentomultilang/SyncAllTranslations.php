<?php
/**
 * Webkul Odoomagentomultilang Language SyncAllTranslations Controller
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang;

/**
 * Webkul Odoomagentomultilang Language SyncAllTranslations Controller class
 */
class SyncAllTranslations extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\Forward
     */
    protected $_resultForwardFactory;

    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Webkul\Odoomagentoconnect\Model\Product $productModel,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection,
        \Webkul\Odoomagentoconnect\Model\Template $templateModel,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableModel
    ) {
    
        $this->_connection = $connection;
        $this->_templateModel = $templateModel;
        $this->_productModel = $productModel;
        $this->_configurableModel = $configurableModel;
        $this->_resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $exportData = [];
        $helper = $this->_connection;
        $helper->getSocketConnect();
        $userId = $helper->getSession()->getUserId();
        if ($userId) {
            $i = 0;
            $mappingCollection = $this->_templateModel->getCollection();
            foreach ($mappingCollection as $map) {
                $exportData[$i] = ['product' => $map->getMagentoId(), 'erp_product' => $map->getOdooId(), 'type' => 'template'];  
                $i++;
            }
            
            $mappingCollection = $this->_productModel->getCollection();
            foreach ($mappingCollection as $map) {
                $parentId = $this->_configurableModel->getParentIdsByChild($map->getMagentoId());
                if (!isset($parentId[0])) {
                    $exportData[$i] = ['product' => $map->getMagentoId(), 'erp_product' => $map->getOdooId(), 'type' => 'product'];  
                    $i++;
                }
            }

            if (!$exportData) {
                $this->messageManager->addSuccess(__("All Catalog Product(s) have already been Translated to Odoo."));
            }
        } else {
            $errorMessage = $helper->getSession()->getErrorMessage();
            $this->messageManager->addError(
                __(
                    "The Catalog Product(s) have not been Translated to Odoo !! Reason : ".$errorMessage
                )
            );
        }
        $this->getResponse()->clearHeaders()->setHeader('content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($exportData));
    }
}

<?php
/**
 * Webkul Odoomagentomultilang NewAction Controller
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang;

class NewAction extends \Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Odoomagentomultilang::odoomagentomultilang_new');
    }
}

<?php

namespace Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang;

/**
* Webkul Odoomagentomultilang Save Controller
 *
* @category  Webkul
* @package   Webkul_Odoomagentomultilang
* @author    Webkul
* @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
* @license   https://store.webkul.com/license.html
*/

use Magento\Framework\Exception\AuthenticationException;
use Webkul\Odoomagentoconnect\Helper\Connection;
use xmlrpc_client;
use xmlrpcval;
use xmlrpcmsg;

class Save extends \Webkul\Odoomagentomultilang\Controller\Adminhtml\Odoomagentomultilang
{
    /**
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {

        $userId = (int)$this->getRequest()->getParam('user_id');
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('odoomagentomultilang/*/');
            return;
        }

        try {
            $this->messageManager->addSuccess(__('You saved the Language Mapping.'));
            $odoomagentomultilangId = (int)$this->getRequest()->getParam('entity_id');
            $odoomagentomultilangmodel = $this->_languageMapping;
            if ($odoomagentomultilangId) {
                $odoomagentomultilangmodel->load($odoomagentomultilangId);
                $odoomagentomultilangmodel->setId($odoomagentomultilangmodel);
                $data['id']=$odoomagentomultilangId;
            }
            if ($odoomagentomultilangId && $odoomagentomultilangmodel->isObjectNew()) {
                $this->messageManager->addError(__('This Language Mapping no longer exists.'));
                $this->_redirect('odoomagentomultilang/*/');
                return;
            }
            $data['created_by'] = 'Manual Mapping';

            $odoomagentomultilangmodel->setData($data);
            $odoomagentomultilangmodel->save();
            $this->_mapOnErp($data['magento_store_id'], $data['odoo_lang']);

            $this->_getSession()->setUserData(false);
            $this->_redirect('odoomagentomultilang/*/');
        } catch (\Magento\Framework\Validator\Exception $e) {
            $messages = $e->getMessages();
            $this->messageManager->addMessages($messages);
            $this->redirectToEdit($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getMessage()) {
                $this->messageManager->addError($e->getMessage());
            }
            $this->redirectToEdit($data);
        }
    }

    protected function _mapOnErp($magentoStoreId, $odooLang)
    {
        $helper = $this->_connection;
        $helper->getSocketConnect();
        $userId = $helper->getSession()->getUserId();
        $errorMessage = $helper->getSession()->getErrorMessage();
        if ($userId > 0) {
            $context = $helper->getOdooContext();
            $client = $helper->getClientConnect();
            $store = $this->_storeManager->getStore($magentoStoreId);
            $storeCode = $store->getCode();
          
            $map_array = [
                'name'=>new xmlrpcval($odooLang, "string"),
                'mage_store_id'=>new xmlrpcval($magentoStoreId, "int"),
                'mage_store_code'=>new xmlrpcval($storeCode, "string"),
                'instance_id'=>$context['instance_id']
            ];
            $map_array = [new xmlrpcval($map_array, "struct")];
            $context = ['context' => new xmlrpcval($context, "struct")];
            $msg = new xmlrpcmsg('execute_kw');
            $msg->addParam(new xmlrpcval($helper::$odooDb, "string"));
            $msg->addParam(new xmlrpcval($userId, "int"));
            $msg->addParam(new xmlrpcval($helper::$odooPwd, "string"));
            $msg->addParam(new xmlrpcval("language.mapping", "string"));
            $msg->addParam(new xmlrpcval("create", "string"));
            $msg->addParam(new xmlrpcval($map_array, "array"));
            $msg->addParam(new xmlrpcval($context, "struct"));
            $resp = $client->send($msg);
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Odoomagentomultilang::odoomagentomultilang_save');
    }

    /**
     * @param \Magento\User\Model\User $model
     * @param array                    $data
     * @return void
     */
    protected function redirectToEdit(array $data)
    {
        $this->_getSession()->setUserData($data);
        $data['entity_id']=isset($data['entity_id'])?$data['entity_id']:0;
        $arguments = $data['entity_id'] ? ['id' => $data['entity_id']]: [];
        $arguments = array_merge($arguments, ['_current' => true, 'active_tab' => '']);
        if ($data['entity_id']) {
            $this->_redirect('odoomagentomultilang/*/edit', $arguments);
        } else {
            $this->_redirect('odoomagentomultilang/*/index', $arguments);
        }
    }
}

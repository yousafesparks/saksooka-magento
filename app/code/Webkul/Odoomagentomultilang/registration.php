<?php
/**
 * Copyright © 2016 Webkul. All rights reserved.
 * See COPYING.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webkul_Odoomagentomultilang',
    __DIR__
);

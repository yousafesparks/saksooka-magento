# Instructions

MOB Multi-Language For Magento2 and Odoo 12

Odoo-Modules directory contains odoo modules, follow Readme.md instruction for installation.

Inside src directory magento2 module is added, follow Readme.md instruction for installation.

Note:- current modules are compatible with magento v2.3.* and odoo v12.0.

# Support

Find us our support policy - https://store.webkul.com/support.html/

# Refund

Find us our refund policy - https://store.webkul.com/refund-policy.html/

---------------------------------------------------------------------------------------- 
Note - This readme file is strictly need to use when you have purchased the software from 
webkul store i.e https://store.webkul.com . If you purchase the module from magento marketplace 
connect this readme file will not work.
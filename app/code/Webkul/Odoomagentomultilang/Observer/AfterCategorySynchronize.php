<?php
namespace Webkul\Odoomagentomultilang\Observer;

/**
 * Webkul Odoomagentomultilang AfterCategorySynchronize Observer
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Odoomagentoconnect\Helper\Connection;

class AfterCategorySynchronize implements ObserverInterface
{
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Category $categoryManager,
        \Webkul\Odoomagentomultilang\Model\Odoomagentomultilang $languageMapping,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection
    ) {
        $this->_requestInterface = $requestInterface;
        $this->_scopeConfig = $scopeConfig;
        $this->_categoryManager = $categoryManager;
        $this->_languageMapping = $languageMapping;
        $this->_connection = $connection;
    }

    /**
     * customer register event handler
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $route = $this->_requestInterface->getControllerName();
        $product = $observer->getEvent()->getProduct();
        $mageId = $observer->getMageId();
        $odooId = $observer->getOdooId();
        $autoTranslations = $this->_scopeConfig->getValue(
            'odoomagentoconnect/automatization_settings/category_translations'
        );
        if ($odooId && $mageId && $autoTranslations == 1) {
            $helper = $this->_connection;
            $mappingCollection = $this->_languageMapping->getCollection();
            foreach ($mappingCollection as $mapping) {
                $odooLang = $mapping->getOdooLang();
                $storeId = $mapping->getMagentoStoreId();
                $category = $this->_categoryManager->setStoreId($storeId)->load($mageId);
                $args = [(int)$odooId, ['name' => $category->getName()]];
                $resp = $helper->callOdooMethod("product.category", "write", $args, ['lang' => $odooLang]);
                if ($resp && $resp[0] == 0) {
                    $error = "Error during update, Lang".$odooLang." Product ".$mageId;
                    $error = $error." Reason >>".$resp[1];
                    $helper->addError($error);
                }
            }
        }
    }
}

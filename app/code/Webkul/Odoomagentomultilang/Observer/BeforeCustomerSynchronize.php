<?php

namespace Webkul\Odoomagentomultilang\Observer;

/**
 * Webkul Odoomagentomultilang BeforeCustomerSynchronize Observer
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Odoomagentoconnect\Helper\Connection;
use xmlrpcval;

class BeforeCustomerSynchronize implements ObserverInterface
{

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Webkul\Odoomagentomultilang\Model\Odoomagentomultilang $languageMapping,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection
    ) {
        $this->_requestInterface = $requestInterface;
        $this->_scopeConfig = $scopeConfig;
        $this->_languageMapping = $languageMapping;
        $this->_connection = $connection;
    }

    /**
     * customer register event handler
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $route = $this->_requestInterface->getControllerName();
        $mageId = $observer->getMageId();
        $helper = $this->_connection;
        $extraFieldArray = $helper->getSession()->getExtraFieldArray();

        $mappingCollection = $this->_languageMapping->getCollection()
            ->addFieldToFilter('magento_store_id', ['eq'=>$mageId])
            ->setPageSize(1);
        $odooLang = $mappingCollection->getFirstItem()->getOdooLang();
        if ($odooLang) {
            $extraFieldArray['lang'] = new xmlrpcval($odooLang, "string");
        }
        $extraFieldArray = $helper->getSession()->setExtraFieldArray($extraFieldArray);
    }
}

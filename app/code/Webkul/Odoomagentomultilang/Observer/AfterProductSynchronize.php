<?php

namespace Webkul\Odoomagentomultilang\Observer;

/**
 * Webkul Odoomagentomultilang AfterProductSynchronize Observer
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Odoomagentoconnect\Helper\Connection;
use xmlrpcval;

class AfterProductSynchronize implements ObserverInterface
{

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product $productManager,
        \Webkul\Odoomagentomultilang\Model\Odoomagentomultilang $languageMapping,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection
    ) {
        $this->_requestInterface = $requestInterface;
        $this->_scopeConfig = $scopeConfig;
        $this->_productManager = $productManager;
        $this->_languageMapping = $languageMapping;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_connection = $connection;
    }

    /**
     * customer register event handler
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $route = $this->_requestInterface->getControllerName();
        $mageId = $observer->getProduct();
        $odooId = $observer->getErpProduct();
        $proType = $observer->getType();
        $autoTranslations = $this->_scopeConfig
            ->getValue('odoomagentoconnect/automatization_settings/product_translations');
        if ($odooId && $mageId && $autoTranslations == 1 && in_array($proType, ['product', 'template'])) {
            $helper = $this->_connection;
            $odooObject = 'product.product';
            if ($proType == 'template') {
                $odooObject = 'product.template';
            }
            $parentId = $this->_catalogProductTypeConfigurable->getParentIdsByChild($mageId);
            if (!empty($parentId)) return false;
            $mappingCollection = $this->_languageMapping->getCollection();
            foreach ($mappingCollection as $mapping) {
                $odooLang = new xmlrpcval($mapping->getOdooLang(), "string");
                $storeId = $mapping->getMagentoStoreId();
                  
                $product = $this->_productManager->setStoreId($storeId)->load($mageId);
                $vals = [
                    'name' => $product->getName(),
                    'description' => $product->getDescription(),
                    'description_sale' => $product->getShortDescription()
                ];
      
                $args = [(int)$odooId, $vals];
                $resp = $helper->callOdooMethod($odooObject, "write", $args, ['lang' => $odooLang]);
                if ($resp && $resp[0] == 0) {
                    $error = "Error during update , Lang".$odooLang;
                    $error = $error." Product ".$mageId." Reason >>".$resp[1];
                    $helper->addError($error);
                }
            }
        }
    }
}

<?php

namespace Webkul\Odoomagentomultilang\Ui\Component\Listing\Column;

/**
 * Webkul Odoomagentomultilang Attribute Ui Component
 *
 * @category  Webkul
 * @package   Webkul_Odoomagentomultilang
 * @author    Webkul
 * @copyright Copyright (c) 2010-Present Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Attribute extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Column name
     */
    const NAME = 'column.price';

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $localeCurrency;
    protected $userFactory;
    protected $odoomagentomultilangFactory;

    /**
     * @param ContextInterface                            $context
     * @param UiComponentFactory                          $uiComponentFactory
     * @param \Magento\Framework\Locale\CurrencyInterface $localeCurrency
     * @param \Magento\Store\Model\StoreManagerInterface  $storeManager
     * @param array                                       $components
     * @param array                                       $data
     */
    public function __construct(
        ContextInterface $context,
        \Magento\User\Model\UserFactory $userFactory,
        \Webkul\Odoomagentomultilang\Model\OdoomagentomultilangFactory $odoomagentomultilangFactory,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->odoomagentomultilangFactory=$odoomagentomultilangFactory;
        $this->userFactory=$userFactory;
    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $odoomagentomultilang=$this->odoomagentomultilangFactory->create()->load($item['attribute_id']);
                $user=$this->userFactory->create()->load($odoomagentomultilang->getUserId());
                if (isset($item[$fieldName])) {
                    $item[$fieldName] = $fieldName;
                }
            }
        }

        return $dataSource;
    }
}

<?php

namespace Esparksinc\Odoo\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Manager;
use Webkul\Odoomagentoconnect\Helper\Connection;


class OdooCheckoutOnepageControllerSuccessActionObserver implements ObserverInterface
{
    protected $order;

    public function __construct(
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Webkul\Odoomagentoconnect\Helper\Connection $connection,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\Odoomagentoconnect\Model\ResourceModel\Order $orderMapping,
        \Webkul\Odoomagentoconnect\Model\Order $orderModel,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger

    ) {
        $this->order = $order;
        $this->_requestInterface = $requestInterface;
        $this->_connection = $connection;
        $this->_orderMapping = $orderMapping;
        $this->_orderModel = $orderModel;
        $this->_scopeConfig = $scopeConfig;
        $this->messageManager = $messageManager;
        $this->logger = $logger;

    }

    public function execute(Observer $observer)
    {
        $orderId = $observer->getEvent()->getOrderIds();
        $order = $this->order->load($orderId);
        $orderIncrementId = $order->getIncrementId();

        $paymentMethod = $order->getPayment()->getMethodInstance()->getCode();

        if($paymentMethod === 'cashondelivery'){
            $this->logger->debug('The payment option is COD so order is not synced');
        }else{

            $lastOrderId = $observer->getEvent()->getData('order');
            if ($lastOrderId->getCustomerIsGuest()) {
                $quote = $observer->getEvent()->getData('quote');
            } else {
                $quote = false;
            }

            $mapping = $this->_orderModel
                ->getCollection()
                ->addFieldToFilter('magento_id', ['eq'=>$lastOrderId->getEntityId()]);

            if (count($mapping) == 0) {
                    if (!$lastOrderId) {
                        return;
                    }
                    else{
                    $helper = $this->_connection;
                    $helper->getSocketConnect();
                    $userId = $helper->getSession()->getUserId();
                    if ($userId > 0) {
                        $odooName = $this->_orderMapping->exportOrder($lastOrderId, $quote);
                        if ($odooName) {
                            $this->logger->debug("Odoo Order ".$odooName[1]." Successfully Create.");
                        }
                    }
                }
            }
        }    
    }
}

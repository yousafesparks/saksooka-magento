<?php
namespace Esparksinc\Odoo\Cron;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Webkul\Odoomagentoconnect\Helper\Connection;

class OrderStatusCron
{
    protected $date;
    protected $logger;
    public function __construct(
        Context $context, 
        Filter $filter, 
        CollectionFactory $collectionFactory, 
        \Webkul\Odoomagentoconnect\Model\ResourceModel\Order $orderModel, 
        \Webkul\Odoomagentoconnect\Model\Order $orderMapping, 
        \Webkul\Odoomagentoconnect\Helper\Connection $connection, 
        \Magento\Framework\Stdlib\DateTime\DateTime $date, 
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_orderModel = $orderModel;
        $this->_orderMapping = $orderMapping;
        $this->_connection = $connection;
        $this->collectionFactory = $collectionFactory;
        $this->date = $date;
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $logger;

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Sales::synchronize');
    }

    public function execute()
    {
        $helper = $this->_connection;
        $helper->getSocketConnect();
        $userId = $helper->getSession()->getUserId();

        $prev_date = date('Y-m-d', strtotime('-01 days'));
        $orderCollection = $this->collectionFactory->create()
            ->addFieldToFilter('created_at', ['gteq' => $prev_date])
            ->addFieldToFilter('status', 'confirmed_order');

        if ($userId)
        {
            $countNonSyncOrder = 0;
            $countSyncOrder = 0;
            $orderIds = '';

            foreach ($orderCollection->getItems() as $order)
            {
                $orderId = $order->getData('entity_id');
                $mapping = $this->_orderMapping->getCollection()
                    ->addFieldToFilter('magento_id', ['eq' => $orderId]);
                if (count($mapping) == 0)
                {
                    $order = $this->_orderModel->exportOrder($order);
                    if ($order == 0)
                    {
                        $countNonSyncOrder++;
                    }
                    else
                    {
                        $countSyncOrder++;
                    }
                }
                else
                {
                    $orderIds = $order->getData('entity_id') . ',';
                }

            }

            if ($countNonSyncOrder) {
                $this->logger->debug($countNonSyncOrder.' order(s) cannot be synchronized at Odoo.');
            }
            if ($countSyncOrder) {
                $this->logger->debug($countSyncOrder.' order(s) synchronized at Odoo.');
            }
            if ($orderIds) {
                $this->logger->debug($orderIds.' order(s) are already synchronized at Odoo.');
            }
        }
        else
        {
            $errorMessage = $helper->getSession()->getErrorMessage();
            $this->logger->debug('Selected order(s) cannot be synchronized at Odoo. !! Reason : '.$errorMessage);
        }
    }
}
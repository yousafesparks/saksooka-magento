<?php
namespace Esparksinc\Customer\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\App\Emulation;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Email extends Template
{
    private $transportBuilder;
    protected $scopeConfig;
    protected $logger;

    public function __construct
    (
        TransportBuilder $transportBuilder,
        Template\Context $context,
        ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }
    public function sendOTPemail($otp,$customerName,$email)
    {
        try {
            
            $this->inlineTranslation->suspend();
            $sendername  = $this->scopeConfig->getValue('trans_email/ident_support/name',ScopeInterface::SCOPE_STORE);
            $senderemail = $this->scopeConfig->getValue('trans_email/ident_support/email',ScopeInterface::SCOPE_STORE);
            $sender = [
                'name' => $sendername,
                'email' => $senderemail,
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('email_change_otp')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'templateVar'  => "OTP",
                    'customername' => $customerName,
                    'otp' => $otp
                ])
                ->setFrom($sender)
                ->addTo($email)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            return 1;      
            
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
            return 0;      

        }
        
    }
}

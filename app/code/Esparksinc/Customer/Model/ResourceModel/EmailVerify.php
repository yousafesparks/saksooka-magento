<?php
namespace Esparksinc\Customer\Model\ResourceModel;

class EmailVerify extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('email_verify', 'email_verify_id');
    }
}

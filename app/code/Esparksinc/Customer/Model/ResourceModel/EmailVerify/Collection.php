<?php
namespace Esparksinc\Customer\Model\ResourceModel\EmailVerify;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'email_verify_id';
    protected $_eventPrefix = 'esparksinc_customer_email_verify_collection';
    protected $_eventObject = 'email_verify_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Esparksinc\Customer\Model\EmailVerify', 'Esparksinc\Customer\Model\ResourceModel\EmailVerify');
    }
}

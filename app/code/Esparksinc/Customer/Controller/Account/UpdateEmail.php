<?php
namespace Esparksinc\Customer\Controller\Account;

class UpdateEmail extends \Magento\Framework\App\Action\Action
{
    protected $custsession;
    protected $resultRedirectFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $custsession,
        \Magento\Framework\Controller\Result\Redirect $resultRedirect
    )
    {
        $this->custsession = $custsession;
        $this->resultRedirectFactory = $resultRedirect;
        parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if($this->custsession->isLoggedIn())
        {
            $this->_view->loadLayout();
            $this->_view->getPage()->getConfig()->getTitle()->set(__('Email Verification'));
            $this->_view->renderLayout();
        }
        else
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/');
            return $resultRedirect;
        }
    }
}

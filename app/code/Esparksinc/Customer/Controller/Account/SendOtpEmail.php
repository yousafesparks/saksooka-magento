<?php
namespace Esparksinc\Customer\Controller\Account;

use Esparksinc\Customer\Helper\Email;

class SendOtpEmail extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;
    protected $_emailVerifyFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
       Email $helperEmail,
       \Magento\Customer\Model\SessionFactory $customerSession,
       \Magento\Customer\Model\CustomerFactory $customers,
       \Esparksinc\Customer\Model\EmailVerifyFactory $emailVerifyFactory
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->helperEmail = $helperEmail;
        $this->customerSession = $customerSession;
        $this->_customers = $customers;
        $this->_emailVerifyFactory = $emailVerifyFactory;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $email = $this->getRequest()->getParam('email');
        $customerData = $this->_customers->create()->getCollection()->addFieldToFilter('email', $email);
        if(count($customerData) > 0)
        {
            $data = array(__('Email already exist'));
            // $this->messageManager->addErrorMessage(__('Email already exist'));
            $resultJson->setData($data);
            return $resultJson;
        }
        $otp  = substr(str_shuffle("0123456789"), 0, 4);        
        $emailverifyfactory = $this->_emailVerifyFactory->create()->load($email,'email');
        if ($emailverifyfactory->getEmailVerifyId()) {
            $emailverifyfactory->setOtp($otp);
            $emailverifyfactory->save();
        }
        else {
            $array = array();
            $array['email'] = $email;
            $array['otp'] = $otp;
            $emailverifyfactory = $this->_emailVerifyFactory->create();
            $emailverifyfactory->addData($array);
            $emailverifyfactory->save();
        }
        $customerid = $this->customerSession->create()->getId();
        $customer = $this->_customers->create()->load($customerid);
        $customerName = $customer->getFirstname().' '.$customer->getLastname();
        $emailsent = $this->helperEmail->sendOTPemail($otp,$customerName,$email);
        $data = array("success");  
        if($emailsent) {
            // email sent
            $resultJson->setData($data);
        } else {
            $resultJson->setData(false);
        }
        return $resultJson;
    }
}

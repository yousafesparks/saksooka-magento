<?php
namespace Esparksinc\Customer\Controller\Account;

class VerifyEmailOtp extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
       \Magento\Customer\Model\SessionFactory $customerSession,
       \Magento\Customer\Model\Customer $customers,
       \Esparksinc\Customer\Model\EmailVerifyFactory $emailVerifyFactory
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->customerSession = $customerSession;
        $this->_customers = $customers;
        $this->_emailVerifyFactory = $emailVerifyFactory;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $otp = $this->getRequest()->getParam('otp');
        $newemail = $this->getRequest()->getParam('email');        
        $emailverifyfactory = $this->_emailVerifyFactory->create()->load($newemail,'email');;
        if($emailverifyfactory->getOtp() == $otp){
            $customerid = $this->customerSession->create()->getId();
            $customer = $this->_customers->load($customerid);
            $customer->setEmail($newemail);
            $customer->save();
            $data = array("success");
            return $resultJson->setData($data);
        }
        else {
            $data = array(__('Invalid OTP.'));
            return $resultJson->setData($data);
        }
    }
}

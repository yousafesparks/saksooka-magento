<?php
namespace Esparksinc\Customer\Controller\Account;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Otpverify extends \Magento\Framework\App\Action\Action
{
    protected $helperapi;
    protected $helpercustomer;
    protected $smsmodel;
    protected $emailfilter;
    protected $registercustomer;
    protected $customerRepositoryInterface;

    public function __construct(Context $context,
        \Magecomp\Sms\Helper\Apicall $helperapi,
        \Magecomp\Sms\Helper\Customer $helpercustomer,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magecomp\Sms\Model\SmsFactory $smsmodel,
        \Magento\Email\Model\Template\Filter $filter)
    {
        $this->helperapi = $helperapi;
        $this->helpercustomer = $helpercustomer;
        $this->smsmodel = $smsmodel;
        $this->registercustomer = $customer;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->emailfilter = $filter;
        parent::__construct($context);
    }

    public function execute()
    {
        try
        {
            if ($this->getRequest()->isPost()) {
                $mobilenumber = $this->getRequest()->getPost('mobile');
                $otp = $this->getRequest()->getPost('otp');
                if (!empty($mobilenumber)) {
                    
                        //Custom Work
                        $customerCollection = $this->registercustomer->getCollection();
                        $customerCollection->addFieldToFilter("mobilenumber", $mobilenumber);
    
                        $smsModel = $this->smsmodel->create();
                        $smscollection = $smsModel->getCollection()
                           ->addFieldToFilter('mobile_number', $mobilenumber)
                           ->addFieldToFilter('otp', $otp);
    
                        if(count($smscollection) == 1)
                        {
                            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                            $resultJson->setData(true);
                            return $resultJson;
                        }
                        else{
                            $this->messageManager->addErrorMessage(__('Invalid otp.'));
                            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                            $resultJson->setData(false);
                            return $resultJson;
                        }
                    
                }
            }
            
        }
        catch (\Exception $e) {
            $data = array("error");
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($data);
            return $resultJson;
        }
    }
}
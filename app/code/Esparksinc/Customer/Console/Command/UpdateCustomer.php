<?php
namespace Esparksinc\Customer\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Console command for UpdateCustomer
*/
class UpdateCustomer extends Command
{
    /**
    * @var \Magento\Framework\App\State
    */
    protected $appState;
    
    protected $customerCollection;
    
    /**
    * @param Magento\Framework\App\Helper\Context $context
    * @param Magento\Store\Model\StoreManagerInterface $storeManager
    * @param Magento\Customer\Model\CustomerFactory $customerFactory,
    * @param Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    */

    /**
    * @param \Magento\Framework\App\State $appState
    */
    public function __construct(
        \Magento\Framework\App\State $appState,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\ResourceModel\CustomerFactory $customerResourceFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->appState = $appState;
        $this->customerCollection = $customerCollection;
        $this->_storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->customerResourceFactory = $customerResourceFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        
        parent::__construct();
    }

    /**
    * {@inheritdoc}
    */
    protected function configure()
    {
        $this->setName('esparksinc:customer:upadte-phone');
        $this->setDescription('Updating Customers');
    }

    /**
    * {@inheritdoc}
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        $getAllCustomers = $this->customerCollection->create()->addAttributetoSelect('*');
        foreach($getAllCustomers as $customer)
        {
            $customerAddress = array();
            foreach ($customer->getAddresses() as $address)
            {
                $customerAddress[] = $address->toArray();
                
            }
            if(!empty($customerAddress))
            {
                $input = $customerAddress['0']['telephone'];

                if(!empty($input)){

                    if($input[0] == "5")
                    {
                        $input = ltrim($input, '0');
                        $input = "966".$input;
                        
                    }
                    elseif($input[0] == "+")
                    {
                        $input = ltrim($input, "+");
                    }
                    elseif($input[0] == "0")
                    {
                        $input = ltrim($input, '0');
                        $input = "966".$input;
                        
                    }

                    $input = "+".$input;
                    $customerSave = $this->customerFactory->create()->load($customer->getId());
                    $customerData = $customerSave->getDataModel();
                    $customerData->setCustomAttribute('mobilenumber',$input);
                    $customerSave->updateData($customerData);
                    // \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory
                    $customerResource = $this->customerResourceFactory->create();
                    $customerResource->saveAttribute($customerSave, 'mobilenumber');
                }

            }
            unset($customerAddress);     
        }
        
        $this->getOrderCollection();           
        
        $output->writeln("CUstomers Synced Successfully");
        
    }

    public function getOrderCollection()
    {
        $collection = $this->_orderCollectionFactory->create()
          ->addAttributeToSelect('*'); //Add condition if you wish
        foreach ($collection as $order) {
            $customer = $this->customerFactory->create()->load($order->getCustomerId());
            $mobileNumber = $customer->getData('mobilenumber');
            if (!empty($mobileNumber)) {
                $shippingAddress = $order->getShippingAddress();
                $shippingAddress->setCustomerAddressMobilenumber($mobileNumber);
                $billingAddress = $order->getBillingAddress();
                $billingAddress->setCustomerAddressMobilenumber($mobileNumber);
                $order->save();
            }
        }
        return;
    }
    
    
}
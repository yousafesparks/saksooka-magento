<?php
namespace Esparksinc\CodNotification\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $transportBuilder;
    protected $storeManager;
    protected $inlineTranslation;
    protected $logger;

    public function __construct
    (
        Context $context,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }
    public function NotifyViaEmail($RecieverEmail,$customer_name,$confirmlink)
    {
        try {
            
            $this->inlineTranslation->suspend();
            $templateId = 'filtered_products'; // template id
            $fromEmail = $this->scopeConfig->getValue('trans_email/ident_custom1/email',ScopeInterface::SCOPE_STORE);  // sender Email id
            $fromName = $this->scopeConfig->getValue('trans_email/ident_custom1/name',ScopeInterface::SCOPE_STORE);  
            $from = ['email' => $fromEmail, 'name' => $fromName];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('notify_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'confirmlink' => $confirmlink,
                    'customer_name' => $customer_name,
                ])
                ->setFrom($from)
                ->addTo($RecieverEmail)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}

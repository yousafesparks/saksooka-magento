<?php 
namespace Esparksinc\CodNotification\Helper;

use Magento\Store\Model\ScopeInterface;

class Cod extends \Magecomp\Sms\Helper\Data
{
    // USER TEMPLATE
    const SMS_IS_COD_ORDER_NOTIFICATION = 'usertemplate/codnotification/enable';
    const SMS_IS_COD_ORDER_NOTIFICATION_TEMPLATE = 'usertemplate/codnotification/template';
    const SMS_IS_COD_ORDER_NOTIFICATION_DLTID = 'usertemplate/codnotification/dltid';


	public function isCodNotificationForUser() {
        return $this->isEnabled() && $this->scopeConfig->getValue(self::SMS_IS_COD_ORDER_NOTIFICATION,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }

    public function getCodNotificationUserTemplate()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_IS_COD_ORDER_NOTIFICATION_TEMPLATE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
        }
    }
    public function getCodNotificationUserDltid()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_IS_COD_ORDER_NOTIFICATION_DLTID,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
        }
    }
}
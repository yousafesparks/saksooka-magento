<?php
namespace Esparksinc\CodNotification\Controller\Order;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;
    protected $storeManager;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
       \Magento\Sales\Model\Order $order,
       \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->order = $order; 
        $this->storeManager = $storeManager;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $params = $this->getRequest()->getParams();
        $orderloaded = $this->order->load($params['orderid']);
        if($orderloaded['notification_id'] === $params['notificationid'])
        {
            $created_at = $orderloaded['created_at'];
            $currentTime = date('Y-m-d H:i:s');
            $endtime = date("Y-m-d H:i:s", strtotime("{$currentTime}"));
            $difference = $this->differenceInHours($created_at,$endtime);
            $baseurl = $baseurl = $this->_url->getUrl('verification/success');
            if($difference <= 24)
            {
                if($orderloaded->getStatus() === 'pending')
                {
                    $orderloaded->setStatus("confirmed_order");
                    $orderloaded->save();
                    $this->messageManager->addSuccess('Order Confirmed');
                }
                else{
                    $baseurl = $baseurl = $this->_url->getUrl();
                    $this->messageManager->addSuccess('Order Already Confirmed');
                }
            }
            else{
                $baseurl = $baseurl = $this->_url->getUrl();
                $this->messageManager->addSuccess('Cannot Proccess your request');
            }
            return $this->resultRedirectFactory->create()->setPath($baseurl);
        }
        else{
            $baseurl = $baseurl = $this->_url->getUrl();
            $this->messageManager->addSuccess('Your link is expired');
            return $this->resultRedirectFactory->create()->setPath($baseurl);
        }
    }
    public function differenceInHours($startdate,$enddate){
        $starttimestamp = strtotime($startdate);
        $endtimestamp = strtotime($enddate);
        $difference = abs($endtimestamp - $starttimestamp)/3600;
        return $difference;
    }
}

<?php
namespace Esparksinc\CodNotification\Cron;

use Esparksinc\CodNotification\Helper\Email;

class Notification
{
    
    protected $_orderCollectionFactory;
    protected $helperdata;
    protected $helperEmail;
    protected $storeManager;
    protected $customer; 

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Varsha\Whatsappnotify\Helper\Data $helperdata,
        \Magento\Sales\Model\Order $order,
        Email $helperEmail,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customer,
        \Magecomp\Sms\Helper\Apicall $helperapi,
        \Esparksinc\CodNotification\Helper\Cod $helperCod,
        \Magento\Email\Model\Template\Filter $filter
    )
    {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->helperdata = $helperdata;
        $this->order = $order; 
        $this->helperEmail = $helperEmail;
        $this->storeManager = $storeManager;
        $this->customer = $customer;
        $this->helperapi = $helperapi;
        $this->helperCod = $helperCod;
        $this->emailfilter = $filter;
    }
    public function notify()
    {
        $pMethod = 'cashondelivery';
        $currentTime = date('Y-m-d H:i:s');
        $prev_date = date('Y-m-d', strtotime('-1 days'));
        $orderCollection = $this->_orderCollectionFactory->create()
            ->addFieldToFilter('created_at', ['gteq' => $prev_date])
            ->addFieldToFilter('status','pending');
        $orderCollection->getSelect()
        ->join(
            ["sop" => "sales_order_payment"],
            'main_table.entity_id = sop.parent_id',
            array('method')
        )
        ->where('sop.method = ?',$pMethod );
        foreach ($orderCollection as $singleorder) {
            
            $currentTime = date('Y-m-d H:i:s');
            $addedThreehours = date("Y-m-d H:i:s", strtotime("{$singleorder['created_at']}"));
            $addedTenhours = date("Y-m-d H:i:s", strtotime("{$singleorder['created_at']}"));
            $addedSixteenhours = date("Y-m-d H:i:s", strtotime("{$singleorder['created_at']}"));
            $diff1 = $this->differenceInHours($addedThreehours,$currentTime);
            $diff2 = $this->differenceInHours($addedTenhours,$currentTime);
            $diff3 = $this->differenceInHours($addedSixteenhours,$currentTime);

            if(($diff1 > 3 && $diff1 < 4)||($diff2 > 10 && $diff2 < 11)||($diff3 > 16 && $diff3 < 17))
            {
                $currentTime = date('Y-m-d H:i:s');
                $notifyid = hash('sha256', $currentTime.$singleorder['entity_id']);
                $orderloaded = $this->order->load($singleorder['entity_id']);
                $orderloaded->setNotificationId($notifyid);
                $orderloaded->save();
                $baseurl = $this->storeManager->getStore()->getUrl();
                $baseurl = str_replace('/admin', '', $baseurl);
                $order_id = $singleorder['entity_id'];
                $confirmlink = $baseurl."verification/order/index/orderid/$order_id/notificationid/$notifyid";
                $customer = $this->customer->load($singleorder['customer_id']);
                $billingAddress = $orderloaded->getBillingAddress();
                $recievername = $customer['firstname'].' '.$customer['lastname'];
                
                $recievernumber = $billingAddress->getTelephone();
                $mobile = $customer->getMobilenumber();
                if($mobile != '' && $mobile != null)
                {
                    $recievernumber = $mobile;
                }

                $orderid = $orderloaded->getIncrementId();
                $ordertotal = $orderloaded->getGrandTotal();
                $storeId = $orderloaded->getStoreId();
                $this->NotifyViaWhatsapp($recievername,$recievernumber,$orderid,$ordertotal,$confirmlink,$storeId);
                $this->NotifyViaSms($customer,$recievernumber,$orderloaded,$ordertotal,$confirmlink);
                $recieveremail = $customer['email'];
                $this->helperEmail->NotifyViaEmail($recieveremail,$recievername,$confirmlink);
            }
            else if($diff3>24){
                $orderloaded = $this->order->load($singleorder['entity_id']);
                $orderloaded->setStatus("canceled");
                $orderloaded->cancel()->save();
            }
        }
		return $this;

    }
    public function NotifyViaWhatsapp($recievername,$recievernumber,$orderid,$ordertotal,$confirmlink,$storeId)
    {
        $stores = $this->storeManager->getStores(true, true);
        foreach ($stores as $storekey => $storevalue) {
            if ($storeId == $storevalue->getId()) {
                $store_name = $storevalue->getCode();
            }
        } 
        $from_number = $this->helperdata->getOrderPlacePhoneForUser();
        $auth_key =$this->helperdata->getShipmenTemplateForUser();
        $url =  $this->helperdata->getOrderPlaceTemplateForUser();
        // $recievernumber = '+'.$recievernumber;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        if($store_name == "en") {
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
            "{ 
                \"from\": { \"phone_number\": \"$from_number\" },
                \"to\": [ { \"phone_number\": \"$recievernumber\" } ],
                \"data\": 
                { 
                    \"message_template\": { \"storage\": \"none\", 
                    \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\",
                    \"template_name\": \"cod_verificaiton_link__english\",
                    \"language\": 
                        { 
                            \"policy\": \"deterministic\", 
                            \"code\": \"en\" }, 
                            \"template_data\": 
                                [  
                                    {
                                        \"data\": \"$recievername\"
                                    },
                                    {
                                        \"data\": \"$confirmlink\"
                                    }
                                ]
                        } 
                } 
            }"
        );
        }
        elseif($store_name == "ar") {
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
                "{ 
                    \"from\": { \"phone_number\": \"$from_number\" },
                    \"to\": [ { \"phone_number\": \"$recievernumber\" } ],
                    \"data\": 
                    { 
                        \"message_template\": { \"storage\": \"none\", 
                        \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\",
                        \"template_name\": \"cod_verificaiton_link__arabic\",
                        \"language\": 
                            { 
                                \"policy\": \"deterministic\", 
                                \"code\": \"ar\" }, 
                                \"template_data\": 
                                    [  
                                        {
                                            \"data\": \"$recievername\"
                                        },
                                        {
                                            \"data\": \"$confirmlink\"
                                        }
                                    ]
                            } 
                    } 
                }"
            );
        }
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$auth_key.'';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }
    public function differenceInHours($startdate,$enddate){
        $starttimestamp = strtotime($startdate);
        $endtimestamp = strtotime($enddate);
        $difference = abs($endtimestamp - $starttimestamp)/3600;
        return $difference;
    }

    public function NotifyViaSms($customer,$recievernumber,$order,$ordertotal,$confirmlink)
    {
        $this->emailfilter->setVariables([
            'order' => $order,
            'customer' => $customer,
            'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
            'mobilenumber' => $recievernumber,
            'cod_link' => $confirmlink
        ]);

        if ($this->helperCod->isCodNotificationForUser())
        {
            $message = $this->helperCod->getCodNotificationUserTemplate();
            $dltid = $this->helperCod->getCodNotificationUserDltid();

            $finalmessage = $this->emailfilter->filter($message);
            $this->helperapi->callApiUrl($recievernumber,$finalmessage,$dltid);
        }
    }
}

<?php
namespace Esparksinc\Checkout\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Checkout implements ArgumentInterface
{
    public $customerSession;

    public $urlInterface;
    
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        $this->_customerSession = $customerSession;
        $this->_urlInterface = $urlInterface;
    }

    public function getCustomerLoggedIn()
    {
        return $this->_customerSession;
    }
}

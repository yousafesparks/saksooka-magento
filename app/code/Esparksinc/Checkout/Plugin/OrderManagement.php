<?php

namespace Esparksinc\Checkout\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;

/**
 * Class OrderManagement
 */
class OrderManagement
{
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Model\Order $order
    ) {
        $this->order = $order;
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
    }

    public function beforePlace(
        OrderManagementInterface $subject,
        OrderInterface $order
    ): array {
        $quoteId = $order->getQuoteId();
        $customerId = $this->customerSession->getCustomer()->getId();
        if ($quoteId && $customerId) {
            $customer = $this->customerFactory->create()->load($customerId);
            if ($customer->getId()) {
                $mobileNumber = $customer->getData('mobilenumber');
                $shippingAddress = $order->getShippingAddress();
                if($shippingAddress->getTelephone())
                {
                    
                }
                else{
                    $shippingAddress->setTelephone($mobileNumber);
                }
                $billingAddress = $order->getBillingAddress();
                $billingAddress->setTelephone($mobileNumber);
            }
        }
        return [$order];
    }
}
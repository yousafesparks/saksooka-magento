<?php

namespace Esparksinc\Summary\ViewModel;

class Product implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    
    private $productFactory;
    private $stockItemRepository;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository
    )
    {
        $this->productFactory = $productFactory;
        $this->stockItemRepository = $stockItemRepository;
    }

    public function getBrand($id)
    {
      $product = $this->productFactory->create()->load($id);
      $productBrand = $product->getResource()->getAttribute('mgs_brand')->getFrontend()->getValue($product);
      return $productBrand;
    }
    public function getQty($sku)
    {
      $productQty = $this->stockItemRepository->getStockItemBySku($sku);
      return $productQty->getQty();;
    }
}

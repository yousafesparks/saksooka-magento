<?php

namespace Esparksinc\EmailValidation\Controller\EmailValidation;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;

class Index extends Action
{
    protected $resultJsonFactory;
    protected $customerModel;

    public function __construct(
        Context $context, 
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Customer $customerModel    
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_customerModel = $customerModel;
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $email = $this->getRequest()->getParam('emailAddress');
        $customerData = $this->_customerModel->getCollection()->addFieldToFilter('email', $email);
        
        if(!count($customerData)) {
            $resultJson->setData(true);
        } else {
            $resultJson->setData(false);
        }
        return $resultJson;
    }
}

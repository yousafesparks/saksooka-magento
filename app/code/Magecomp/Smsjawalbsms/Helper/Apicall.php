<?php 
namespace Magecomp\Smsjawalbsms\Helper;
class Apicall extends \Magento\Framework\App\Helper\AbstractHelper
{
	const XML_JAWALB_SMS_API_USERNAME = 'sms/smsgatways/jawalbusername';
    const XML_JAWALB_SMS_API_PASSWORD = 'sms/smsgatways/jawalbpassword';
    const XML_JAWALB_SMS_SENDER = 'sms/smsgatways/jawalbsmssender';
    const XML_JAWALB_SMS_APIURL = 'sms/smsgatways/jawalbsmsapiurl';

	public function __construct(\Magento\Framework\App\Helper\Context $context)
	{
		parent::__construct($context);
	}

    public function getTitle() {
        return __("Jawalb SMS");
    }

    public function getApiUrl()	{
        return $this->scopeConfig->getValue(
            self::XML_JAWALB_SMS_APIURL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getApiUsername(){
        return $this->scopeConfig->getValue(
            self::XML_JAWALB_SMS_API_USERNAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getApiPassword(){
        return $this->scopeConfig->getValue(
            self::XML_JAWALB_SMS_API_PASSWORD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiSenderName(){
        return $this->scopeConfig->getValue(
            self::XML_JAWALB_SMS_SENDER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function validateSmsConfig()
    {
        return $this->getApiUrl() && $this->getApiUsername() && $this->getApiPassword() && $this->getApiSenderName();
    }

    public function callApiUrl($mobilenumbers,$message)
    {
        try
        {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testsms.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($message);
            $logger->info($mobilenumbers);

            $url = $this->getApiUrl();
            $user = $this->getApiUsername();
            $password=$this->getApiPassword();
            $senderid = $this->getApiSenderName();

            $ch = curl_init();
            if (!$ch)
            {
                return "Couldn't initialize a cURL handle";
            }
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,
                "user=$user&pass=$password&to=$mobilenumbers&message=$message&sender=$senderid");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $curlresponse = curl_exec($ch); // execute
            $logger->info($curlresponse);
            if(curl_errno($ch))
            {
                return 'Error: '.curl_error($ch);
            }
            curl_close($ch);
            return true;
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
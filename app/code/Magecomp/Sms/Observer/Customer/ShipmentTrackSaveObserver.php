<?php
namespace Magecomp\Sms\Observer\Customer;

use Magento\Framework\Event\ObserverInterface;

class ShipmentTrackSaveObserver implements ObserverInterface
{
    protected $helperapi;
    protected $helpershipment;
    protected $emailfilter;
    protected $customerFactory;
    protected $orderRepository;
    protected $helpershipmenttrack;

    public function __construct(
        \Magecomp\Sms\Helper\Apicall $helperapi,
        \Magecomp\Sms\Helper\Shipment $helpershipment,
        \Magento\Email\Model\Template\Filter $filter,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Shipment $shipment,
        \Magecomp\Sms\Helper\Shipmenttrack $helpershipmenttrack)
    {
        $this->helperapi = $helperapi;
        $this->helpershipment = $helpershipment;
        $this->emailfilter = $filter;
        $this->customerFactory = $customerFactory;
        $this->orderRepository = $orderRepository;
        $this->shipment = $shipment;
        $this->helpershipmenttrack = $helpershipmenttrack;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if(!$this->helpershipment->isEnabled())
            return $this;


        $track = $observer->getEvent()->getTrack();

        $shipment = $this->shipment->load($track->getParentId());
        $order = $this->orderRepository->get($track->getOrderId());
        $customer = $this->customerFactory->create()->load($order->getCustomerId());
        $billingAddress = $order->getBillingAddress();

        $mobilenumber = $billingAddress->getTelephone();
        $mobile = $customer->getMobilenumber();

        if ($mobile != '' && $mobile != null) {
            $mobilenumber = $mobile;
        }
        $trackhtml = "";


            $trackLink = $this->helpershipmenttrack->getShipmentTrackingLink($track->getCarrierCode());
            if($trackLink){
                $this->emailfilter->setVariables([
                    'trackingnumber' => $track->getTrackNumber()
                ]);
                $trackLink =  $this->emailfilter->filter($trackLink);
            }else{
                $trackLink = $track->getTrackNumber();
            }

            $trackhtml .= $this->helpershipmenttrack->getShipmentTrackingNumberLabel()." " .$trackLink." ".
                $this->helpershipmenttrack->getShipmentTrackingTitleLabel() ." " . $track->getTitle().",";




        $this->emailfilter->setVariables([
            'shipment' => $shipment,
            'order' => $order,
            'customer' => $customer,
            'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
            'mobilenumber' => $mobilenumber,
            'trackinginfo' => $trackhtml
        ]);

        $message = $this->helpershipmenttrack->getShipmentNotificationUserTemplate();
        $finalmessage = $this->emailfilter->filter($message);

        $apiResponse = $this->helperapi->callApiUrl($mobilenumber, $finalmessage);




        return $this;
    }
}

<?php
namespace Magecomp\Sms\Observer\Customer;

use Magento\Framework\Event\ObserverInterface;

class OrderSaveObserver implements ObserverInterface
{
    protected $helperapi;
    protected $helperorder;
    protected $emailfilter;
    protected $customerFactory;
    protected $checkoutsession;

    public function __construct(
        \Magecomp\Sms\Helper\Apicall $helperapi,
        \Magecomp\Sms\Helper\Order $helperorder,
        \Magento\Email\Model\Template\Filter $filter,
        \Magento\Checkout\Model\Session $session,
        \Magento\Customer\Model\CustomerFactory $customerFactory)
    {
        $this->helperapi = $helperapi;
        $this->helperorder = $helperorder;
        $this->emailfilter = $filter;
        $this->checkoutsession = $session;
        $this->customerFactory = $customerFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if(!$this->helperorder->isEnabled())
            return $this;

        $order = $observer->getEvent()->getOrder();
        if($order)
        {
            $billingAddress = $order->getBillingAddress();
            $mobilenumber = $billingAddress->getTelephone();

            if($order->getCustomerId() > 0)
            {
                $customer = $this->customerFactory->create()->load($order->getCustomerId());
                $mobile = $customer->getMobilenumber();
                if($mobile != '' && $mobile != null)
                {
                    $mobilenumber = $mobile;
                }

                $this->emailfilter->setVariables([
                    'order' => $order,
                    'customer' => $customer,
                    'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
                    'mobilenumber' => $mobilenumber
                ]);
            }
            else
            {
                $this->emailfilter->setVariables([
                    'order' => $order,
                    'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
                    'mobilenumber' => $mobilenumber
                ]);
            }

            if ($this->helperorder->isOrderNotificationForUser())
            {
                $message = $this->helperorder->getOrderNotificationUserTemplate();
                $dltid = $this->helperorder->getOrderNotificationUserDltid();

                $finalmessage = $this->emailfilter->filter($message);
                $this->helperapi->callApiUrl($mobilenumber,$finalmessage,$dltid);
            }

            if($this->helperorder->isOrderNotificationForAdmin() && $this->helperorder->getAdminNumber())
            {
                $message = $this->helperorder->getOrderNotificationForAdminTemplate();
                $dltid = $this->helperorder->getOrderNotificationForAdminDltid();

                $finalmessage = $this->emailfilter->filter($message);
                $this->helperapi->callApiUrl($this->helperorder->getAdminNumber(),$finalmessage,$dltid);
            }
            $this->checkoutsession->setGuestOrderConfirm('0');
        }
        return $this;
    }
}

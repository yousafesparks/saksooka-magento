<?php 
namespace Magecomp\Sms\Helper;

use Magento\Store\Model\ScopeInterface;

class Invoice extends \Magecomp\Sms\Helper\Data
{
    // USER TEMPLATE
    const SMS_IS_CUSTOMER_INVOICE_NOTIFICATION = 'usertemplate/userinvoice/enable';
    const SMS_CUSTOMER_INVOICE_NOTIFICATION_TEMPLATE = 'usertemplate/userinvoice/template';
    const SMS_CUSTOMER_INVOICE_NOTIFICATION_DLTID = 'usertemplate/userinvoice/dltid';


	//ADMIN TEMPLATE
    const SMS_IS_ADMIN_INVOICE_NOTIFICATION = 'admintemplate/admininvoice/enable';
    const SMS_ADMIN_INVOICE_NOTIFICATION_TEMPLATE = 'admintemplate/admininvoice/template';
    const SMS_ADMIN_INVOICE_NOTIFICATION_DLTID = 'admintemplate/admininvoice/dltid';


    public function isInvoiceNotificationForUser($storeid) {
        return $this->isEnabled() && $this->scopeConfig->getValue(self::SMS_IS_CUSTOMER_INVOICE_NOTIFICATION,
            ScopeInterface::SCOPE_STORE,
            $storeid);
    }

    public function getInvoiceNotificationUserTemplate($storeid)
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_CUSTOMER_INVOICE_NOTIFICATION_TEMPLATE,
                ScopeInterface::SCOPE_STORE,
                $storeid);
        }
    }
    public function getInvoiceNotificationUserDltid($storeid)
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_CUSTOMER_INVOICE_NOTIFICATION_DLTID,
                ScopeInterface::SCOPE_STORE,
                $storeid);
        }
    }

    public function isInvoiceNotificationForAdmin($storeid)
    {
        return $this->isEnabled() && $this->scopeConfig->getValue(self::SMS_IS_ADMIN_INVOICE_NOTIFICATION,
            ScopeInterface::SCOPE_STORE,
            $storeid);
    }

    public function getInvoiceNotificationForAdminTemplate($storeid)
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_ADMIN_INVOICE_NOTIFICATION_TEMPLATE,
                ScopeInterface::SCOPE_STORE,
                $storeid);
        }
    }
    public function getInvoiceNotificationForAdminDltid($storeid)
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_ADMIN_INVOICE_NOTIFICATION_DLTID,
                ScopeInterface::SCOPE_STORE,
                $storeid);
        }
    }
}
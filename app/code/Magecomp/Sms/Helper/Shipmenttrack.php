<?php 
namespace Magecomp\Sms\Helper;

use Magento\Store\Model\ScopeInterface;

class Shipmenttrack extends \Magecomp\Sms\Helper\Data
{
    // USER TEMPLATE
    const SMS_IS_CUSTOMER_SHIPMENT_NOTIFICATION = 'usertemplate/usershipmenttrack/enable';
    const SMS_CUSTOMER_SHIPMENT_NOTIFICATION_TEMPLATE = 'usertemplate/usershipmenttrack/template';
    const SMS_CUSTOMER_SHIPMENT_TRACKING_NUMBER = 'usertemplate/usershipmenttrack/trackingnumber';
    const SMS_CUSTOMER_SHIPMENT_TRACKING_TITLE = 'usertemplate/usershipmenttrack/trackingtitle';

	public function isShipmentNotificationForUser() {
        return $this->isEnabled() && $this->scopeConfig->getValue(self::SMS_IS_CUSTOMER_SHIPMENT_NOTIFICATION,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }

    public function getShipmentNotificationUserTemplate()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_CUSTOMER_SHIPMENT_NOTIFICATION_TEMPLATE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
        }
    }
    public function getShipmentTrackingNumberLabel()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_CUSTOMER_SHIPMENT_TRACKING_NUMBER,
                ScopeInterface::SCOPE_STORE,
                $this->getStoreid());
        }
    }
    public function getShipmentTrackingTitleLabel()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_CUSTOMER_SHIPMENT_TRACKING_TITLE,
                ScopeInterface::SCOPE_STORE,
                $this->getStoreid());
        }
    }
    public function getShipmentTrackingLink($carrercode)
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue("usertemplate/usershipmenttrack/".$carrercode."_link",
                ScopeInterface::SCOPE_STORE,
                $this->getStoreid());
        }
    }

}
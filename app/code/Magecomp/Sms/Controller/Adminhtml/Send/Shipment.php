<?php
/**
 * Magento Magecomp_Sms extension
 *
 * @category   Magecomp
 * @package    Magecomp_Sms
 * @author     Magecomp
 */

namespace Magecomp\Sms\Controller\Adminhtml\Send;


class Shipment extends \Magento\Backend\App\Action
{
    protected $resultRedirect;
    protected $helperapi;
    protected $customerFactory;
    protected $emailfilter;
    protected $helpershipment;
    protected $orderRepository;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\ResultFactory $result,
        \Magecomp\Sms\Helper\Apicall $helperapi,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Email\Model\Template\Filter $filter,
        \Magecomp\Sms\Helper\Shipment $helpershipment,
        \Magento\Sales\Model\Order\Shipment $shipment,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirect = $result;
        $this->helpershipment = $helpershipment;
        $this->helperapi = $helperapi;
        $this->customerFactory = $customerFactory;
        $this->emailfilter = $filter;
        $this->shipment = $shipment;
        $this->orderRepository = $orderRepository;
    }

    public function execute()
    {
        $storeId = $this->getRequest()->getParam('store');
        $shipmentid = $this->getRequest()->getParam('shipment_id');
        try {

            $shipment = $this->shipment->load($shipmentid);
            $order = $this->orderRepository->get($shipment->getOrderId());
            $customer = $this->customerFactory->create()->load($order->getCustomerId());
            $billingAddress = $order->getBillingAddress();

            $mobilenumber = $billingAddress->getTelephone();
            $mobile = $customer->getMobilenumber();

            if ($mobile != '' && $mobile != null) {
                $mobilenumber = $mobile;
            }

            $this->emailfilter->setVariables([
                'shipment' => $shipment,
                'order' => $order,
                'customer' => $customer,
                'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
                'mobilenumber' => $mobilenumber
            ]);

            $message = $this->helpershipment->getShipmentNotificationUserTemplate($storeId);
            $finalmessage = $this->emailfilter->filter($message);
            $apiResponse = $this->helperapi->callApiUrl($mobilenumber, $finalmessage);

            if ($apiResponse === true) {
                $this->getMessageManager()->addSuccess("SMS Sent Successfully to the Customer Mobile : " . $mobilenumber);
            } else {
                $this->getMessageManager()->addError("Something Went Wrong While sending SMS");
            }

            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/shipment/view/shipment_id/" . $shipmentid, ['store' => $storeId]);
            return;
        } catch (\Exception $e) {
            $this->getMessageManager()->addError("There is some Technical problem, Please tray again");
            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/shipment/view/shipment_id/" . $shipmentid, ['store' => $storeId]);
            return;
        }

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magecomp_Sms::magecompsms');
    }
}
<?php
/**
 * Magento Magecomp_Sms extension
 *
 * @category   Magecomp
 * @package    Magecomp_Sms
 * @author     Magecomp
 */

namespace Magecomp\Sms\Controller\Adminhtml\Send;


class Invoicecustom extends \Magento\Backend\App\Action
{
    protected $resultRedirect;
    protected $helperapi;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\ResultFactory $result,
        \Magecomp\Sms\Helper\Apicall $helperapi
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirect = $result;
        $this->helperapi = $helperapi;
    }

    public function execute()
    {

        $invoiceid = $this->getRequest()->getParam('current_invoice_id');
        $mobilenumber = $this->getRequest()->getParam('customsms_mob');
        $message = $this->getRequest()->getParam('customsms_message');
        try {
            $apiResponse = $this->helperapi->callApiUrl($mobilenumber, $message);

            if ($apiResponse === true) {
                $this->getMessageManager()->addSuccess("SMS Sent Successfully to the Customer Mobile : " . $mobilenumber);
            } else {
                $this->getMessageManager()->addError("Something Went Wrong While sending SMS");
            }

            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/invoice/view/invoice_id/" . $invoiceid, ['store' => $storeId]);
            return;
        } catch (\Exception $e) {
            $this->getMessageManager()->addError("There is some Technical problem, Please tray again");
            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/invoice/view/invoice_id/" . $invoiceid, ['store' => $storeId]);
            return;
        }

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magecomp_Sms::magecompsms');
    }
}
<?php
/**
 * Magento Magecomp_Sms extension
 *
 * @category   Magecomp
 * @package    Magecomp_Sms
 * @author     Magecomp
 */

namespace Magecomp\Sms\Controller\Adminhtml\Bulk;

class Getnumber extends \Magento\Backend\App\Action
{
    protected $resultRedirect;
    protected $_customerFactory;
    protected $customerShippingAddress;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\ResultFactory $result,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Customer\Api\AddressRepositoryInterface $customerShippingAddress
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirect = $result;
        $this->_customerFactory = $customerFactory;
        $this->customerShippingAddress = $customerShippingAddress;
    }

    public function execute()
    {
        $customerCollection = $this->_customerFactory->create();
        $telephones = "";
        foreach ($customerCollection as $customer)
        {
            $shippingAddressId = $customer->getDefaultShipping();
            if($shippingAddressId) {
                $billingAddress = $this->customerShippingAddress->getById($shippingAddressId);
                $telephone = $billingAddress->getTelephone();
                $telephones .= $telephone.",";
            }

        }
        echo $telephones;

    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magecomp_Sms::magecompsms');
    }
}
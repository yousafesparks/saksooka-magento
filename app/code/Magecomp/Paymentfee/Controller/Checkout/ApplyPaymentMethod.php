<?php
namespace Magecomp\Paymentfee\Controller\Checkout;

class ApplyPaymentMethod extends \Magento\Framework\App\Action\Action
{

    protected $resultForwardFactory;
    protected $layoutFactory;
    protected $cart;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Checkout\Model\Cart $cart
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        $this->layoutFactory = $layoutFactory;
        $this->cart = $cart;

        parent::__construct($context);
    }

    public function execute()
    {

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testsdfds.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('Your text message');
        $paymentMethod = $this->getRequest()->getParam('payment_method');

        $quote = $this->cart->getQuote();
        $quote->getPayment()->setMethod($paymentMethod);

        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();
    }
}

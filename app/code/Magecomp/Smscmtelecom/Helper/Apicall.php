<?php 
namespace Magecomp\Smscmtelecom\Helper;

class Apicall extends \Magento\Framework\App\Helper\AbstractHelper
{
	const XML_CMTELECOM_API_SENDERID = 'sms/smsgatways/cmtelecomsenderid';
    const XML_CMTELECOM_API_AUTHKEY = 'sms/smsgatways/cmtelecomauthkey';
	const XML_CMTELECOM_API_URL = 'sms/smsgatways/cmtelecomapiurl';

	public function __construct(\Magento\Framework\App\Helper\Context $context)
	{
		parent::__construct($context);
	}

    public function getTitle() {
        return __("Cmtelecom");
    }

    public function getApiSenderId(){
        return $this->scopeConfig->getValue(
            self::XML_CMTELECOM_API_SENDERID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getAuthKey()	{
        return $this->scopeConfig->getValue(
            self::XML_CMTELECOM_API_AUTHKEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

	public function getApiUrl()	{
		return $this->scopeConfig->getValue(
            self::XML_CMTELECOM_API_URL,
			 \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
	}

	public function validateSmsConfig() {
        return $this->getApiUrl() && $this->getAuthKey() && $this->getApiSenderId();
    }
    static public function buildMessageXml($recipient, $message,$producttoken,$sendername) {
        $xml = new \SimpleXMLElement('<MESSAGES/>');

        $authentication = $xml->addChild('AUTHENTICATION');
        $authentication->addChild('PRODUCTTOKEN', '7E4C24FE-4002-4A0E-8886-BA9E586C0798');

        $msg = $xml->addChild('MSG');
        $msg->addChild('FROM', 'SAKSOOKA');
        $msg->addChild('TO', $recipient);
        $msg->addChild('DCS', 8);
        $msg->addChild('MINIMUMNUMBEROFMESSAGEPARTS', 1);
        $msg->addChild('MAXIMUMNUMBEROFMESSAGEPARTS', 8);
        $msg->addChild('BODY', $message);


        return $xml->asXML();
    }
	public function callApiUrl($mobilenumbers,$message)
	{
        try {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Your text message');


            $url = $this->getApiUrl();
            $producttoken = $this->getAuthKey();
            $sendername = $this->getApiSenderId();
            $mobilenumbers = '00'.$mobilenumbers;
            $logger->info('URL : ' . $url);
            $logger->info('Sender Name : ' . $sendername);
            $logger->info('Mobile number : ' . $mobilenumbers);
            $message = htmlentities($message);
            $xml = self::buildMessageXml(trim($mobilenumbers), trim($message),trim($producttoken),trim($sendername));
            $logger->info('Message : ' );
            $logger->info($message);

            $ch = curl_init();

            curl_setopt_array($ch, array(
                    CURLOPT_URL            => 'https://gw.cmtelecom.com/gateway.ashx',
                    CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
                    CURLOPT_POST           => true,
                    CURLOPT_POSTFIELDS     => $xml,
                    CURLOPT_RETURNTRANSFER => true
                )
            );
            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $logger->info('Response number : ' . $response);

            if(curl_errno($ch) || $httpcode != 200)
            {

                curl_close($ch);
                return 'Error: '.curl_error($ch);
            }

            curl_close($ch);
            return true;
        }
        catch (\Exception $e) {

            return $e->getMessage();
        }
	}
}
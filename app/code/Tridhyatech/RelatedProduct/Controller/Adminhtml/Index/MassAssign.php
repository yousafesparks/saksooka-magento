<?php
/**
 * @author Tridhyatech Team
 * @copyright Copyright (c) 2020 Tridhyatech (https://www.tridhya.com)
 * @package Tridhyatech_RelatedProduct
 */
namespace Tridhyatech\RelatedProduct\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class MassAssign extends Action
{
    /**
     * @var Filter
     */
    protected $filter;
 
    /**
     * @var CollectionFactory
     */
    protected $prodCollFactory;
 
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Catalog\Api\Data\ProductLinkInterface
     */
    protected $productLinkInterface;
 
    /**
     * @param Context                                         $context
     * @param Filter                                          $filter
     * @param CollectionFactory                               $prodCollFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Model\ProductFactory           $productFactory
     * @param \Magento\Catalog\Api\Data\ProductLinkInterface  $productLinkInterface
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $prodCollFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Api\Data\ProductLinkInterface $productLinkInterface)
    {
        $this->filter = $filter;
        $this->prodCollFactory = $prodCollFactory;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->productLinkInterface = $productLinkInterface;
        parent::__construct($context);
    }
 
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException | \Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->prodCollFactory->create());
        $allIds = $collection->getAllIds();
        foreach ($collection->getAllIds() as $productId) {
            $product = $this->productFactory->create()->load($productId);
            foreach ($allIds as $n => $linkProductId) {
                if ($linkProductId == $productId) continue;
                try {
                    $linkedProduct = $this->productRepository->getById($linkProductId);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
                    $linkedProduct = false;
                }

                if ($linkedProduct) {
                    $links = $product->getProductLinks();
                    $productLinks = $this->productLinkInterface;
                    $linkData = $productLinks
                        ->setSku($product->getSku())
                        ->setLinkedProductSku($linkedProduct->getSku())
                        ->setLinkType("related")
                        ->setPosition($n + 1);
                    $links[] = $linkData;
                    $product->setProductLinks($links);
                    $product->save();
                }
            }
        }
        $this->messageManager->addSuccess(__('A total of %1 product(s) have been linked as related products.', $collection->getSize()));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('catalog/product/index');
    }
}

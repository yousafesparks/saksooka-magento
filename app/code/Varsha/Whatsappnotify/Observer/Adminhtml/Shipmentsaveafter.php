<?php
namespace Varsha\Whatsappnotify\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;

class Shipmentsaveafter implements ObserverInterface
{
protected $objectManager;
    protected $helperdata;
    protected $helpershipment;
    protected $emailfilter;
    protected $customerFactory;
    protected $orderRepository;
    protected $helpershipmenttrack;

    public function __construct(
    \Magento\Framework\ObjectManagerInterface $objectManager,
    \Varsha\Whatsappnotify\Helper\Data $helperdata,
        \Varsha\Whatsappnotify\Helper\Shipment $helpershipment,
        \Magento\Email\Model\Template\Filter $filter,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Shipment $shipment)
    {
    $this->objectManager = $objectManager;
         $this->helperdata = $helperdata;
        $this->helpershipment = $helpershipment;
        $this->emailfilter = $filter;
        $this->customerFactory = $customerFactory;
        $this->orderRepository = $orderRepository;
        $this->shipment = $shipment;
     	  
    }
	
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testsms.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
    $logger->info('shipment save-');
        if(!$this->helpershipment->isEnabled())
            return $this;

        $shipment   = $observer->getShipment();
        $order      = $shipment->getOrder();

        if($shipment)
        {
        $logger->info('shipment save-'.$order->getShippingMethod());
            $billingAddress = $order->getBillingAddress();
            $mobilenumber = $billingAddress->getTelephone();

            if($order->getCustomerId() > 0)
            {
                $customer = $this->customerFactory->create()->load($order->getCustomerId());
                $mobile = $customer->getMobilenumber();          
            }
        }
if($order->getShippingMethod() == "flatrate_flatrate") { 
    $logger->info("flat rate");
$from_number = $this->helperdata->getOrderPlacePhoneForUser();
$auth_key =$this->helperdata->getShipmenTemplateForUser();
$url =  $this->helperdata->getOrderPlaceTemplateForUser();
    
$Track_url_shipment =$this->helperdata->getShipmentPlaceTrackForUser();
$shipment_company_number =  $this->helperdata->getShipmentPlaceCompanyForUser();
    
if ($order->getCustomerFirstname()) {
    $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname().' '.$billingAddress->getLastname();
 } else {
  // guest customer
   $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname().' '.$billingAddress->getLastname();
 }

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$storeid = $order->getStoreId(); //storecode here
    // get array of stores with storecode as key
    $stores = $storeManager->getStores(true, true);
    foreach ($stores as $storekey => $storevalue) {
        if ($storeid == $storevalue->getId()) {
            $store_name = $storevalue->getCode();
        }
    }        
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
$trackLink = "Private Driver";
$trace_url = "Your order will ver delivered within 24 hours";
$logger->info('trace url-'.$trace_url);
//$mobilenumber = "+917013890843";
$ship_method = $order->getShippingDescription();

$ship_compay_number = "0501162400";
$logger->info('shipment-'.$ship_compay_number);

$logger->info('shipment-'.$from_number.'xxx-'.$mobilenumber.'xxx-'.$customerName.'xxx-'.$trackLink.'xxx-'.$ship_method.'xxx-'.$ship_compay_number.'xxx-'.$trace_url.'xxx-'.$store_name);
if($store_name == "en") {
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_shipped_english_store_2\", \"language\": { \"policy\": \"deterministic\", \"code\": \"en\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$trackLink\"},{\"data\": \"$ship_method\"},{\"data\": \"$ship_compay_number\"},{\"data\": \"$trace_url\"} ] } } }");
}
else 
{
$trackLink = "سائق خاص";
$trace_url = "سيتم تسليم طلبك خلال 24 ساعة";
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_shipped_arabic_store_1\", \"language\": { \"policy\": \"deterministic\", \"code\": \"ar\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$trackLink\"},{\"data\": \"$ship_method\"},{\"data\": \"$ship_compay_number\"},{\"data\": \"$trace_url\"} ] } } }");
}
$headers = array();
$headers[] = 'Authorization: Bearer '.$auth_key.'';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$resArr = json_decode($result);
        return $this;
        }
        }
}
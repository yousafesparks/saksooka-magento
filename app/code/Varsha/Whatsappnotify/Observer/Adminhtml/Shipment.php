<?php
namespace Varsha\Whatsappnotify\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;

class Shipment implements ObserverInterface
{
protected $objectManager;
    protected $helperdata;
    protected $helpershipment;
    protected $emailfilter;
    protected $customerFactory;
    protected $orderRepository;
    protected $helpershipmenttrack;



    public function __construct(
    \Magento\Framework\ObjectManagerInterface $objectManager,
    \Varsha\Whatsappnotify\Helper\Data $helperdata,
        \Varsha\Whatsappnotify\Helper\Shipment $helpershipment,
        \Magento\Email\Model\Template\Filter $filter,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Shipment $shipment)
    {
    $this->objectManager = $objectManager;
         $this->helperdata = $helperdata;
        $this->helpershipment = $helpershipment;
        $this->emailfilter = $filter;
        $this->customerFactory = $customerFactory;
        $this->orderRepository = $orderRepository;
        $this->shipment = $shipment;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
          $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testsms.log');
          $logger = new \Zend\Log\Logger();
          $logger->addWriter($writer);
          $logger->info('xxxx-'); 

        if(!$this->helpershipment->isEnabled())
            return $this;

         $track = $observer->getEvent()->getTrack();


        $shipment = $this->shipment->load($track->getParentId());
        $order = $this->orderRepository->get($track->getOrderId());
    $ship_method = $order->getShippingMethod();
        $customer = $this->customerFactory->create()->load($order->getCustomerId());
        $billingAddress = $order->getBillingAddress();

        $mobilenumber = $billingAddress->getTelephone();
        $mobile = $customer->getMobilenumber();

        if ($mobile != '' && $mobile != null) {
           // $mobilenumber = $mobile;
        }
$trackLink = $track->getTrackNumber();
    $logger->info('shipment method-'.$trackLink);
//$carrire_code = $track->getCarrierCode();
$from_number = $this->helperdata->getOrderPlacePhoneForUser();
$auth_key =$this->helperdata->getShipmenTemplateForUser();
$url =  $this->helperdata->getOrderPlaceTemplateForUser();

$Track_url_shipment =$this->helperdata->getShipmentPlaceTrackForUser();
$shipment_company_number =  $this->helperdata->getShipmentPlaceCompanyForUser();

if ($order->getCustomerFirstname()) {
    $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname().' '.$billingAddress->getLastname();
 } else {
  // guest customer
   $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname();
 }
if ($this->helpershipment->isShipmentEnabledForUser())
{
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$storeid = $order->getStoreId(); //storecode here
    // get array of stores with storecode as key
    $stores = $storeManager->getStores(true, true);
    foreach ($stores as $storekey => $storevalue) {
        if ($storeid == $storevalue->getId()) {
            $store_name = $storevalue->getCode();
        }
    }
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);

$trace_url = $Track_url_shipment.$trackLink;
$logger->info('trace url-'.$trace_url);
//$mobilenumber = "+917013890843";
$ship_method = $track->getTitle();
if($track->getTitle() == "Smsa Shipping") {
$ship_compay_number = $shipment_company_number;
$logger->info('shipment-'.$ship_compay_number);
if($store_name == "ar") {
$ship_method = "سمسا اكسبرس";
}
}
else {
$ship_compay_number = "99999999999";
$logger->info('shipment-'.$ship_compay_number);
}
$logger->info('shipment-'.$from_number.'xxx-'.$mobilenumber.'xxx-'.$customerName.'xxx-'.$trackLink.'xxx-'.$track->getTitle().'xxx-'.$ship_compay_number.'xxx-'.$trace_url.'xxx-'.$store_name);
if($store_name == "en") {
//curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"+966547733996\" }, \"to\": [ { \"phone_number\": \"+966547733996\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"0a697321_7334_588b_r98s_b9d80e2afa18\", \"template_name\": \"greetings_test\", \"language\": { \"policy\": \"fallback\", \"code\": \"en\" }, \"template_data\": [ { \"data\": \"Arya Stark\" } ] } } }");
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_shipped_english_store_2\", \"language\": { \"policy\": \"deterministic\", \"code\": \"en\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$trackLink\"},{\"data\": \"$ship_method\"},{\"data\": \"$ship_compay_number\"},{\"data\": \"$trace_url\"} ] } } }");
}
else
{
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_shipped_arabic_store_1\", \"language\": { \"policy\": \"deterministic\", \"code\": \"ar\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$trackLink\"},{\"data\": \"$ship_method\"},{\"data\": \"$ship_compay_number\"},{\"data\": \"$trace_url\"} ] } } }");
}
$headers = array();
$headers[] = 'Authorization: Bearer '.$auth_key.'';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$resArr = json_decode($result);
            }
 return $this;
        }
    }

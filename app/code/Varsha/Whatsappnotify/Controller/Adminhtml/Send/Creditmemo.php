<?php
/**
 * Magento Magecomp_Sms extension
 *
 * @category   Magecomp
 * @package    Magecomp_Sms
 * @author     Magecomp
 */

namespace Varsha\Whatsappnotify\Controller\Adminhtml\Send;

use Magento\Backend\Model\Session\Quote as BackendModelSession;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;

class Creditmemo extends \Magento\Backend\App\Action
{
    protected $resultRedirect;
    protected $helperapi;
    protected $backendModelSession;
    protected $orderRepository;
    protected $customerFactory;
    protected $emailfilter;
    protected $helperorder;
    protected $helperdata;
protected $creditmemoRepository;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\ResultFactory $result,
        \Varsha\Whatsappnotify\Helper\Apicall $helperapi,
        BackendModelSession $backendModelSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Email\Model\Template\Filter $filter,
        \Varsha\Whatsappnotify\Helper\Data $helperdata,
    CreditmemoRepositoryInterface $creditmemoRepository
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirect = $result;
        $this->helperapi = $helperapi;
        $this->backendModelSession = $backendModelSession;
        $this->orderRepository = $orderRepository;
        $this->customerFactory = $customerFactory;
        $this->helperdata = $helperdata;
        $this->emailfilter = $filter;
       $this->creditmemoRepository = $creditmemoRepository;
    }

    public function execute()
    {

        $creditmemo_id = $this->getRequest()->getParam('creditmemo_id');
     $creditmemo = $this->creditmemoRepository->get($creditmemo_id);
            $order = $this->orderRepository->get($creditmemo->getOrderId());
    
        $order      = $creditmemo->getOrder();
        try {
            
            $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/test.log');
    
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);

          $logger->info("credit memeo");
   // $postData = $this->_request->getPost('mpstorecredit');
   // if (isset($postData['refund_exchange']) && $postData['refund_exchange'] == "on") {
   // $credit = $postData['refund_exchange_amount'];
   // $logger->info("credit memeo".$credit);
    //$logger->info("credit memeo".$postData['refund_exchange'].$postData['refund_exchange_amount']); 
        if($creditmemo)
        {
            $billingAddress = $order->getBillingAddress();
            $mobilenumber = $billingAddress->getTelephone();

            if($order->getCustomerId() > 0)
            {
                $customer = $this->customerFactory->create()->load($order->getCustomerId());
                $mobile = $customer->getMobilenumber();
                if($mobile != '' && $mobile != null)
                {
                   // $mobilenumber = $mobile;
                }
 
               
            }
            else
            {
                
            }
$from_number = $this->helperdata->getOrderPlacePhoneForUser();
$auth_key =$this->helperdata->getShipmenTemplateForUser();
$url =  $this->helperdata->getOrderPlaceTemplateForUser();
    
if ($order->getCustomerFirstname()) {
   $customerName = $order->getCustomerFirstname();
 } else {
  // guest customer
   $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname();
 }
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$storeid = $order->getStoreId(); //storecode here
    // get array of stores with storecode as key
    $stores = $storeManager->getStores(true, true);
    foreach ($stores as $storekey => $storevalue) {
        if ($storeid == $storevalue->getId()) {
            $store_name = $storevalue->getCode();
        }
    }       
$logger->info("credit memeo".$customerName."credit memeo".$from_number."credit memeo".$url."credit memeo".$mobilenumber."credit memeo".$store_name);
$grand_total = $creditmemo->getGrandTotal();
//$grand_total = $postData['refund_exchange_amount'];
$grand_total = sprintf("%1.2f",$grand_total);
$logger->info("total".$grand_total);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
//$mobilenumber = "+917013890843";
if($store_name == "en") {
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_credit_english_store\", \"language\": { \"policy\": \"deterministic\", \"code\": \"en\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$grand_total\"} ] } } }");
}
else 
{
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_credit_arabic_store\", \"language\": { \"policy\": \"deterministic\", \"code\": \"ar\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$grand_total\"}] } } }");
}
$headers = array();
$headers[] = 'Authorization: Bearer '.$auth_key.'';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$resArr = json_decode($result);
        }
   // }
   // else {
   // $logger->info("not set");
   // }

           // if ($apiResponse === true) {
                $this->getMessageManager()->addSuccess("Message Sent Successfully to the Customer Mobile : " . $mobilenumber);
           // } else {
           //     $this->getMessageManager()->addError("Something Went Wrong While sending SMS");
           // }

        $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/creditmemo/view/creditmemo_id/" . $creditmemo_id, ['store' => $storeId]);
            return;
        } catch (\Exception $e) {
            $this->getMessageManager()->addError("There is some Technical problem, Please tray again");
            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/creditmemo/view/creditmemo_id/" . $creditmemo_id, ['store' => $storeId]);
            return;
        }

    }
}
<?php
/**
 * Magento Magecomp_Sms extension
 *
 * @category   Magecomp
 * @package    Magecomp_Sms
 * @author     Magecomp
 */

namespace Varsha\Whatsappnotify\Controller\Adminhtml\Send;

use Magento\Backend\Model\Session\Quote as BackendModelSession;

class Shipment extends \Magento\Backend\App\Action
{
    protected $resultRedirect;
    protected $helperapi;
    protected $backendModelSession;
    protected $orderRepository;
    protected $customerFactory;
    protected $emailfilter;
    protected $helperorder;
    protected $helperdata;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\ResultFactory $result,
        \Varsha\Whatsappnotify\Helper\Apicall $helperapi,
        BackendModelSession $backendModelSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Email\Model\Template\Filter $filter,
        \Varsha\Whatsappnotify\Helper\Data $helperdata,
        \Magento\Sales\Model\Order\Shipment $shipment
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirect = $result;
        $this->helperapi = $helperapi;
        $this->backendModelSession = $backendModelSession;
        $this->orderRepository = $orderRepository;
        $this->customerFactory = $customerFactory;
        $this->helperdata = $helperdata;
        $this->emailfilter = $filter;
        $this->shipment = $shipment;
    }

    public function execute()
    {

        $shipmentid = $this->getRequest()->getParam('shipment_id');
        try {
            
            $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);

          $logger->info("latest order sendddd===>");
                $shipment = $this->shipment->load($shipmentid);
            $order = $this->orderRepository->get($shipment->getOrderId());
                $ship_method = $order->getShippingMethod();
        $customer = $this->customerFactory->create()->load($order->getCustomerId());
        $billingAddress = $order->getBillingAddress();
$ship_method = $order->getShippingDescription();
        $mobilenumber = $billingAddress->getTelephone();
        $mobile = $customer->getMobilenumber();

        if ($mobile != '' && $mobile != null) {
           // $mobilenumber = $mobile;
        }
        $from_number = $this->helperdata->getOrderPlacePhoneForUser();
$auth_key =$this->helperdata->getShipmenTemplateForUser();
$url =  $this->helperdata->getOrderPlaceTemplateForUser();
    
$Track_url_shipment =$this->helperdata->getShipmentPlaceTrackForUser();
$shipment_company_number =  $this->helperdata->getShipmentPlaceCompanyForUser();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$storeid = $order->getStoreId(); //storecode here
    // get array of stores with storecode as key
    $stores = $storeManager->getStores(true, true);
    foreach ($stores as $storekey => $storevalue) {
        if ($storeid == $storevalue->getId()) {
            $store_name = $storevalue->getCode();
        }
    } 
if ($order->getTracksCollection()->count()) {
$tracksCollection = $order->getTracksCollection();
foreach ($tracksCollection->getItems() as $track) {
$trackLink = $track->getTrackNumber();
$ship_method = $track->getTitle();
}
$trace_url = $Track_url_shipment.$trackLink;
$logger->info('trace url-'.$trace_url);


if($track->getTitle() == "Smsa Shipping") {
$ship_compay_number = $shipment_company_number;
$logger->info('shipment-'.$ship_compay_number);
if($store_name == "ar") {
$ship_method = "سمسا اكسبرس";
}
}
else {
$ship_compay_number = $shipment_company_number;
$logger->info('shipment-'.$ship_compay_number);
}
}
else {
if($store_name == "en") {
$trackLink = "Private Driver";
$trace_url = "Your order will ver delivered within 24 hours";
}
else {
$trackLink = "سائق خاص";
$trace_url = "سيتم تسليم طلبك خلال 24 ساعة";
}
$logger->info('trace url-'.$trace_url);
//$mobilenumber = "+917013890843";


$ship_compay_number = "0501162400";
$logger->info('shipment-'.$ship_compay_number);
}
$logger->info('shipment method-'.$trackLink);

    
if ($order->getCustomerFirstname()) {
    $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname().' '.$billingAddress->getLastname();
 } else {
  // guest customer
   $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname();
 }

       
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
//$mobilenumber = "+917013890843";
        

$logger->info('shipment-'.$from_number.'xxx-'.$mobilenumber.'xxx-'.$customerName.'xxx-'.$trackLink.'xxxxxx-'.$ship_compay_number.'xxx-'.$trace_url.'xxx-'.$store_name);
if($store_name == "en") {
//curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"+966547733996\" }, \"to\": [ { \"phone_number\": \"+966547733996\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"0a697321_7334_588b_r98s_b9d80e2afa18\", \"template_name\": \"greetings_test\", \"language\": { \"policy\": \"fallback\", \"code\": \"en\" }, \"template_data\": [ { \"data\": \"Arya Stark\" } ] } } }");
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_shipped_english_store_2\", \"language\": { \"policy\": \"deterministic\", \"code\": \"en\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$trackLink\"},{\"data\": \"$ship_method\"},{\"data\": \"$ship_compay_number\"},{\"data\": \"$trace_url\"} ] } } }");
}
else 
{

curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_shipped_arabic_store_1\", \"language\": { \"policy\": \"deterministic\", \"code\": \"ar\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$trackLink\"},{\"data\": \"$ship_method\"},{\"data\": \"$ship_compay_number\"},{\"data\": \"$trace_url\"} ] } } }");
}
$headers = array();
$headers[] = 'Authorization: Bearer '.$auth_key.'';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$resArr = json_decode($result);
           

           // if ($apiResponse === true) {
                $this->getMessageManager()->addSuccess("Message Sent Successfully to the Customer Mobile : " . $mobilenumber);
           // } else {
           //     $this->getMessageManager()->addError("Something Went Wrong While sending SMS");
           // }

        $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/shipment/view/shipment_id/" . $shipmentid, ['store' => $storeId]);
            return;
        } catch (\Exception $e) {
            $this->getMessageManager()->addError("There is some Technical problem, Please tray again");
            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/shipment/view/shipment_id/" . $shipmentid, ['store' => $storeId]);
            return;
        }

    }
}
<?php
/**
 * Magento Magecomp_Sms extension
 *
 * @category   Magecomp
 * @package    Magecomp_Sms
 * @author     Magecomp
 */

namespace Varsha\Whatsappnotify\Controller\Adminhtml\Send;

use Magento\Backend\Model\Session\Quote as BackendModelSession;

class Order extends \Magento\Backend\App\Action
{
    protected $resultRedirect;
    protected $helperapi;
    protected $backendModelSession;
    protected $orderRepository;
    protected $customerFactory;
    protected $emailfilter;
    protected $helperorder;
    protected $helperdata;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\ResultFactory $result,
        \Varsha\Whatsappnotify\Helper\Apicall $helperapi,
        BackendModelSession $backendModelSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Email\Model\Template\Filter $filter,
        \Varsha\Whatsappnotify\Helper\Data $helperdata
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirect = $result;
        $this->helperapi = $helperapi;
        $this->backendModelSession = $backendModelSession;
        $this->orderRepository = $orderRepository;
        $this->customerFactory = $customerFactory;
        $this->helperdata = $helperdata;
        $this->emailfilter = $filter;
    }

    public function execute()
    {

        $orderid = $this->getRequest()->getParam('order_id');
        try {
            
            $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);

          $logger->info("latest order sendddd===>");
                $order = $this->orderRepository->get($orderid);
                $logger->info("latest order senddddorder===>".$orderid);
                $order_information = $order->loadByIncrementId($orderid);

                $billingAddress = $order_information->getBillingAddress();
                $mobilenumber = $billingAddress->getTelephone();
                if($order->getCustomerId() > 0)
                {
                    $customer = $this->customerFactory->create()->load($order_information->getCustomerId());
                    $mobile = $customer->getMobilenumber();
                    if($mobile != '' && $mobile != null)
                    {
                      /*  $mobilenumber = $mobile;*/
                    }
                    $this->emailfilter->setVariables([
                        'order' => $order,
                        'customer' => $customer,
                        'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
                        'mobilenumber' => $mobilenumber
                    ]);
                }else{
                    $this->emailfilter->setVariables([
                        'order' => $order,
                        'order_total' => $order->formatPriceTxt($order->getGrandTotal()),
                        'mobilenumber' => $mobilenumber
                    ]);
                }
if ($order->getInvoiceCollection()->count()) {    
foreach ($order->getInvoiceCollection() as $invoice) {
    $invoiceIncrementID = $invoice->getEntityId();
}
$invoice_link = "https://www.sakdevsite.com/en/sales/order/invoice/order_id/2676/";
}
else {
$invoiceIncrementID = "xxxx";
}
               // $message = $this->helperdata->getOrderPlaceTemplateForUser();
               // $finalmessage = $this->emailfilter->filter($message);
                //$this->helperapi->callApiUrl($mobilenumber,$finalmessage);
                $from_number = $this->helperdata->getOrderPlacePhoneForUser();
            $auth_key =$this->helperdata->getShipmenTemplateForUser();
            $url =  $this->helperdata->getOrderPlaceTemplateForUser();
               if ($order->getCustomerFirstname()) {
   $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname().' '.$billingAddress->getLastname();
               $logger->info("if ===>".$customerName."if---".$mobilenumber);
 } else {
  // guest customer
   $billingAddress = $order->getBillingAddress();
   $customerName = $billingAddress->getFirstname();
               $logger->info("else cust ===>".$customerName."else---".$mobilenumber);
 }
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$storeid = $order->getStoreId(); //storecode here
    // get array of stores with storecode as key
    $stores = $storeManager->getStores(true, true);
    foreach ($stores as $storekey => $storevalue) {
        if ($storeid == $storevalue->getId()) {
            $store_name = $storevalue->getCode();
        }
    } 
$invoice_link = "https://".$_SERVER['HTTP_HOST']."/".$store_name."/guest/order/printorder/order_id/".$order->getEntityId();;      
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
$order_number =$order->getIncrementId();
$grand_totla = $order->getGrandTotal();
$grand_totla = sprintf("%1.2f",$grand_totla);


//$mobilenumber = "+917013890843";
//curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"+966547733996\" }, \"to\": [ { \"phone_number\": \"+966547733996\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"0a697321_7334_588b_r98s_b9d80e2afa18\", \"template_name\": \"greetings_test\", \"language\": { \"policy\": \"fallback\", \"code\": \"en\" }, \"template_data\": [ { \"data\": \"Arya Stark\" } ] } } }");
if($store_name == "en") {
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_placed_english_store_1\", \"language\": { \"policy\": \"deterministic\", \"code\": \"en\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$order_number\"},{\"data\": \"$grand_totla\"},{\"data\": \"$invoice_link\"} ] } } }");
}
else {
curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"from\": { \"phone_number\": \"$from_number\" }, \"to\": [ { \"phone_number\": \"$mobilenumber\" } ], \"data\": { \"message_template\": { \"storage\": \"none\", \"namespace\": \"043201b4_891e_408b_bc66_935303cbb7e9\", \"template_name\": \"order_placed_arabic_store_1\", \"language\": { \"policy\": \"deterministic\", \"code\": \"ar\" }, \"template_data\": [  {\"data\": \"$customerName\"},{\"data\": \"$order_number\"},{\"data\": \"$grand_totla\"},{\"data\": \"$invoice_link\"} ] } } }");
}

$headers = array();
$headers[] = 'Authorization: Bearer '.$auth_key.'';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$resArr = json_decode($result);
$logger->info("latest order Id ===>".$grand_totla.'customer Email ==>'.$order_number.'url ==>'.$invoice_link.'order_id'.$from_number.'order_id'.$mobilenumber.'order_id'.$store_name);

           // if ($apiResponse === true) {
                $this->getMessageManager()->addSuccess("Message Sent Successfully to the Customer Mobile : " . $mobilenumber);
           // } else {
           //     $this->getMessageManager()->addError("Something Went Wrong While sending SMS");
           // }

            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/order/view/order_id/" . $orderid, ['store' => $storeId]);
            return;
        } catch (\Exception $e) {
            $this->getMessageManager()->addError("There is some Technical problem, Please tray again");
            $storeId = $this->getRequest()->getParam('store');
            $this->_redirect("sales/order/view/order_id/" . $orderid, ['store' => $storeId]);
            return;
        }

    }
}
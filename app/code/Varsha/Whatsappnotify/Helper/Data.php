<?php 
namespace Varsha\Whatsappnotify\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    // GENERAL Configuration
	const XML_PATH_ENABLED ='whatsappnotify/moduleoption/enable';

    // USER TEMPLATE configuration
 	const XML_SMS_USER_ORDER_PLACE_ENABLE = 'whatsappnotify/orderplace/enable';
 	const XML_SMS_USER_USER_ORDER_PLACE_TEXT = 'whatsappnotify/smsgatways/gateway';
	const XML_SMS_USER_SHIPMENT_ENABLE = 'whatsappnotify/shipment/enable';
	const XML_SMS_USER_SHIPMENT_TEXT = 'whatsappnotify/smsgatways/gateway1';
	const XML_SMS_USER_PHONE_NUMBER = 'whatsappnotify/smsgatways/gateway2';
	const XML_VARSHA_USER_SHIPMENT_TRACKING = 'whatsappnotify/shipment/trackings';
	const XML_SMS_USER_SHIPMENT_COMPANY = 'whatsappnotify/shipment/company';

	protected $_storeManager;

	public function __construct(
	\Magento\Framework\App\Helper\Context $context,
	\Magento\Framework\ObjectManagerInterface $objectManager,
	\Magento\Framework\Registry $registry,
	\Magento\Store\Model\StoreManagerInterface $storeManager)
	{
		$this->registry = $registry;
		$this->_storeManager = $storeManager;
		parent::__construct($context);
	}

    public function getStoreid()
    {
        return $this->_storeManager->getStore()->getId();
    }

	public function isEnabled()
	{
        return $this->scopeConfig->getValue(self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }

	public function isOrderPlaceForUserEnabled()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_ORDER_PLACE_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function getOrderPlaceTemplateForUser()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_USER_ORDER_PLACE_TEXT,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function getOrderPlacePhoneForUser()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_PHONE_NUMBER,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function getShipmentPlaceTrackForUser()
	{
        return $this->scopeConfig->getValue(self::XML_VARSHA_USER_SHIPMENT_TRACKING,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function getShipmentPlaceCompanyForUser()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_SHIPMENT_COMPANY,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function isShipmentEnabledForUser()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_SHIPMENT_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function getShipmenTemplateForUser()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_SHIPMENT_TEXT,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
}
<?php 
namespace Varsha\Whatsappnotify\Helper;

use Magento\Store\Model\ScopeInterface;

class Shipment extends \Varsha\Whatsappnotify\Helper\Data
{
    // USER TEMPLATE
    const SMS_IS_CUSTOMER_SHIPMENT_NOTIFICATION = 'whatsappnotify/shipment/enable';
    const SMS_CUSTOMER_SHIPMENT_NOTIFICATION_TEMPLATE = 'whatsappnotify/shipment/template';
const XML_SMS_USER_SHIPMENT_AUTH = 'whatsappnotify/smsgatways/gateway1';
	const XML_SMS_USER_PHONE = 'whatsappnotify/smsgatways/gateway2';
const XML_SMS_USER_USER_ORDER_URL = 'whatsappnotify/smsgatways/gateway';


	public function isShipmentNotificationForUser() {
        return $this->isEnabled() && $this->scopeConfig->getValue(self::SMS_IS_CUSTOMER_SHIPMENT_NOTIFICATION,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }

    public function getShipmentNotificationUserTemplate()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_CUSTOMER_SHIPMENT_NOTIFICATION_TEMPLATE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
        }
    }
public function getOrderPlaceTemplateForUser1()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_USER_ORDER_URL,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
	public function getOrderPlacePhoneForUser1()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_SHIPMENT_AUTH,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
public function getShipmenTemplateForUser1()
	{
        return $this->scopeConfig->getValue(self::XML_SMS_USER_PHONE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreid());
    }
    public function isShipmentNotificationForAdmin()
    {
        return $this->isEnabled() && $this->scopeConfig->getValue(self::SMS_IS_ADMIN_SHIPMENT_NOTIFICATION,
                ScopeInterface::SCOPE_STORE,
                $this->getStoreid());
    }

    public function getShipmentNotificationForAdminTemplate()
    {
        if($this->isEnabled())
        {
            return  $this->scopeConfig->getValue(self::SMS_ADMIN_SHIPMENT_NOTIFICATION_TEMPLATE,
                ScopeInterface::SCOPE_STORE,
                $this->getStoreid());
        }
    }
}
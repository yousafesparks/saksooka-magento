<?php

namespace Varsha\Whatsappnotify\Plugin;


use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\ObjectManagerInterface;

class NotifyBtnShipmentView
{
    protected $object_manager;
    protected $_backendUrl;

    public function __construct(
        ObjectManagerInterface $om,
        UrlInterface $backendUrl
    )
    {
        $this->object_manager = $om;
        $this->_backendUrl = $backendUrl;
    }


    public function beforeSetLayout( \Magento\Shipping\Block\Adminhtml\View $subject )
    {

        $sendOrderSms = $this->_backendUrl->getUrl('varshawhatsappnotify/send/shipment/shipment_id/'.$subject->getShipment()->getId() );
        $subject->addButton(
            'sendordermessage',
            [
                'label' => __('ReSend Whatsapp'),
                'onclick' => "setLocation('" . $sendOrderSms . "')",
                'class' => 'notify primary'
            ]
        );

        return null;
    }

}
<?php

namespace Varsha\Whatsappnotify\Plugin;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\ObjectManagerInterface;

class NotifyBtnOrderView
{
    protected $object_manager;
    protected $_backendUrl;

    public function __construct(
        ObjectManagerInterface $om,
        UrlInterface $backendUrl
    ) {
        $this->object_manager = $om;
        $this->_backendUrl = $backendUrl;
    }

    public function beforeSetLayout( \Magento\Sales\Block\Adminhtml\Order\View $subject )
    {
        $sendOrderSms = $this->_backendUrl->getUrl('varshawhatsappnotify/send/order/order_id/'.$subject->getOrderId() );
        $subject->addButton(
            'sendordermessage',
            [
                'label' => __('ReSend Whatsapp'),
                'onclick' => "setLocation('" . $sendOrderSms . "')",
                'class' => 'notify primary'
            ]
        );

        return null;
    }

}
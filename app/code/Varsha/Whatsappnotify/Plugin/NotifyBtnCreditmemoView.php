<?php

namespace Varsha\Whatsappnotify\Plugin;


use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\ObjectManagerInterface;

class NotifyBtnCreditmemoView
{
    protected $object_manager;
    protected $_backendUrl;

    public function __construct(
        ObjectManagerInterface $om,
        UrlInterface $backendUrl
    )
    {
        $this->object_manager = $om;
        $this->_backendUrl = $backendUrl;
    }


    public function beforeSetLayout( \Magento\Sales\Block\Adminhtml\Order\Creditmemo\View $subject )
    {

        $sendOrderSms = $this->_backendUrl->getUrl('varshawhatsappnotify/send/creditmemo/creditmemo_id/'.$subject->getCreditmemo()->getId() );
        $subject->addButton(
            'sendordermessage',
            [
                'label' => __('ReSend Whatsapp'),
                'onclick' => "setLocation('" . $sendOrderSms . "')",
                'class' => 'notify primary'
            ]
        );

        return null;
    }

}
<?php
namespace TB\FixDuplicateOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderPlacebefore implements ObserverInterface
{
protected $_responseFactory;
protected $_url;


public function __construct(
    \Magento\Framework\App\ResponseFactory $responseFactory,
    \Magento\Framework\UrlInterface $url
) {
    $this->_responseFactory = $responseFactory;
    $this->_url = $url;
}

public function execute(\Magento\Framework\Event\Observer $observer)
{
    $event = $observer->getEvent();

    $order = $observer->getEvent()->getOrder();
    $quoteID = $order->getQuoteId();
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();  
    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
    $connection = $resource->getConnection();
    $tableName = $resource->getTableName('sales_order');
    $selectedTime = date('Y-m-d h:i:s');
    $endTime = strtotime("-15 seconds", strtotime($selectedTime));
    $last15Sec = date('Y-m-d h:i:s', $endTime);

    echo $sql = "SELECT * FROM `".$tableName."` WHERE `quote_id` = ".$quoteID." and `created_at` >= '$last15Sec'";
    if($result = $connection->fetchRow($sql)){
        $customerBeforeAuthUrl = $this->_url->getUrl('checkout');
        $this->_responseFactory->create()->setRedirect($customerBeforeAuthUrl)->sendResponse();
        exit();
    }
    //var_dump($result);
    //echo  "Hello Testing ";
    //die();
}
}
<?php
namespace MGS\ClaueTheme\Block;

class ParentByChild extends \Magento\Framework\View\Element\Template
{
	 protected $_productloader;  

	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
		\Magento\Catalog\Model\ProductFactory $_productloader,
        array $data = []
    ) {
        $this->configurable = $configurable;
		$this->_productloader = $_productloader;
        parent::__construct($context, $data);
    }

    /**
     * @param int $childId
     * @return int
     */
    public function getParentProductId($childProductId)
    {
         $parentConfigObject = $this->configurable->getParentIdsByChild($childProductId);
	    if($parentConfigObject) {
			return $parentConfigObject[0];
	    }
	    return false;
    }
	public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($this->getParentProductId($id));
    }
	public function getLoadProductCustom($id)
    {
        return $this->_productloader->create()->load($id);
    }

}
<?php
/**
 *
 * LICENSE
 *
 * This source file is subject to the Eltrino LLC EULA
 * that is bundled with this package in the file LICENSE_EULA.txt.
 * It is also available through the world-wide-web at this URL:
 * http://eltrino.com/license-eula.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eltrino.com so we can send you a copy immediately.
 *
 * @category    Eltrino
 * @package     Eltrino_Region
 * @copyright   Copyright (c) 2019 Eltrino LLC. (http://eltrino.com)
 * @license     http://eltrino.com/license-eula.txt  Eltrino LLC EULA
 */

namespace Eltrino\PrintOrder\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Eltrino\PrintOrder\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eltrinoDisabledRegion = $setup->getTable('eltrino_guests_orders');

        $setup->run("DROP TABLE IF EXISTS {$eltrinoDisabledRegion}");

        $table = $setup->getConnection()->newTable(
            $setup->getTable($eltrinoDisabledRegion)
        )->addColumn(
            'guests_order_id',
            Table::TYPE_INTEGER,
            null,
            [
                'auto_increment' => true,
                'primary' => true,
                'unsigned' => true,
                'nullable' => false
            ],
            'Guests Order Entity Id'
        )->addColumn(
            'hash',
            Table::TYPE_TEXT,
            null,
            [
                'default' => NULL
            ],
            'Hash of order'
        )->addColumn(
            'order_id',
            Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
            ],
            'Order Id'
        )->addColumn(
            'expired_at',
            Table::TYPE_DATETIME,
            null,
            [
                'nullable' => false
            ],
            'Date when hash is expired'
        );

        $setup->getConnection()->createTable($table);

        $setup->endSetup();

    }
}

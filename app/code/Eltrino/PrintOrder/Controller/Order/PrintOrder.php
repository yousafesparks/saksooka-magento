<?php

/**
 *
 * LICENSE
 *
 * This source file is subject to the Eltrino LLC EULA
 * that is bundled with this package in the file LICENSE_EULA.txt.
 * It is also available through the world-wide-web at this URL:
 * http://eltrino.com/license-eula.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eltrino.com so we can send you a copy immediately.
 *
 * @category    Eltrino
 * @package     Eltrino_Region
 * @copyright   Copyright (c) 2019 Eltrino LLC. (http://eltrino.com)
 * @license     http://eltrino.com/license-eula.txt  Eltrino LLC EULA
 */


namespace Eltrino\PrintOrder\Controller\Order;

class PrintOrder extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\App\Action\Context
     */
    protected $_context;

    /**
     * @var \Eltrino\PrintOrder\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $registry,
        \Eltrino\PrintOrder\Helper\Data $helper
    )
    {
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_helper = $helper;
        $this->_context = $context;
        $this->_coreRegistry = $registry;

        parent::__construct($context);
    }

    public function execute()
    {
         $guestOrderHash = $this->getRequest()->getParam('order_id');
        try {
            $guestOrder = $this->_helper->getOrderById($guestOrderHash);
            $order = $this->_helper->getOrderById($guestOrderHash);

            $this->_coreRegistry->register('current_order', $order);

            $this->_context->getView()->loadLayout(array('print', 'sales_order_print'));
            $this->_context->getView()->renderLayout();
        } catch (\Eltrino\PrintOrder\Model\Exception $e) {
            $this->_forward('noRoute');
        }
    }
}

<?php

/**
 *
 * LICENSE
 *
 * This source file is subject to the Eltrino LLC EULA
 * that is bundled with this package in the file LICENSE_EULA.txt.
 * It is also available through the world-wide-web at this URL:
 * http://eltrino.com/license-eula.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eltrino.com so we can send you a copy immediately.
 *
 * @category    Eltrino
 * @package     Eltrino_Region
 * @copyright   Copyright (c) 2019 Eltrino LLC. (http://eltrino.com)
 * @license     http://eltrino.com/license-eula.txt  Eltrino LLC EULA
 */


namespace Eltrino\PrintOrder\Model;

use Eltrino\PrintOrder\Model\GuestOrder;

class Observer
{

    /**
     * @var GuestOrder
     */
    protected $_guestOrder;

    /**
     * @param GuestOrder $guestOrder
     */
    public function __construct(
        GuestOrder $guestOrder
    )
    {
        $this->_guestOrder = $guestOrder;
    }

    /**
     * Clean expired guests orders.
     *
     * @param \Magento\Cron\Model\Schedule $schedule
     *
     * @return \Eltrino\PrintOrder\Model\Observer
     */
    public function cleanExpiredGuestsOrders($schedule)
    {
        /** @var \Eltrino\PrintOrder\Model\ResourceModel\GuestOrder\Collection $collection */
        $collection = $this->_guestOrder->getCollection();

        $collection->cleanExpiredGuestsOrders();

        return $this;
    }
}

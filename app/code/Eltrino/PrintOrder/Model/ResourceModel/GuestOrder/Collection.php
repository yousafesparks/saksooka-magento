<?php

/**
 *
 * LICENSE
 *
 * This source file is subject to the Eltrino LLC EULA
 * that is bundled with this package in the file LICENSE_EULA.txt.
 * It is also available through the world-wide-web at this URL:
 * http://eltrino.com/license-eula.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eltrino.com so we can send you a copy immediately.
 *
 * @category    Eltrino
 * @package     Eltrino_Region
 * @copyright   Copyright (c) 2019 Eltrino LLC. (http://eltrino.com)
 * @license     http://eltrino.com/license-eula.txt  Eltrino LLC EULA
 */


namespace Eltrino\PrintOrder\Model\ResourceModel\GuestOrder;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Clean expired guests orders.
     *
     * @return \Eltrino\PrintOrder\Model\ResourceModel\GuestOrder\Collection
     */
    public function cleanExpiredGuestsOrders()
    {
        $this->addOrder('expired_at')
            ->addFieldToFilter(
                'expired_at',
                ['to' => date('Y-m-d H:i:s', time())]
            );
        \Zend_Debug::dump($this->getSelect()->__toString());
        $this->walk('delete');

        return $this;
    }

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Eltrino\PrintOrder\Model\GuestOrder',
            'Eltrino\PrintOrder\Model\ResourceModel\GuestOrder'
        );
    }
}

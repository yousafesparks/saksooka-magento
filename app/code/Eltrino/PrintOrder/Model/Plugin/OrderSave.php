<?php

/**
 *
 * LICENSE
 *
 * This source file is subject to the Eltrino LLC EULA
 * that is bundled with this package in the file LICENSE_EULA.txt.
 * It is also available through the world-wide-web at this URL:
 * http://eltrino.com/license-eula.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eltrino.com so we can send you a copy immediately.
 *
 * @category    Eltrino
 * @package     Eltrino_Region
 * @copyright   Copyright (c) 2019 Eltrino LLC. (http://eltrino.com)
 * @license     http://eltrino.com/license-eula.txt  Eltrino LLC EULA
 */

namespace Eltrino\PrintOrder\Model\Plugin;

use Eltrino\PrintOrder\Model\GuestOrder;
use Eltrino\PrintOrder\Helper\Data as PrintOrderHelper;

class OrderSave
{
    /**
     * @var \Eltrino\PrintOrder\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Eltrino\PrintOrder\Model\GuestOrder
     */
    protected $_guestOrder;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * @param GuestOrder $guestOrder
     * @param PrintOrderHelper $helper
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        GuestOrder $guestOrder,
        PrintOrderHelper $helper,
        \Magento\Framework\App\State $state
    )
    {
        $this->_guestOrder = $guestOrder;
        $this->_helper = $helper;
        $this->_appState = $state;
    }

    /**
     *
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $order
    )
    {
        try {
            /** @var $guestOrder \Eltrino\PrintOrder\Model\GuestOrder */
            $guestOrder = $this->_helper->createFromOrder($order);
            $guestOrder->save();
        } catch (\Eltrino\PrintOrder\Model\Exception $e) {
            // if DeveloperMode enabled throw Exception, otherwise skip saving of such object
            //if ($this->_appState->getMode() == \Magento\Framework\App\State::MODE_DEVELOPER) {
            //    throw new \Eltrino\PrintOrder\Model\Exception(__($e->getMessage()));
            //}
        }

        return $order;
    }
}

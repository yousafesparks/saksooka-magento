<?php
/**
 *
 * @package ShraOne\PriceDecimal
 *

 */

namespace ShraOne\PriceDecimal\Helper;

use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	const MODULE_ENABLED = 'shraone_pricedecimal/general/enable';
	
	/**
     * Return Config Value by XML Config Path
     * @param $store
     *
     * @return mixed
     */
	public function isModuleEnabled($store = null)
	{
		return $this->scopeConfig->getValue(self::MODULE_ENABLED, ScopeInterface::SCOPE_STORE, $store);
	}
}

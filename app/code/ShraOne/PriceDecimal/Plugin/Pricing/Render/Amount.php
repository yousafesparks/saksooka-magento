<?php
/**
 *
 */

namespace ShraOne\PriceDecimal\Plugin\Pricing\Render;

use Magento\Framework\Pricing\PriceCurrencyInterface;

class Amount
{
	/** @var \Magento\Framework\Pricing\PriceCurrencyInterface  */
    protected $priceCurrency;
	
	/** @var \ShraOne\PriceDecimal\Helper\Data  */
    protected $moduleHelper;

    /**
     * @param \ShraOne\PriceDecimal\Helper\Data $moduleHelper
	 * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
		\ShraOne\PriceDecimal\Helper\Data $moduleHelper,
		PriceCurrencyInterface $priceCurrency
    ) {
        $this->moduleHelper  = $moduleHelper;
        $this->priceCurrency  = $priceCurrency;
		
    }
	
	/**
     * Format price value
	 *
	 * around function of Magento\Framework\Pricing\Render\Amount::formatCurrency()
     *
     * @param float $amount
     * @param bool $includeContainer
     * @param int $precision
     * @return float
     */
    public function aroundFormatCurrency(
        \Magento\Framework\Pricing\Render\Amount $subject,
		callable $proceed,
        $price,
		$includeContainer = true,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION
    ) {
		if($this->moduleHelper->isModuleEnabled()){
			$priceNumber = floor($price);
			$fraction = $price - $priceNumber;
			if($fraction > 0 && $fraction < 1){
				//do nothing, we use default
				//$precision = 0;
			} else{
				$precision = 0;
			}
		}
		
        $renderPrice = $this->priceCurrency->format($price, $includeContainer, $precision);
		return $renderPrice;
    }
}

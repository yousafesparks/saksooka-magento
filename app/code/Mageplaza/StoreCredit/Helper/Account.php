<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreCredit
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\StoreCredit\Helper;

use Exception;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Core\Helper\AbstractData;
use Mageplaza\StoreCredit\Model\CustomerFactory;

/**
 * Class Account
 * @package Mageplaza\StoreCredit\Helper
 */
class Account extends AbstractData
{
    /**
     * @var Customer[]
     */
    protected $accountByCustomerId = [];

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Customer constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param CustomerSession $customerSession
     * @param CustomerRegistry $customerRegistry
     * @param CustomerFactory $customerFactory
     * @param HttpContext $httpContext
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        Session $customerSession,
        CustomerRegistry $customerRegistry,
        CustomerFactory $customerFactory,
        HttpContext $httpContext,
        Data $helper
    ) {
        $this->customerSession  = $customerSession;
        $this->customerRegistry = $customerRegistry;
        $this->customerFactory  = $customerFactory;
        $this->httpContext      = $httpContext;
        $this->helper           = $helper;

        parent::__construct($context, $objectManager, $storeManager);
    }

    /**
     * @return Customer|null
     */
    public function getCurrentCustomer()
    {
        return $this->getCustomerById($this->getCustomerSession()->getId());
    }

    /**
     * @return CustomerSession
     */
    public function getCustomerSession()
    {
        return $this->customerSession;
    }

    /**
     * @param $customerId
     *
     * @return Customer
     */
    public function getCustomerById($customerId)
    {
        try {
            if (!$customerId) {
                $customerId = $this->getCustomerSession()->getId();
            }

            if (!isset($this->accountByCustomerId[$customerId])) {
                $this->accountByCustomerId[$customerId] = $this->customerRegistry->retrieve($customerId);
            }

            return $this->accountByCustomerId[$customerId];
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param $email
     *
     * @return Customer
     */
    public function getCustomerByEmail($email)
    {
        try {
            return $this->customerRegistry->retrieveByEmail($email);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param null $customerId
     *
     * @return int
     */
    public function getBalance($customerId = null)
    {
        if ($customer = $this->getCustomerById($customerId)) {
            return floatval($customer->getMpCreditBalance());
        }

        return 0;
    }

    /**
     * @param null $customerId
     * @param bool $includeContainer
     * @param null $scope
     * @param null $currency
     * @param int $precision
     *
     * @return int
     */
    public function getFormattedBalance(
        $customerId = null,
        $includeContainer = false,
        $scope = null,
        $currency = null,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION
    ) {
        $customer = $this->getCustomerById($customerId);
        $currency = $currency ?: $customer ? $customer->getStore()->getBaseCurrency() : null;

        return $this->helper->formatPrice(
            $this->getBalance($customerId),
            $includeContainer,
            $scope,
            $currency,
            $precision
        );
    }

    /**
     * Checking customer login status
     *
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->_request->isAjax() ? $this->getCustomerSession()->isLoggedIn()
            : (bool) $this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }
}

/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_EditOrder
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

var config = {
    paths: {
        mpEditOrderInfo: 'Mageplaza_EditOrder/js/order/edit/orderInfo',
        mpEditAccountInfo: 'Mageplaza_EditOrder/js/order/edit/customerInfo',
        mpEditBillingAddress: 'Mageplaza_EditOrder/js/order/edit/billingAddress',
        mpEditShippingAddress: 'Mageplaza_EditOrder/js/order/edit/shippingAddress',
        mpEditShippingMethod: 'Mageplaza_EditOrder/js/order/edit/shippingMethod',
        mpListShippingMethod: 'Mageplaza_EditOrder/js/order/edit/shipping/method/list',
        mpEditPaymentMethod: 'Mageplaza_EditOrder/js/order/edit/paymentMethod',
        mpEditItems: 'Mageplaza_EditOrder/js/order/edit/items',
        mpEditQuickEdit: 'Mageplaza_EditOrder/js/order/edit/quickEdit'
    }
};

<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_EditOrder
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\EditOrder\Model\Order;

use Exception;
use Magento\Backend\Model\Session\Quote as QuoteSession;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use Magento\InventoryReservationsApi\Model\ReservationInterface;
use Magento\InventorySales\Model\IsProductSalableForRequestedQtyCondition\IsSalableWithReservationsCondition;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\ResourceModel\Quote\Item as ResourceItem;
use Magento\Sales\Api\OrderManagementInterface as OrderManagement;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item;
use Mageplaza\EditOrder\Helper\Data as HelperData;
use Mageplaza\EditOrder\Model\Quote\QuoteManagement;

/**
 * Class Total
 * @package Mageplaza\EditOrder\Model\Order
 */
class Total
{
    const TYPE_COLLECT_ITEMS    = 'items';
    const TYPE_COLLECT_SHIPPING = 'shipping';

    /**
     * @var QuoteManagement
     */
    protected $quoteManagement;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var QuoteSession
     */
    protected $quoteSession;

    /**
     * @var OrderManagement
     */
    protected $orderManagement;

    /**
     * @var StockItemInterface
     */
    protected $stockItem;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var ResourceItem
     */
    protected $itemResourceModel;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * Total constructor.
     *
     * @param QuoteManagement $quoteManagement
     * @param QuoteFactory $quoteFactory
     * @param QuoteSession $quoteSession
     * @param OrderManagement $orderManagement
     * @param StockItemInterface $stockItem
     * @param HelperData $helperData
     * @param ResourceItem $itemResourceModel
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        QuoteManagement $quoteManagement,
        QuoteFactory $quoteFactory,
        QuoteSession $quoteSession,
        OrderManagement $orderManagement,
        StockItemInterface $stockItem,
        HelperData $helperData,
        ResourceItem $itemResourceModel,
        ResourceConnection $resourceConnection
    ) {
        $this->quoteManagement    = $quoteManagement;
        $this->quoteFactory       = $quoteFactory;
        $this->quoteSession       = $quoteSession;
        $this->orderManagement    = $orderManagement;
        $this->stockItem          = $stockItem;
        $this->_helperData        = $helperData;
        $this->itemResourceModel  = $itemResourceModel;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param Order $order
     * @param array $data
     * @param array $qty
     *
     * @return array
     * @throws Exception
     */
    public function saveOrder($order, $data, $qty = [])
    {
        $total = $this->collectTotals($order, $data);

        if (count($total)) {
            if (isset($total['ship_amount'])) {
                $order->setShippingMethod($data['method'])
                    ->setShippingAmount($total['ship_amount'])
                    ->setShippingTaxAmount($total['ship_tax_amount'])
                    ->setShippingDiscountAmount($total['ship_discount_amount'])
                    ->setShippingDescription($total['ship_description'])
                    ->setBaseShippingTaxAmount($total['ship_tax_amount'])
                    ->setBaseShippingDiscountAmount($total['ship_discount_amount'])
                    ->setBaseShippingAmount($total['ship_amount'])
                    ->setShippingInclTax($data['total_fee'])
                    ->setBaseShippingInclTax($data['total_fee']);
            }

            $order->setTaxAmount($total['tax_amount'])
                ->setDiscountAmount(-$total['discount_amount'])
                ->setGrandTotal($total['grand_total']);

            $order->setBaseTaxAmount($total['tax_amount'])
                ->setBaseDiscountAmount(-$total['discount_amount'])
                ->setBaseGrandTotal($total['grand_total']);

            if (isset($total['subtotal'])) {
                $order->setSubtotal($total['subtotal']);
                $order->setBaseSubtotal($total['subtotal'])
                    ->setSubtotalInclTax($total['subtotal_incl_tax'])
                    ->setBaseSubtotalInclTax($total['subtotal_incl_tax']);
            }

            if ($data['type'] === self::TYPE_COLLECT_ITEMS) {
                try {
                    if (!empty($qty)) {
                        foreach ($order->getItems() as $orderItem) {
                            if (array_key_exists($orderItem->getQuoteItemId(), $qty)) {
                                // $this->resetSalableQuantity($orderItem, $qty[$orderItem->getQuoteItemId()]);
                            }
                        }
                    }
                    $this->orderManagement->place($order);
                } catch (Exception $e) {
                    if ($this->_helperData->versionCompare('2.3.0')) {
                        $objectManager = ObjectManager::getInstance();
                        $isSalable     = $objectManager->create(
                            IsSalableWithReservationsCondition::class
                        );
                        $stockId       = $this->stockItem->getStockId();
                        foreach ($order->getAllItems() as $item) {
                            $result = $isSalable->execute(
                                $item->getSku(),
                                $stockId,
                                $item->getQtyOrdered()
                            );

                            if ($result->getErrors()) { //remove items error
                                $item->delete();
                            }
                        }

                        $order->save();
                    }

                    return [
                        'error' => $e->getMessage()
                    ];
                }
            } else {
                $order->save();
            }
        }

        return [
            'success' => true
        ];
    }

    /**
     * @param $item
     * @param $qty
     */
    public function resetSalableQuantity($item, $qty)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName  = $this->resourceConnection->getTableName('inventory_reservation');

        $columns = [
            ReservationInterface::STOCK_ID,
            ReservationInterface::SKU,
            ReservationInterface::QUANTITY,
            ReservationInterface::METADATA,
        ];

        $data = [[1, $item->getSku(), $qty, '{}']];
        $connection->insertArray($tableName, $columns, $data);
    }

    /**
     * @param Order $order
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function collectTotals($order, $data)
    {
        $totals = [];

        if ($data['type'] === self::TYPE_COLLECT_SHIPPING) {
            $totals = $this->collectTotalsByShipping($order, $data);
        }

        if ($data['type'] === self::TYPE_COLLECT_ITEMS) {
            $totals = $this->collectTotalsByItems($order, $data);
        }

        return $totals;
    }

    /**
     * @param Order $order
     * @param array $shipData
     *
     * @return array
     */
    public function collectTotalsByShipping($order, $shipData)
    {
        $oldShipAmount     = $order->getBaseShippingAmount();
        $oldGrandTotal     = $order->getBaseGrandTotal();
        $oldTaxAmount      = $order->getBaseTaxAmount();
        $oldShipTax        = $order->getBaseShippingTaxAmount();
        $oldDiscountAmount = $order->getBaseDiscountAmount();
        $oldShipDiscount   = $order->getBaseShippingDiscountAmount();

        $newShipTaxAmount  = $shipData['ship_tax_percent'] * $shipData['ship_amount'] / 100;
        $newTaxAmount      = $oldTaxAmount - $oldShipTax + $newShipTaxAmount;
        $newDiscountAmount = -$oldDiscountAmount - $oldShipDiscount + $shipData['ship_discount_amount'];
        $newGrandTotal     = $oldGrandTotal - $oldDiscountAmount -
            $oldTaxAmount + $newTaxAmount - $newDiscountAmount - $oldShipAmount + $shipData['ship_amount'];

        return [
            'ship_amount'          => $shipData['ship_amount'],
            'ship_tax_amount'      => $newShipTaxAmount,
            'ship_discount_amount' => $shipData['ship_discount_amount'],
            'ship_description'     => $shipData['ship_description'],
            'tax_amount'           => $newTaxAmount,
            'discount_amount'      => $newDiscountAmount,
            'grand_total'          => $newGrandTotal
        ];
    }

    /**
     * @param Order $order
     * @param array $itemsData
     *
     * @return array
     * @throws Exception
     */
    public function collectTotalsByItems($order, $itemsData)
    {
        $oldTaxAmount = $order->getBaseTaxAmount();

        $oldItemsTaxAmount = $this->getItemsTaxAmount($order);
        $shipTax           = $order->getBaseShippingTaxAmount();
        $otherTax          = $oldTaxAmount - $oldItemsTaxAmount - $shipTax;
        $newTaxAmount      = $itemsData['tax_amount'] + $shipTax + $otherTax;

        $oldDiscountAmount      = $order->getBaseDiscountAmount();
        $oldItemsDiscountAmount = $this->getItemsDiscountAmount($order);
        $shipDiscount           = $order->getBaseShippingDiscountAmount();
        $otherDiscount          = -$oldDiscountAmount - $oldItemsDiscountAmount - $shipDiscount;
        $newDiscountAmount      = $itemsData['discount_amount'] + $shipDiscount + $otherDiscount;

        $shipAmount = $order->getBaseShippingAmount();

        $quote      = $this->quoteFactory->create()->loadByIdWithoutStore($this->quoteSession->getQuoteId());
        
        $removeItem = [];
        foreach ($order->getAllItems() as $item) {
            $item->delete(); //remove all old items
            $removeItem[$item->getSku()] = $item;
        }

        $order->setItems($this->quoteManagement->getResolveItems($quote)); //set new items for order item

        foreach ($order->getAllItems() as $item) {
            if (array_key_exists($item->getSku(), $removeItem)) {
                unset($removeItem[$item->getSku()]);
            }
        }

        if (!empty($removeItem)) {
            foreach ($removeItem as $item) {
                // $this->resetSalableQuantity($item, $item->getQtyOrdered());
            }
        }

        $newSubtotal        = $this->getSubtotalByItems($order);
        $newSubtotalInclTax = $itemsData['tax_amount'] + $newSubtotal;
        $newGrandTotal      = $newSubtotal + $shipAmount + $newTaxAmount - $newDiscountAmount;

        return [
            'tax_amount'        => $newTaxAmount,
            'subtotal'          => $newSubtotal,
            'discount_amount'   => $newDiscountAmount,
            'grand_total'       => $newGrandTotal,
            'subtotal_incl_tax' => $newSubtotalInclTax
        ];
    }

    /**
     * @param Quote $quote
     *
     * @return bool
     */
    public function isQuoteItemOutStock($quote)
    {
        foreach ($this->quoteManagement->getResolveItems($quote) as $key => $item) {
            if (!$item->getProduct()->getQuantityAndStockStatus()['is_in_stock']) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get total discount amount all items
     *
     * @param Order $order
     *
     * @return float|null
     */
    public function getItemsDiscountAmount($order)
    {
        $discount = 0;

        /** @var Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            $discount += $item->getBaseDiscountAmount();
        }

        return $discount;
    }

    /**
     * Get total tax amount all items
     *
     * @param Order $order
     *
     * @return float
     */
    public function getItemsTaxAmount($order)
    {
        $tax = 0;

        /** @var Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            $tax += $item->getBaseTaxAmount();
        }

        return $tax;
    }

    /**
     * @param Order $order
     *
     * @return float
     */
    public function getSubtotalByItems($order)
    {
        $subtotal = 0;

        /** @var Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            $subtotal += $item->getBaseRowTotal();
        }

        return $subtotal;
    }
}

<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Mageplaza
 * @package   Mageplaza_EditOrder
 * @copyright Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license   https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\EditOrder\Plugin\Model\Quote\Item;

use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Store\Model\Store;
use Magento\Tax\Api\Data\TaxDetailsItemInterface;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;
use Mageplaza\EditOrder\Helper\Data as HelperData;

/**
 * Class CustomTax
 * @package Mageplaza\EditOrder\Plugin\Model\Quote\Item
 */
class CustomTax
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var bool
     */
    protected $_isError = false;

    /**
     * CustomTax constructor.
     *
     * @param RequestInterface $request
     * @param HelperData $helperData
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        RequestInterface $request,
        HelperData $helperData,
        ManagerInterface $messageManager
    ) {
        $this->_request        = $request;
        $this->_helperData     = $helperData;
        $this->_messageManager = $messageManager;
    }

    /**
     * @param CommonTaxCollector $subject
     * @param CommonTaxCollector $result
     * @param AbstractItem $quoteItem
     * @param TaxDetailsItemInterface $itemTaxDetails
     * @param TaxDetailsItemInterface $baseItemTaxDetails
     * @param Store $store
     *
     * @return mixed
     * @SuppressWarnings(Unused)
     */
    public function afterUpdateItemTaxInfo(
        CommonTaxCollector $subject,
        $result,
        $quoteItem,
        $itemTaxDetails,
        $baseItemTaxDetails,
        $store
    ) {
        if (!$this->_helperData->isEnabled()) {
            return $result;
        }

        $request = $this->_request->getParams();

        if (isset($request['item'])) {
            try {
                foreach ($request['item'] as $itemId => $data) {
                    if (!empty($data['tax']) && ($itemId === (int) $quoteItem->getId())) {
                        $itemPriceWithoutDiscount = $quoteItem->getRowTotal() - $quoteItem->getTotalDiscountAmount();
                        $taxPercent               = $data['tax'] ?: 0;
                        $taxAmount                = $itemPriceWithoutDiscount * $taxPercent / 100;

                        $price = isset($data['custom_price']) ? $data['custom_price'] : $data['price'];

                        $quoteItem->setTaxPercent($taxPercent);
                        $quoteItem->setTaxAmount($taxAmount);
                        $quoteItem->setBaseTaxAmount($taxAmount);
                        $quoteItem->setMpCustomTaxPercent($taxPercent);
                        $quoteItem->setPriceInclTax($price + $taxAmount / $data['qty']);
                        $quoteItem->setBasePriceInclTax($price + $taxAmount / $data['qty']);
                        $quoteItem->setRowTotalInclTax($quoteItem->getRowTotal() + $taxAmount);
                        $quoteItem->setBaseRowTotalInclTax($quoteItem->getBaseRowTotal() + $taxAmount);
                    }
                }
            } catch (Exception $e) {
                if (!$this->_isError) {
                    $this->_messageManager->addErrorMessage(__('Unknown error, Unable to update order.'));
                    $this->_isError = true;
                }
            }
        }

        return $result;
    }
}

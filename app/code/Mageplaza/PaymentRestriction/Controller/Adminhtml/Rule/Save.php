<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_PaymentRestriction
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\PaymentRestriction\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Helper\Js;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Mageplaza\PaymentRestriction\Controller\Adminhtml\Rule;
use Mageplaza\PaymentRestriction\Helper\Data;
use Mageplaza\PaymentRestriction\Model\RuleFactory;

/**
 * Class Save
 * @package Mageplaza\PaymentRestriction\Controller\Adminhtml\Rule
 */
class Save extends Rule
{
    /**
     * JS helper
     *
     * @var \Magento\Backend\Helper\Js
     */
    public $jsHelper;

    /**
     * @var DateTime
     */
    public $date;

    /**
     * @var Data
     */
    protected $_helperData;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param RuleFactory $ruleFactory
     * @param Js $jsHelper
     * @param DateTime $date
     * @param Data $helperData
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RuleFactory $ruleFactory,
        Js $jsHelper,
        DateTime $date,
        Data $helperData
    )
    {
        $this->jsHelper = $jsHelper;
        $this->date = $date;
        $this->_helperData = $helperData;

        parent::__construct($ruleFactory, $registry, $context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data = $this->getRequest()->getPostValue()) {
            /** @var \Mageplaza\PaymentRestriction\Model\Rule $rule */
            $rule = $this->initRule();
            $this->prepareData($rule, $data['rule']);

            /** get rule conditions */
            $rule->loadPost($data['rule']);
            $this->_eventManager->dispatch('mageplaza_paymentrestriction_rule_prepare_save', ['post' => $rule, 'request' => $this->getRequest()]);

            try {
                $rule->save();

                $this->messageManager->addSuccess(__('The rule has been saved.'));
                $this->_getSession()->setData('mageplaza_paymentrestriction_rule_data', false);

                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath('mppaymentrestriction/*/edit', ['id' => $rule->getId(), '_current' => true]);
                } else {
                    $resultRedirect->setPath('mppaymentrestriction/*/');
                }

                return $resultRedirect;
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Rule.'));
            }

            $this->_getSession()->setData('mageplaza_paymentrestriction_rule_data', $data);

            $resultRedirect->setPath('mppaymentrestriction/*/edit', ['id' => $rule->getId(), '_current' => true]);

            return $resultRedirect;
        }

        $resultRedirect->setPath('mppaymentrestriction/*/');

        return $resultRedirect;
    }

    /**
     * @param $rule
     * @param array $data
     *
     * @return $this
     */
    protected function prepareData($rule, $data = [])
    {
        if ($rule->getCreatedAt() == null) {
            $data['created_at'] = $this->date->date();
        }
        $data['started_at'] = ($data['started_at_name'] == '') ? $this->date->date('m/d/Y') : $data['started_at_name'];
        $data['updated_at'] = $this->date->date();

        if (isset($data['schedule_name'])) {
            $data['schedule'] = $this->_helperData->jsonEncode($data['schedule_name']);
        }
        if (!isset($data['payment_methods'])) {
            $data['payment_methods'] = [];
        }
        $rule->addData($data);

        return $this;
    }
}
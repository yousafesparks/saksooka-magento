<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_PaymentRestriction
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\PaymentRestriction\Plugin\Model;

use Magento\Quote\Model\PaymentMethodManagement as PaymentMethodManagementPlugin;
use Mageplaza\PaymentRestriction\Model\Config\Source\Action;
use Mageplaza\PaymentRestriction\Model\Config\Source\Location;
use Mageplaza\PaymentRestriction\Plugin\PaymentRestrictionPlugin;

/**
 * Class PaymentMethodManagement
 * @package Mageplaza\PaymentRestriction\Plugin\Model
 */
class PaymentMethodManagement extends PaymentRestrictionPlugin
{
    /**
     * @var bool|Rule
     */
    protected $appliedRule;

    /**
     * @var bool
     */
    protected $ruleActive = false;

    /**
     * @param PaymentMethodManagementPlugin $subject
     * @param $availableMethods
     *
     * @return array
     * @throws \Exception
     */
    public function afterGetList(PaymentMethodManagementPlugin $subject, $availableMethods)
    {
        $newAvailableMethods = $availableMethods;
        if ($this->_helperData->isEnabled($this->_storeManagement->getStore()->getId())) {
            $newAvailableMethods = [];
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $this->_coreRegistry->registry('mp_paymentrestriction_quote');
            if (!$quote && $this->_request->getFullActionName() == 'onestepcheckout_index_index') {
                $quote = $this->_checkoutSession->getQuote();
            }
            if ($quote) {
                $appliedSaleRuleIds = $quote->getShippingAddress()->getAppliedRuleIds();
                $appliedSaleRuleIds = explode(',', $appliedSaleRuleIds);
                $shippingAddress = $quote->getShippingAddress();
                /** @var \Mageplaza\PaymentRestriction\Model\ResourceModel\Rule\Collection $ruleCollection */
                $ruleCollection = $this->_helperData->getPaymentRestrictionRuleCollection();
                /** @var \Mageplaza\ShippingRules\Model\Rule $rule */
                foreach ($ruleCollection as $rule) {
                    $ruleLocations = $rule->getLocation();
                    $ruleLocations = explode(',', $ruleLocations);
                    if (in_array(Location::ORDER_FRONTEND, $ruleLocations)) {
                        if ($this->_helperData->getScheduleFilter($rule)) {
                            if ($rule->getSaleRulesInactive()) {
                                $saleRuleInactive = explode(',', $rule->getSaleRulesInactive());
                                foreach ($saleRuleInactive as $inActive) {
                                    if (in_array($inActive, $appliedSaleRuleIds)) {
                                        $this->ruleActive = true;
                                        break;
                                    }
                                }
                                if ($this->ruleActive) {
                                    $this->appliedRule = null;
                                    break;
                                }
                            }
                            if ($rule->getSaleRulesActive()) {
                                $saleRuleActive = explode(',', $rule->getSaleRulesActive());
                                foreach ($saleRuleActive as $active) {
                                    if (in_array($active, $appliedSaleRuleIds)) {
                                        $this->ruleActive = true;
                                        break;
                                    }
                                }
                                if ($this->ruleActive) {
                                    $this->appliedRule = $rule;
                                    break;
                                }
                            }
                            if ($rule->validate($shippingAddress)) {
                                $this->appliedRule = $rule;
                                break;
                            }
                        }
                    }
                }

                if ($this->appliedRule) {
                    $pickedPaymentMethods = $this->appliedRule->getPaymentMethods();
                    $pickedPaymentMethods = explode(',', $pickedPaymentMethods);

                    foreach ($availableMethods as $paymentCode => $paymentModel) {
                        $options[$paymentCode] = [
                            'label' => $paymentModel->getTitle(),
                            'value' => $paymentModel->getCode()
                        ];
                        if ($this->appliedRule->getAction() == Action::SHOW) {
                            if (in_array($paymentModel->getCode(), $pickedPaymentMethods)) {
                                $newAvailableMethods[] = $paymentModel;
                            }
                        } else {
                            if (!in_array($paymentModel->getCode(), $pickedPaymentMethods)) {
                                $newAvailableMethods[] = $paymentModel;
                            }
                        }
                    }
                } else {
                    $newAvailableMethods = $availableMethods;
                }
            }
        }

        return $newAvailableMethods;
    }
}

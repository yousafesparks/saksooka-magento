<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_PaymentRestriction
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\PaymentRestriction\Plugin\Model\Braintree\Observer;

use Magento\Braintree\Block\Paypal\Button;
use Magento\Braintree\Observer\AddPaypalShortcuts as AddPaypalShortcutsPlugin;
use Magento\Catalog\Block\ShortcutButtons;
use Magento\Framework\Event\Observer;
use Mageplaza\PaymentRestriction\Model\Config\Source\Action;
use Mageplaza\PaymentRestriction\Model\Config\Source\Location;
use Mageplaza\PaymentRestriction\Plugin\PaypalShortcutsPlugin;

/**
 * Class AddPaypalShortcuts
 * @package Mageplaza\PaymentRestriction\Plugin\Model\Braintree\Observer
 */
class AddPaypalShortcuts extends PaypalShortcutsPlugin
{
    /**
     * Block class
     */
    const PAYPAL_SHORTCUT_BLOCK = Button::class;

    /**
     * @var bool|Rule
     */
    protected $appliedRule;

    /**
     * @var bool
     */
    protected $ruleActive = false;

    /**
     * @param AddPaypalShortcutsPlugin $subject
     * @param callable $proceed
     * @param Observer $observer
     *
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundExecute(AddPaypalShortcutsPlugin $subject, callable $proceed, Observer $observer)
    {
        if ($this->_helperData->isEnabled()) {
            $quote = $this->_checkoutSession->getQuote();
            if ($quote && ($cartId = $quote->getId()) && $this->_request->getFullActionName() != 'catalog_product_view') {
                $this->_collectTotals($cartId);
                $appliedSaleRuleIds = $quote->getShippingAddress()->getAppliedRuleIds();
                $appliedSaleRuleIds = explode(',', $appliedSaleRuleIds);
                $shippingAddress = $quote->getShippingAddress();
                /** @var \Mageplaza\PaymentRestriction\Model\ResourceModel\Rule\Collection $ruleCollection */
                $ruleCollection = $this->_helperData->getPaymentRestrictionRuleCollection();
                /** @var \Mageplaza\ShippingRules\Model\Rule $rule */
                foreach ($ruleCollection as $rule) {
                    $ruleLocations = $rule->getLocation();
                    $ruleLocations = explode(',', $ruleLocations);
                    if (in_array(Location::ORDER_FRONTEND, $ruleLocations)) {
                        if ($this->_helperData->getScheduleFilter($rule)) {
                            if ($rule->getSaleRulesInactive()) {
                                $saleRuleInactive = explode(',', $rule->getSaleRulesInactive());
                                foreach ($saleRuleInactive as $inActive) {
                                    if (in_array($inActive, $appliedSaleRuleIds)) {
                                        $this->ruleActive = true;
                                        break;
                                    }
                                }
                                if ($this->ruleActive) {
                                    $this->appliedRule = null;
                                    break;
                                }
                            }
                            if ($rule->getSaleRulesActive()) {
                                $saleRuleActive = explode(',', $rule->getSaleRulesActive());
                                foreach ($saleRuleActive as $active) {
                                    if (in_array($active, $appliedSaleRuleIds)) {
                                        $this->ruleActive = true;
                                        break;
                                    }
                                }
                                if ($this->ruleActive) {
                                    $this->appliedRule = $rule;
                                    break;
                                }
                            }
                            if ($rule->validate($shippingAddress)) {
                                $this->appliedRule = $rule;
                                break;
                            }
                        }
                    }
                }
                if ($this->appliedRule) {
                    $pickedPaymentMethods = $this->appliedRule->getPaymentMethods();
                    $pickedPaymentMethods = explode(',', $pickedPaymentMethods);
                    if ($this->appliedRule->getAction() == Action::SHOW) {
                        if (!in_array('braintree_paypal', $pickedPaymentMethods)) {
                            return;
                        }
                    } else {
                        if (in_array('braintree_paypal', $pickedPaymentMethods)) {
                            return;
                        }
                    }
                }
            }
        }
        // Remove button from catalog pages
        if ($observer->getData('is_catalog_product')) {
            return;
        }

        /** @var ShortcutButtons $shortcutButtons */
        $shortcutButtons = $observer->getEvent()->getContainer();

        $shortcut = $shortcutButtons->getLayout()->createBlock(self::PAYPAL_SHORTCUT_BLOCK);

        $shortcutButtons->addShortcut($shortcut);
    }
}

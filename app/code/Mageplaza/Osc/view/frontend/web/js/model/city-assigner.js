/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CityDropdown
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */

define([
    'Magento_Customer/js/customer-data'
], function (customerData) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return function (formData) {
        if (formData
            && countryData()[formData.country_id]
            && countryData()[formData.country_id]['cities']
        ) {
            var city_id = null;
            if (formData.extension_attributes !== undefined && formData.extension_attributes.city_id !== undefined) {
                city_id = formData.extension_attributes.city_id;
            } else if (formData.custom_attributes !== undefined && formData.custom_attributes.city_id !== undefined) {
                city_id = formData.custom_attributes.city_id.value;
                formData.extension_attributes = {'city_id': city_id};
            }else if (formData.city_id !== undefined) {
            	city_id = formData.city_id;
            }
            if (city_id) {
                formData.city = countryData()[formData.country_id]['cities'][city_id];
            }
        }
    };
});
<?php
namespace Mageplaza\Osc\Model\Plugin;

class GuestSave
{
    protected $_checkoutSession;

    public function __construct(\Magento\Checkout\Model\Session $checkoutSession){
        $this->_checkoutSession = $checkoutSession;
    }

    public function aroundExecute(\Bss\GuestToCustomer\Observer\GuestSave $subject,  callable $proceed, \Magento\Framework\Event\Observer $observer)
    {
        $oscData = $this->_checkoutSession->getOscData();
        if (isset($oscData['register']) && $oscData['register']
            && isset($oscData['password'])
            && $oscData['password']
        ) {
            return $this;
        }
        $result = $proceed($observer);
        return $result;
    }
}

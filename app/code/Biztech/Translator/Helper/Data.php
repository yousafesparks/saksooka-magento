<?php
/** Copyright © 2016 store.biztechconsultancy.com. All Rights Reserved. **/
namespace Biztech\Translator\Helper;

use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Website;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ENABLED = 'translator/general/is_active';
    const XML_PATH_INSTALLED = 'translator/activation/installed';
    const XML_PATH_DATA = 'translator/activation/data';
    const XML_PATH_WEBSITES = 'translator/activation/websites';
    const XML_PATH_EN = 'translator/activation/en';
    const XML_PATH_KEY = 'translator/activation/key';

    protected $_resourceConfig;
    protected $encryptor;
    protected $coreConfig;
    protected $_store;
    protected $_storeManager;
    protected $logger;

    /**
     * Data constructor.
     * @param Context $context
     * @param EncryptorInterface $encryptor
     * @param ModuleListInterface $moduleList
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param Config $resourceConfig
     * @param ReinitableConfigInterface $coreConfig
     * @param StoreManagerInterface $store
     */
    public function __construct(
        Context $context,
        EncryptorInterface $encryptor,
        ModuleListInterface $moduleList,
        StoreManagerInterface $storeManager,
        Config $resourceConfig,
        ReinitableConfigInterface $coreConfig,
        \Magento\Store\Model\StoreManagerInterface $store
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->logger = $context->getLogger();
        $this->_storeManager = $storeManager;
        $this->_resourceConfig = $resourceConfig;
        $this->encryptor = $encryptor;
        $this->coreConfig = $coreConfig;
        $this->_store = $store;
        parent::__construct($context);
    }

    public function getScopeConfig()
    {
        return $this->scopeConfig;
    }

    public function getConfigValue($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $query
     * @return string
     */
    public function buildHttpQuery($query)
    {
        $query_array = [];
        foreach ($query as $key => $key_value) {
            $query_array[] = $key . '=' . urlencode($key_value);
        }
        return implode('&', $query_array);
    }

    /**
     * @param $xmlString
     * @return array
     */
    public function parseXml($xmlString)
    {
        libxml_use_internal_errors(true);
        $xmlObject = simplexml_load_string($xmlString);
        $result = [];
        if (!empty($xmlObject)) {
            $this->convertXmlObjToArr($xmlObject, $result);
        }
        return $result;
    }

    /**
     * @param $obj
     * @param $arr
     */
    public function convertXmlObjToArr($obj, &$arr)
    {
        $children = $obj->children();
        $executed = false;
        foreach ($children as $elementName => $node) {
            if (is_array($arr) && array_key_exists($elementName, $arr)) {
                if (is_array($arr[$elementName]) && array_key_exists(0, $arr[$elementName])) {
                    $i = count($arr[$elementName]);
                    $this->convertXmlObjToArr($node, $arr[$elementName][$i]);
                } else {
                    $tmp = $arr[$elementName];
                    $arr[$elementName] = [];
                    $arr[$elementName][0] = $tmp;
                    $i = count($arr[$elementName]);
                    $this->convertXmlObjToArr($node, $arr[$elementName][$i]);
                }
            } else {
                $arr[$elementName] = [];
                $this->convertXmlObjToArr($node, $arr[$elementName]);
            }
            $executed = true;
        }
        if (!$executed && $children->getName() == "") {
            $arr = (String)$obj;
        }
        return;
    }

    /**
     * @return array
     */
    public function getAllStoreDomains()
    {
        $domains = [];
        foreach ($this->_storeManager->getWebsites() as $website) {
            $url = $website->getConfig('web/unsecure/base_url');
            if ($domain = trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url))) {
                $domains[] = $domain;
            }
            $url = $website->getConfig('web/secure/base_url');
            if ($domain = trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url))) {
                $domains[] = $domain;
            }
        }
        return array_unique($domains);
    }

    /**
     * @return mixed
     *
     */
    public function getDataInfo()
    {
        //@codingStandardsIgnoreStart
        $data = $this->scopeConfig->getValue(self::XML_PATH_DATA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return json_decode(base64_decode($this->encryptor->decrypt($data)));
        //@codingStandardsIgnoreEnd
    }

    /**
     * @return array
     */
    public function getAllWebsites()
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_INSTALLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (!$value) {
            return [];
        }
        $data = $this->scopeConfig->getValue(self::XML_PATH_DATA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $web = $this->scopeConfig->getValue(self::XML_PATH_WEBSITES, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $websites = explode(',', str_replace($data, '', $this->encryptor->decrypt($web)));
        $websites = array_diff($websites, [""]);
        return $websites;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getFormatUrl($url)
    {
        $input = trim($url, '/');
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        //@codingStandardsIgnoreStart
        $urlParts = parse_url($input);
        //@codingStandardsIgnoreEnd
        if (isset($urlParts['path'])) {
            $domain = preg_replace('/^www\./', '', $urlParts['host'] . $urlParts['path']);
        } else {
            $domain = preg_replace('/^www\./', '', $urlParts['host']);
        }
        return $domain;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        $websiteId = $this->_store->getStore()->getWebsite()->getId();
        $isenabled = $this->scopeConfig->getValue(self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($isenabled) {
            if ($websiteId) {
                $websites = $this->getAllWebsites();
                $key = $this->scopeConfig->getValue(self::XML_PATH_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if ($key == null || $key == '') {
                    return false;
                } else {
                    $en = $data = $this->scopeConfig->getValue(self::XML_PATH_EN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    if ($isenabled && $en && in_array($websiteId, $websites)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                $en = $en = $data = $this->scopeConfig->getValue(self::XML_PATH_EN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if ($isenabled && $en) {
                    return true;
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function isTranslatorEnabled()
    {
        $storeId = $this->_store->getStore()->getId();
        $isEnabled = $this->scopeConfig->getValue('translator/general/is_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        if ($isEnabled) {
            return true;
        }
        return false;
    }

    public function isUrlKeyAttribute()
    {
        $flag = false;
        $translateAll = $this->scopeConfig->getValue('translator/general/massaction_product_translate_fields');
        $finalAttributeSet = array_values(explode(',', $translateAll));

        if (in_array('url_key', $finalAttributeSet)) {
            $flag = true;
        }
        
        return $flag;
    }
}

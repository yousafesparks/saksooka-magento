<?php

namespace Biztech\Translator\Block\Adminhtml\Crondata;

use Biztech\Translator\Helper\Data as BizHelper;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory]
     */
    protected $_setsFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Type
     */
    protected $_type;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $_status;
    protected $_collectionFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_visibility;

    /**
     * @var \Magento\Store\Model\WebsiteFactory
     */
    protected $_websiteFactory;
    protected $helper;
    protected $status;

    /**
     * @param \Magento\Backend\Block\Template\Context                     $context
     * @param \Magento\Backend\Helper\Data                                $backendHelper
     * @param \Magento\Store\Model\WebsiteFactory                         $websiteFactory
     * @param \Biztech\Translator\Model\ResourceModel\Crondata\Collection $collectionFactory
     * @param \Magento\Framework\Module\Manager                           $moduleManager
     * @param \Biztech\Translator\Model\Config\Source\Status              $status
     * @param BizHelper                                                   $helper
     * @param array                                                       $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Biztech\Translator\Model\ResourceModel\Crondata\Collection $collectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Biztech\Translator\Model\Config\Source\Status $status,
        BizHelper $helper,
        array $data = []
    ) {
        $this->status = $status;
        $this->_logger = $context->getLogger();
        $this->_collectionFactory = $collectionFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->moduleManager = $moduleManager;
        $this->helper = $helper;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('translator/cron/index', ['_current' => true]);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        if ($this->helper->isTranslatorEnabled()) {
            parent::_construct();
        }

        $this->setId('crondataGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * @return Store
     */
    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return $this->_storeManager->getStore($storeId);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {
            $collection = $this->_collectionFactory->load();
            $this->setCollection($collection);
            parent::_prepareCollection();
            return $this;
        } catch (\Exception $e) {
            $this->_logger->debug($e->getMessage());
        }
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'lang_from',
            [
                'header' => __('Language From'),
                'index' => 'lang_from',
                'class' => 'lang_from',
                'renderer' => 'Biztech\Translator\Block\Adminhtml\Crondata\Renderer\Langfrom'
            ]
        );

        $this->addColumn(
            'lang_to',
            [
                'header' => __('Language To'),
                'index' => 'lang_to',
                'class' => 'lang_to',
                'renderer' => 'Biztech\Translator\Block\Adminhtml\Crondata\Renderer\Langto'
            ]
        );

        $this->addColumn(
            'store_id',
            [
                'header' => __('Store'),
                'index' => 'store_id',
                'class' => 'store_id',
                'renderer' => 'Biztech\Translator\Block\Adminhtml\Crondata\Renderer\Storename'
            ]
        );
        
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'class' => 'status',
                'type' => 'options',
                'options' => $this->status->toOptionArray()
            ]
        );


        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }
}

<?php
/**
 * Copyright © 2016 store.biztechconsultancy.com. All Rights Reserved..
 */
namespace Biztech\Translator\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Biztech\Translator\Helper\Data;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Store\Model\Website;
use Magento\Store\Model\Store;

class Enabledisable extends Field
{
    const XML_PATH_ACTIVATION = 'translator/activation/key';
    protected $_scopeConfig;
    protected $_helper;
    protected $_resourceConfig;
    protected $_web;
    protected $_store;

    public function __construct(
        Context $context,
        Data $helper,
        Config $resourceConfig,
        Website $web,
        Store $store,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->storeManager = $context->getStoreManager();
        $this->_web = $web;
        $this->_resourceConfig = $resourceConfig;
        $this->_store = $store;
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $websites = $this->_helper->getAllWebsites();
        if (!empty($websites)) {
            $website_id = $this->getRequest()->getParam('website');
            $website = $this->_web->load($website_id);
            if ($website && in_array($website->getWebsiteId(), $websites)) {
                $html = $element->getElementHtml();
            } elseif (!$website_id) {
                $html = $element->getElementHtml();
                $isEnable = $this->_scopeConfig->getValue('translator/general/is_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if (!$isEnable) {
                    $this->_resourceConfig->saveConfig('translator/general/is_active', 0, 'default', 0);
                }
            } else {
                $html = sprintf('<strong class="required" style="color:red;">%s</strong>', __('Please buy additional domains'));
            }
        } else {
            $websitecode = $this->_request->getParam('website');
            $websiteId = $this->_store->load($websitecode)->getWebsiteId();
            $isenabled = $this->_storeManager->getWebsite($websiteId)->getConfig('translator/activation/key');
            $modulestatus = $this->_resourceConfig;
            if ($isenabled != null || $isenabled != '') {
                $html = sprintf('<strong class="required" style="color:red;">%s</strong>', __('Please select a website'));
                $modulestatus->saveConfig('translator/general/is_active', 0, 'default', 0);
            } else {
                $html = sprintf('<strong class="required" style="color:red;">%s</strong>', __('Please enter a valid key'));
                $modulestatus->saveConfig('translator/general/is_active', 0, 'default', 0);
            }
        }
        return $html;
    }
}

<?php
/**
 * Copyright © 2016 store.biztechconsultancy.com. All Rights Reserved..
 */
namespace Biztech\Translator\Block\Adminhtml\Catalog\Product;

use Biztech\Translator\Helper\Data;
use Biztech\Translator\Helper\Language;
use Biztech\Translator\Helper\Translator;
use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Helper\Product;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;

class Edit extends \Magento\Catalog\Block\Adminhtml\Product\Edit
{
    /**
     * @var string
     */
    protected $_template = 'Biztech_Translator::translator/catalog/product/edit.phtml';
    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadataInterface;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $helperData;
    /**
     * @var \Biztech\Translator\Helper\Translator
     */
    protected $translatorhelper;
    /**
     * @var \Biztech\Translator\Helper\Language
     */
    protected $languagehelper;
	
	protected $_escaper;
    
    /**
     * Edit constructor.
     * @param Context $context
     * @param EncoderInterface $jsonEncoder
     * @param SetFactory $attributeSetFactory
     * @param Registry $registry
     * @param Product $productHelper
     * @param Data $helperData
     * @param array $data
     * @param ProductMetadataInterface $productMetadataInterface
     */
    public function __construct(
        Context $context,
        EncoderInterface $jsonEncoder,
        SetFactory $attributeSetFactory,
        Registry $registry,
        Product $productHelper,
        Data $helperData,
        Language $languagehelper,
        Translator $translatorhelper,
        ProductMetadataInterface $productMetadataInterface,
		\Magento\Framework\Escaper $escaper,
        array $data = []
    ) {
        $this->helperData = $helperData;
        $this->languagehelper = $languagehelper;
        $this->translatorhelper = $translatorhelper;
        $this->productMetadataInterface = $productMetadataInterface;
        parent::__construct(
            $context,
            $jsonEncoder,
            $attributeSetFactory,
            $registry,
            $productHelper,
			$escaper,
            $data
        );
    }

    /**
     * @return mixed
     */
    public function getBiztechTranslatorConfiguration()
    {
        if ($this->helperData->isEnabled() && $this->helperData->isTranslatorEnabled()) {
            $storeId = $this->getRequest()->getParam('store', 0);
            $translatedFields = $this->_scopeConfig->getValue('translator/general/massaction_product_translate_fields', \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
            $url = $this->getUrl('translator/translator/translate');
            $config = $this->languagehelper->getConfiguration($url, $translatedFields, $storeId);
            return $config;
        }
    }

    /**
     * Getting magneto version
     * @return String
     */
    public function getVersion()
    {
        $version = $this->productMetadataInterface->getVersion();
        return $version;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->helperData->isEnabled();
    }

    /**
     * @return StoreID
     */
    public function getStoreid()
    {
        $storeId = $this->getLayout()->createBlock('\Magento\Backend\Block\Store\Switcher')->getStoreId();
        return $storeId;
    }

    /**
     * getting target language to translate.
     * @param $storeid
     * @return String
     */
    public function getdefaultvalue($storeid = null)
    {
        if (is_null($storeid)) {
            return $this->_scopeConfig->getValue('translator/general/languages');
        } else {
            return $this->_scopeConfig->getValue('translator/general/languages', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeid);
        }
    }

    /**
     * Getting locale language based on the store.
     * @param  $storeId
     * @return String
     */
    public function getlocale($storeId)
    {
            return $this->_scopeConfig->getValue('general/locale/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Add elements in layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        if ($this->helperData->isEnabled() && $this->helperData->isTranslatorEnabled()) {
            $storeId = $this->getRequest()->getParam('store', 0);
            $language = $this->translatorhelper->getLanguage($storeId);
            $fullNameLanguage = $this->translatorhelper->getLanguageFullNameByCode($language, $storeId);
            $translateBtnText = trim($this->_scopeConfig->getValue('translator/general/translate_btntext', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId));
            $translateBtnText = $translateBtnText ? $translateBtnText : 'Translate To';
            $this->getToolbar()->addChild(
                'translate_button',
                'Magento\Backend\Block\Widget\Button',
                [
                    'label' => sprintf('%s: %s', $translateBtnText, $fullNameLanguage),
                    'id' => 'translate_button_all',
                    'title' => __('Translate To'),
                ]
            );
        }
        $version = $this->productMetadataInterface->getVersion();
        if (version_compare($version, '2.1', '<')) {
            parent::_prepareLayout();
        }
    }
}

<?php
/**
 * Copyright © 2016 store.biztechconsultancy.com. All Rights Reserved..
 */
namespace Biztech\Translator\Controller\Adminhtml\Translator;

use Magento\Backend\App\Action\Context;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;

class massTranslateCMSPage extends \Magento\Backend\App\Action
{
    protected $filter;
    protected $collectionFactory;
    protected $scopeConfig;
    protected $_languageHelper;
    protected $_translatorModel;
    protected $_logger;

    /**
     * @param Context                                  $context
     * @param Filter                                   $filter
     * @param CollectionFactory                        $collectionFactory
     * @param ScopeConfigInterface                     $scopeConfig
     * @param \Biztech\Translator\Helper\Logger\Logger $logger
     * @param \Biztech\Translator\Helper\Language      $languageHelper
     * @param \Biztech\Translator\Helper\Translator    $translatorHelper
     * @param \Biztech\Translator\Model\Translator     $translatorModel
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ScopeConfigInterface $scopeConfig,
        \Biztech\Translator\Helper\Logger\Logger $logger,
        \Biztech\Translator\Helper\Language $languageHelper,
        \Biztech\Translator\Helper\Translator $translatorHelper,
        \Biztech\Translator\Model\Translator $translatorModel
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_logger = $logger;
        $this->_languageHelper = $languageHelper;
        $this->_translatorHelper = $translatorHelper;
        $this->_translatorModel = $translatorModel;
        parent::__construct($context);
    }

    /**
     * Mass translate CMS page.
     * @return Json response.
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $cmsPage = $this->filter->getCollection($this->collectionFactory->create());
        $filters = $this->getRequest()->getParam('filters');
        if (isset($filters['store_id'])) {
            $storeId = $filters['store_id'];
        } else {
            $storeId = 0;
        }
        
        $selectPageCount = $cmsPage->getSize();
        $translatedPageCount = 0;
        $languages = $this->_languageHelper->getLanguages();
        if ($this->getRequest()->getParam('lang_to') != 'locale') {
            $langto = $this->getRequest()->getParam('lang_to');
        } else {
            $langto = $this->_translatorHelper->getLanguage($storeId);
        }
        $langFrom = $this->_translatorHelper->getFromLanguage($storeId);

        try {
            $pageTranslateFields = $this->scopeConfig->getValue('translator/general/massaction_cmspage_translate_fields', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

            if (!$cmsPage || !$pageTranslateFields) {
            }
            $finalFields = explode(',', $pageTranslateFields);

            foreach ($cmsPage as $item) {
                foreach ($finalFields as $attributeCode) {
                    $attributeCode = str_replace('page_', '', $attributeCode);
                    if (!isset($item->getData()[$attributeCode]) || empty($item->getData()[$attributeCode])) {
                        continue;
                    }

                    $translate = $this->_translatorModel->getTranslate($item->getData()[$attributeCode], $langto, $langFrom);


                    if (isset($translate['status']) && $translate['status'] == 'fail') {
                        $error = '"' . $item->getData()['page_id'] . '" can\'t be translate for "CMS Page : ' . $attributeCode . '" . Error : ' . $translate['text'];
                        $this->_logger->error($error);
                        $this->messageManager->addError($error);
                    } else {
                        $item->setData($attributeCode, $translate['text']);
                    }
                }
                try {
                    $item->save();
                    if (isset($translate['status']) && $translate['status'] != 'fail') {
                        $translatedPageCount++;
                    }
                } catch (LocalizedException $e) {
                    $this->_logger->debug($e->getRawMessage());
                }
            }
            if ($translatedPageCount == 0) {
                $this->messageManager->addError(__('Any of CMS page has not been translated. Please see /var/log/translator.log file for detailed information.'));
                $resultRedirect->setPath('cms/page/index');
                return $resultRedirect;
            } else {
                $this->messageManager->addSuccess($translatedPageCount . __(' CMS Page(s) of ') . $selectPageCount . __(' has been translated to :') . $languages[$langto]);
            }
        } catch (LocalizedException $e) {
            $this->_logger->error($e->getRawMessage());
            $this->messageManager->addError($e->getRawMessage());
            $resultRedirect->setPath('cms/page/index');
        }
        $resultRedirect->setPath('cms/page/index');
        return $resultRedirect;
    }
}

<?php
namespace Biztech\Translator\Controller\Adminhtml\Translator;

use Magento\Backend\App\Action\Context;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;

class massTranslateCMSBlock extends \Magento\Backend\App\Action
{
    protected $filter;
    protected $collectionFactory;
    protected $scopeConfig;
    protected $_logger;
    protected $_blockFactory;
    protected $_languageHelper;
    protected $_translatorHelper;
    protected $_translatorModel;

    /**
     * @param Context                                  $context
     * @param Filter                                   $filter
     * @param CollectionFactory                        $collectionFactory
     * @param ScopeConfigInterface                     $scopeConfig
     * @param \Magento\Cms\Model\BlockFactory          $blockfactory
     * @param \Biztech\Translator\Helper\Language      $languageHelper
     * @param \Biztech\Translator\Helper\Translator    $translatorHelper
     * @param \Biztech\Translator\Model\Translator     $translatorModel
     * @param \Biztech\Translator\Helper\Logger\Logger $logger
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ScopeConfigInterface $scopeConfig,
        \Magento\Cms\Model\BlockFactory $blockfactory,
        \Biztech\Translator\Helper\Language $languageHelper,
        \Biztech\Translator\Helper\Translator $translatorHelper,
        \Biztech\Translator\Model\Translator $translatorModel,
        \Biztech\Translator\Helper\Logger\Logger $logger
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_blockFactory = $blockfactory;
        $this->_languageHelper = $languageHelper;
        $this->_translatorHelper = $translatorHelper;
        $this->_translatorModel = $translatorModel;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * For mass translating CMS page.
     * @return json response
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $params = $this->getRequest()->getParams();
        if (array_key_exists('selected', $params)) {
            $selectIds = $this->getRequest()->getParam('selected');
            $implodeIds = implode(',', $selectIds);
            $cmsBlock = $this->_blockFactory->create()->getCollection()->addFieldToFilter('block_id', array('in' => $implodeIds ));
        } else {
            $cmsBlock = $this->_blockFactory->create()->getCollection();
        }
        
        $filters = $this->getRequest()->getParam('filters');
        if (isset($filters['store_id'])) {
            $storeId = $filters['store_id'];
        } else {
            $storeId = 0;
        }
        
        $selectBlockCount = count($cmsBlock);
       
        $translatedBlockCount = 0;
        $languages = $this->_languageHelper->getLanguages();
        if ($this->getRequest()->getParam('lang_to') != 'locale') {
            $langto = $this->getRequest()->getParam('lang_to');
        } else {
            $langto = $this->_translatorHelper->getLanguage($storeId);
        }
        $langFrom = $this->_translatorHelper->getFromLanguage($storeId);

        try {
            $pageTranslateFields = ['content', 'title'];
            if (!$cmsBlock || !$pageTranslateFields) {
            }
            $finalFields = $pageTranslateFields;
            foreach ($cmsBlock as $item) {
                foreach ($finalFields as $attributeCode) {
                    if (!isset($item->getData()[$attributeCode]) || empty($item->getData()[$attributeCode])) {
                        continue;
                    }
                    $translate = $this->_translatorModel->getTranslate($item->getData()[$attributeCode], $langto, $langFrom);

                    if (isset($translate['status']) && $translate['status'] == 'fail') {
                        $error = '"' . $item->getData()['block_id'] . '" can\'t be translate for "CMS Block : ' . $attributeCode . '" . Error : ' . $translate['text'];
                        $this->_logger->error($error);
                        $this->messageManager->addError($error);
                    } else {
                        $item->setData($attributeCode, $translate['text']);
                    }
                }
                try {
                    $item->save();
                    if (isset($translate['status']) && $translate['status'] != 'fail') {
                        $translatedBlockCount++;
                    }
                } catch (LocalizedException $e) {
                    $this->_logger->debug($e->getRawMessage());
                }
            }
            if ($translatedBlockCount == 0) {
                $this->messageManager->addError(__('Any of CMS Block has not been translated. Please see /var/log/translator.log file for detailed information.'));
                $resultRedirect->setPath('cms/block/index');
                return $resultRedirect;
            } else {
                $this->messageManager->addSuccess($translatedBlockCount . __(' CMS Block(s) of ') . $selectBlockCount . __('has been translated to :') . $languages[$langto]);
            }
        } catch (LocalizedException $e) {
            $this->_logger->error($e->getRawMessage());
            $this->messageManager->addError($e->getRawMessage());
            $resultRedirect->setPath('cms/block/index');
        }
        $resultRedirect->setPath('cms/block/index');
        return $resultRedirect;
    }
}

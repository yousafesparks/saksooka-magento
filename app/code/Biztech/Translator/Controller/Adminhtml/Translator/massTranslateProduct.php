<?php
/**
 * Copyright © 2016 store.biztechconsultancy.com. All Rights Reserved..
 */
namespace Biztech\Translator\Controller\Adminhtml\Translator;

use Biztech\Translator\Helper\Data;
use Biztech\Translator\Model\CrondataFactory;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Ui\Component\MassAction\Filter;
use Biztech\Translator\Helper\Language;
use Biztech\Translator\Model\Translator;
use Biztech\Translator\Helper\Logger\Logger;

/**
 * Setting cron for the translation for mass action on the selected products.
 */
class massTranslateProduct extends \Magento\Backend\App\Action
{
    protected $filter;
    protected $collectionFactory;
    protected $scopeConfig;
    protected $_logger;
    protected $_bizhelper;
    protected $_cronDataFactory;
    protected $_date;
    protected $_languageHelper;
    protected $_translator;


    /**
     * massTranslateProduct constructor.
     * @param Context              $context
     * @param Filter               $filter
     * @param CollectionFactory    $collectionFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger               $logger
     * @param Data                 $bizhelper
     * @param Language             $languagehelper
     * @param Translator           $translator
     * @param CrondataFactory      $cronDataFactory
     * @param DateTime             $datetime
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        Data $bizhelper,
        Language $languagehelper,
        Translator $translator,
        CrondataFactory $cronDataFactory,
        DateTime $datetime
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_logger = $logger;
        $this->_bizhelper = $bizhelper;
        $this->_cronDataFactory = $cronDataFactory;
        $this->_languageHelper = $languagehelper;
        $this->_translator = $translator;
        $this->_date = $datetime;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $langTo = '';
        $lanFrom = '';
        
        $data = $this->getRequest()->getParams();
        $ids = $this->getRequest()->getParam('selected');

        $storeId = 0;
        $filters = (array)$this->getRequest()->getParam('filters', []);

        if (isset($filters['store_id'])) {
            $storeId = (int)$filters['store_id'];
        }

        if (isset($ids) && is_array($ids) && !empty($ids)) {
            $this->setTranslationJob($ids, $storeId);
        } elseif ($this->getRequest()->getParam('excluded') == 'false') {
            if (isset($filters)) {
                $filterids = $this->filter->getCollection($this->collectionFactory->create());
                $grid_ids = array();
                foreach ($filterids as $allids) {
                    $grid_ids[] = $allids->getEntityId();
                }
                $ids = $grid_ids;
            } else {
                $ids = $this->collectionFactory->create()->getAllIds();
            }
            

            $this->setTranslationJob($ids, $storeId);
        } else {
            $this->messageManager->addError(__('Couldn\'t Register Cron Process! Please try again later'));
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('catalog/product/index', ['store' => $storeId]);
    }

    /**
     * create cron process for translating selected product based store.
     * @param array  $ids     productids.
     * @param [type] $storeId storeid to translate product for that store.
     */
    protected function setTranslationJob(array $ids, $storeId)
    {
        sort($ids);

        if ($this->getRequest()->getParam('lang_to') != 'locale') {
            $langTo = $this->getRequest()->getParam('lang_to');
        } else {
            $langTo = $this->_languageHelper->getLanguage($storeId);
        }

        $langFrom = $this->_languageHelper->getFromLanguage($storeId);

        $cronTranslate1 = $this->_cronDataFactory->create();
        $cronTranslate1 = $cronTranslate1->getCollection()->addFieldToFilter('status', 'pending');

        if ($cronTranslate1->count() > 0) {
            foreach ($cronTranslate1 as $abortCron1) {
                $cronDataUpdate = $this->_cronDataFactory->create();
                $abortCron = $cronDataUpdate->load($abortCron1->getId())->setUpdateCronDate($this->_date->gmtDate())->setIsAbort(1)->setStatus('abort')->save();
            }
        } elseif (isset($data['is_abort']) && $data['is_abort'] == 1 && $cronTranslate1->count() > 0) {
            foreach ($cronTranslate1 as $abortCron1) {
                $cronDataUpdate1 = $this->_cronDataFactory->create();
                $abortCron = $cronDataUpdate1->load($abortCron1->getId())->setUpdateCronDate($this->_date->gmtDate())->setIsAbort(1)->setStatus('abort')->save();
            }
        }

        try {
            $cronTranslate = $this->_cronDataFactory->create();
            $cronTranslate->setCronName('Cron Translation')
                ->setStoreId($storeId)
                ->setProductIds(json_encode($ids))
                ->setLangFrom($langFrom)
                ->setLangTo($langTo)
                ->setStatus('pending');
            $cronTranslate->save();


            $jobCode = $cronTranslate::BIZ_CRON_JOB_CODE;
            $cronSet = $this->_translator->setTranslateCron($storeId, $jobCode);

            $this->messageManager->addSuccess(__('Cron Process Registered! Please enable log to view any error occured'));
        } catch (\LocalizedException $e) {
            $this->_logger->debug($e->getRawMessage());
        }
    }
}

/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_StoreCredit
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

 define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/totals',
    'Magento_Catalog/js/price-utils'
], function (Component, totals, priceUtils) {
    "use strict";

    return Component.extend({
        defaults: {
            template: 'Mageplaza_StoreCredit/totals/credit'
        },

        getTotal: function () {
            return totals.getSegment('mp_store_credit_spent');
        },

        getValue: function () {
            return priceUtils.formatPrice(this.getTotal().value);
        }
    });
}
);
